package client.data;

import java.util.*;

/**
 * Used to pass game information into views<br>
 * <br>
 * PROPERTIES:<br>
 * <ul>
 * <li>Id: Unique game ID</li>
 * <li>Title: Game title (non-empty string)</li>
 * <li>Players: List of players who have joined the game (can be empty)</li>
 * </ul>
 * 
 */
public class GameInfo
{
	private int id;
	private String title;
	private List<PlayerInfo> players;
	
	public GameInfo()
	{
		setId(-1);
		setTitle("");
		players = new ArrayList<PlayerInfo>();
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void addPlayer(PlayerInfo newPlayer)
	{
		players.add(newPlayer);
	}

	public void setPlayers(List<PlayerInfo> players) { this.players = players; }
	
	public List<PlayerInfo> getPlayers()
	{
		return Collections.unmodifiableList(players);
	}
	
	@Override
	public boolean equals(Object o) {
		if( o == null) { return false; }
		if(o instanceof GameInfo) {
			GameInfo g = (GameInfo) o;
			if(this.id != g.getId()) { return false; }
			if(this.players.size() != g.getPlayers().size()) { return false; }
			
			for(int i = 0; i < players.size(); i++) {
				if(!players.get(i).equals(g.getPlayers().get(i))) { return false; }
			}
		}
		return true;
		
	}
}

