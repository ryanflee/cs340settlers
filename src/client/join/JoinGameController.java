package client.join;

import shared.definitions.CatanColor;
import client.base.*;
import client.data.*;
import client.facade.Facade;
import client.misc.*;
import client.poller.Poller;
import shared.model.player.Player;

import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * Implementation for the join game controller
 */
public class JoinGameController extends Controller implements IJoinGameController, Observer {

	private INewGameView newGameView;
	private ISelectColorView selectColorView;
	private IMessageView messageView;
	private IAction joinAction;
	private GameInfo[] gameInfos;
	private PlayerInfo playerInfo;
	private GameInfo selectedGame;
	
	/**
	 * JoinGameController constructor
	 * 
	 * @param view Join game view
	 * @param newGameView New game view
	 * @param selectColorView Select color view
	 * @param messageView Message view (used to display error messages that occur while the user is joining a game)
	 */
	public JoinGameController(IJoinGameView view, INewGameView newGameView, 
								ISelectColorView selectColorView, IMessageView messageView) {

		super(view);

		setNewGameView(newGameView);
		setSelectColorView(selectColorView);
		setMessageView(messageView);
		Facade.addListener(this);
		
	}
	
	public IJoinGameView getJoinGameView() {
		
		return (IJoinGameView)super.getView();
	}
	
	/**
	 * Returns the action to be executed when the user joins a game
	 * 
	 * @return The action to be executed when the user joins a game
	 */
	public IAction getJoinAction() {
		
		return joinAction;
	}

	/**
	 * Sets the action to be executed when the user joins a game
	 * 
	 * @param value The action to be executed when the user joins a game
	 */
	public void setJoinAction(IAction value) {	
		
		joinAction = value;
	}
	
	public INewGameView getNewGameView() {
		
		return newGameView;
	}

	public void setNewGameView(INewGameView newGameView) {
		
		this.newGameView = newGameView;
	}
	
	public ISelectColorView getSelectColorView() {
		
		return selectColorView;
	}
	public void setSelectColorView(ISelectColorView selectColorView) {
		
		this.selectColorView = selectColorView;
	}
	
	public IMessageView getMessageView() {
		
		return messageView;
	}
	public void setMessageView(IMessageView messageView) {
		
		this.messageView = messageView;
	}

	@Override
	public void start() {
		IJoinGameView jgv = getJoinGameView();
		if (this.playerInfo == null) { this.playerInfo = new PlayerInfo(); }
		Facade.getGames();
		jgv.setGames(this.gameInfos, this.playerInfo);
		jgv.showModal();
	}

	@Override
	public void startCreateNewGame() {

		getNewGameView().showModal();
	}

	@Override
	public void cancelCreateNewGame() {
		if(getNewGameView().isModalShowing()){
			getNewGameView().closeModal();
		}
	}

	@Override
	public void createNewGame() {
		INewGameView newGame = getNewGameView();
		String title = newGame.getTitle();
		boolean randNums = newGame.getRandomlyPlaceNumbers();
		boolean randHex = newGame.getRandomlyPlaceHexes();
		boolean randPort = newGame.getUseRandomPorts();
		Facade.createGame(randHex, randNums, randPort, title);
		Facade.getGames();
		if(getNewGameView().isModalShowing()){
			getNewGameView().closeModal();
		}
		getJoinGameView().setGames(this.gameInfos, this.playerInfo);
	}

	@Override
	public void startJoinGame(GameInfo game) {
		if(game == null)return;
		ISelectColorView scv = getSelectColorView();
		scv.initColors();
		int index = 0;
		for (PlayerInfo p : game.getPlayers()) {
			if (p.getId() != this.playerInfo.getId()) {
				scv.setColorEnabled(p.getColor(), false);
			} else {
				this.playerInfo.setPlayerIndex(index);
			}
			index++;
		}
		this.selectedGame = game;
		Facade.setGameInfo(this.selectedGame);
//		if(scv.isModalShowing()) {
//			scv.closeModal();
//		}
		if(!scv.isModalShowing()) {
			scv.showModal();
		}
		
	}

	@Override
	public void cancelJoinGame() {
		if(getJoinGameView().isModalShowing()){
			getJoinGameView().closeModal();
		}
	}

	@Override
	public void joinGame(CatanColor color) {
		for(PlayerInfo p : this.selectedGame.getPlayers()) {
			if(p.getColor().equals(color)) {
				messageView.setTitle("Invalid Color Choice");
				messageView.setMessage("Color is already in use.");
				messageView.showModal();
				return;
			}
		}
		// If join succeeded
		if(getSelectColorView().isModalShowing()){
			getSelectColorView().closeModal();
		}
		if(getJoinGameView().isModalShowing()){
			getJoinGameView().closeModal();
		}
		this.playerInfo.setColor(color);
		Facade.setPlayerInfo(this.playerInfo);
		boolean joined = Facade.joinGame(selectedGame.getId(), color);
		for (PlayerInfo p : selectedGame.getPlayers()) {
			if(p.getId() == playerInfo.getId()) {
				p.setColor(color);
				break;
			}
		}
		Facade.setGameInfo(this.selectedGame);
		if(joined) {
			joinAction.execute();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof List<?>) {
			boolean change = false;
			List<?> list = (List<?>)arg;
			if (!list.isEmpty()) {
				Object obj = list.get(0);
				
				if (obj instanceof GameInfo) {
					if(!getSelectColorView().isModalShowing()) {
						// it's a list of games!
						GameInfo[] games;
						if(this.gameInfos == null || gameInfos.length != list.size()) {
							change = true;
						}
						games = new GameInfo[list.size()];
						int i = 0;
						for (Object curObj : list) {
							games[i++] = (GameInfo) curObj;
							if(!change) {
								if(this.gameInfos != null && !games[i - 1].equals(this.gameInfos[i - 1])) {
									change = true;
								}
							}
						}
						if(change) {
							this.gameInfos = games;		
							if(this.selectedGame != null) {
								startJoinGame(this.selectedGame);
							}
						}
						if (change && getJoinGameView().isModalShowing()) {
							//getJoinGameView().closeModal();
							getJoinGameView().setGames(this.gameInfos, this.playerInfo);
							getJoinGameView().refresh();
							//getJoinGameView().showModal();
						}
					} else {
						for(Object curObj : list) {
							if(((GameInfo)curObj).getId() == this.selectedGame.getId()){
								startJoinGame((GameInfo) curObj);
							}
						}
					}
					
				}
			}
		}

		else if (arg instanceof PlayerInfo) {
			this.playerInfo = (PlayerInfo) arg;
		}
	}
	
}

