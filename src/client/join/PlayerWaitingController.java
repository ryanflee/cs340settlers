package client.join;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import client.base.*;
import client.data.GameInfo;
import client.data.PlayerInfo;
import client.facade.Facade;
import client.poller.Poller;
import shared.model.Model;
import shared.model.player.Player;


/**
 * Implementation for the player waiting controller
 */
public class PlayerWaitingController extends Controller implements IPlayerWaitingController, Observer {

	private GameInfo gameInfo;
	private Model model;
	
	public PlayerWaitingController(IPlayerWaitingView view) {

		super(view);
		Facade.addListener(this);
		model = null;
	}

	@Override
	public IPlayerWaitingView getView() {

		return (IPlayerWaitingView)super.getView();
	}

	@Override
	public void start() {
		IPlayerWaitingView pwv = getView();

		if(model == null) { return; }

		List<Player> list = model.getPlayers();
		List<PlayerInfo> playerList = new ArrayList<PlayerInfo>();

		int count = 0;
		for (Player p : list) {
			if(p == null) { break; }
			PlayerInfo curPlayer = new PlayerInfo();
			curPlayer.setName(p.getName());
			curPlayer.setPlayerIndex(count);
			curPlayer.setColor(p.getColor());
			playerList.add(curPlayer);
			count++;
		}
		

//		List<PlayerInfo> playerList = gameInfo.getPlayers();
//		if(playerList != null) {
//			if (playerList.size() == 4) { return; }
//		}
//		PlayerInfo[] playerArray = new PlayerInfo[playerList.size()];
//		int count = 0;
//		for (PlayerInfo p : gameInfo.getPlayers()) {
//			playerArray[count] = p;
//			System.out.println("Player Color: " + p.getColor().toString());
//			count++;
//		}
		gameInfo = Facade.getGameInfo();
		gameInfo.setPlayers(playerList);
		Facade.setGameInfo(gameInfo);
		if(playerList.size() == 4 || count == 4) {
			if(pwv.isModalShowing()) {
				pwv.closeModal();
			}
			return; 
		}
		PlayerInfo[] playerArray = new PlayerInfo[playerList.size()];
		pwv.setPlayers(playerList.toArray(playerArray));

		pwv.setAIChoices(new String[]{"LARGEST_ARMY"});
		if(pwv.isModalShowing()) {
			pwv.closeModal();
		}
		pwv.showModal();
		//Poller.start();
	}

	@Override
	public void addAI() {
		String ai = getView().getSelectedAI();
		Facade.addAI(ai);
		start();
	}

	@Override
	public void update(Observable o, Object arg) {
		if( arg instanceof Model ) {
			this.model = (Model) arg;
			if (!this.model.getGameStarted()) {
				start();
			}
			else if (getView().isModalShowing()) {
				getView().closeModal();
			}
		}
	}
}

