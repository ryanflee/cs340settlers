package client.communication;

import client.base.*;
import client.facade.Facade;
import shared.model.Model;
import shared.model.logs.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * Chat controller implementation
 */
public class ChatController extends Controller implements IChatController, Observer {

	public ChatController(IChatView view) {
		
		super(view);
		Facade.addListener(this);
	}

	@Override
	public IChatView getView() {
		return (IChatView)super.getView();
	}

	@Override
	public void sendMessage(String message) {
		Facade.sendChat(message);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			Model m = (Model) arg;
			List<ChatEntry> chats = m.getComments();
			List<LogEntry> entries = new ArrayList<LogEntry>();
			IChatView cv = getView();
			for(ChatEntry chat : chats) {
				entries.add(new LogEntry(chat.getPlayer().getColor(), chat.getValue()));
			}
			getView().setEntries(entries);
		}
	}
}

