package client.communication;

import java.util.*;
import java.util.List;

import client.base.*;
import client.facade.Facade;
import shared.definitions.*;
import shared.model.Model;
import shared.model.logs.HistoryEntry;


/**
 * Game history controller implementation
 */
public class GameHistoryController extends Controller implements IGameHistoryController, Observer {

	public GameHistoryController(IGameHistoryView view) {
		
		super(view);
		Facade.addListener(this);
		initFromModel();
	}
	
	@Override
	public IGameHistoryView getView() {
		
		return (IGameHistoryView)super.getView();
	}
	
	private void initFromModel() {
		

	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			Model m = (Model) arg;
			List<HistoryEntry> histories = m.getHistory();
			List<LogEntry> logs = new ArrayList<LogEntry>();
			for(HistoryEntry history : histories) {
				logs.add(new LogEntry(history.getPlayer().getColor(), history.getValue()));
			}
			getView().setEntries(logs);
		}
	}
}

