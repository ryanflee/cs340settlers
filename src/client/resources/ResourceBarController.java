package client.resources;

import java.util.*;

import shared.model.Model;
import shared.model.resourcecard.ResourceCard;

import client.base.*;
import client.facade.Facade;


/**
 * Implementation for the resource bar controller
 */
public class ResourceBarController extends Controller implements IResourceBarController, Observer {

	private Map<ResourceBarElement, IAction> elementActions;
	private Model model;
	
	public ResourceBarController(IResourceBarView view) {

		super(view);
		Facade.addListener(this);
		elementActions = new HashMap<ResourceBarElement, IAction>();
	}

	@Override
	public IResourceBarView getView() {
		return (IResourceBarView)super.getView();
	}

	/**
	 * Sets the action to be executed when the specified resource bar element is clicked by the user
	 * 
	 * @param element The resource bar element with which the action is associated
	 * @param action The action to be executed
	 */
	public void setElementAction(ResourceBarElement element, IAction action) {
		elementActions.put(element, action);
	}

	@Override
	public void buildRoad() {	
		executeElementAction(ResourceBarElement.ROAD);
	}

	@Override
	public void buildSettlement() {
		executeElementAction(ResourceBarElement.SETTLEMENT);
	}

	@Override
	public void buildCity() {
		executeElementAction(ResourceBarElement.CITY);
	}

	@Override
	public void buyCard() {
		executeElementAction(ResourceBarElement.BUY_CARD);
	}

	@Override
	public void playCard() {
		executeElementAction(ResourceBarElement.PLAY_CARD);
	}
	
	private void executeElementAction(ResourceBarElement element) {
		
		if (elementActions.containsKey(element)) {
			
			IAction action = elementActions.get(element);
			action.execute();
		}
	}
	
	private void init() {
		if (Facade.getPlayerIndex() != -1) {
			if(Facade.canBuildRoad())
				getView().setElementEnabled(ResourceBarElement.ROAD, true);
			else
				getView().setElementEnabled(ResourceBarElement.ROAD, false);
			getView().setElementAmount(ResourceBarElement.ROAD, Facade.getPlayerRoads());
			
			if(Facade.canBuildSettlement())
				getView().setElementEnabled(ResourceBarElement.SETTLEMENT, true);
			else
				getView().setElementEnabled(ResourceBarElement.SETTLEMENT, false);
			getView().setElementAmount(ResourceBarElement.SETTLEMENT, Facade.getPlayerSettlements());
			
			if(Facade.canBuildCity())
				getView().setElementEnabled(ResourceBarElement.CITY, true);
			else
				getView().setElementEnabled(ResourceBarElement.CITY, false);
			getView().setElementAmount(ResourceBarElement.CITY, Facade.getPlayerCities());

			List<ResourceCard> cards = Facade.getPlayerResources();
			int[] list = new int[]{0,0,0,0,0};
			for(ResourceCard c:cards){
				switch(c.getType()){
				case BRICK:list[0]++;break;
				case ORE:list[1]++;break;
				case SHEEP:list[2]++;break;
				case WOOD:list[3]++;break;
				case WHEAT:list[4]++;break;
				}
			}
			getView().setElementAmount(ResourceBarElement.BRICK, list[0]);
			getView().setElementAmount(ResourceBarElement.ORE, list[1]);
			getView().setElementAmount(ResourceBarElement.SHEEP, list[2]);
			getView().setElementAmount(ResourceBarElement.WOOD, list[3]);
			getView().setElementAmount(ResourceBarElement.WHEAT, list[4]);
			
			if(Facade.getPlayerDevCards().size()>0)
				getView().setElementEnabled(ResourceBarElement.PLAY_CARD, true);
			else
				getView().setElementEnabled(ResourceBarElement.PLAY_CARD, false);
			
			if(Facade.canBuyDevCard())
				getView().setElementEnabled(ResourceBarElement.BUY_CARD, true);
			else
				getView().setElementEnabled(ResourceBarElement.BUY_CARD,  false);
			getView().setElementAmount(ResourceBarElement.SOLDIERS, Facade.getPlayerKnights());
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			model = (Model)arg;
			init();
		}
	}

}

