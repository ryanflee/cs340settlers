package client.proxy;

import java.io.*;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.net.HttpURLConnection;
import java.net.URL;

import shared.DataParser;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.exceptions.DuplicateUsernameException;
import shared.model.exceptions.InvalidCredentialsException;


/**
 * The real proxy for "live" usage.
 * @author Ryan F. Lee
 *
 */
public class RealProxy extends Proxy {


	private static final String HTTP_GET = "GET";
	private static final String HTTP_POST = "POST";
	private String url_PREFIX;
	private String userCookie;
	private String gameCookie;

	private boolean joinGame;
	private boolean gameOver;
	private int versionNum;
	
	//For testing ONLY
	private int gameID;

	public RealProxy() {
		super();
	}
	
	public RealProxy(String host, String port) {
		super(host, port);
		url_PREFIX = "http://" + host + ":" + port;
		userCookie = null;
		gameCookie = null;
		versionNum = -1;
		gameID = 0;
		gameOver = false;
		joinGame = true;
	}

//	public static void main(final String[] args) throws InvalidCredentialsException, DuplicateUsernameException {
//		RealProxy rp = new RealProxy("localhost", "8081");
//
//		rp.userCookie = "%7B%22name%22%3A%22Sam%22%2C%22password%22%3A%22sam%22%2C%22playerID%22%3A123%7D";
//		System.out.println(rp.getPID());
//
//	}

	@Override
	public int getVersionNum() {
		return this.versionNum;
	}
	
	@Override
	public boolean getGameOver() {
		return this.gameOver;
	}
	
	@Override
	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	@Override
	public boolean userLogin(String username, String password) {
		if(username == null || password == null) { return false; }
		String json = encoder.userLogin(username, password);
		String result = doPost("/user/login", json);
		if(result == null) {
			return false;
		}
		if(result.equals("Success")) { 
			String cookie = null;
			try {
				cookie = URLDecoder.decode(this.userCookie, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			System.out.println("COOKIE: " + cookie);
			dataParser.parseUserCookie(cookie);
		}
		if(!result.equals("Success")) { return false; }
		return true;
	}

	@Override
	public boolean register(String username, String password) {
		if(username == null || password == null) { return false; }
		String json = encoder.register(username, password);
		String result = doPost("/user/register", json);
		if(result == null) {
			return false;
		}
		if(result.equals("Success")) {
			String cookie = null;
			try {
				cookie = URLDecoder.decode(this.userCookie, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			System.out.println("COOKIE: " + cookie);
			dataParser.parseUserCookie(cookie);
		}
		if(!result.equals("Success")) { return false; }
		return true;
	}

	@Override
	public boolean getGames() {
		String result = doGet("/games/list");
		if(result == null) {
			return false;
		}
		dataParser.parseGameList(result);
		return true;
	}

	@Override
	public boolean createGame(boolean randTiles, boolean randNums,
			boolean randPorts, String gameName) {
		if(gameName == null) { return false; }
		String json = encoder.createGame(randTiles, randNums, randPorts, gameName);
		String result = doPost("/games/create", json);
		if(result == null) {
			return false;
		}
		this.gameID = dataParser.parseNewGame(result);
		this.joinGame = false;
		joinGame(gameID, CatanColor.ORANGE);
		this.gameCookie = null;
		return true;
	}

	@Override
	public boolean joinGame(int gameID, CatanColor color) {
		String json = encoder.joinGame(gameID, color);
		String result = doPost("/games/join", json);
		if(result == null) {
			return false;
		}
		if(!result.equals("Success")) { return false; }
		joinGame = true;
		return true;
	}

	@Override
	public boolean saveGame(int gameID, String fileName) {
		if(fileName == null || fileName.length() == 0) { return false; }
		String json = encoder.saveGame(gameID, fileName);
		String result = doPost("/games/save", json);
		if(result == null) {
			return false;
		}
		if(!result.equals("Success")) { return false; }
		return true;
	}

	@Override
	public boolean loadGame(String fileName) {
		String json = encoder.loadGame(fileName);
		String result = doPost("/games/load", json);
		if(result == null) {
			return false;
		}
		if(!result.equals("Success")) { return false; }
		return true;
	}

	@Override
	public boolean getModel(int version) {
		if(this.gameCookie == null) { return false; }
		if(version == -1) { return getModelNoCheck(); }
		String result = doGet("/game/model?version=" + version);
		if(result == null) {
			return false;
		}
		if(result.equals("\"true\"")) {
			return true;
		}
		this.versionNum = buildModel(result);
		return true;
	}
	
	@Override
	public boolean getModel() {
		if(this.gameCookie == null) { return false; }
		String result = doGet("/game/model");
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
	}

	private boolean getModelNoCheck() {
		String result = doGet("/game/model");
		if(result == null) {
			return false;
		}
		this.versionNum = dataParser.buildModelFromJson(result,true);
		return true;
	}

	@Override
	public boolean resetGame() {
		String result = doPost("/game/reset", "");
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
	}

	@Override
	public boolean getCommands() {
		String result = doGet("/game/commands");
		if(result == null) {
			return false;
		}
		//dataParser.parseCommands(result);
		return true;
	}

	@Override
	public boolean postCommands(List<Object> commands) {
		String json = encoder.postCommands(commands);
		String result = doPost("/game/commands", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean getAI() {
		String result = doGet("/game/listAI");
		if(result == null) {
			return false;
		}
		//dataParser.parseAI(result);
		return true;
		
	}

	@Override
	public boolean addAI(String ai) {
		String json = encoder.addAI(ai);
		String result = doPost("/game/addAI", json);
		if(result == null) {
			return false;
		}
		if(!result.equals("Success")) { return false; }
		getModelNoCheck();
		return true;
	}

	@Override
	public boolean sendChat(int playerID, String message) {
		String json = encoder.sendChat(playerID, message);
		String result = doPost("/moves/sendChat", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean rollNumber(int playerID, int numRolled) {
		String json = encoder.rollNumber(playerID, numRolled);
		String result = doPost("/moves/rollNumber", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean robPlayer(int playerID, int victimID, HexLocation robberLoc) {
		String json = encoder.robPlayer(playerID, victimID, robberLoc);
		String result = doPost("/moves/robPlayer", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean finishTurn(int playerID) {
		String json = encoder.finishTurn(playerID);
		String result = doPost("/moves/finishTurn", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean buyDevCard(int playerID) {
		String json = encoder.buyDevCard(playerID);
		String result = doPost("/moves/buyDevCard", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean playYearOfPlenty(int playerID, ResourceType type1,
			ResourceType type2) {
		String json = encoder.playYearOfPlenty(playerID, type1, type2);
		String result = doPost("/moves/Year_of_Plenty", json);
		if(result == null) {
			return false;
		} else {
			this.versionNum = buildModel(result);
			return true;
		}
	}

	@Override
	public boolean playRoadBuilding(int playerID, EdgeLocation spot1,
			EdgeLocation spot2) {
		String json = encoder.playRoadBuilding(playerID, spot1, spot2);
		String result = doPost("/moves/Road_Building", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean playKnight(int playerID, int victimID, HexLocation robberLoc) {
		String json = encoder.playKnight(playerID, victimID, robberLoc);
		String result = doPost("/moves/Soldier", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean playMonopoly(int playerID, ResourceType type) {
		String json = encoder.playMonopoly(playerID, type);
		String result = doPost("/moves/Monopoly", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean playMonument(int playerID) {
		String json = encoder.playMonument(playerID);
		String result = doPost("/moves/Monument", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean buildRoad(int playerID, EdgeLocation roadLocation,
			boolean isInitSetup) {
		String json = encoder.buildRoad(playerID, roadLocation, isInitSetup);
		String result = doPost("/moves/buildRoad", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean buildSettlement(int playerID, VertexLocation vertLoc,
			boolean isInitSetup) {
		String json = encoder.buildSettlement(playerID, vertLoc, isInitSetup);
		String result = doPost("/moves/buildSettlement", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean buildCity(int playerID, VertexLocation vertLoc) {
		String json = encoder.buildCity(playerID, vertLoc);
		String result = doPost("/moves/buildCity", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean offerTrade(int playerID, int receiverID,
			Map<ResourceType, Integer> offer) {
		if(playerID >= 4 || receiverID >= 4) { return false; }
		String json = encoder.offerTrade(playerID, receiverID, offer);
		String result = doPost("/moves/offerTrade", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean acceptTrade(int playerID, boolean willAccept) {
		String json = encoder.acceptTrade(playerID, willAccept);
		String result = doPost("/moves/acceptTrade", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
		
	}

	@Override
	public boolean maritimeTrade(int playerID, int ratio, ResourceType input,
			ResourceType output) {
		String json = encoder.maritimeTrade(playerID, ratio, input, output);
		String result = doPost("/moves/maritimeTrade", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
	
	}

	@Override
	public boolean discardCards(int playerID, Map<ResourceType, Integer> toDiscard) {
		String json = encoder.discardCards(playerID, toDiscard);
		String result = doPost("/moves/discardCards", json);
		if(result == null) {
			return false;
		}
		this.versionNum = buildModel(result);
		return true;
	}

	@Override
	public boolean changeLogLevel(String newLevel) {
		String json = encoder.changeLogLevel(newLevel);
		String result = doPost("/moves/changeLogLevel", json);
		if(result == null) {
			return false;
		}
		if(!result.equals("Success")) { return false; }
		return true;
	}

	@Override
	public void finishGame() {
		this.gameCookie = null;
		this.versionNum = -1;
		this.gameOver = false;
	}

	public void getUpdate() {
		if(this.userCookie == null) {
			return;
		}
		if(this.gameCookie == null) {
			getGames();
		} 
		if (this.gameOver) {
			getModelNoCheck();
		}
		else {
			getModel();
		}
	}
	
	
	private String doGet(String urlPath) {
		try {
			URL url = new URL(url_PREFIX + urlPath);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(HTTP_GET);
			
			if(this.userCookie != null) {
				String cookie = "catan.user=" + userCookie;
				if(this.gameCookie != null) {
					cookie += "; catan.game=" + gameCookie;
				}
				connection.setRequestProperty("Cookie", cookie);
			}
			
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				return getResponseBody(connection.getInputStream());
			}
			else {
				return null;
			}
		}
		catch (IOException e) {
			return null;
		}
	}


	private String doPost(String urlPath, String jsonBody) {
		try {
			URL url = new URL(url_PREFIX + urlPath);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(HTTP_POST);
			connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
			
			if(this.userCookie != null) {
				String cookie = "catan.user=" + userCookie;
				if(this.gameCookie != null) {
					cookie += ";catan.game=" + gameCookie;
				}
				connection.setRequestProperty("Cookie", cookie);
			}
			
			connection.setDoInput(true);
			connection.setDoOutput(true);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jsonBody);
			out.close();
			connection.getOutputStream().close();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				if(urlPath.equals("/user/login") || urlPath.equals("/user/register") || urlPath.equals("/games/join")) {
					parseCookie(connection.getHeaderField("Set-Cookie"));
				}
				return getResponseBody(connection.getInputStream());

			} else {
				return null;
			}
		}
		catch (IOException e) {
			return null;
		}
	}
	
	private String getResponseBody(InputStream input) {
		if(input == null) { return null; }
		BufferedReader br = new BufferedReader(new InputStreamReader(input));
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	private void parseCookie(String header) {
		if(header == null) { return; }
		if(header.contains("catan.user")) {
			this.userCookie = header.substring(11, header.length() - 8);
		} else 
		if(header.contains("catan.game") && joinGame) {
			this.gameCookie = header.substring(11, header.length() - 8);
		}
	}
	
	private int buildModel(String result) {
		return dataParser.buildModelFromJson(result,false);
	}
	
	private int buildModel(String result, boolean atGunPoint) {
		return dataParser.buildModelFromJson(result, atGunPoint);
	}

	public int getGameID() {
		return this.gameID;
	}

	@Override
	public void logout() {
		doPost("/user/logout", "");
		this.userCookie = null;
		this.gameCookie = null;
	}

}
