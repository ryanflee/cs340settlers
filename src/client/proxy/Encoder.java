package client.proxy;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.resourcecard.ResourceCard;
import com.google.gson.stream.JsonWriter;

/**
 * Generates JSON encoding of all requests made by the Proxy
 * All functions return JSON string
 */
public class Encoder {

	JsonWriter writer;
	Writer out;
	
	public Encoder() {

	}
	
	
	//User Operations
	public String userLogin(String username, String password) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("username").value(username);
			writer.name("password").value(password);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String register(String username, String password){
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("username").value(username);
			writer.name("password").value(password);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		

	}
	
	
	//Pre-game Operations
	public String getGames() {return null;}
	
	public String createGame(boolean randTiles, boolean randNums, boolean randPorts, String gameName){
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("randomTiles").value(randTiles);
			writer.name("randomNumbers").value(randNums);
			writer.name("randomPorts").value(randPorts);
			writer.name("name").value(gameName);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public String joinGame(int gameID, CatanColor color){
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("id").value(gameID);
			writer.name("color").value(color.toJson());
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public String saveGame(int gameID, String fileName) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("id").value(gameID);
			writer.name("name").value(fileName);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public String loadGame(String fileName){
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("name").value(fileName);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	
	//In-game Operations
	public String postCommands(List<Object> commands) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginArray();
			for(Object o : commands) {
				writer.beginObject();
				writer.name("command").value(o.toString());
				writer.endObject();
			}
			writer.endArray();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String addAI(String s) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("AIType").value(s);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	//Moves
	public String sendChat(int playerID, String message) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("sendChat");
			writer.name("playerIndex").value(playerID);
			writer.name("content").value(message);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String rollNumber(int playerID, int numRolled) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("rollNumber");
			writer.name("playerIndex").value(playerID);
			writer.name("number").value(numRolled);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String robPlayer(int playerID, int victimID, HexLocation robberLoc) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("robPlayer");
			writer.name("playerIndex").value(playerID);
			writer.name("victimIndex").value(victimID);
			writer.name("location");
				writer.beginObject();
				writer.name("x").value(robberLoc.getX());
				writer.name("y").value(robberLoc.getY());
				writer.endObject();
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String finishTurn(int playerID) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("finishTurn");
			writer.name("playerIndex").value(playerID);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String buyDevCard(int playerID) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("buyDevCard");
			writer.name("playerIndex").value(playerID);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String playYearOfPlenty(int playerID, ResourceType type1, ResourceType type2) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("Year_of_Plenty");
			writer.name("playerIndex").value(playerID);
			writer.name("resource1").value(type1.toJson());
			writer.name("resource2").value(type2.toJson());
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String playRoadBuilding(int playerID, EdgeLocation spot1, EdgeLocation spot2) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("Road_Building");
			writer.name("playerIndex").value(playerID);
			writer.name("spot1");
				writer.beginObject();
				writer.name("x").value(spot1.getX());
				writer.name("y").value(spot1.getY());
				writer.name("direction").value(spot1.getJsonDir());
				writer.endObject();
			writer.name("spot2");
				writer.beginObject();
				writer.name("x").value(spot2.getX());
				writer.name("y").value(spot2.getY());
				writer.name("direction").value(spot2.getJsonDir());
				writer.endObject();
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String playKnight(int playerID, int victimID, HexLocation robberLoc) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("Soldier");
			writer.name("playerIndex").value(playerID);
			writer.name("victimIndex").value(victimID);
			writer.name("location");
				writer.beginObject();
				writer.name("x").value(robberLoc.getX());
				writer.name("y").value(robberLoc.getY());
				writer.endObject();
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String playMonopoly(int playerID, ResourceType type) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("Monopoly");
			writer.name("resource").value(type.toJson());
			writer.name("playerIndex").value(playerID);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String playMonument(int playerID) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("Monument");
			writer.name("playerIndex").value(playerID);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String buildRoad(int playerID, EdgeLocation roadLocation, boolean isInitSetup) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("buildRoad");
			writer.name("playerIndex").value(playerID);
			writer.name("roadLocation");
				writer.beginObject();
				writer.name("x").value(roadLocation.getX());
				writer.name("y").value(roadLocation.getY());
				writer.name("direction").value(roadLocation.getJsonDir());
				writer.endObject();
			writer.name("free").value(isInitSetup);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String buildSettlement(int playerID, VertexLocation vertLoc, boolean isInitSetup) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("buildSettlement");
			writer.name("playerIndex").value(playerID);
			writer.name("vertexLocation");
				writer.beginObject();
				writer.name("x").value(vertLoc.getX());
				writer.name("y").value(vertLoc.getY());
				writer.name("direction").value(vertLoc.getJsonDir());
				writer.endObject();
			writer.name("free").value(isInitSetup);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String buildCity(int playerID, VertexLocation vertLoc) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("buildCity");
			writer.name("playerIndex").value(playerID);
			writer.name("vertexLocation");
				writer.beginObject();
				writer.name("x").value(vertLoc.getX());
				writer.name("y").value(vertLoc.getY());
				writer.name("direction").value(vertLoc.getJsonDir());
				writer.endObject();
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String offerTrade(int playerID, int receiverID, Map<ResourceType, Integer> offer) {
		
		
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("offerTrade");
			writer.name("playerIndex").value(playerID);
			writer.name("offer");
				writer.beginObject();
				writer.name("brick").value(offer.get(ResourceType.BRICK));
				writer.name("ore").value(offer.get(ResourceType.ORE));
				writer.name("sheep").value(offer.get(ResourceType.SHEEP));
				writer.name("wheat").value(offer.get(ResourceType.WHEAT));
				writer.name("wood").value(offer.get(ResourceType.WOOD));
				writer.endObject();
			writer.name("receiver").value(receiverID);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String acceptTrade(int playerID, boolean willAccept) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("acceptTrade");
			writer.name("playerIndex").value(playerID);
			writer.name("willAccept").value(willAccept);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//WHAT DOES IT MEAN IN SAYING THE TRADE IS OPTIONAL????
	public String maritimeTrade(int playerID, int ratio, ResourceType input, ResourceType output) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("maritimeTrade");
			writer.name("playerIndex").value(playerID);
			writer.name("ratio").value(ratio);
			writer.name("inputResource").value(input.toJson());
			writer.name("outputResource").value(output.toJson());
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String discardCards(int playerID, Map<ResourceType, Integer> toDiscard) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("type").value("discardCards");
			writer.name("playerIndex").value(playerID);
			writer.name("discardedCards");
				writer.beginObject();
				writer.name("brick").value(toDiscard.get(ResourceType.BRICK));
				writer.name("ore").value(toDiscard.get(ResourceType.ORE));
				writer.name("sheep").value(toDiscard.get(ResourceType.SHEEP));
				writer.name("wheat").value(toDiscard.get(ResourceType.WHEAT));
				writer.name("wood").value(toDiscard.get(ResourceType.WOOD));
				writer.endObject();
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	//Util
	public String changeLogLevel(String newLevel) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		try {
			writer.beginObject();
			writer.name("logLevel").value(newLevel);
			writer.endObject();
			String ret = out.toString();
			writer.close();
			return ret;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	
}
