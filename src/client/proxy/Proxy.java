package client.proxy;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import shared.DataParser;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.authentication.*;
import shared.model.exceptions.InvalidCredentialsException;
import shared.model.exceptions.DuplicateUsernameException;
import shared.model.resourcecard.ResourceCard;

/**
 * The Catan client's server proxy. This is called periodically by the Poller to refresh the client's local game model.
 * @author Ryan F. Lee
 *
 */
public abstract class Proxy {
	
	protected Encoder encoder;
	protected DataParser dataParser;
	protected String host;
	protected String port;
	
	public Proxy() {
		this.encoder = new Encoder();
		this.dataParser = new DataParser();
	}
	
	public Proxy(DataParser dp) {
		this.encoder = new Encoder();
		this.dataParser = dp;
	}
	
	public Proxy(String host, String port) {
		this();
		this.host = host;
		this.port = port;
	}
	
	public DataParser getDataParser() {
		return this.dataParser;
	}
	
	public abstract int getVersionNum();
	
	public abstract boolean getGameOver();
	public abstract void setGameOver(boolean gameOver);
		
	//User Operations
	public abstract boolean userLogin(String username, String password);

	public abstract boolean register(String username, String password);


	//Pre-game Operations
	public abstract boolean getGames();

	public abstract boolean createGame(boolean randTiles, boolean randNums, boolean randPorts, String gameName);

	public abstract boolean joinGame(int gameID, CatanColor color);

	public abstract boolean saveGame(int gameID, String fileName);

	public abstract boolean loadGame(String fileName);


	//In-game Operations
	public abstract boolean getModel(int version);
	
	public abstract boolean getModel();

	public abstract boolean resetGame();

	public abstract boolean getCommands();

	public abstract boolean postCommands(List<Object> commands);

	public abstract boolean getAI();

	public abstract boolean addAI(String ai);


	//Moves
	public abstract boolean sendChat(int playerID, String message);

	public abstract boolean rollNumber(int playerID, int numRolled);

	public abstract boolean robPlayer(int playerID, int victimID, HexLocation robberLoc);

	public abstract boolean finishTurn(int playerID);

	public abstract boolean buyDevCard(int playerID);

	public abstract boolean playYearOfPlenty(int playerID, ResourceType type1, ResourceType type2);

	public abstract boolean playRoadBuilding(int playerID, EdgeLocation spot1, EdgeLocation spot2);

	public abstract boolean playKnight(int playerID, int victimID, HexLocation robberLoc);

	public abstract boolean playMonopoly(int playerID, ResourceType type);

	public abstract boolean playMonument(int playerID);

	public abstract boolean buildRoad(int playerID, EdgeLocation roadLocation, boolean isInitSetup);

	public abstract boolean buildSettlement(int playerID, VertexLocation vertLoc, boolean isInitSetup);

	public abstract boolean buildCity(int playerID, VertexLocation vertLoc);

	public abstract boolean offerTrade(int playerID, int receiverID, Map<ResourceType, Integer> offer);

	public abstract boolean acceptTrade(int playerID, boolean willAccept);

	public abstract boolean maritimeTrade(int playerID, int ratio, ResourceType input, ResourceType output);

	public abstract boolean discardCards(int playerID, Map<ResourceType, Integer> toDiscard);


	//Util
	public abstract boolean changeLogLevel(String newLevel);

	public abstract void getUpdate();

	public abstract void finishGame();

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataParser == null) ? 0 : dataParser.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + Integer.parseInt(port);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proxy other = (Proxy) obj;
		if (dataParser == null) {
			if (other.dataParser != null)
				return false;
		} else if (!dataParser.equals(other.dataParser))
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		return true;
	}

	public abstract void logout();
}
