package client.proxy;

import java.util.List;
import java.util.Map;

import shared.DataParser;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.authentication.AuthCookie;
import shared.model.exceptions.DuplicateUsernameException;
import shared.model.exceptions.InvalidCredentialsException;
import shared.model.resourcecard.ResourceCard;

/**
 * The "mock proxy" class for testing.
 * @author Ryan F. Lee
 *
 */
public class Mocksy extends Proxy {

	private String jsonModel;


	public Mocksy() {
		super();
		this.jsonInit();
	}

	public Mocksy(DataParser dp) {
		super(dp);
	}

	public Mocksy(String host, String port) {
		super(host, port);
	}

	@Override
	public int getVersionNum() {
		return -1;
	}

	@Override
	public boolean userLogin(String username, String password) {
		this.dataParser.parseUserCookie("{\"name\":\"Sam\",\"password\":\"sam\",\"playerID\":0}");
		return true;
	}

	@Override
	public boolean register(String username, String password) {
		return true;
	}

	@Override
	public boolean getGames() {
		String jsonGames = "[{\"title\":\"Default Game\",\"id\":0,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]},{\"title\":\"AI Game\",\"id\":1,\"players\":[{\"color\":\"orange\",\"name\":\"Pete\",\"id\":10},{\"color\":\"purple\",\"name\":\"Hannah\",\"id\":-2},{\"color\":\"white\",\"name\":\"Quinn\",\"id\":-3},{\"color\":\"green\",\"name\":\"Squall\",\"id\":-4}]},{\"title\":\"Empty Game\",\"id\":2,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]}]";
		this.dataParser.parseGameList(jsonGames);
		return true;
		//return "[{\"title\":\"Default Game\",\"id\":0,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]},{\"title\":\"AI Game\",\"id\":1,\"players\":[{\"color\":\"orange\",\"name\":\"Pete\",\"id\":10},{\"color\":\"purple\",\"name\":\"Hannah\",\"id\":-2},{\"color\":\"white\",\"name\":\"Quinn\",\"id\":-3},{\"color\":\"green\",\"name\":\"Squall\",\"id\":-4}]},{\"title\":\"Empty Game\",\"id\":2,\"players\":[{\"color\":\"orange\",\"name\":\"Sam\",\"id\":0},{\"color\":\"blue\",\"name\":\"Brooke\",\"id\":1},{\"color\":\"red\",\"name\":\"Pete\",\"id\":10},{\"color\":\"green\",\"name\":\"Mark\",\"id\":11}]}]";
	}

	@Override
	public boolean createGame(boolean randTiles, boolean randNums,
			boolean randPorts, String gameName) {
		return true;
		//return "{\"title\":\"test\",\"id\":3,\"players\":[{},{},{},{}]}";
	}

	@Override
	public boolean joinGame(int gameID, CatanColor color) {
		//this.dataParser.parseNewGame("");
		this.dataParser.buildModelFromJson(jsonModel,false);
		return true;
		//return "Success";
	}

	@Override
	public boolean saveGame(int gameID, String fileName) {
		return true;
		//return "Success";
	}

	@Override
	public boolean loadGame(String fileName) {
		return true;
		//return "Success";
	}

	@Override
	public boolean getModel(int version) {
		if(this.jsonModel == null) {
			jsonInit();
		}
		dataParser.buildModelFromJson(this.jsonModel,false);
		return true;
	}

	@Override
	public boolean getModel() {
		if(this.jsonModel == null) {
			jsonInit();
		}
		dataParser.buildModelFromJson(this.jsonModel,false);
		return true;
	}

	@Override
	public boolean resetGame() {
		dataParser.buildModelFromJson(this.jsonModel,false);
		return true;
	}

	@Override
	public boolean getCommands() {
		//"[{\"vertexLocation\": {\"direction\": \"W\",\"x\": 0,\"y\": 0},\"free\": false,\"type\": \"buildSettlement\",\"playerIndex\": 0}]";
		return true;
	}

	@Override
	public boolean postCommands(List<Object> commands) {
		dataParser.buildModelFromJson(this.jsonModel,false);
		return true;
	}

	@Override
	public boolean getAI() {
		//"[\"LARGEST_ARMY\"]";
		return true;
	}

	@Override
	public boolean addAI(String ai) {
		//"Success";
		return true;
	}

	@Override
	public boolean sendChat(int playerID, String message) {
		return getModel(0);
	}

	@Override
	public boolean rollNumber(int playerID, int numRolled) {
		return getModel(0);
	}

	@Override
	public boolean robPlayer(int playerID, int victimID, HexLocation robberLoc) {
		return getModel(0);
	}

	@Override
	public boolean finishTurn(int playerID) {
		return getModel(0);
	}

	@Override
	public boolean buyDevCard(int playerID) {
		return getModel(0);
	}

	@Override
	public boolean playYearOfPlenty(int playerID, ResourceType type1,
			ResourceType type2) {
		return getModel(0);
	}

	@Override
	public boolean playRoadBuilding(int playerID, EdgeLocation spot1,
			EdgeLocation spot2) {
		return getModel(0);
	}

	@Override
	public boolean playKnight(int playerID, int victimID, HexLocation robberLoc) {
		return getModel(0);
	}

	@Override
	public boolean playMonopoly(int playerID, ResourceType type) {
		return getModel(0);
	}

	@Override
	public boolean playMonument(int playerID) {
		return getModel(0);
	}

	@Override
	public boolean buildRoad(int playerID, EdgeLocation roadLocation,
			boolean isInitSetup) {
		return getModel(0);
	}

	@Override
	public boolean buildSettlement(int playerID, VertexLocation vertLoc,
			boolean isInitSetup) {
		return getModel(0);
	}

	@Override
	public boolean buildCity(int playerID, VertexLocation vertLoc) {
		return getModel(0);
	}

	@Override
	public boolean offerTrade(int playerID, int receiverID,
			Map<ResourceType, Integer> offer) {
		return getModel(0);
	}

	@Override
	public boolean acceptTrade(int playerID, boolean willAccept) {
		return getModel(0);
	}

	@Override
	public boolean maritimeTrade(int playerID, int ratio, ResourceType input,
			ResourceType output) {
		return getModel(0);
	}

	@Override
	public boolean discardCards(int playerID, Map<ResourceType, Integer> toDiscard) {
		return getModel(0);
	}

	@Override
	public boolean changeLogLevel(String newLevel) {
		//"Success";
		return true;
	}

	@Override
	public void getUpdate() {
		getModel();
	}

	@Override
	public void finishGame() { }

	private void jsonInit() {
		this.jsonModel = "{\"deck\":{\"yearOfPlenty\":2,\"monopoly\":2,\"soldier\":14,\"roadBuilding\":2,\"monument\":5},\"map\":{\"hexes\":[" +
				"{\"location\":{\"x\":0,\"y\":-2}},{\"resource\":\"brick\",\"location\":{\"x\":1,\"y\":-2},\"number\":4}," +
				"{\"resource\":\"wood\",\"location\":{\"x\":2,\"y\":-2},\"number\":11}," +
				"{\"resource\":\"brick\",\"location\":{\"x\":-1,\"y\":-1},\"number\":8}," +
				"{\"resource\":\"wood\",\"location\":{\"x\":0,\"y\":-1},\"number\":3}," +
				"{\"resource\":\"ore\",\"location\":{\"x\":1,\"y\":-1},\"number\":9},{\"resource\":\"sheep\",\"location\":" +
				"{\"x\":2,\"y\":-1},\"number\":12},{\"resource\":\"ore\",\"location\":{\"x\":-2,\"y\":0},\"number\":5}," +
				"{\"resource\":\"sheep\",\"location\":{\"x\":-1,\"y\":0},\"number\":10}," +
				"{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":0},\"number\":11},{\"resource\":\"brick\",\"location\":" +
				"{\"x\":1,\"y\":0},\"number\":5},{\"resource\":\"wheat\",\"location\":{\"x\":2,\"y\":0},\"number\":6},{\"resource\":\"wheat\",\"location\":" +
				"{\"x\":-2,\"y\":1},\"number\":2},{\"resource\":\"sheep\",\"location\":{\"x\":-1,\"y\":1},\"number\":9},{\"resource\":\"wood\",\"location\":" +
				"{\"x\":0,\"y\":1},\"number\":4},{\"resource\":\"sheep\",\"location\":{\"x\":1,\"y\":1},\"number\":10},{\"resource\":\"wood\",\"location\":" +
				"{\"x\":-2,\"y\":2},\"number\":6},{\"resource\":\"ore\",\"location\":{\"x\":-1,\"y\":2},\"number\":3}," +
				"{\"resource\":\"wheat\",\"location\":{\"x\":0,\"y\":2},\"number\":8}],\"roads\":[" +
				"{\"owner\":1,\"location\":{\"direction\":\"S\",\"x\":-1,\"y\":-1}},{\"owner\":0,\"location\":{\"direction\":\"SW\",\"x\":2,\"y\":-1}}," +
				"{\"owner\":3,\"location\":{\"direction\":\"SW\",\"x\":-1,\"y\":1}}," +
				"{\"owner\":3,\"location\":{\"direction\":\"SW\",\"x\":2,\"y\":-2}},{\"owner\":0,\"location\":{\"direction\":\"S\",\"x\":1,\"y\":-1}}," +
				"{\"owner\":0,\"location\":{\"direction\":\"S\",\"x\":0,\"y\":1}},{\"owner\":2,\"location\":{\"direction\":\"S\",\"x\":0,\"y\":0}}," +
				"{\"owner\":1,\"location\":{\"direction\":\"SW\",\"x\":-2,\"y\":1}},{\"owner\":0,\"location\":{\"direction\":\"SW\",\"x\":2,\"y\":0}}],\"cities\":" +
				"[{\"owner\":0,\"location\":{\"direction\":\"SE\",\"x\":-1,\"y\":0}}],\"settlements\":[{\"owner\":3,\"location\":{\"direction\":\"SW\",\"x\":-1,\"y\":1}}," +
				"{\"owner\":3,\"location\":{\"direction\":\"SE\",\"x\":1,\"y\":-2}},{\"owner\":2,\"location\":{\"direction\":\"SW\",\"x\":0,\"y\":0}}," +
				"{\"owner\":2,\"location\":{\"direction\":\"SW\",\"x\":1,\"y\":-1}},{\"owner\":1,\"location\":{\"direction\":\"SW\",\"x\":-2,\"y\":1}}," +
				"{\"owner\":0,\"location\":{\"direction\":\"SE\",\"x\":0,\"y\":1}},{\"owner\":1,\"location\":{\"direction\":\"SW\",\"x\":-1,\"y\":-1}}," +
				"{\"owner\":0,\"location\":{\"direction\":\"E\",\"x\":1,\"y\":1}}],\"radius\":3,\"ports\":[{\"ratio\":2,\"resource\":\"brick\",\"direction\":\"NE\",\"location\":" +
				"{\"x\":-2,\"y\":3}},{\"ratio\":3,\"direction\":\"SE\",\"location\":{\"x\":-3,\"y\":0}}," +
				"{\"ratio\":2,\"resource\":\"sheep\",\"direction\":\"NW\",\"location\":{\"x\":3,\"y\":-1}},{\"ratio\":3,\"direction\":\"NW\",\"location\":" +
				"{\"x\":2,\"y\":1}},{\"ratio\":3,\"direction\":\"N\",\"location\":{\"x\":0,\"y\":3}},{\"ratio\":2,\"resource\":\"ore\",\"direction\":\"S\",\"location\":" +
				"{\"x\":1,\"y\":-3}},{\"ratio\":3,\"direction\":\"SW\",\"location\":{\"x\":3,\"y\":-3}},{\"ratio\":2,\"resource\":\"wheat\",\"direction\":\"S\",\"location\":" +
				"{\"x\":-1,\"y\":-2}},{\"ratio\":2,\"resource\":\"wood\",\"direction\":\"NE\",\"location\":{\"x\":-3,\"y\":2}}],\"robber\":{\"x\":1,\"y\":0}},\"players\":[" +
				"{\"resources\":{\"brick\":15,\"wood\":15,\"sheep\":2,\"wheat\":4,\"ore\":5},\"oldDevCards\":" +
				"{\"yearOfPlenty\":-1,\"monopoly\":-1,\"soldier\":-1,\"roadBuilding\":-1,\"monument\":0},\"newDevCards\":" +
				"{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":10,\"cities\":0,\"settlements\":" +
				"3,\"soldiers\":1,\"victoryPoints\":4,\"monuments\":0,\"playedDevCard\":true,\"discarded\":false,\"playerID\":0,\"playerIndex\":0,\"name\":\"Sam\",\"color\":\"orange\"}," +
				"{\"resources\":{\"brick\":3,\"wood\":3,\"sheep\":3,\"wheat\":3,\"ore\":3},\"oldDevCards\":" +
				"{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"newDevCards\":" +
				"{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":13,\"cities\":4," +
				"\"settlements\":3,\"soldiers\":0,\"victoryPoints\":2,\"monuments\":0,\"playedDevCard\":false,\"discarded\":" +
				"false,\"playerID\":1,\"playerIndex\":1,\"name\":\"Brooke\",\"color\":\"blue\"}," +
				"{\"resources\":{\"brick\":0,\"wood\":1,\"sheep\":1,\"wheat\":0,\"ore\":0},\"oldDevCards\":" +
				"{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"newDevCards\":" +
				"{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0},\"roads\":13,\"cities\":4,\"settlements\":3,\"soldiers\":0,\"victoryPoints\":2,\"monuments\":0,\"playedDevCard\":" +
				"false,\"discarded\":false,\"playerID\":10,\"playerIndex\":2,\"name\":\"Pete\",\"color\":\"red\"},{\"resources\":" +
				"{\"brick\":0,\"wood\":1,\"sheep\":1,\"wheat\":0,\"ore\":1},\"oldDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0}" +
				",\"newDevCards\":{\"yearOfPlenty\":0,\"monopoly\":0,\"soldier\":0,\"roadBuilding\":0,\"monument\":0}," +
				"\"roads\":13,\"cities\":4,\"settlements\":3,\"soldiers\":0,\"victoryPoints\":2,\"monuments\":0,\"playedDevCard\":false," +
				"\"discarded\":false,\"playerID\":11,\"playerIndex\":3,\"name\":\"Mark\",\"color\":\"green\"}],\"log\":{\"lines\":[" +
				"{\"source\":\"Brooke\",\"message\":\"Brooke built a settlement\"},{\"source\":\"Brooke\",\"message\":\"Brooke\u0027s turn just ended\"}," +
				"{\"source\":\"Pete\",\"message\":\"Pete built a road\"},{\"source\":\"Pete\",\"message\":\"Pete built a settlement\"}," +
				"{\"source\":\"Pete\",\"message\":\"Pete\u0027s turn just ended\"},{\"source\":\"Mark\",\"message\":\"Mark built a road\"}," +
				"{\"source\":\"Mark\",\"message\":\"Mark built a settlement\"},{\"source\":\"Mark\",\"message\":\"Mark\u0027s turn just ended\"}," +
				"{\"source\":\"Mark\",\"message\":\"Mark built a road\"},{\"source\":\"Mark\",\"message\":\"Mark built a settlement\"}," +
				"{\"source\":\"Mark\",\"message\":\"Mark\u0027s turn just ended\"},{\"source\":\"Pete\",\"message\":\"Pete built a road\"}," +
				"{\"source\":\"Pete\",\"message\":\"Pete built a settlement\"},{\"source\":\"Pete\",\"message\":\"Pete\u0027s turn just ended\"}," +
				"{\"source\":\"Brooke\",\"message\":\"Brooke built a road\"},{\"source\":\"Brooke\",\"message\":\"Brooke built a settlement\"}," +
				"{\"source\":\"Brooke\",\"message\":\"Brooke\u0027s turn just ended\"},{\"source\":\"Sam\",\"message\":\"Sam built a road\"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam built a settlement\"},{\"source\":\"Sam\",\"message\":\"Sam\u0027s turn just ended\"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam built 2 roads\"},{\"source\":\"Sam\",\"message\":\"Sam built a road\"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam built a road\"},{\"source\":\"Sam\",\"message\":\"Sam used a soldier \"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam moved the robber and robbed Brooke.\"},{\"source\":\"Sam\",\"message\":\"Sam used Year of Plenty and got a sheep and a ore\"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam stole everyones wheat\"},{\"source\":\"Sam\",\"message\":\"Sam built a road\"}," +
				"{\"source\":\"Sam\",\"message\":\"Sam built a settlement\"},{\"source\":\"Sam\",\"message\":\"Sam upgraded to a city\"}]}," +
				"\"chat\":{\"lines\":[]},\"bank\":{\"brick\":25,\"wood\":0,\"sheep\":21,\"wheat\":29,\"ore\":23},\"turnTracker\":" +
				"{\"status\":\"firstround\",\"currentTurn\":0,\"longestRoad\":-1,\"largestArmy\":-1},\"tradeOffer\":{\"sender\":0,\"receiver\":2,\"offer\":" +
				"{\"brick\":-3,\"wood\":0,\"sheep\":-2,\"wheat\":1,\"ore\":4}},\"winner\":-1,\"version\":10}";

	}

	@Override
	public boolean getGameOver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setGameOver(boolean gameOver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub
		
	}
}
