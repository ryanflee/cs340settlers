package client.poller;

import java.awt.event.*;

import client.proxy.*;
import javax.swing.Timer;
/**
 * The Catan client's Poller. This periodically will check to see if anything has changed in the server's game model.
 * @author Jordan Wilbur
 *
 */
public class Poller {
	private Proxy proxy;
	private static int waitTime = 500;
	private Timer timer;
	private static Poller poller;
	
	public static void init(Proxy p) {
		poller = new Poller(p);
	}
	
	public static void init(Proxy p, int t) {
		poller = new Poller(p,t);
	}
	
	private Poller(Proxy proxy) {
		this(proxy,waitTime);
	}
	
	private Poller(Proxy proxy, int timeInterval) {
		poller = this;
		this.proxy = proxy;
		timer = new Timer(timeInterval, new ActionListener() {
			public void actionPerformed(ActionEvent e){
				poll();
			}
		});
	}
	
	public static void start() {
		if(poller == null)
			assert false;
		poller._start();
	}
	
	public static void end() {
		if(poller == null)
			assert false;
		poller._end();
	}
	
	public static void forcePoll() {
		if (poller == null) {
			assert false;
		}
		poller._forcePoll();
	}
	
	/**
	 * This function is called to start the poller.
	 */
	private void _start() {
		poller.timer.start();
	}
	
	/**
	 * This function is called to stop the poller.
	 */
	private void _end() {
		if(poller == null)return;
		poller.timer.stop();
	}
	
	private void poll(){
		//System.out.println("polling");
		proxy.getUpdate();
	}
	
	private void _forcePoll() {
		proxy.getModel(-1);
	}
}