package client.maritime;

import java.util.*;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import shared.model.Model;
import client.base.*;
import client.facade.Facade;


/**
 * Implementation for the maritime trade controller
 */
public class MaritimeTradeController extends Controller implements IMaritimeTradeController, Observer  {

	private IMaritimeTradeOverlay tradeOverlay;
	private Model m;
	private int ratio;
	private ResourceType give,get;
	
	public MaritimeTradeController(IMaritimeTradeView tradeView, IMaritimeTradeOverlay tradeOverlay) {
		
		super(tradeView);
		Facade.addListener(this);
		setTradeOverlay(tradeOverlay);
	}
	
	public IMaritimeTradeView getTradeView() {
		
		return (IMaritimeTradeView)super.getView();
	}
	
	public IMaritimeTradeOverlay getTradeOverlay() {
		return tradeOverlay;
	}

	public void setTradeOverlay(IMaritimeTradeOverlay tradeOverlay) {
		this.tradeOverlay = tradeOverlay;
	}

	@Override
	public void startTrade() {
		this.initTradeOverLay(m);
		getTradeOverlay().showModal();
	}

	@Override
	public void makeTrade() {
		tradeOverlay.setCancelEnabled(false);
		Facade.maritimeTrade(ratio, give, get);
		if(getTradeOverlay().isModalShowing())
			getTradeOverlay().closeModal();
	}
	
	@Override
	public void cancelTrade() {
		tradeOverlay.reset();
		if(getTradeOverlay().isModalShowing())
			getTradeOverlay().closeModal();
	}

	@Override
	public void setGetResource(ResourceType resource) {
		get = resource;
		tradeOverlay.hideGetOptions();
		tradeOverlay.selectGetOption(resource,1);
		tradeOverlay.setTradeEnabled(true);
	}

	@Override
	public void setGiveResource(ResourceType resource) {
		give = resource;
		tradeOverlay.hideGiveOptions();
		List<ResourceType> left = new ArrayList<>();
		for(ResourceType type : ResourceType.values()) {
			if(Facade.resourceLeft(type) > 0)
				left.add(type);
		}
		ratio = Facade.maritimeTradeValue(resource);
		tradeOverlay.showGetOptions(left.toArray(new ResourceType[left.size()]));
		tradeOverlay.selectGiveOption(resource, ratio); 
	}

	@Override
	public void unsetGetValue() {
		tradeOverlay.setTradeEnabled(false);
		List<ResourceType> left = new ArrayList<>();
		for(ResourceType type : ResourceType.values()) {
			if(Facade.resourceLeft(type) > 0)
				left.add(type);
		}
		tradeOverlay.showGetOptions(left.toArray(new ResourceType[left.size()]));
	}

	@Override
	public void unsetGiveValue() {
		initTradeOverLay(m);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model)
		{
			m = (Model)arg;
			//System.out.println(this.getTradeOverlay().isModalShowing());
			if(!Facade.canOfferTrade())
				this.getTradeView().enableMaritimeTrade(false);
			else
				this.getTradeView().enableMaritimeTrade(true);
		}
		
	}
	
	private void initTradeOverLay(Model m) {
		tradeOverlay.reset();
		tradeOverlay.setTradeEnabled(false);
		tradeOverlay.setCancelEnabled(true);
		tradeOverlay.hideGetOptions();
		tradeOverlay.hideGiveOptions();
		List<ResourceType> trade = new ArrayList<ResourceType>();
		for(ResourceType type : ResourceType.values()) {
			int amt = Facade.maritimeTradeValue(type);
			if(amt != -1) {
				trade.add(type);
				tradeOverlay.selectGiveOption(type, amt);
			}
		}
		tradeOverlay.showGiveOptions(trade.toArray(new ResourceType[trade.size()]));
	}

}

