package client.discard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import shared.model.Model;
import shared.model.resourcecard.ResourceCard;
import client.base.*;
import client.facade.Facade;
import client.misc.*;


/**
 * Discard controller implementation
 */
public class DiscardController extends Controller implements IDiscardController, Observer {

	private IWaitView waitView;
	private Model model;
	private int handSize, brick, ore, sheep, wheat, wood;
	private int brickAmt, oreAmt, sheepAmt, wheatAmt, woodAmt;
	private int totalDiscard;
	
	/**
	 * DiscardController constructor
	 * 
	 * @param view View displayed to let the user select cards to discard
	 * @param waitView View displayed to notify the user that they are waiting for other players to discard
	 */
	public DiscardController(IDiscardView view, IWaitView waitView) {
		
		super(view);
		
		this.waitView = waitView;
		Facade.addListener(this);
	}

	public IDiscardView getDiscardView() {
		return (IDiscardView)super.getView();
	}
	
	public IWaitView getWaitView() {
		return waitView;
	}

	public void checkDiscard(){
		int halfHand = handSize / 2;
		getDiscardView().setStateMessage("Discard: " + (totalDiscard) + "/" + (halfHand));
		//Check to see if you have the max amount to discard
		if(totalDiscard == halfHand){
			//If you do disable all buttons except for decreasing values
			getDiscardView().setDiscardButtonEnabled(true);
			if(brickAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, true);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, false);}
			if(oreAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, true);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, false);}
			if(sheepAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, true);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, false);}
			if(wheatAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, true);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, false);}
			if(woodAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, true);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, false);}
		}
		else{
			//If you don't allow everything it's full potential
			getDiscardView().setDiscardButtonEnabled(false);
			if(brickAmt < brick){
				if(brickAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, true, true);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, true, false);}
			}
			else{
				if(brickAmt == 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, false);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, true);}
			}
			if(oreAmt < ore){
				if(oreAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, true, true);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, true, false);}
			}
			else{
				if(oreAmt == 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, false);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, true);}
			}
			if(sheepAmt < sheep){
				if(sheepAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, true, true);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, true, false);}
			}
			else{
				if(sheepAmt == 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, false);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, true);}
			}
			if(wheatAmt < wheat){
				if(wheatAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, true, true);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, true, false);}
			}
			else{
				if(wheatAmt == 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, false);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, true);}
			}
			if(woodAmt < wood){
				if(woodAmt > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, true, true);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, true, false);}
			}
			else{
				if(woodAmt == 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, false);}
				else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, true);}
			}
		}
	}
	
	@Override
	public void increaseAmount(ResourceType resource) {
		//Increment how many cards are selected to be discarded
		totalDiscard++;
		//Check which resource was incremented and see if it's met its boundaries
		if(resource == ResourceType.BRICK){
			brickAmt++;
			if(brickAmt == brick){
				getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
				getDiscardView().setResourceDiscardAmount(resource, brickAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, brickAmt);
			}
		}
		//Do the same for all resource types
		if(resource == ResourceType.ORE){
			oreAmt++;
			if(oreAmt == ore){
				getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
				getDiscardView().setResourceDiscardAmount(resource, oreAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, oreAmt);
			}
		}
		if(resource == ResourceType.SHEEP){
			sheepAmt++;
			if(sheepAmt == sheep){
				getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
				getDiscardView().setResourceDiscardAmount(resource, sheepAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, sheepAmt);
			}
		}
		if(resource == ResourceType.WHEAT){
			wheatAmt++;
			if(wheatAmt == wheat){
				getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
				getDiscardView().setResourceDiscardAmount(resource, wheatAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, wheatAmt);
			}
		}
		if(resource == ResourceType.WOOD){
			woodAmt++;
			if(woodAmt == wood){
				getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
				getDiscardView().setResourceDiscardAmount(resource, woodAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, woodAmt);
			}
		}
		//Check to see if you can successfully discard
		checkDiscard();
		updateNumbers();
	}

	@Override
	public void decreaseAmount(ResourceType resource) {
		//Total has been decremented. Should be no way to submit. 
		totalDiscard--;
		if(resource == ResourceType.BRICK){
			brickAmt--;
			if(brickAmt == 0){
				getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
				getDiscardView().setResourceDiscardAmount(resource, brickAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, brickAmt);
			}
		}
		if(resource == ResourceType.ORE){
			oreAmt--;
			if(oreAmt == 0){
				getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
				getDiscardView().setResourceDiscardAmount(resource, oreAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, oreAmt);
			}
		}
		if(resource == ResourceType.SHEEP){
			sheepAmt--;
			if(sheepAmt == 0){
				getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
				getDiscardView().setResourceDiscardAmount(resource, sheepAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, sheepAmt);
			}
		}
		if(resource == ResourceType.WHEAT){
			wheatAmt--;
			if(wheatAmt == 0){
				getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
				getDiscardView().setResourceDiscardAmount(resource, wheatAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, wheatAmt);
			}
		}
		if(resource == ResourceType.WOOD){
			woodAmt--;
			if(woodAmt == 0){
				getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
				getDiscardView().setResourceDiscardAmount(resource, woodAmt);
			}
			else{
				getDiscardView().setResourceAmountChangeEnabled(resource, true, true);
				getDiscardView().setResourceDiscardAmount(resource, woodAmt);
			}
		}
		//Check to see if valid discard.  If not make sure all are able to increment or decrement if possible.
		checkDiscard();
		updateNumbers();
	}

	@Override
	public void discard() {
		//Put together the resource cards you are discarding
		Map<ResourceType, Integer> toDiscard = new HashMap<ResourceType, Integer>();
		toDiscard.put(ResourceType.BRICK, brickAmt);
		toDiscard.put(ResourceType.ORE, oreAmt);
		toDiscard.put(ResourceType.SHEEP, sheepAmt);
		toDiscard.put(ResourceType.WHEAT, wheatAmt);
		toDiscard.put(ResourceType.WOOD, woodAmt);
		
		//Close the view and open the waiting view
		if(getDiscardView().isModalShowing()){
			getDiscardView().closeModal();
		}
		getWaitView().setMessage("You have discarded. Now waiting for other players.");
		Facade.discardCards(toDiscard);
		//getWaitView().showModal();
		Facade.discardCards(toDiscard);
	}

	private void updateNumbers() {
		getDiscardView().setResourceDiscardAmount(ResourceType.WOOD, woodAmt);
		getDiscardView().setResourceDiscardAmount(ResourceType.WHEAT, wheatAmt);
		getDiscardView().setResourceDiscardAmount(ResourceType.SHEEP, sheepAmt);
		getDiscardView().setResourceDiscardAmount(ResourceType.ORE, oreAmt);
		getDiscardView().setResourceDiscardAmount(ResourceType.BRICK, brickAmt);
	}


	public void init(){
		//Check to see if they need to discard anything
		if(Facade.getPlayerResources().size() > 7)
		{
			handSize = Facade.getPlayerResources().size();
			brick = ore = sheep = wheat = wood = 0;
			brickAmt = oreAmt = sheepAmt = wheatAmt = woodAmt = 0;
			totalDiscard = 0;
			getDiscardView().setStateMessage("Discard: " + (totalDiscard) + "/" + (handSize/2));
			//Gather which cards they actually have
			List<ResourceCard> cards = Facade.getPlayerResources();
			for(ResourceCard c:cards){
				switch(c.getType()){
				case BRICK:brick++;break;
				case ORE:ore++;break;
				case SHEEP:sheep++;break;
				case WHEAT:wheat++;break;
				case WOOD:wood++;break;
				}
			}
			getDiscardView().setResourceMaxAmount(ResourceType.BRICK, brick);
			getDiscardView().setResourceMaxAmount(ResourceType.ORE, ore);
			getDiscardView().setResourceMaxAmount(ResourceType.SHEEP, sheep);
			getDiscardView().setResourceMaxAmount(ResourceType.WHEAT, wheat);
			getDiscardView().setResourceMaxAmount(ResourceType.WOOD, wood);
			
			getDiscardView().setResourceDiscardAmount(ResourceType.BRICK, 0);
			getDiscardView().setResourceDiscardAmount(ResourceType.ORE, 0);
			getDiscardView().setResourceDiscardAmount(ResourceType.SHEEP, 0);
			getDiscardView().setResourceDiscardAmount(ResourceType.WHEAT, 0);
			getDiscardView().setResourceDiscardAmount(ResourceType.WOOD, 0);
			
			//List each amount for each resource type
			if(brick > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, true, false);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.BRICK, false, false);}
			if(ore > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, true, false);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.ORE, false, false);}
			if(sheep > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, true, false);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.SHEEP, false, false);}
			if(wheat > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, true, false);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WHEAT, false, false);}
			if(wood > 0){getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, true, false);}
			else{getDiscardView().setResourceAmountChangeEnabled(ResourceType.WOOD, false, false);}
			
			getDiscardView().setDiscardButtonEnabled(false);
			if(getDiscardView().isModalShowing()){
				getDiscardView().closeModal();
			}
			getDiscardView().showModal();
		}
		else{
			Map<ResourceType,Integer> discard = new HashMap<ResourceType,Integer>();
			discard.put(ResourceType.BRICK, 0);
			discard.put(ResourceType.ORE, 0);
			discard.put(ResourceType.SHEEP, 0);
			discard.put(ResourceType.WHEAT, 0);
			discard.put(ResourceType.WOOD, 0);
			
			getWaitView().setMessage("Someone rolled a seven. You need to wait until everyone has discarded.");
			if(getWaitView().isModalShowing()){
				getWaitView().closeModal();
			}
			getWaitView().showModal();
			Facade.discardCards(discard);
		}
	}
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model){
			model = (Model) arg;
			//System.out.println(model.gameManager.getStatus());
			if(model.gameManager.getStatus().equalsIgnoreCase("discarding")){
				//System.out.println("I am about to discard!");
				init();
			}
			else{
				if(getWaitView().isModalShowing()){
					getWaitView().closeModal();
				}
				if(getDiscardView().isModalShowing()){
					getDiscardView().closeModal();
				}
			}
		}
	}	
}
