package client.turntracker;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.CatanColor;
import shared.model.Model;
import shared.model.developmentcard.LargestArmy;
import shared.model.developmentcard.LongestRoad;
import shared.model.player.Player;
import client.base.*;
import client.data.PlayerInfo;
import client.facade.Facade;


/**
 * Implementation for the turn tracker controller
 */
public class TurnTrackerController extends Controller implements ITurnTrackerController, Observer {

	private Model model;
	private boolean initialized;
	
	public TurnTrackerController(ITurnTrackerView view) {
		
		super(view);
		Facade.addListener(this);
		
		initFromModel();
	}
	
	@Override
	public ITurnTrackerView getView() {
		
		return (ITurnTrackerView)super.getView();
	}

	@Override
	public void endTurn() {

		ITurnTrackerView ttv = getView();
		ttv.updateGameState("Waiting", false);
		Facade.finishTurn();
	}
	
	private void initFromModel() {
		if(model == null) {
			return;
		}		
	}
	

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if( arg instanceof Model && arg != null) {
			this.model = (Model) arg;
			ITurnTrackerView ttv = getView();

			List<Player> players = model.getPlayers();
			int curPlayer = model.gameManager.getCurrentPlayer();
			int count = 0;
			for (Player p : players) {
				if (p == null) { break; }
				count++;
			}
			if (count != 4) { return; }

			int indexLA = model.gameManager.getLargestArmy();
			int indexLR = model.gameManager.getLongestRoad();

			int index = 0;
			for ( Player p : players ) {
				if(p == null) { break; }

				if(!initialized) {
					getView().initializePlayer(index, p.getName(), p.getColor());
				}
				boolean isTurn = false;
				boolean largestArmy = (index == indexLA);
				boolean longestRoad = (index == indexLR);
				if (index == curPlayer) {
					isTurn = true;
				}
				getView().updatePlayer(index, p.getVictoryPoints(), isTurn, largestArmy, longestRoad);
				getView().updateColor(index, p.getColor());
				index++;
			}
			PlayerInfo player = Facade.getPlayerInfo();
			if(player != null && player.getColor() != null) {
				ttv.setLocalPlayerColor(player.getColor());
			}
			initialized = true;


			String state = model.gameManager.getStatus();
			if(curPlayer != player.getPlayerIndex()) { state = "waiting"; }
			if(model.getWinner() != -1) { state = "finished"; }
			switch(state.toLowerCase()) {
				case "playing":
					ttv.updateGameState("Finish Turn", true);
					break;
				case "rolling":
					ttv.updateGameState("Roll the Dice", false);
					break;
				case "robbing":
					ttv.updateGameState("Place the Robber", false);
					break;
				case "discarding":
					ttv.updateGameState("Discard Cards", false);
					break;
				case "finished":
					ttv.updateGameState("Game Over", false);
					break;
				default:
					ttv.updateGameState("Waiting for Other Players", false);
					break;
			}

		}
	}

}

