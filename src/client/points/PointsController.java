package client.points;

import java.util.Observable;
import java.util.Observer;

import shared.model.Model;

import client.base.*;
import client.facade.Facade;
import client.poller.Poller;


/**
 * Implementation for the points controller
 */
public class PointsController extends Controller implements IPointsController, Observer  {

	private IGameFinishedView finishedView;
	private boolean finished;

	/**
	 * PointsController constructor
	 * 
	 * @param view Points view
	 * @param finishedView Game finished view, which is displayed when the game is over
	 */
	public PointsController(IPointsView view, IGameFinishedView finishedView) {

		super(view);
		this.finished = false;
		setFinishedView(finishedView);
		Facade.addListener(this);
	}

	public IPointsView getPointsView() {

		return (IPointsView)super.getView();
	}

	public IGameFinishedView getFinishedView() {
		return finishedView;
	}
	public void setFinishedView(IGameFinishedView finishedView) {
		this.finishedView = finishedView;
	}

	private void initFromModel(Model m) {
		if(finished && !getFinishedView().isModalShowing()) {
			Facade.finishGame();
			Poller.forcePoll();
		}
		if (Facade.getPlayerIndex() != -1) {
			int p = Facade.getPlayerPoints();
			if(p>10)p=10;
			getPointsView().setPoints(p);
			if(p >= 10) {
				this.finishedView.setWinner(Facade.getPlayerInfo().getName(), true);
				if (this.finishedView.isModalShowing()) {
					this.finishedView.closeModal();
				}
				if (!finished) {
					this.finishedView.showModal();
				}
				finished = true;
			}
			else if (m.getWinner() != -1) {
				String winner = m.getPlayers().get(m.getWinner()).getName();
				this.finishedView.setWinner(winner, false);
				if (this.finishedView.isModalShowing()) {
					this.finishedView.closeModal();
				}
				if (!finished) {
					this.finishedView.showModal();
				}
				finished = true;
			}
			//Facade.gameOver();
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			initFromModel((Model)arg);
		} else {
			this.finished = false;
		}

	}

}

