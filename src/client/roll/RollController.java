package client.roll;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Timer;

import shared.model.Model;

import client.base.*;
import client.facade.Facade;


/**
 * Implementation for the roll controller
 */
public class RollController extends Controller implements IRollController, Observer {

	private IRollResultView resultView;
	private Timer timer = null;
	
	/**
	 * RollController constructor
	 * 
	 * @param view Roll view
	 * @param resultView Roll result view
	 */
	public RollController(IRollView view, IRollResultView resultView) {

		super(view);
		Facade.addListener(this);
		setResultView(resultView);
	}
	
	public IRollResultView getResultView() {
		return resultView;
	}
	public void setResultView(IRollResultView resultView) {
		this.resultView = resultView;
	}

	public IRollView getRollView() {
		return (IRollView)getView();
	}
	
	@Override
	public void rollDice() {
		if (timer != null) {
			timer.stop();
			timer = null;
		}
		int roll = (int)(Math.random()*6+1) + (int)(Math.random()*6+1);
		resultView.setRollValue(roll);
		
		Facade.rollNumber(roll);
		if(getResultView().isModalShowing()) {
			getResultView().closeModal();
		}
		getResultView().showModal();
		//Facade.rollNumber(7);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			if(Facade.canRollNumber()) {
				getRollView().showModal();
				if(timer != null) {
					timer.stop();
					timer = null;
				}
				timer = new Timer(4000,new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						timer.stop();
						timer = null;
						if(getRollView().isModalShowing())
							getRollView().closeModal();
						rollDice();
					}
				});
				timer.start();
			}
		}
		
	}

}

