package client.domestic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.*;
import shared.model.Model;
import shared.model.logs.ChatEntry;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;
import client.base.*;
import client.data.GameInfo;
import client.data.PlayerInfo;
import client.facade.Facade;
import client.misc.*;


/**
 * Domestic trade controller implementation
 */
public class DomesticTradeController extends Controller implements IDomesticTradeController, Observer {

	private IDomesticTradeOverlay tradeOverlay;
	private IWaitView waitOverlay;
	private IAcceptTradeOverlay acceptOverlay;
	private Model model;
	private int brick, ore, sheep, wheat, wood;
	private int sendBrickVal, sendOreVal, sendSheepVal, sendWheatVal, sendWoodVal;
	private int recBrickVal, recOreVal, recSheepVal, recWheatVal, recWoodVal;
	private boolean sendResource, sendBrick, sendOre, sendSheep, sendWheat, sendWood;
	private boolean recResource, recBrick, recOre, recSheep, recWheat, recWood;
	private boolean playerTrade;
	private int brickAmt, oreAmt, sheepAmt, wheatAmt, woodAmt;
	private int tradePlayerIndex;
	private int[] tradeOffer;
	
	/**
	 * DomesticTradeController constructor
	 * 
	 * @param tradeView Domestic trade view (i.e., view that contains the "Domestic Trade" button)
	 * @param tradeOverlay Domestic trade overlay (i.e., view that lets the user propose a domestic trade)
	 * @param waitOverlay Wait overlay used to notify the user they are waiting for another player to accept a trade
	 * @param acceptOverlay Accept trade overlay which lets the user accept or reject a proposed trade
	 */
	public DomesticTradeController(IDomesticTradeView tradeView, IDomesticTradeOverlay tradeOverlay,
									IWaitView waitOverlay, IAcceptTradeOverlay acceptOverlay) {

		super(tradeView);
		
		setTradeOverlay(tradeOverlay);
		setWaitOverlay(waitOverlay);
		setAcceptOverlay(acceptOverlay);
		Facade.addListener(this);
	}
	
	public IDomesticTradeView getTradeView() {
		
		return (IDomesticTradeView)super.getView();
	}

	public IDomesticTradeOverlay getTradeOverlay() {
		return tradeOverlay;
	}

	public void setTradeOverlay(IDomesticTradeOverlay tradeOverlay) {
		this.tradeOverlay = tradeOverlay;
	}

	public IWaitView getWaitOverlay() {
		return waitOverlay;
	}

	public void setWaitOverlay(IWaitView waitView) {
		this.waitOverlay = waitView;
	}

	public IAcceptTradeOverlay getAcceptOverlay() {
		return acceptOverlay;
	}

	public void setAcceptOverlay(IAcceptTradeOverlay acceptOverlay) {
		this.acceptOverlay = acceptOverlay;
	}

	@Override
	public void startTrade() {
		this.initTradeOverlay();
		getTradeOverlay().showModal();
	}

	@Override
	public void decreaseResourceAmount(ResourceType resource) {
		//Find out which resource is being decremented
		if(resource == ResourceType.BRICK){
			brickAmt--;
			//Print out what the value will now be
			tradeOverlay.setResourceAmount(resource, Integer.toString(brickAmt));
			//If it is 0 potentially not allow a trade
			if(brickAmt == 0){
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, false);
				if(sendBrick){
					sendBrickVal = 0;
				}
				if(recBrick){
					recBrickVal = 0;
				}
			}
			//If it is above a zero then it could be used in a trade
			else{
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, true);
				if(sendBrick){
					sendBrickVal = 1;
				}
				if(recBrick){
					recBrickVal = 1;
				}
			}
		}
		//Same process for all resource types
		else if(resource == ResourceType.ORE){
			oreAmt--;
			tradeOverlay.setResourceAmount(resource, Integer.toString(oreAmt));
			if(oreAmt == 0){
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, false);
				if(sendOre){
					sendOreVal = 0;
				}
				if(recOre){
					recOreVal = 0;
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, true);
				if(sendOre){
					sendOreVal = 1;
				}
				if(recOre){
					recOreVal = 1;
				}
			}
		}
		else if(resource == ResourceType.SHEEP){
			sheepAmt--;
			tradeOverlay.setResourceAmount(resource, Integer.toString(sheepAmt));
			if(sheepAmt == 0){
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, false);
				if(sendSheep){
					sendSheepVal = 0;
				}
				if(recSheep){
					recSheepVal = 0;
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, true);
				if(sendSheep){
					sendSheepVal = 1;
				}
				if(recSheep){
					recSheepVal = 1;
				}
			}
		}
		else if(resource == ResourceType.WHEAT){
			wheatAmt--;
			tradeOverlay.setResourceAmount(resource, Integer.toString(wheatAmt));
			if(wheatAmt == 0){
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, false);
				if(sendWheat){
					sendWheatVal = 0;
				}
				if(recWheat){
					recWheatVal = 0;
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, true);
				if(sendWheat){
					sendWheatVal = 1;
				}
				if(recWheat){
					recWheatVal = 1;
				}
			}
		}
		else if(resource == ResourceType.WOOD){
			woodAmt--;
			tradeOverlay.setResourceAmount(resource, Integer.toString(woodAmt));
			if(woodAmt == 0){
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, false);
				if(sendWood){
					sendWoodVal = 0;
				}
				if(recWood){
					recWoodVal = 0;
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(resource, true, true);
				if(sendWood){
					sendWoodVal = 1;
				}
				if(recWood){
					recWoodVal = 1;
				}
			}
		}
		
		//At the end always check to see if all conditions have been met to make a trade
		checkTrade();
	}

	@Override
	public void increaseResourceAmount(ResourceType resource) {
		//Find out which resource is being incremented
		if(resource == ResourceType.BRICK){
			brickAmt++;
			//Enable it to be used in trade since it is less than 0
			if(sendBrick){
				sendBrickVal = 1;
			}
			if(recBrick){
				recBrickVal = 1;
			}
			//Write out what number it has gotten to
			tradeOverlay.setResourceAmount(resource, Integer.toString(brickAmt));
			//Restrict sending only however many you have in your hand.
			if(brickAmt == brick){
				if(sendBrick){
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, false, true);
				}
				else{
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, true, true);
				}
			}
			//You can ask for as much as you want
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, true, true);
			}
		}
		//Do the same for all resource types
		else if(resource == ResourceType.ORE){
			oreAmt++;
			if(sendOre){
				sendOreVal = 1;
			}
			if(recOre){
				recOreVal = 1;
			}
			tradeOverlay.setResourceAmount(resource, Integer.toString(oreAmt));
			if(oreAmt == ore){
				if(sendOre){
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, false, true);
				}
				else{
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, true, true);
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, true, true);
			}
		}
		else if(resource == ResourceType.SHEEP){
			sheepAmt++;
			if(sendSheep){
				sendSheepVal = 1;
			}
			if(recSheep){
				recSheepVal = 1;
			}
			tradeOverlay.setResourceAmount(resource, Integer.toString(sheepAmt));
			if(sheepAmt == sheep){
				if(sendSheep){
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, false, true);
				}
				else{
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, true, true);
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, true, true);
			}
		}
		else if(resource == ResourceType.WHEAT){
			wheatAmt++;
			if(sendWheat){
				sendWheatVal = 1;
			}
			if(recWheat){
				recWheatVal = 1;
			}
			tradeOverlay.setResourceAmount(resource, Integer.toString(wheatAmt));
			if(wheatAmt == wheat){
				if(sendWheat){
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, false, true);
				}
				else{
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, true, true);
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, true, true);
			}
		}
		else if(resource == ResourceType.WOOD){
			woodAmt++;
			if(sendWood){
				sendWoodVal = 1;
			}
			if(recWood){
				recWoodVal = 1;
			}
			tradeOverlay.setResourceAmount(resource, Integer.toString(woodAmt));
			if(woodAmt == wood){
				if(sendWood){
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, false, true);
				}
				else{
					tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, true, true);
				}
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, true, true);
			}
		}
		//At the end always check if all criteria have been met for a valid trade
		checkTrade();
	}

	@Override
	public void sendTradeOffer() {
		Map<ResourceType, Integer> offer = new HashMap<ResourceType, Integer>();
		if(sendBrick){
			offer.put(ResourceType.BRICK, brickAmt);
		}
		else if(recBrick){
			brickAmt = -brickAmt;
			offer.put(ResourceType.BRICK, brickAmt);	
		} else {
			offer.put(ResourceType.BRICK, 0);
		}
		if(sendOre){
			offer.put(ResourceType.ORE, oreAmt);
		}
		else if(recOre){
			oreAmt = -oreAmt;
			offer.put(ResourceType.ORE, oreAmt);	
		}else {
			offer.put(ResourceType.ORE, 0);
		}
		if(sendSheep){
			offer.put(ResourceType.SHEEP, sheepAmt);
		}
		else if(recSheep){
			sheepAmt = -sheepAmt;
			offer.put(ResourceType.SHEEP, sheepAmt);	
		}else {
			offer.put(ResourceType.SHEEP, 0);
		}
		if(sendWheat){
			offer.put(ResourceType.WHEAT, wheatAmt);
		}
		else if(recWheat){
			wheatAmt = -wheatAmt;
			offer.put(ResourceType.WHEAT, wheatAmt);	
		}else {
			offer.put(ResourceType.WHEAT, 0);
		}
		if(sendWood){
			offer.put(ResourceType.WOOD, woodAmt);
		}
		else if(recWood){
			woodAmt = -woodAmt;
			offer.put(ResourceType.WOOD, woodAmt);	
		}
		else {
			offer.put(ResourceType.WOOD, 0);
		}
		int receiverID = tradePlayerIndex;
		if(getTradeOverlay().isModalShowing()){
			getTradeOverlay().closeModal();
		}
		getWaitOverlay().showModal();
		Facade.offerTrade(receiverID, offer);

	}

	@Override
	public void setPlayerToTradeWith(int playerIndex) {
		//If the index is -1 it isn't a valid player and you shouldn't trade
		if(playerIndex == -1){
			playerTrade = false;
		}
		else{
			playerTrade = true;
			tradePlayerIndex = playerIndex;
		}
		//Check if trade is possible
		checkTrade();
	}

	@Override
	public void setResourceToReceive(ResourceType resource) {
		//Check which resource you are setting
		if(resource == ResourceType.BRICK){
			//Initialize it's state
			brickAmt = 0;
			sendBrick = false;
			recBrick = true;
			tradeOverlay.setResourceAmount(resource, Integer.toString(brickAmt));
			tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, true, false);
		}
		//Do the same for all resource types
		else if(resource == ResourceType.ORE){
			oreAmt = 0;
			sendOre = false;
			recOre = true;
			tradeOverlay.setResourceAmount(resource, Integer.toString(oreAmt));
			tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, true, false);
		}
		else if(resource == ResourceType.SHEEP){
			sheepAmt = 0;
			sendSheep = false;
			recSheep = true;
			tradeOverlay.setResourceAmount(resource, Integer.toString(sheepAmt));
			tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, true, false);
		}
		else if(resource == ResourceType.WHEAT){
			wheatAmt = 0;
			sendWheat = false;
			recWheat = true;
			tradeOverlay.setResourceAmount(resource, Integer.toString(wheatAmt));
			tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, true, false);
		}
		if(resource == ResourceType.WOOD){
			woodAmt = 0;
			sendWood = false;
			recWood = true;
			tradeOverlay.setResourceAmount(resource, Integer.toString(woodAmt));
			tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, true, false);
		}
		//Check to see if you can still make a trade
		checkTrade();
	}
	
	@Override
	public void setResourceToSend(ResourceType resource) {
		//Check which resource you are setting
		if(resource == ResourceType.BRICK){
			//initialize all of its values
			brickAmt = 0;
			sendBrick = true;
			recBrick = false;
			tradeOverlay.setResourceAmount(resource, Integer.toString(brickAmt));
			//If you have some of that resource type in your hand allow the possibility to increment the value
			if(brick > 0){
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, true, false);
			}
			//Otherwise don't allow to increment
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.BRICK, false, false);
			}
		}
		//Do the same for all resource types
		else if(resource == ResourceType.ORE){
			oreAmt = 0;
			sendOre = true;
			recOre = false;
			tradeOverlay.setResourceAmount(resource, Integer.toString(oreAmt));
			if(ore > 0){
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, true, false);
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.ORE, false, false);
			}
		}
		else if(resource == ResourceType.SHEEP){
			sheepAmt = 0;
			sendSheep = true;
			recSheep = false;
			tradeOverlay.setResourceAmount(resource, Integer.toString(sheepAmt));
			if(sheep > 0){
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, true, false);
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.SHEEP, false, false);
			}
		}
		else if(resource == ResourceType.WHEAT){
			wheatAmt = 0;
			sendWheat = true;
			recWheat = false;
			tradeOverlay.setResourceAmount(resource, Integer.toString(wheatAmt));
			if(wheat > 0){
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, true, false);
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WHEAT, false, false);
			}
		}
		else if(resource == ResourceType.WOOD){
			woodAmt = 0;
			sendWood = true;
			recWood = false;
			tradeOverlay.setResourceAmount(resource, Integer.toString(woodAmt));
			if(wood > 0){
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, true, false);
			}
			else{
				tradeOverlay.setResourceAmountChangeEnabled(ResourceType.WOOD, false, false);
			}
		}
		//Check to see if still can do a valid trade
		checkTrade();
	}

	@Override
	public void unsetResource(ResourceType resource) {
		if(resource == ResourceType.BRICK)
			tradeOverlay.setResourceAmount(resource, "");
			tradeOverlay.setResourceAmountChangeEnabled(resource, false, false);
		if(resource == ResourceType.ORE)
			tradeOverlay.setResourceAmount(resource, "");
			tradeOverlay.setResourceAmountChangeEnabled(resource, false, false);
		if(resource == ResourceType.SHEEP)
			tradeOverlay.setResourceAmount(resource, "");
			tradeOverlay.setResourceAmountChangeEnabled(resource, false, false);
		if(resource == ResourceType.WHEAT)
			tradeOverlay.setResourceAmount(resource, "");
			tradeOverlay.setResourceAmountChangeEnabled(resource, false, false);
		if(resource == ResourceType.WOOD)
			tradeOverlay.setResourceAmount(resource, "");
			tradeOverlay.setResourceAmountChangeEnabled(resource, false, false);
	}

	@Override
	public void cancelTrade() {
		if(getTradeOverlay().isModalShowing()){
			getTradeOverlay().closeModal();
		}
		getAcceptOverlay().reset();
	}

	@Override
	public void acceptTrade(boolean willAccept) {
		Facade.acceptTrade(tradeOffer, willAccept);
		if(getAcceptOverlay().isModalShowing()){
			getAcceptOverlay().closeModal();
		}
		getAcceptOverlay().reset();
	}

	private void init(){
		if ((Facade.getPlayerIndex() >= 0) && (Facade.getPlayerIndex() < 4)) {
			List<ResourceCard> cards = Facade.getPlayerResources();
			for(ResourceCard c:cards){
				switch(c.getType()){
				case BRICK:brick++;break;
				case ORE:ore++;break;
				case SHEEP:sheep++;break;
				case WHEAT:wheat++;break;
				case WOOD:wood++;break;
				}
			}
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model) {
			model = (Model)arg;
			
			getTradeView().enableDomesticTrade(Facade.canOfferTrade());
			//if(model.gameManager.getStatus().equalsIgnoreCase("playing") && 
			//		(Facade.getPlayerIndex() == model.gameManager.getCurrentPlayer())){
			//}
			
			//if tradeOffer.recieverID = me then I need to pop up accept overlay
			if(model.gameManager.getTradeReceiver() != null){
				if(model.gameManager.getTradeReceiver().getPlayerIndex() == Facade.getPlayerIndex()){
					initAcceptOverlay();
				}	
			}
			if(model.gameManager.getTradeOffer() == null){
				if(getWaitOverlay().isModalShowing()){
					getWaitOverlay().closeModal();	
				}
			}
			else{
				init();
			}
		}
	}

	public void initAcceptOverlay(){
		tradeOffer = new int[5];
		tradeOffer = Facade.getTradeOffer();
		
//		System.out.println(tradeOffer[0]);
//		System.out.println(tradeOffer[1]);
//		System.out.println(tradeOffer[2]);
//		System.out.println(tradeOffer[3]);
//		System.out.println(tradeOffer[4]);
		
		if(model.gameManager.getTradeSender() != null){
			acceptOverlay.setPlayerName(Facade.getTradeSender().getName());
		}
		
		if(tradeOffer[0] > 0){getAcceptOverlay().addGetResource(ResourceType.WOOD, tradeOffer[0]);}
		else if (tradeOffer[0] < 0){getAcceptOverlay().addGiveResource(ResourceType.WOOD, -tradeOffer[0]);}
		
		if(tradeOffer[1] > 0){getAcceptOverlay().addGetResource(ResourceType.BRICK, tradeOffer[1]);}
		else if (tradeOffer[1] < 0){getAcceptOverlay().addGiveResource(ResourceType.BRICK, -tradeOffer[1]);}
		
		if(tradeOffer[2] > 0){getAcceptOverlay().addGetResource(ResourceType.SHEEP, tradeOffer[2]);}
		else if (tradeOffer[2] < 0){getAcceptOverlay().addGiveResource(ResourceType.SHEEP, -tradeOffer[2]);}
		
		if(tradeOffer[3] > 0){getAcceptOverlay().addGetResource(ResourceType.WHEAT, tradeOffer[3]);}
		else if (tradeOffer[3] < 0){getAcceptOverlay().addGiveResource(ResourceType.WHEAT, -tradeOffer[3]);}
		
		if(tradeOffer[4] > 0){getAcceptOverlay().addGetResource(ResourceType.ORE, tradeOffer[4]);}
		else if (tradeOffer[4] < 0){getAcceptOverlay().addGiveResource(ResourceType.ORE, -tradeOffer[4]);}
		
		getAcceptOverlay().setAcceptEnabled(Facade.canAcceptTrade(tradeOffer));
		getAcceptOverlay().showModal();
		return;
	}
	
	public void initTradeOverlay(){	
		GameInfo game = Facade.getGameInfo();
		List<PlayerInfo> players = game.getPlayers();
		int playerIndex = Facade.getPlayerIndex();
		List<PlayerInfo> newPlayers = new ArrayList<PlayerInfo>();
		//Go through list of players and remove yourself
		int count = 0;
		for(PlayerInfo p : players){
			if(p == null) { break; }
			if(p.getPlayerIndex() == playerIndex){
				continue;
			}
			else{
				newPlayers.add(p);
				count++;
			}

		}	
		//Make a list of players that you are able to trade with.
		PlayerInfo[] value;
		value = new PlayerInfo[count];
		for(int i = 0; i < value.length; i++){
			PlayerInfo nextPlayer = new PlayerInfo();
			nextPlayer.setId(newPlayers.get(i).getId());
			nextPlayer.setPlayerIndex(newPlayers.get(i).getPlayerIndex());
			nextPlayer.setName(newPlayers.get(i).getName());
			nextPlayer.setColor(newPlayers.get(i).getColor());
			value[i] = nextPlayer;
		}		
		tradeOverlay.setPlayers(value);
		tradeOverlay.reset();
		tradeOverlay.setTradeEnabled(false);
		tradeOverlay.setCancelEnabled(true);
		tradeOverlay.setStateMessage("select the resources you want to trade");
		tradeOverlay.setResourceSelectionEnabled(true);
		tradeOverlay.setPlayerSelectionEnabled(true);
		brickAmt = oreAmt = sheepAmt = wheatAmt = woodAmt = 0;
		brick = ore = sheep = wheat = wood = 0;
		sendBrickVal = sendOreVal = sendSheepVal = sendWheatVal = sendWoodVal = 0;
		recBrickVal = recOreVal = recSheepVal = recWheatVal = recWoodVal = 0;
		sendResource = sendBrick = sendOre = sendSheep = sendWheat = sendWood = false;
		recResource = recBrick = recOre = recSheep = recWheat = recWood = false;
		playerTrade = false;
		init();
	}
	
	public void checkTrade(){
		if(recBrickVal + recOreVal + recSheepVal + recWheatVal + recWoodVal > 0){
			recResource = true;
		}
		else{
			recResource = false;
		}
		if(sendBrickVal + sendOreVal + sendSheepVal + sendWheatVal + sendWoodVal > 0){
			sendResource = true;
		}
		else{
			sendResource = false;
		}
		if(playerTrade){
			if(recResource && sendResource){
				tradeOverlay.setTradeEnabled(true);
				tradeOverlay.setStateMessage("Trade!");	
			}
			else{
				tradeOverlay.setTradeEnabled(false);
				tradeOverlay.setStateMessage("select the resources you want to trade");	
			}
		}
		else{
			if(recResource && sendResource){
				tradeOverlay.setTradeEnabled(false);
				tradeOverlay.setStateMessage("choose whom you want to trade with");	
			}
			else{
				tradeOverlay.setTradeEnabled(false);
				tradeOverlay.setStateMessage("select the resources you want to trade");	
			}
		}
	}
}
