package client.devcards;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.model.Model;
import shared.model.developmentcard.DevCard;
import client.base.*;
import client.facade.Facade;


/**
 * "Dev card" controller implementation
 */
public class DevCardController extends Controller implements IDevCardController, Observer{

	private IBuyDevCardView buyCardView;
	private IAction soldierAction;
	private IAction roadAction;
	private Model model;
	private int sCount = 0;
	private int yopCount = 0;
	private int monopolyCount = 0;
	private int monumentCount = 0;
	private int roadCount = 0;
	
	/**
	 * DevCardController constructor
	 * 
	 * @param view "Play dev card" view
	 * @param buyCardView "Buy dev card" view
	 * @param soldierAction Action to be executed when the user plays a soldier card.  It calls "mapController.playSoldierCard()".
	 * @param roadAction Action to be executed when the user plays a road building card.  It calls "mapController.playRoadBuildingCard()".
	 */
	public DevCardController(IPlayDevCardView view, IBuyDevCardView buyCardView, 
								IAction soldierAction, IAction roadAction) {

		super(view);
		
		this.buyCardView = buyCardView;
		this.soldierAction = soldierAction;
		this.roadAction = roadAction;
		Facade.addListener(this);
	}

	public IPlayDevCardView getPlayCardView() {
		return (IPlayDevCardView)super.getView();
	}

	public IBuyDevCardView getBuyCardView() {
		return buyCardView;
	}

	@Override
	public void startBuyCard() {
		
		getBuyCardView().showModal();
	}

	@Override
	public void cancelBuyCard() {
		
		getBuyCardView().closeModal();
	}

	@Override
	public void buyCard() {
		
		if(Facade.canBuyDevCard()){
			Facade.buyDevCard();
		}
		if(getBuyCardView().isModalShowing()){
			getBuyCardView().closeModal();
		}
	}

	@Override
	public void startPlayCard() {
		init();
		
		getPlayCardView().setCardAmount(DevCardType.SOLDIER, sCount);
		getPlayCardView().setCardAmount(DevCardType.YEAR_OF_PLENTY, yopCount);
		getPlayCardView().setCardAmount(DevCardType.MONOPOLY, monopolyCount);
		getPlayCardView().setCardAmount(DevCardType.MONUMENT, monumentCount);
		getPlayCardView().setCardAmount(DevCardType.ROAD_BUILD, roadCount);
		
		getPlayCardView().setCardEnabled(DevCardType.SOLDIER, Facade.canUseSoldier());
		getPlayCardView().setCardEnabled(DevCardType.YEAR_OF_PLENTY, Facade.canUseYearOfPlenty());
		getPlayCardView().setCardEnabled(DevCardType.MONOPOLY, Facade.canUseMonopoly());
		getPlayCardView().setCardEnabled(DevCardType.MONUMENT, Facade.canUseMonument());
		if(Facade.getPlayerRoads() <= 1){
			getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, false);
		}
		else{
			getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, Facade.canUseRoadBuilder());
		}
		if(getPlayCardView().isModalShowing()){
			getPlayCardView().closeModal();
		}
		getPlayCardView().showModal();
		
	}

	@Override
	public void cancelPlayCard() {

		getPlayCardView().closeModal();
	}

	@Override
	public void playMonopolyCard(ResourceType resource) {
		if (Facade.canUseMonopoly()){
			Facade.playMonopoly(resource);
			monopolyCount--;
		}
	}

	@Override
	public void playMonumentCard() {
		if(Facade.canUseMonument()){
			Facade.playMonument();
			monumentCount--;
		}	
	}

	@Override
	public void playRoadBuildCard() {
		roadAction.execute();
		roadCount--;
	}

	@Override
	public void playSoldierCard() {
		soldierAction.execute();
		sCount--;
	}

	@Override
	public void playYearOfPlentyCard(ResourceType resource1, ResourceType resource2) {
		if(Facade.canUseYearOfPlenty()){
			Facade.playYearOfPlenty(resource1, resource2);
			yopCount--;
		}
	}
	
	public void init(){
		List<DevCard> devCards = Facade.getPlayerDevCards();
		
		sCount = 0;
		yopCount = 0;
		monopolyCount = 0;
		monumentCount = 0;
		roadCount = 0;
		
		if (devCards != null) {
			for(DevCard dc: devCards){
				if(DevCardType.SOLDIER == dc.getType()){
					//Enable Soldier card
					if(Facade.canUseSoldier())
						getPlayCardView().setCardEnabled(DevCardType.SOLDIER, true);
					else
						getPlayCardView().setCardEnabled(DevCardType.SOLDIER, false);
					sCount++;
				}
				if(DevCardType.YEAR_OF_PLENTY == dc.getType()){
					//Enable Year of plenty card
					if(Facade.canUseYearOfPlenty())
						getPlayCardView().setCardEnabled(DevCardType.YEAR_OF_PLENTY, true);
					else
						getPlayCardView().setCardEnabled(DevCardType.YEAR_OF_PLENTY, false);
					yopCount++;
				}
				if(DevCardType.MONOPOLY == dc.getType()){
					//Enable Monopoly Card
					if(Facade.canUseMonopoly())
						getPlayCardView().setCardEnabled(DevCardType.MONOPOLY, true);
					else
						getPlayCardView().setCardEnabled(DevCardType.MONOPOLY, false);
					monopolyCount++;
				}
				if(DevCardType.MONUMENT == dc.getType()){
					//Enable Monument card
					if(Facade.canUseMonument())
						getPlayCardView().setCardEnabled(DevCardType.MONUMENT, true);
					else
						getPlayCardView().setCardEnabled(DevCardType.MONUMENT, false);
					monumentCount++;
				}
				if(DevCardType.ROAD_BUILD == dc.getType()){
					//Enable Road Build
					if(Facade.canUseRoadBuilder())
						getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, true);
					else
						getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, false);
					roadCount++;
				}
			}
		}
		
		if(sCount == 0)
			getPlayCardView().setCardEnabled(DevCardType.SOLDIER, false);
		if(yopCount == 0)
			getPlayCardView().setCardEnabled(DevCardType.YEAR_OF_PLENTY, false);
		if(monopolyCount == 0)
			getPlayCardView().setCardEnabled(DevCardType.MONOPOLY, false);
		if(monumentCount == 0)
			getPlayCardView().setCardEnabled(DevCardType.MONUMENT, false);
		if(roadCount == 0)
			getPlayCardView().setCardEnabled(DevCardType.ROAD_BUILD, false);	
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Model)
		{
			model = (Model)arg;
			init();
		}
	}
}

