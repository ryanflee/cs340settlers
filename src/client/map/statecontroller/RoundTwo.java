package client.map.statecontroller;

import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.player.Player;
import client.data.RobPlayerInfo;
import client.facade.Facade;
import client.map.IMapView;
import client.poller.Poller;

/**
 * This class represents the Round Two state of the map when players are placing their second initial pieces. 
 * @author jjwilbur
 *
 */
public class RoundTwo extends MapState {

	public RoundTwo(IMapView view) {
		super(view);
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		return Facade.canBuildRoad(edgeLoc, true, false);
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		return Facade.canBuildSettlement(vertLoc, false);
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public void placeRoad(EdgeLocation edgeLoc) {
		if (Facade.canBuildRoad(edgeLoc, true, false)) {
			Facade.buildRoadFree(edgeLoc, true);
		}
	}

	@Override
	public void placeSettlement(VertexLocation vertLoc) {
		if (Facade.canBuildSettlement(vertLoc, false)) {
			Facade.buildSettlementFree(vertLoc, false);
		}
	}

	@Override
	public void placeCity(VertexLocation vertLoc) {
		// do nothing
	}

	@Override
	public void placeRobber(HexLocation hexLoc) {
		// do nothing
	}

	@Override
	public void startMove(PieceType pieceType, boolean isFree,
			boolean allowDisconnected) {
		
		Player thisPlayer = this.model.gameManager.getPlayers().get(Facade.getPlayerIndex());
		
		if ((pieceType == PieceType.ROAD) || (pieceType == PieceType.SETTLEMENT)) {
			this.view.startDrop(pieceType, thisPlayer.getColor(), false);
		}
		
		else {
			// can't do it!
		}
	}

	@Override
	public void cancelMove() {
		// probably can't do this either
	}

	@Override
	public void playSoldierCard() {
		// do nothing
		
	}

	@Override
	public void playRoadBuildingCard() {
		// do nothing
	}

	@Override
	public void robPlayer(RobPlayerInfo victim) {
		// do nothing
	}
	
	/**
	 * Function that redraws the map based off of the most recent model.
	 * @param newModel The model that is being passed in to redraw the map.
	 */
	public void initFromModel(Model newModel) {
		
		this.model = newModel;

		if (this.model.getGameStarted()) {
			int playerIndex = Facade.getPlayerIndex();
			
			if (playerIndex != newModel.gameManager.getCurrentPlayer()) {
				// waiting for other players
			}
			
			else {
				// your turn!
				int numSettlements = Facade.getPlayerSettlements();
				int numRoads = Facade.getPlayerRoads();
				
				// do road first and allow it to be disconnected
				if (numRoads > 13) {
					//System.out.println("Place road");
					this.startMove(PieceType.ROAD, true, true);
				}
				
				// then settlement will have to be connected to the new road
				else if (numSettlements > 3) {
					this.startMove(PieceType.SETTLEMENT, true, false);
				}
				
				// if player has placed two roads and two settlements, 
				// finish their turn for them
				else {
					System.out.println("Finishing turn automatically");
					Facade.finishTurn();
				}
			}
		}
	}

}
