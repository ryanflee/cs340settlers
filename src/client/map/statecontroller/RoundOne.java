package client.map.statecontroller;

import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.board.Hex;
import shared.model.player.Player;
import client.data.RobPlayerInfo;
import client.facade.Facade;
import client.map.IMapView;
import client.poller.Poller;

/**
 * This class represents the Round One state of the map when players are first placing their initial pieces.
 * @author jjwilbur
 *
 */
public class RoundOne extends MapState {

	public RoundOne(IMapView view) {
		super(view);
	}

	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		return Facade.canBuildRoad(edgeLoc,true,false);
	}

	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		return Facade.canBuildSettlement(vertLoc,false);
	}

	public boolean canPlaceCity(VertexLocation vertLoc) {
		return Facade.canBuildCity(vertLoc);
	}

	public boolean canPlaceRobber(HexLocation hexLoc) {
		return false;
	}

	public void placeRoad(EdgeLocation edgeLoc) {
		if (Facade.canBuildRoad(edgeLoc, true,false)) { 
			Facade.buildRoadFree(edgeLoc, true);
		}
	}

	public void placeSettlement(VertexLocation vertLoc) {
		if (Facade.canBuildSettlement(vertLoc, false)) {
			Facade.buildSettlementFree(vertLoc, false);
		}	
	}

	public void placeCity(VertexLocation vertLoc) {
		if (Facade.canBuildCity(vertLoc)) {
			Facade.buildCity(vertLoc);
		}
		
	}

	public void placeRobber(HexLocation hexLoc) {
		// do nothing
	}

	public void startMove(PieceType pieceType, boolean isFree,
			boolean allowDisconnected) {
		
		Player thisPlayer = this.model.gameManager.getPlayers().get(Facade.getPlayerIndex());
		
		if ((pieceType == PieceType.ROAD) || (pieceType == PieceType.SETTLEMENT)) {
			this.view.startDrop(pieceType, thisPlayer.getColor(), false);
		}
		
		else {
			// can't do it!
		}
	}

	public void cancelMove() {
		// probably can't do this either
	}

	public void playSoldierCard() {
		// do nothing
	}

	public void playRoadBuildingCard() {
		// do nothing
	}

	public void robPlayer(RobPlayerInfo victim) {
		// do nothing
	}
	
	/**
	 * Function that redraws the map based off of the most recent model.
	 * @param newModel The model that is being passed in to redraw the map.
	 */
	public void initFromModel(Model newModel) {

		this.model = newModel;
		
		if (this.model.getGameStarted()) {
			int playerIndex = Facade.getPlayerIndex();
			
			if (playerIndex != newModel.gameManager.getCurrentPlayer()) {
				// waiting for other players
			}
			
			else {
				// your turn!
				int numSettlements = Facade.getPlayerSettlements();
				int numRoads = Facade.getPlayerRoads();
				
				// do road first and allow it to be disconnected
				if (numRoads > 14) {
					this.startMove(PieceType.ROAD, true, true);
				}
				
				// then settlement will have to be connected to the new road
				else if (numSettlements > 4) {
					this.startMove(PieceType.SETTLEMENT, true, false);
				}
				
				// if player has placed a road and a settlement, 
				// finish their turn for them
				else {
					Facade.finishTurn();
				}
			}
		}
	}
}
