package client.map.statecontroller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.board.Hex;
import shared.model.pieces.Building;
import shared.model.player.Player;
import client.data.RobPlayerInfo;
import client.facade.Facade;
import client.map.IMapView;
import client.map.IRobView;

/**
 * This class represents the Robbing state of the map when players are placing the robber and stealing cards from other players.
 * @author jjwilbur
 *
 */
public class Robbing extends MapState{

	IRobView robView;
	HexLocation newRobberLoc;
	
	public Robbing(IMapView view) {
		super(view);
	}
	
	public Robbing(IMapView view, IRobView robView) {
		super(view);
		this.robView = robView;
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		return Facade.canPlaceRobber(hexLoc);
	}

	@Override
	public void placeRoad(EdgeLocation edgeLoc) {
		// do nothing
	}

	@Override
	public void placeSettlement(VertexLocation vertLoc) {
		// do nothing
		
	}

	@Override
	public void placeCity(VertexLocation vertLoc) {
		// do nothing
		
	}
	
	private Set<RobPlayerInfo> findOwnersToRob(List<Building> bldgs) {
		
		Set<RobPlayerInfo> candidates = new HashSet<RobPlayerInfo>();
		for (Building b : bldgs) {
			int ownerIndex = b.getOwner();
			if (ownerIndex != Facade.getPlayerIndex()) {
				Player p = model.getPlayers().get(ownerIndex);
				if (p.getHandSize() > 0) {
					RobPlayerInfo rpi = new RobPlayerInfo();
					rpi.setId(p.getId());
					rpi.setName(p.getName());
					rpi.setPlayerIndex(ownerIndex);
					rpi.setColor(p.getColor());
					rpi.setNumCards(p.getHandSize());
					
					candidates.add(rpi);
				}
			}
		}
		return candidates;
	}
	
	@Override
	public void placeRobber(HexLocation hexLoc) {
		if (Facade.canPlaceRobber(hexLoc)) {
			this.newRobberLoc = hexLoc;
			
			List<Building> bldgs = this.model.gameManager.getBoard().getAssociatedBuildings(hexLoc);
			Set<RobPlayerInfo> candidates = findOwnersToRob(bldgs);
			
			if (!candidates.isEmpty()) {
				robView.setPlayers(candidates.toArray(new RobPlayerInfo[candidates.size()]));
				robView.showModal();
			}
			
			else {
				robPlayer(null);
			}
		}
		
	}

	@Override
	public void startMove(PieceType pieceType, boolean isFree,
			boolean allowDisconnected) {
		
		if (pieceType == PieceType.ROBBER) {
			view.startDrop(pieceType, Facade.getPlayerInfo().getColor(), false);
		}
		
		else {
			// do nothing
		}
	}

	@Override
	public void cancelMove() {
		// do nothing
		
	}

	@Override
	public void playSoldierCard() {
		// do nothing
		
	}

	@Override
	public void playRoadBuildingCard() {
		// do nothing
		
	}

	@Override
	public void robPlayer(RobPlayerInfo victim) {
		
		int victimIndex = -1; 
		if (victim != null) {
			victimIndex = victim.getPlayerIndex();
		}
		
		Facade.robPlayer(victimIndex, this.newRobberLoc);
	}
	
	/**
	 * Function that redraws the map based off of the most recent model.
	 * @param newModel The model that is being passed in to redraw the map.
	 */
	public void initFromModel(Model newModel) {
		this.model = newModel;
		
		int playerIndex = Facade.getPlayerIndex();
		if (playerIndex != newModel.gameManager.getCurrentPlayer()) {
			// waiting for other players
		}
		else {
			startMove(PieceType.ROBBER, true, true);
		}
	}

}
