package client.map.statecontroller;

import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import client.data.RobPlayerInfo;
import client.map.IMapView;

public abstract class MapState {
	
	IMapView view;
	Model model;
	
	public MapState(IMapView view) {
		this.view = view;
	}
	
	public abstract void initFromModel(Model model);
	public abstract boolean canPlaceRoad(EdgeLocation edgeLoc);
	public abstract boolean canPlaceSettlement(VertexLocation vertLoc);
	public abstract boolean canPlaceCity(VertexLocation vertLoc);
	public abstract boolean canPlaceRobber(HexLocation hexLoc);
	public abstract void placeRoad(EdgeLocation edgeLoc);
	public abstract void placeSettlement(VertexLocation vertLoc);
	public abstract void placeCity(VertexLocation vertLoc);
	public abstract void placeRobber(HexLocation hexLoc);
	public abstract void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected);
	public abstract void cancelMove();
	public abstract void playSoldierCard();
	public abstract void playRoadBuildingCard();
	public abstract void robPlayer(RobPlayerInfo victim);
}
