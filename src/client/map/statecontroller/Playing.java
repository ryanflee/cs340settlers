package client.map.statecontroller;

import java.util.HashSet;
import java.util.Set;

import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.board.Hex;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.Building;
import shared.model.pieces.Road;
import shared.model.player.Player;
import client.data.RobPlayerInfo;
import client.facade.Facade;
import client.map.IMapView;
import client.map.IRobView;

/**
 * This class represents the Playing state of the map when players are going through and playing the game.
 * @author jjwilbur
 *
 */
public class Playing extends MapState{
	private boolean soldier = false, road = false;
	private HexLocation newRobberLoc = null;
	private EdgeLocation road1 = null;
	private Model model;
	IRobView robView;
	
	public Playing(IMapView view, IRobView irb) {
		super(view);
		this.robView = irb;
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		if(road) {
			if(road1 != null) return (edgeLoc != road1 && Facade.canBuildRoad(edgeLoc,false,true));
			return Facade.canBuildRoad(edgeLoc,false,true);
		}
		return Facade.canBuildRoad(edgeLoc,false,false);
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		return Facade.canBuildSettlement(vertLoc, false);
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc) {
		return Facade.canBuildCity(vertLoc);
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		return Facade.canPlaceRobber(hexLoc);
	}

	@Override
	public void placeRoad(EdgeLocation edgeLoc) {
		if(this.road){
			if(road1 == null) {
				road1 = edgeLoc;
				this.view.placeRoad(edgeLoc, Facade.getPlayerInfo().getColor());
				Road newRoad = new Road(edgeLoc, this.model.gameManager.getCurrentPlayer());
				try {
					this.model.getBoard().getHexByLocation(edgeLoc.getHexLoc()).addRoad(edgeLoc, newRoad);
				} catch (InvalidLocationException e) {
					e.printStackTrace();
				}
			}
			else {
				Facade.playRoadBuilder(road1, edgeLoc);
				road = false;
				road1 = null;				
			}
		}
		else
			Facade.buildRoad(edgeLoc, false);
	}

	@Override
	public void placeSettlement(VertexLocation vertLoc) {
		Facade.buildSettlement(vertLoc, false);		
	}

	@Override
	public void placeCity(VertexLocation vertLoc) {
		Facade.buildCity(vertLoc);
	}

	@Override
	public void placeRobber(HexLocation hexLoc) {
		if (Facade.canPlaceRobber(hexLoc)) {
			this.newRobberLoc = hexLoc;
			
			Set<RobPlayerInfo> candidates = new HashSet<RobPlayerInfo>();
			
			// check all six vertices for someone to rob
			VertexLocation northwest = new VertexLocation(hexLoc, VertexDirection.NorthWest).getNormalizedLocation();
			VertexLocation northeast = new VertexLocation(hexLoc, VertexDirection.NorthEast).getNormalizedLocation();
			VertexLocation east = new VertexLocation(hexLoc, VertexDirection.East).getNormalizedLocation();
			VertexLocation southeast = new VertexLocation(hexLoc, VertexDirection.SouthEast).getNormalizedLocation();
			VertexLocation southwest = new VertexLocation(hexLoc, VertexDirection.SouthWest).getNormalizedLocation();
			VertexLocation west = new VertexLocation(hexLoc, VertexDirection.West).getNormalizedLocation();
			
			candidates = findOwnerToRob(northwest, candidates);
			candidates = findOwnerToRob(northeast, candidates);
			candidates = findOwnerToRob(east, candidates);
			candidates = findOwnerToRob(southeast, candidates);
			candidates = findOwnerToRob(southwest, candidates);
			candidates = findOwnerToRob(west, candidates);
			
			if (!candidates.isEmpty()) {
				robView.setPlayers(candidates.toArray(new RobPlayerInfo[candidates.size()]));
				robView.showModal();
			}
			
			else {
				robPlayer(null);
			}
		}
	}

	private Set<RobPlayerInfo> findOwnerToRob(VertexLocation vloc, Set<RobPlayerInfo> candidates) {

		Hex hex = model.getBoard().getHexByLocation(vloc.getHexLoc());
		Building bldg = hex.getBuilding(vloc.getDir());
		if (bldg != null) {
			int ownerIndex = bldg.getOwner();
			if (ownerIndex != Facade.getPlayerIndex()) {
				Player p = model.getPlayers().get(ownerIndex);
				if (p.getHandSize() > 0) {
					RobPlayerInfo rpi = new RobPlayerInfo();
					rpi.setId(p.getId());
					rpi.setName(p.getName());
					rpi.setPlayerIndex(ownerIndex);
					rpi.setColor(p.getColor());
					rpi.setNumCards(p.getHandSize());

					candidates.add(rpi);
				}
			}
		}

		return candidates;
	}

	@Override
	public void startMove(PieceType pieceType, boolean isFree,
			boolean allowDisconnected) {
		
		view.startDrop(pieceType, Facade.getPlayerInfo().getColor(), true);
	}

	@Override
	public void cancelMove() {
		// undo cost of move?
	}

	@Override
	public void playSoldierCard() {
		view.startDrop(PieceType.ROBBER, Facade.getPlayerInfo().getColor(), false);
	}

	@Override
	public void playRoadBuildingCard() {
		road = true;
		view.startDrop(PieceType.ROAD, Facade.getPlayerInfo().getColor(), false);
		view.startDrop(PieceType.ROAD, Facade.getPlayerInfo().getColor(), false);
	}

	@Override
	public void robPlayer(RobPlayerInfo victim) {
		
		int victimIndex = -1; 
		if (victim != null) {
			victimIndex = victim.getPlayerIndex();
		}
		
		Facade.playSoldier(victimIndex, this.newRobberLoc);
	}
	
	/**
	 * Function that redraws the map based off of the most recent model.
	 * @param newModel The model that is being passed in to redraw the map.
	 */
	public void initFromModel(Model newModel) {
		this.model = newModel;
		System.out.println("Current player: " + this.model.gameManager.getCurrentPlayer());
	}

}
