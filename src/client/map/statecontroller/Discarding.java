package client.map.statecontroller;

import java.util.Random;

import shared.definitions.CatanColor;
import shared.definitions.HexType;
import shared.definitions.PieceType;
import shared.definitions.PortType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.Model;
import client.data.RobPlayerInfo;
import client.map.IMapView;

/**
 * This class represents the Discarding state of the map when players are discarding cards from their hand.
 * @author jjwilbur
 *
 */
public class Discarding extends MapState {

	public Discarding(IMapView view) {
		super(view);
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		return false;
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public void placeRoad(EdgeLocation edgeLoc) {
		//do nothing
	}

	@Override
	public void placeSettlement(VertexLocation vertLoc) {
		// do nothing
	}

	@Override
	public void placeCity(VertexLocation vertLoc) {
		//do nothing
	}

	@Override
	public void placeRobber(HexLocation hexLoc) {
		//do nothing
	}

	@Override
	public void startMove(PieceType pieceType, boolean isFree,
			boolean allowDisconnected) {
		//do nothing
	}

	@Override
	public void cancelMove() {
		//do nothing
	}

	@Override
	public void playSoldierCard() {
		//do nothing
	}

	@Override
	public void playRoadBuildingCard() {
		//do nothing
	}

	@Override
	public void robPlayer(RobPlayerInfo victim) {
		//do nothing
	}
	
	/**
	 * Function that redraws the map based off of the most recent model.
	 * @param newModel The model that is being passed in to redraw the map.
	 */
	public void initFromModel(Model newModel) {
		
	}

}
