package client.map;

import java.util.*;

import shared.definitions.*;
import shared.locations.*;
import shared.model.*;
import shared.model.board.*;
import shared.model.pieces.Building;
import shared.model.pieces.City;
import shared.model.pieces.Road;
import shared.model.pieces.Settlement;
import client.base.*;
import client.data.*;
import client.facade.Facade;
import client.map.statecontroller.*;

/**
 * Implementation for the map controller
 */
public class MapController extends Controller implements IMapController, Observer {
	
	private IRobView robView;
	private MapState mapState = null;
	private Map<String, MapState> states;
	private Model model;
	public String state = "";
	
	
	public MapController(IMapView view, IRobView robView) {
		super(view);
		setRobView(robView);
		
		states = new HashMap<String,MapState>();
		states.put("discarding", new Discarding(getView()));
		states.put("playing", new Playing(getView(), getRobView()));
		states.put("robbing", new Robbing(getView(), getRobView()));
		states.put("rolling", new Rolling(getView()));
		states.put("firstround", new RoundOne(getView()));
		states.put("secondround", new RoundTwo(getView()));
		
		Facade.addListener(this);
	}
	
	public IMapView getView() {
		
		return (IMapView)super.getView();
	}
	
	private IRobView getRobView() {
		return robView;
	}
	private void setRobView(IRobView robView) {
		this.robView = robView;
	}

	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		return mapState.canPlaceRoad(edgeLoc);
	}

	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		return mapState.canPlaceSettlement(vertLoc);
	}

	public boolean canPlaceCity(VertexLocation vertLoc) {
		return mapState.canPlaceCity(vertLoc);
	}

	public boolean canPlaceRobber(HexLocation hexLoc) {
		return mapState.canPlaceRobber(hexLoc);
	}

	public void placeRoad(EdgeLocation edgeLoc) {
		mapState.placeRoad(edgeLoc);
	}

	public void placeSettlement(VertexLocation vertLoc) {		
		mapState.placeSettlement(vertLoc);
	}

	public void placeCity(VertexLocation vertLoc) {		
		mapState.placeCity(vertLoc);
	}

	public void placeRobber(HexLocation hexLoc) {
		
		mapState.placeRobber(hexLoc);
		
		//getRobView().showModal();
	}
	
	public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected) {	
		
		mapState.startMove(pieceType, isFree, allowDisconnected);
		//getView().startDrop(pieceType, Facade.getPlayerInfo().getColor(), true);
	}
	
	public void cancelMove() {
		mapState.cancelMove();
	}
	
	public void playSoldierCard() {	
		mapState.playSoldierCard();
	}
	
	public void playRoadBuildingCard() {	
		mapState.playRoadBuildingCard();
	}
	
	public void robPlayer(RobPlayerInfo victim) {	
		mapState.robPlayer(victim);
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String newState) {
		if(!states.containsKey(newState.toLowerCase()))return;
		state = newState;
		mapState = states.get(state.toLowerCase());
	}

	@Override
	public void update(Observable o, Object arg) {
		assert arg != null;

		if (arg instanceof Model) {
			Model m = (Model)arg;
			String status = m.gameManager.getStatus();
			this.setState(status);
			if(mapState == null) { return; }
//			System.out.println(status);
			this.model = m;
			
			if (this.model.getGameStarted()) {
//				System.out.println("Game started");
				drawBoard(m.getBoard());
				mapState.initFromModel(m);
			}
			else {
//				System.out.println("Game has NOT started");
			}
		}
	}
	
	private void drawBoard(Board board) {
		
		// draw hexes, starting with ocean ones
		//getView().addHex(new HexLocation(0, -3), HexType.WATER);
		//getView().addHex(new HexLocation(0, 3), HexType.WATER);
		//getView().addHex(new HexLocation(1, -3), HexType.WATER);
		//getView().addHex(new HexLocation(1, 2), HexType.WATER);
		//getView().addHex(new HexLocation(2, -3), HexType.WATER);
		//getView().addHex(new HexLocation(2, 1), HexType.WATER);
		//getView().addHex(new HexLocation(-1, -2), HexType.WATER);
		//getView().addHex(new HexLocation(-1, 3), HexType.WATER);
		//getView().addHex(new HexLocation(-2, -1), HexType.WATER);
		//getView().addHex(new HexLocation(-2, 3), HexType.WATER);
		//getView().addHex(new HexLocation(3, 0), HexType.WATER);
		//getView().addHex(new HexLocation(3, -1), HexType.WATER);
		//getView().addHex(new HexLocation(3, -2), HexType.WATER);
		//getView().addHex(new HexLocation(3, -3), HexType.WATER);
		//getView().addHex(new HexLocation(-3, 0), HexType.WATER);
		//getView().addHex(new HexLocation(-3, 1), HexType.WATER);
		//getView().addHex(new HexLocation(-3, 2), HexType.WATER);
		//getView().addHex(new HexLocation(-3, 3), HexType.WATER);
		
		List<Hex> hexes = board.getAllHexes();
		for (Hex h : hexes) {
			
			HexLocation hloc = h.getHexLocation();
			
			getView().addHex(hloc, h.getHexType());
			int number = h.getNumberToken();
			if ((number > 1 && number < 7) || (number > 7 && number < 13)) {
				getView().addNumber(hloc, h.getNumberToken());
			}
			
			// draw buildings
			for (Building b : h.getAllBuildings()) {
				
				int ownerIndex = b.getOwner();
				CatanColor c = Facade.getModel().getPlayers().get(ownerIndex).getColor();
				
				if (b instanceof Settlement) {
					getView().placeSettlement(b.getLocation(), c);
				}
				
				else if (b instanceof City) {
					getView().placeCity(b.getLocation(), c);
				}
			}
			
			// draw roads
			for (Road r : h.getAllRoads()) {
				
				int ownerIndex = r.getOwner();
				CatanColor c = Facade.getModel().getPlayers().get(ownerIndex).getColor();
				
				getView().placeRoad(r.getLocation(), c);
			}
		}
		
		// place robber
		Hex robberHex = board.getRobberHex();
		getView().placeRobber(robberHex.getHexLocation());
		
		// draw ports
		Map<EdgeLocation, PortType> ports = board.getPorts();
		for (EdgeLocation eloc : ports.keySet()) {
			PortType ptype = ports.get(eloc);
			getView().addPort(eloc, ptype);
		}
	}
}

