package client.login;

import client.base.*;
import client.facade.Facade;
import client.misc.*;

import java.net.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;


/**
 * Implementation for the login controller
 */
public class LoginController extends Controller implements ILoginController, Observer {

	private IMessageView messageView;
	private IAction loginAction;
	
	/**
	 * LoginController constructor
	 * 
	 * @param view Login view
	 * @param messageView Message view (used to display error messages that occur during the login process)
	 */
	public LoginController(ILoginView view, IMessageView messageView) {

		super(view);
		this.messageView = messageView;
		Facade.addListener(this);
	}
	
	public ILoginView getLoginView() {
		
		return (ILoginView)super.getView();
	}
	
	public IMessageView getMessageView() {
		
		return messageView;
	}
	
	/**
	 * Sets the action to be executed when the user logs in
	 * 
	 * @param value The action to be executed when the user logs in
	 */
	public void setLoginAction(IAction value) {
		
		loginAction = value;
	}
	
	/**
	 * Returns the action to be executed when the user logs in
	 * 
	 * @return The action to be executed when the user logs in
	 */
	public IAction getLoginAction() {
		
		return loginAction;
	}

	@Override
	public void start() {
		
		getLoginView().showModal();
	}

	@Override
	public void signIn() {
		
		// TODO: log in user
		ILoginView lv = getLoginView();
		String user = lv.getLoginUsername();
		String pass = lv.getLoginPassword();
		boolean success = Facade.userLogin(user, pass);
		if(success) {
			// If log in succeeded
			if(getLoginView().isModalShowing())
				getLoginView().closeModal();
			loginAction.execute();
		} else {
			messageView.setTitle("Error!");
			messageView.setMessage("Sign in failed.");
			messageView.showModal();
		}


	}

	@Override
	public void register() {
		
		// TODO: register new user (which, if successful, also logs them in)
		ILoginView lv = getLoginView();
		String user = lv.getRegisterUsername();
		String pass1 = lv.getRegisterPassword();
		String pass2 = lv.getRegisterPasswordRepeat();
		if(!validateUsername(user) || !validatePassword(pass1)) {
			invalidRegisterInput();
			return;
		}
		if(!pass1.equals(pass2)) {
			invalidPasswordMatch();
			return;
		}
		
		boolean success = Facade.register(user, pass1);
		if(success) {
			if(getLoginView().isModalShowing())
				getLoginView().closeModal();
			loginAction.execute();
		} else {
			messageView.setTitle("Error!");
			messageView.setMessage("Register failed.");
			messageView.showModal();
		}
	}
	
	private void invalidRegisterInput() {
		messageView.setTitle("Warning!");
		messageView.setMessage("Invalid username or password.");
		messageView.showModal();
	}
	
	private void invalidPasswordMatch() {
		messageView.setTitle("Warning!");
		messageView.setMessage("Passwords don't match.");
		messageView.showModal();
	}
	
	
    private boolean validatePassword(String input)
    {
        final int MIN_PASS_LENGTH = 5;
        if (input.length() < MIN_PASS_LENGTH)
        {
            return false;
        }
        else
        {
            for (char c : input.toCharArray())
            {
                if (!Character.isLetterOrDigit(c)
                        && c != '_' && c != '-')
                {
                    return false;
                }
                if(!isValidChar((int) c)){ return false; }
            }
        }
        return true;
    }
    
    private boolean validateUsername(String username)
    {
        final int MIN_UNAME_LENGTH = 3;
        final int MAX_UNAME_LENGTH = 7;

        if (username.length() < MIN_UNAME_LENGTH
                || username.length() > MAX_UNAME_LENGTH)
        {
            return false;
        }
        else
        {
            for (char c : username.toCharArray())
            {
                if (!Character.isLetterOrDigit(c)
                        && c != '_' && c != '-')
                {
                    return false;
                }
                if(!isValidChar((int) c)){ return false; }
            }
        }

        return true;
    }
    
    private boolean isValidChar(int c) {
		if( !(c >= 65 && c <= 90) && !(c >= 97 && c <= 122) ){
			if(c != 45 && c != 95) {
				return false;
			}
		}
    	
    	return true;
    	
    }
	
	@Override
	public void update(Observable o, Object arg) {}
}

