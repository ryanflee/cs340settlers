package client.facade;

import shared.DataParser;
import shared.definitions.CatanColor;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.resourcecard.ResourceCard;
import shared.model.pieces.*;
import shared.model.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import client.data.GameInfo;
import client.data.PlayerInfo;
import client.proxy.Proxy;
import client.proxy.RealProxy;
import shared.model.board.Board;
import shared.model.board.Hex;
import shared.model.developmentcard.*;
import shared.model.logs.ChatEntry;
import shared.model.logs.HistoryEntry;

/**
 * Facade between the GUI and Proxy
 *
 */
public class Facade extends Observable implements Observer {
	private Model model;
    private Proxy proxy;
    private PlayerInfo playerInfo;
    private GameInfo gameInfo;
    
	private static Facade instance;
	
	private Facade(Observable updater) {
		this.model = new Model();
		this.proxy = new RealProxy();
		updater.addObserver(this);
	}
	
	private Facade(Proxy p) {
		this.proxy = p;
		this.proxy.getDataParser().addObserver(this);
		this.gameInfo = null;
		this.playerInfo = new PlayerInfo();
	}
	
	public static void init(Observable updater){
		if(instance == null){
			instance = new Facade(updater);
		}
		return;
	}
	
	public static void init(Proxy p) {
		if (instance == null) {
			instance = new Facade(p);
		}
	}
	
	public static void addListener(Observer o) {
		instance.addObserver(o);
	}
	
	private Model _getModel(){
		return this.model;
	}
	
	public static Model getModel() {
		return instance._getModel();
	}
	
	private void _setModel(Model m){
		this.model = m;
	}
	public static void setModel(Model m) {
		instance._setModel(m);
	}
	
	private GameInfo _getGameInfo() {
		return this.gameInfo;
	}
	public static GameInfo getGameInfo() {
		return instance._getGameInfo();
	}
	
	private void _setGameInfo(GameInfo gi) {
		this.gameInfo = gi;
	}
	public static void setGameInfo(GameInfo gi) {
		instance._setGameInfo(gi);
	}
	
	private void _setPlayerInfo(PlayerInfo pi) {
		this.playerInfo = pi;
	}
	
	public static void setPlayerInfo(PlayerInfo pi) {
		instance._setPlayerInfo(pi);
	}
	
	private PlayerInfo _getPlayerInfo() {
		return this.playerInfo;
	}
	
	public static PlayerInfo getPlayerInfo() {
		return instance._getPlayerInfo();
	}
	
    public static boolean isLoggedIn() {
    	return instance._isLoggedIn();
    }
    
    private boolean _isLoggedIn() {
    	if ( playerInfo.getPlayerIndex() == -1 ) { return false; }
    	else { return true; }
    }

	private int _getPID(){
		if ( playerInfo == null ) { return -1; }
		return playerInfo.getId();
	}
	
	public static int getPID(){
		return instance._getPID();
	}
	
	private int _getPlayerIndex(){
		if ( playerInfo == null ) { return -1; }
		return playerInfo.getPlayerIndex();
	}
	
	public static int getPlayerIndex(){
		return instance._getPlayerIndex();
	}
	
    //Update function
	@Override
	public void update(Observable o, Object arg) {
		assert arg != null;
		if (arg instanceof Model) {
			// set new model
			Model m = (Model)arg;
			this.model = m;
			int pid = _getPID();
			List<Player> players = this.model.gameManager.getPlayers();
			for (int i = 0; i < players.size(); i++) {
				if ((players.get(i) != null) && (players.get(i).getId() == pid)) {
					this.playerInfo.setPlayerIndex(i);
				}
			}
			// notify GUI that model has changed
			this.setChanged();
			this.notifyObservers(m);
		}
		
		else if (arg instanceof List<?>) {
			// we know it's a list...
			List<?> list = (List<?>)arg;
			if (!list.isEmpty()) {
				Object obj = list.get(0);
				
				if (obj instanceof GameInfo) {
					// it's a list of games!
					List<GameInfo> gameList = (List<GameInfo>)list;
					
					// pass gameList on to the GUI
					this.setChanged();
					this.notifyObservers(gameList);
				}
			}
		}
		
		else if (arg instanceof PlayerInfo) {
			this.playerInfo = (PlayerInfo) arg;
			this.setChanged();
			this.notifyObservers(this.playerInfo);
		}
	}
    
	//User functions
	
	private boolean _userLogin(String username, String password){
		boolean isValid = proxy.userLogin(username, password);
		return isValid;
	}
	
	public static boolean userLogin(String username, String password){
		return instance._userLogin(username, password);
	}
	
	private boolean _register(String username, String password){
		boolean isValid = proxy.register(username, password);
		return isValid;
	}
	
	public static boolean register(String username, String password){
		return instance._register(username, password);
	}
	
	//Games functions
	
	
	public static boolean getGames() {
		return instance._getGames();
	}
	
	private boolean _getGames() {
		boolean isValid = proxy.getGames();
		return isValid;
	}
	
	
	private boolean _createGame(boolean randTiles, boolean randNums, boolean randPorts, String gameName){
		boolean isValid = proxy.createGame(randTiles, randNums, randPorts, gameName);
		return isValid;
	}
	
	public static boolean createGame(boolean randTiles, boolean randNums, boolean randPorts, String gameName){
		return instance._createGame(randTiles, randNums, randPorts, gameName);
	}
	
	private boolean _joinGame(int gameID, CatanColor color){
		boolean isValid = proxy.joinGame(gameID, color);
//		if(isValid){
//			int id = model.getPlayerIndex(playerInfo.getId());
//		}
		return isValid;
	}
	
	public static boolean joinGame(int gameID, CatanColor color){
		return instance._joinGame(gameID, color);
	}
	
	private boolean _saveGame(int gameID, String fileName){
		boolean isValid = proxy.saveGame(gameID, fileName);
		return isValid;
	}
	
	public static boolean saveGame(int gameID, String fileName){
		return instance._saveGame(gameID, fileName);
	}
	
	private boolean _loadGame(String fileName){
		boolean isValid = proxy.loadGame(fileName);
		return isValid;
	}
	
	public static boolean loadGame(String fileName){
		return instance._loadGame(fileName);
	}
	//Game functions
	
	private boolean _resetGame(){
		boolean isValid = proxy.resetGame();
		return isValid;
	}
	
	public static boolean resetGame(){
		return instance._resetGame();
	}
	
	private boolean _postCommands(List<Object> commands){
		boolean isValid = proxy.postCommands(commands);
		return isValid;
	}
	
	public static boolean postCommands(List<Object> commands){
		return instance._postCommands(commands);
	}
	
	private boolean _getCommands(){
		boolean isValid = proxy.getCommands();
		return isValid;
	}
	
	public static boolean getCommands(){
		return instance._getCommands();
	}
	
	private boolean _addAI(String ai){
		boolean isValid = proxy.addAI(ai);
		return isValid;
	}
	
	public static boolean addAI(String ai){
		return instance._addAI(ai);
	}
	
	private boolean _getAI(){
		boolean isValid = proxy.getAI();
		return isValid;
	}
	
	public static boolean getAI(){
		return instance._getAI();
	}
	
	//Move functions
	
	private boolean _sendChat(String message){
		boolean isValid = (this._canSendChat() && (message != null) && !message.trim().isEmpty());
		if (isValid){
			isValid = proxy.sendChat(playerInfo.getPlayerIndex(),message);
		}
		return isValid;
	}
	
	public static boolean sendChat(String message){
		return instance._sendChat(message);
	}
	
	private boolean _rollNumber(int numRolled){
		boolean isValid = this.canRollNumber();
		if (isValid){
			isValid = proxy.rollNumber(playerInfo.getPlayerIndex(),numRolled);
		}
		return isValid;
	}
	
	public static boolean rollNumber(int numRolled){
		return instance._rollNumber(numRolled);
	}
	
	private boolean _robPlayer(int victimID, HexLocation hloc){
		boolean isValid = this.canPlaceRobber(hloc);
		if (isValid){
			isValid = proxy.robPlayer(playerInfo.getPlayerIndex(),victimID, hloc);
		}
		return isValid;
	}
	
	public static boolean robPlayer(int victimID, HexLocation hloc){
		return instance._robPlayer(victimID, hloc);
	}
	
	private boolean _finishTurn(){
		boolean isValid = this.canFinishTurn();
		if (isValid){
			isValid = proxy.finishTurn(playerInfo.getPlayerIndex());
		}
		return isValid;
	}
	
	public static boolean finishTurn(){
		return instance._finishTurn();
	}
	
    /**
     * Function that lets the player buy a development card.
     * @param player The player that wants to buy a development card.
     */
    private boolean _buyDevCard() {
    	boolean isValid = this.canBuyDevCard();
    	if (isValid) {
    		isValid = proxy.buyDevCard(playerInfo.getPlayerIndex());
    	}
    	return isValid;
	}
    
    public static boolean buyDevCard(){
    	return instance._buyDevCard();
    }
    
    /**
     * Function that lets the player play a Year Of Plenty development card.
     * @param player The player that is playing the card.
     * @param rc1 The first resource type they want.
     * @param rc2 The second resource type they want.
     */
    private boolean _playYearOfPlenty(ResourceType rc1, ResourceType rc2) {
    	boolean isValid = this.canUseYearOfPlenty();
    	if (isValid) {
    		isValid = proxy.playYearOfPlenty(playerInfo.getPlayerIndex(),rc1, rc2);
    	}
    	return isValid;
    }
    
    public static boolean playYearOfPlenty(ResourceType rc1, ResourceType rc2){
    	return instance._playYearOfPlenty(rc1, rc2);
    }
    /**
     * Function that lets the player play a Road Builder development card.
     * @param player The player that is playing the card.
     * @param el1 The first location they want to place a road.
     * @param el2 The second location they want to place a road.
     */
    private boolean _playRoadBuilder(EdgeLocation el1, EdgeLocation el2) {
    	boolean isValid = this.canUseRoadBuilder();
    	if (isValid) {
    		EdgeLocation edge1 = el1.getNormalizedLocation();
    		EdgeLocation edge2 = el2.getNormalizedLocation();
    		isValid = proxy.playRoadBuilding(playerInfo.getPlayerIndex(),edge1, edge2);
    	}
    	return isValid;
    }
    
    public static boolean playRoadBuilder(EdgeLocation el1, EdgeLocation el2){
    	return instance._playRoadBuilder(el1, el2);
    }
    
    /**
     * Function that lets the player play a Soldier development card.
     * @param player The player that is playing the card.
     * @param victim The person the player wants to steal from.
     * @param robber The location the playing is going to move the robber to.
     */
    private boolean _playSoldier(int victim, HexLocation robber) {
    	boolean isValid = this.canUseSoldier();
    	if (isValid) {
    		isValid = proxy.playKnight(playerInfo.getPlayerIndex(),victim, robber);
    	}
    	return isValid;
    }
    
    public static boolean playSoldier(int victim, HexLocation robber){
    	return instance._playSoldier(victim, robber);
    }
    
    /**
     * Function that lets the player play a Monopoly development card.
     * @param player The player that is playing the Monopoly card.
     * @param rt The resource type the player wants to monopolize.
     */
    private boolean _playMonopoly(ResourceType rt) {
    	boolean isValid = this.canUseMonopoly();
    	if (isValid) {
    		isValid = proxy.playMonopoly(playerInfo.getPlayerIndex(),rt);
    	}
    	return isValid;
    }
    
    public static boolean playMonopoly(ResourceType rt){
    	return instance._playMonopoly(rt);
    }
    
    /**
     * Function that lets the player play a Monument development card.
     * @param player The player that is playing the Monument card.
     */
    private boolean _playMonument() {
    	boolean isValid = this.canUseMonument();
    	if (isValid) {
    		isValid = proxy.playMonument(playerInfo.getPlayerIndex());
    	}
    	return isValid;
    }    
    
    public static boolean playMonument(){
    	return instance._playMonument();
    }
    /**
     * Function that lets the player place a road on a specified location.
     * @param player The player that is placing the road.
     * @param el The location they are placing the road.
     */
	private boolean _buildRoad(EdgeLocation el, boolean override) {
		EdgeLocation edge = el.getNormalizedLocation();
		boolean isValid = this.canBuildRoad(edge,override, false);
    	if (isValid) {
    		isValid = proxy.buildRoad(playerInfo.getPlayerIndex(),edge, false);
    	}
    	return isValid;
    }
	
	public static boolean buildRoad(EdgeLocation el, boolean override){
		return instance._buildRoad(el,override);
	}

    /**
     * Function that lets the player place a city on a specified location.
     * @param player The player that is placing the city.
     * @param vl The location they are placing the city.
     */
    private boolean _buildSettlement(VertexLocation vl, boolean override) {
    	VertexLocation vertex = vl.getNormalizedLocation();
    	boolean isValid = this.canBuildSettlement(vertex, override);
    	if (isValid) {
    		isValid = proxy.buildSettlement(playerInfo.getPlayerIndex(),vertex, false);
    	}
    	return isValid;
	}
    
    private boolean _buildSettlementFree(VertexLocation vl, boolean override) {
    	VertexLocation vertex = vl.getNormalizedLocation();
    	boolean isValid = this.canBuildSettlement(vertex, override);
    	if (isValid) {
    		isValid = proxy.buildSettlement(playerInfo.getPlayerIndex(),vertex, true);
    	}
    	return isValid;
	}
    
    public static boolean buildSettlementFree(VertexLocation vl, boolean override) {
    	return instance._buildSettlementFree(vl,override);
    }
    
    private boolean _buildRoadFree(EdgeLocation e, boolean override) {
    	EdgeLocation edge = e.getNormalizedLocation();
    	boolean isValid = this.canBuildRoad(edge, override, false);
    	if (isValid) {
    		isValid = proxy.buildRoad(playerInfo.getPlayerIndex(),edge, true);
    	}
    	return isValid;
	}
    
    public static boolean buildRoadFree(EdgeLocation vl, boolean override) {
    	return instance._buildRoadFree(vl,override);
    }
    
    public static boolean buildSettlement(VertexLocation vl, boolean override){
    	return instance._buildSettlement(vl, override);
    }
    
	/**
	 * Function that lets the player place a settlement on a specified location.
	 * @param player The player that is placing the settlement.
	 * @param vl The location they are placing the settlement.
	 */
    private boolean _buildCity(VertexLocation vl) {
    	VertexLocation vertex = vl.getNormalizedLocation();
    	boolean isValid = this.canBuildCity(vertex);
    	if (isValid) {
    		isValid = proxy.buildCity(playerInfo.getPlayerIndex(),vertex);
    	}
    	return isValid;
	}
    
    public static boolean buildCity(VertexLocation vl) {
    	return instance._buildCity(vl);
    }

    private boolean _offerTrade(int receiverID, Map<ResourceType, Integer> offer){
    	boolean isValid = this.canOfferTrade();
    	if (isValid){
    		isValid = proxy.offerTrade(playerInfo.getPlayerIndex(),receiverID, offer);
    	}
    	return isValid;
    }
    
    public static boolean offerTrade(int receiverID, Map<ResourceType, Integer> offer){
    	return instance._offerTrade(receiverID, offer);
    }
    
    private boolean _acceptTrade(int[] tradeOffer, boolean willAccept){
    	boolean isValid = false;
    	if (willAccept) { // if accepting a trade, make sure you have the cards to do it
    		isValid = canAcceptTrade(tradeOffer);
    		if (isValid){
    			isValid = proxy.acceptTrade(playerInfo.getPlayerIndex(),willAccept);
    		}
    	}
    	else { // if not accepting, no need to check for cards
    		isValid = proxy.acceptTrade(playerInfo.getPlayerIndex(), willAccept);
    	}
    	return isValid;
    }
    
    public static boolean acceptTrade(int[] tradeOffer, boolean willAccept){
    	return instance._acceptTrade(tradeOffer, willAccept);
    }
    
    private boolean _maritimeTrade(int ratio, ResourceType input, ResourceType output){
    	boolean isValid = this.canMaritimeTrade();
    	if (isValid){
    		isValid = proxy.maritimeTrade(playerInfo.getPlayerIndex(),ratio, input, output);
    	}
    	return isValid;
    }
    
    public static boolean maritimeTrade(int ratio, ResourceType input, ResourceType output){
    	return instance._maritimeTrade(ratio, input, output);
    }
    
    private boolean _discardCards(Map<ResourceType, Integer> toDiscard){
    	boolean isValid = instance.canDiscardCards();
    	if (isValid){
    		isValid = proxy.discardCards(playerInfo.getPlayerIndex(),toDiscard);
    	}
    	return isValid;
    }
    
    public static boolean discardCards(Map<ResourceType, Integer> toDiscard){
    	return instance._discardCards(toDiscard);
    }
    
    // Util functions
    
    private boolean _changeLogLevel(String newLevel){
    	boolean isValid = proxy.changeLogLevel(newLevel);
    	return isValid;
    }
    
    public static boolean changeLogLevel(String newLevel){
    	return instance._changeLogLevel(newLevel);
    }
    
    //update Bank
    /**
     * Function that returns the list of resource cards of the specified player.
     * @param player The player who's hand is desired.
     * @return Returns the list of resource cards the specific player has. 
     */
    private List<ResourceCard> _getPlayerResources() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	List<ResourceCard> result = currPlayer.getHand();
    	return result;
	}

    public static List<ResourceCard> getPlayerResources(){
    	return instance._getPlayerResources();
    }
    /**
     * Function that returns the number of roads a player has available to use.
     * @param player The player who's number of roads is desired.
     * @return Returns the number of roads the specific player has.
     */
    private int _getPlayerRoads() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int roads = currPlayer.getRoads();
    	return roads;
	}
    
    public static int getPlayerRoads(){
    	return instance._getPlayerRoads();
    }

    /**
     * Function that returns the number of settlements a player has available to use.
     * @param player The player who's number of settlements is desired.
     * @return Returns the number of settlements the specific player has.
     */
    private int _getPlayerSettlements() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int settlements = currPlayer.getSettlements();
    	return settlements;
    }

    public static int getPlayerSettlements(){
    	return instance._getPlayerSettlements();
    }
    /**
     * Function that returns the number of cities a player has available to use.
     * @param player The player who's number of cities is desired.
     * @return Returns the number of cities the specific player has.
     */
    private int _getPlayerCities() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int cities = currPlayer.getCities();
    	return cities;
    }

    public static int getPlayerCities(){
    	return instance._getPlayerCities();
    }
    
    /**
     * Function that returns the development cards that a player has in their hand.
     * @param player The player who's hand is being looked at.
     * @return Returns the list of development cards that the specific player has.
     */
    private List<DevCard> _getPlayerDevCards() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	if(playerInfo == null)return null;
    	if(playerInfo.getPlayerIndex() == -1)return null;
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	List<DevCard> devCards = currPlayer.getDevCards();
    	return devCards;
    }

    public static List<DevCard> getPlayerDevCards(){
    	return instance._getPlayerDevCards();
    }
    
    /**
     * Function that returns the amount of soldiers or knights that a player owns.
     * @param player The player who's hand is being looked at.
     * @return Returns the amount of soldiers/knights the specific player has.
     */
    private int _getPlayerKnights() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int knights = currPlayer.getSoldiers();
    	return knights;
    }

    public static int getPlayerKnights(){
    	return instance._getPlayerKnights();
    }
    /**
     * Function that returns the total amount of victory points that a player has including development cards.
     * @param player The player who's victory points are being returned.
     * @return Returns the total amount of victory points the specific player has.
     */
    private int _getPlayerVictoryPoints() {
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int victoryPoints = currPlayer.getTotalVictoryPoints();
    	return victoryPoints;
    }

    public static int getPlayerVictoryPoints(){
    	return instance._getPlayerVictoryPoints();
    }
    /**
     * Function that returns the amount of victory points that other players can see.
     * @param player The player who's victory points are being returned.
     * @return Returns the amount of victory points that other players can see.
     */
    private int _getPlayerPoints() {
    	if(model == null)return 0;
    	ArrayList<Player> players = model.gameManager.getPlayers();
    	if (playerInfo.getPlayerIndex() == -1 || players.size() < 4) {
    		return 0;
    	}
    	Player currPlayer = players.get(playerInfo.getPlayerIndex());
    	int points = currPlayer.getVictoryPoints();
    	return points;
    }
    
    public static int getPlayerPoints(){
    	return instance._getPlayerPoints();
    }
    //Window visibility
    
    /**
     * Function that returns whether or not the Dice Window should be showing or not.
     * @return Returns true or false depending on if it is the rolling phase of the turn.
     */
    private boolean _showDiceWindow() {
    	String status = model.gameManager.getStatus();
    	if (status.toLowerCase().equals("rolling")){
    		return true;
    	}
    	return false;
    }
    
    public static boolean showDiceWindow(){
    	return instance._showDiceWindow();
    }
    
    /**
     * Function that returns whether or not the Trade Window should be showing or not.
     * @return Returns true or false depending on if it is the playing phase of the turn or not.
     */
    private boolean _showTradeWindow() {
    	String status = model.gameManager.getStatus();
    	if (status.toLowerCase().equals("playing")){
    		return true;
    	}
    	return false;
    }
    
    public static boolean showTradeWindow(){
    	return instance._showTradeWindow();
    }
    
    /**
     * Function that returns whether or not the Building Window should be showing or not.
     * @return Returns true or false depending on if it is the playing phase of the turn or not.
     */
    private boolean _showBuildingMode() {
    	String status = model.gameManager.getStatus();
    	if (status.toLowerCase().equals("playing")){
    		return true;
    	}
    	return false;
	}

    public static boolean showBuildingMode(){
    	return instance._showBuildingMode();
    }
    /**
     * Function that simulates rolling two 6 sided dice and returns what the values total to.
     * @return Returns two dice values added together.
     */
    private int _getDiceResult() {
    	Random rand1 = new Random();
    	Random rand2 = new Random();
    	int dice1 = rand1.nextInt(6) + 1;
    	int dice2 = rand2.nextInt(6) + 1;
    	int diceSum = dice1 + dice2;
    	return diceSum;
	}
    
    public static int getDiceResult(){
    	return instance._getDiceResult();
    }

    //update logs
    private List<HistoryEntry> _getGameHistory() {
    	List<HistoryEntry> gameHistory = model.getHistory();
    	return gameHistory;
    }
    
    public static List<HistoryEntry> getGameHistory(){
    	return instance._getGameHistory();
    }

    private List<ChatEntry> _getComments() {
    	List<ChatEntry> comments = model.getComments();
    	return comments;
    }
    
    public static List<ChatEntry> getComments(){
    	return instance._getComments();
    }
    
    //Can-Do Methods
    private boolean _canDiscardCards() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canDiscardCards(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canDiscardCards(){
    	return instance._canDiscardCards();
    }
    
    private boolean _canRollNumber() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canRollNumber(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canRollNumber(){
    	return instance._canRollNumber();
    }
    
    private boolean _canBuildRoad(EdgeLocation el, boolean override, boolean buildRoadCard) {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildRoad(playerInfo.getPlayerIndex(),el, override, buildRoadCard);
    	return check;
    }
    
    public static boolean canBuildRoad(EdgeLocation el, boolean override, boolean buildRoadCard){
    	return instance._canBuildRoad(el,override,buildRoadCard);
    }
    
    private boolean _canBuildRoad() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildRoad(playerInfo.getPlayerIndex(),false);
    	return check;
    }
    
    public static boolean canBuildRoad(){
    	return instance._canBuildRoad();
    }
    
    private boolean _canBuildSettlement(VertexLocation vl, boolean override) {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildSettlement(playerInfo.getPlayerIndex(),vl,override);
    	return check;	
    }
    
    public static boolean canBuildSettlement(VertexLocation vl, boolean override){
    	return instance._canBuildSettlement(vl,override);
    }
    
    private boolean _canBuildSettlement() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildSettlement(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canBuildSettlement(){
    	return instance._canBuildSettlement();
    }
    
    private boolean _canBuildCity(VertexLocation vl) {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildCity(playerInfo.getPlayerIndex(),vl);
    	return check;
    }
    
    public static boolean canBuildCity(VertexLocation vl){
    	return instance._canBuildCity(vl);
    }
    
    private boolean _canBuildCity() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canBuildCity(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canBuildCity(){
    	return instance._canBuildCity();
    }
    
    private boolean _canOfferTrade() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canOfferTrade(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canOfferTrade(){
    	return instance._canOfferTrade();
    }
    
    private boolean _canMaritimeTrade() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canMaritimeTrade(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canMaritimeTrade(){
    	return instance._canMaritimeTrade();
    }
    
    private boolean _canFinishTurn() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canFinishTurn(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canFinishTurn(){
    	return instance._canFinishTurn();
    }
    
    private boolean _canBuyDevCard() {
    	if (model.getWinner() != -1) return false;
    	if(playerInfo==null)return false;
    	boolean check = model.gameManager.canBuyDevCard(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canBuyDevCard(){
    	return instance._canBuyDevCard();
    }
    
    private boolean _canUseYearOfPlenty() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canUseYearOfPlenty(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canUseYearOfPlenty(){
    	return instance._canUseYearOfPlenty();
    }
    
    private boolean _canUseRoadBuilder() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canUseRoadBuilder(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canUseRoadBuilder(){
    	return instance._canUseRoadBuilder();
    }
    
    private boolean _canUseSoldier() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canUseSoldier(playerInfo.getPlayerIndex());
    	return check;
	} 
    
    public static boolean canUseSoldier(){
    	return instance._canUseSoldier();
    }
    
    private boolean _canUseMonopoly() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canUseMonopoly(playerInfo.getPlayerIndex());
    	return check;
	} 
    
    public static boolean canUseMonopoly(){
    	return instance._canUseMonopoly();
    }
    
    private boolean _canUseMonument() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canUseMonument(playerInfo.getPlayerIndex());
    	return check;
    }
    
    public static boolean canUseMonument(){
    	return instance._canUseMonument();
    }
    
    private boolean _canPlaceRobber(HexLocation hloc) {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canPlaceRobber(playerInfo.getPlayerIndex(),hloc);
    	return check;
    } 
    
    public static boolean canPlaceRobber(HexLocation hloc){
    	return instance._canPlaceRobber(hloc);
    }
    
    private boolean _canSendChat() {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canSendChat(playerInfo.getPlayerIndex());
    	return check;
    } 
    
    public static boolean canSendChat(){
    	return instance._canSendChat();
    }
    private boolean _canAcceptTrade(int[] tradeOffer) {
    	if (model.getWinner() != -1) return false;
    	boolean check = model.gameManager.canAcceptTrade(playerInfo.getPlayerIndex(),tradeOffer);
    	return check;
    }
    
    public static boolean canAcceptTrade(int[] tradeOffer){
    	return instance._canAcceptTrade(tradeOffer);
    }

    public static int maritimeTradeValue(ResourceType type) {
    	return instance._maritimeTradeValue(type);
    }

    private int _maritimeTradeValue(ResourceType type) {
    	return model.gameManager.maritimeTradeValue(playerInfo.getPlayerIndex(), type);
    }
    
    public static int resourceLeft(ResourceType type) {
    	return instance._resourceLeft(type);
    }
    
    private int _resourceLeft(ResourceType type) {
    	return model.gameManager.resourceLeft(type);
    }
    

    public static PlayerInfo getTradeReceiver(){
    	return instance._getTradeReceiver();
    }
    
    private PlayerInfo _getTradeReceiver(){
    	return model.gameManager.getTradeReceiver();
    }
    
    public static int[] getTradeOffer(){
    	return instance._getTradeOffer();
    }
    
    private int[] _getTradeOffer(){
    	return model.gameManager.getTradeOffer();
    }
    
    public static PlayerInfo getTradeSender(){
    	return instance._getTradeSender();
    }
    
    private PlayerInfo _getTradeSender(){
    	return model.gameManager.getTradeSender();
    }

    public static void gameOver() { instance._gameOver(); }
    
    private void _gameOver() { proxy.setGameOver(true); }
    
	public static void finishGame() { instance._finishGame(); }

	private void _finishGame() { proxy.finishGame(); }
    
    /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Facade other = (Facade) obj;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (proxy == null) {
			if (other.proxy != null)
				return false;
		} else if (!proxy.equals(other.proxy))
			return false;
		return true;
	}

	public void _close() {
		proxy.logout();
	}
	
	public static void close() {
		instance._close();
	}
}
