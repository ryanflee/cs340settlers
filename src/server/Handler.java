package server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.*;

import server.data.IDatabase;
import server.data.lazy.Game;
import server.data.lazy.User;
import server.encoding.GameBuilder;
import server.encoding.RequestDecoder;
import server.encoding.UserBuilder;
import server.exceptions.FactoryException;
import server.facade.*;
import server.factory.*;
import server.command.ICommand;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import server.persistence.IPersistPlugin;

public class Handler implements HttpHandler{
	private Logger logger = Logger.getLogger("catan.server");
	
	private XStream xmlStream = new XStream(new DomDriver());
	
	private ArrayList<String> loggedInUsers;
	
	private UserCommandFactory ucf;
	private GameCommandFactory gcf;
	private GamesCommandFactory gscf;
	private MovesCommandFactory mcf;
	private IFacade facade;
	private IPersistPlugin plugin;
	private int commandsBetweenCheckpoints;
	
	private SwaggerHandlers.BasicFile swaggerBasic;
	private SwaggerHandlers.JSONAppender swaggerData;

	public Handler(IFacade f, IPersistPlugin plugin, int commandsBetweenCheckpoints) {
		ucf = new UserCommandFactory();
		gcf = new GameCommandFactory();
		gscf = new GamesCommandFactory();
		mcf = new MovesCommandFactory();
		facade = f;
		this.loggedInUsers = new ArrayList<String>();
		swaggerBasic = new SwaggerHandlers.BasicFile("");
		swaggerData = new SwaggerHandlers.JSONAppender("");

		this.plugin = plugin;
		this.commandsBetweenCheckpoints = commandsBetweenCheckpoints;
		loadUsers();
		loadGames();

	}


	@Override
	public void handle(HttpExchange exchange) throws IOException {
		System.out.println(exchange.getRequestURI());
		
		InputStream fis = exchange.getRequestBody();
		Scanner scanner = new java.util.Scanner(fis,"UTF-8").useDelimiter("\\A");
        String param = scanner.hasNext() ? scanner.next() : "";
		String[] commands = exchange.getRequestURI().toString().replaceFirst("^/", "").split("/",3);

		ICommand cmd = null;
		System.out.println("REQUEST:" + commands[0] + "/" + commands[1]);
		String request = commands[0].toLowerCase(), command = commands[1].toLowerCase();
		int gameId = -1;
		if(command.equals("logout")) {
			String cookies = (URLDecoder.decode(exchange.getRequestHeaders().get("Cookie").get(0),"UTF-8"));
			String[] s = cookies.split("=|;");
			this.loggedInUsers.remove(s[1]);
			exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, "Success".length());
			exchange.getResponseBody().write("Success".getBytes());
			exchange.getResponseBody().close();
			return;
		}
		if(request.equals("moves") || request.equals("game") || command.equals("join")) {
			String cookies = (URLDecoder.decode(exchange.getRequestHeaders().get("Cookie").get(0),"UTF-8"));
			String[] s = cookies.split("=|;");
			for(String scook : s) {
				System.out.println("Cookie: " + scook);
			}
			int ind = 0;
			while(ind < s.length - 1 && !s[ind].trim().equalsIgnoreCase("catan.user")) {
				ind++;
			}
			System.out.println("First Index: " + ind);
			if(ind == s.length - 1) {
				exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Cookie".length());
				exchange.getResponseBody().write("Invalid Cookie".getBytes());
				return;
			}
			JsonElement jelly = new JsonParser().parse(s[ind+1]);

			JsonObject job = jelly.getAsJsonObject();
			String username, password;
			try {
				username = job.get("name").getAsString();
				password = job.get("password").getAsString();
			}
			catch(Exception e) {
				exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Cookie".length());
				exchange.getResponseBody().write("Invalid Cookie".getBytes());
				return;
			}
			int id = facade.validate(username,password);
			if(id == -1) {
				exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Credentials".length());
				exchange.getResponseBody().write("Invalid Credentials".getBytes());
				return;
			}
			if(command.equals("join")) {
				param = param.substring(0,param.length()-1) + ",\"username\":" + username + ",\"pid\":" + id + "}";
			}
			if(request.equals("moves") || request.equals("game")) {
				if(s.length < 4) {
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Cookie".length());
					exchange.getResponseBody().write("Invalid Cookie".getBytes());
					return;
				}
				else {
					ind = 0;
					while(ind < s.length && !s[ind].trim().equals("catan.game")) {
						ind++;
					}
					System.out.println("Second Index: " + ind);
					if(ind == s.length - 1) {
						exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Invalid Cookie".length());
						exchange.getResponseBody().write("Invalid Cookie".getBytes());
						return;
					}
					gameId = Integer.parseInt(s[ind+1]);
				}
			}
		}
		
		try {
			if(request.equals("docs")) {
				if (commands[2].startsWith("view")) {
					swaggerBasic.handle(exchange);
				}
				else if (commands[2].startsWith("data")) {
					swaggerData.handle(exchange);
				}
				return;
			}
			
			cmd = getICommand(request, commands[1].toLowerCase(), param, gameId);
			String response = cmd.execute();
			
			// PERSISTENT STORAGE
			if (command.equalsIgnoreCase("join") || command.equalsIgnoreCase("addai")) {
				if(command.equalsIgnoreCase("join"))
					gameId = Integer.parseInt(response);
				plugin.startTransaction();
				plugin.getGameDAO().update(gameId, facade.getModel(gameId));
				plugin.endTransaction(true);
			}
			else if (command.equalsIgnoreCase("register")) {
				JsonParser jp = new JsonParser();
				JsonElement jelem = jp.parse(response);
				JsonObject respObj = jelem.getAsJsonObject();
				String username = respObj.get("name").getAsString();
				String password = respObj.get("password").getAsString();
				
				plugin.startTransaction();
				plugin.getUserDAO().register(username, password);
				plugin.endTransaction(true);
			}
			else if (command.equalsIgnoreCase("create")) {
				JsonParser jp = new JsonParser();
				JsonElement jelem = jp.parse(response);
				JsonObject respObj = jelem.getAsJsonObject();
				int gId = respObj.get("id").getAsInt();
				
				plugin.startTransaction();
				Game g = facade.getDB().getGamesDAO().getGameById(gId);
				System.out.println("Game #" + gId + ": " + g.getName());
				plugin.getGameDAO().create(g.getName(), facade.getModel(gId));
				plugin.endTransaction(true);
			}
			else if (request.equalsIgnoreCase("moves")) {
				
				plugin.startTransaction();
				plugin.saveCommand(request + "/" + command, param, gameId);
				if (plugin.getCommandDAO().enumerateCommands(gameId) >= commandsBetweenCheckpoints) {
					plugin.getGameDAO().update(gameId, facade.getModel(gameId));
					plugin.getCommandDAO().deleteAll(gameId);
				}
				plugin.endTransaction(true);
			}
			// DONE THANKS
			
			if(commands[0].toLowerCase().equals("user")) {
				if(this.loggedInUsers.contains(response)) {
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "User already logged in".length());
					exchange.getResponseBody().write("User already logged in".getBytes());
					return;
				}
				exchange.getResponseHeaders().set("Set-Cookie", "catan.user=" + URLEncoder.encode(response, "UTF-8") + ";Path=/;");
				this.loggedInUsers.add(response);
				response = "Success";
			}
			if (commands[1].toLowerCase().equals("addai")) {
				if (!response.toLowerCase().equals("success")) {
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length());
				}
				else {
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length());
				}
				exchange.getResponseBody().write(response.getBytes());
				return;
			}
			if(commands[1].toLowerCase().equals("join")) {

				exchange.getResponseHeaders().set("Set-Cookie", "catan.game=" + response + ";Path=/;");
				response = "Success";
			}
//			System.out.println("Sending response: " + response);

			if(response.equals("Success")) {
				exchange.getResponseHeaders().set("Content-Type", "text/html");
			} else {
				exchange.getResponseHeaders().set("Content-Type", "application/json");
			}
			exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length());
			exchange.getResponseBody().write(response.getBytes());
			
			exchange.getResponseBody().close();
			return;
		}
		catch(Exception e) {
			exchange.getResponseHeaders().set("Content-Type", "application/json");
			exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Failure".length());
			exchange.getResponseBody().write("Failure".getBytes());
		}
		finally {
			exchange.getResponseBody().close();
		}
		
	}
	
	private ICommand getICommand(String request, String command, String param, int gameId) throws Exception {
		ICommand cmd = null;
		switch(request){
		case "user": cmd = ucf.buildCommand(facade, command.toLowerCase(), param, gameId);break;
		case "game": cmd = gcf.buildCommand(facade,command.toLowerCase(), param, gameId);break;
		case "games":cmd = gscf.buildCommand(facade,command.toLowerCase(), param, gameId);break;
		case "moves":cmd = mcf.buildCommand(facade,command.toLowerCase(), param, gameId);break;
		default: throw new Exception("Invalid arguments");
		}
		return cmd;
	}

	private void loadUsers() {
		UserBuilder ub = new UserBuilder();
		IDatabase db = facade.getDB();
		try {
			plugin.startTransaction();
			List<String> userInfos = plugin.getUserDAO().getUsers();
			plugin.endTransaction(true);
			List<User> users = ub.buildUsers(userInfos);
			if (users != null) {
				for(User u : users) {
					db.getUserDAO().addUser(u);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Handler Error: Error loading saved users");
		}
	}

	private void loadGames() {
		GameBuilder gb = new GameBuilder();
		IDatabase db = facade.getDB();
		try {
			plugin.startTransaction();
			List<String> gameInfos = plugin.getGameDAO().getGames();
			plugin.endTransaction(true);
			List<Game> games = gb.buildGames(gameInfos);
			if (games != null) {
				for(Game g : games) {
					System.out.println(g.getGameId());
					db.getGamesDAO().addNewGame(g);
					int id = g.getGameId();
					plugin.startTransaction();
					List<String> commands = plugin.getCommandDAO().getCommands(id);
					plugin.endTransaction(true);
					loadCommands(commands, id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Handler Error: Error loading saved games");
		}

	}

	private void loadCommands(List<String> cmdStrings, int gameId) {
		for(String s : cmdStrings) {
			String[] cmdParts = s.split("\\^_\\^");
			if(cmdParts.length == 2) {
				String type = cmdParts[0];
				String[] types = type.replaceFirst("^/", "").split("/",3);
				String params = cmdParts[1];
				try {
					ICommand cmd = getICommand(types[0], types[1], params, gameId);
					cmd.execute();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Handler Error: Error loading saved games");
				}

			}
		}
	}
}
