package server.persistence;

/**
 * Interface that all plugins feed from.  The interface itself functions as a 
 * factory to create the Database access objects it will need access to
 * @author jjwilbur
 *
 */

public interface IPersistPlugin {

	public void clear();
	public void startTransaction() throws Exception;
	public void endTransaction(boolean commit);
	public IGameDAO getGameDAO();
	public IUserDAO getUserDAO();
	public IGiveUp getCommandDAO();
	public void saveCommand(String commandType, String commandParams, int gameId) throws Exception;
	
}
