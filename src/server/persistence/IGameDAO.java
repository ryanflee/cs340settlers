package server.persistence;

import java.util.List;

import server.data.lazy.Game;

/**
 * Data Access Object for persistent storage of games.
 * @author Ryan F. Lee
 *
 */
public interface IGameDAO {

	/**
	 * Creates a new game in the persistent storage.
	 * @param id The unique ID of the game to create.
	 * @param newGame A string representing the state of the new game.
	 * @return {@code true} if the creation succeeds, {@code false} otherwise.
	 * @throws Exception 
	 */

	public int create(String name, String game) throws Exception;
	
	/**
	 * Retrieves a game from storage by its ID.
	 * @param id The unique ID of the game to be retrieved.
	 * @return A string representing the game state retrieved from storage.
	 * @throws Exception 
	 */
	public String getGameById(int id) throws Exception;
	
	/**
	 * Retrieves all stored games from the persistent storage.
	 * @return A list of strings representing game states retrieved from storage.
	 * @throws Exception 
	 */
	public List<String> getGames() throws Exception;
	
	/**
	 * Updates the specified game in the persistent storage. This operation will overwrite the existing data.
	 * @param id The unique ID of the game to update.
	 * @param updatedGame A string representing the updated state of the game.
	 * @return {@code true} if the update succeeds, {@code false} otherwise.
	 * @throws Exception 
	 */
	public boolean update(int id, String updatedGame) throws Exception;
	
	/**
	 * Deletes the specified game from the persistent storage.
	 * @param id The unique ID of the game to delete.
	 * @return {@code true} if the deletion succeeds, {@code false} otherwise.
	 * @throws Exception 
	 */
	public boolean delete(int id) throws Exception;
	
}
