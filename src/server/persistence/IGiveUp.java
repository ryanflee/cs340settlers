package server.persistence;

import java.util.List;

import server.command.ICommand;

public interface IGiveUp {

	/**
	 * Adds a command object to persistent storage
	 * @param type - the path of the command (i.e. /moves/robplayer)
	 * @param params - the json params of the command (String)
	 * @param gameId - the game's id (int)
     * @return true or false based on whether the creation was successful
	 * @throws Exception 
     */
	public boolean create(String type, String params, int gameId) throws Exception;

	/**
	 * Gets all of the command objects
	 * @return String representation of the command object with path concatenated to json params
	 * @throws Exception 
     */
	public List<String> getCommands(int gameId) throws Exception;

	/**
	 * Removes all commands from persistent storage
	 * @return true or false based on whether the deletion was successful
	 * @throws Exception 
     */
	public boolean deleteAll(int gameId) throws Exception;
	
	/**
	 * Counts how many commands are stored for a given game.
	 * @param gameId The unique id of the game to count commands for.
	 * @return The number of stored commands for the game.
	 * @throws Exception 
	 */
	public int enumerateCommands(int gameId) throws Exception;
}
