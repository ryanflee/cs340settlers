package server.persistence;

import java.util.List;

import server.data.lazy.Game;
import server.data.lazy.User;

public interface IUserDAO {

	/**
	 * Used to register a given username and password
	 * @param username - String 
	 * @param password - String
	 * @return An integer ID for the new player, or -1 if the operation fails.
	 * @throws Exception
	 */
	public int register(String username, String password) throws Exception;
	
	/**
	 * Gets all the users currently stored
	 * @return A list of strings with user names
	 * @throws Exception 
	 */
	public List<String> getUsers() throws Exception;
	
	/**
	 * @throws Exception 
	 * 
	 */
	public boolean delete(int id) throws Exception;
	
}
