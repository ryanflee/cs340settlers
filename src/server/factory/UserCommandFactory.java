package server.factory;

import server.command.ICommand;
import server.command.user.*;
import server.encoding.RequestDecoder;
import server.encoding.params.LoginParams;
import server.exceptions.FactoryException;
import server.facade.IFacade;

/**
 * Factory that generates and returns Command objects for all User action commands
 * @author Jake Mingus
 *
 */
public class UserCommandFactory implements ICommandFactory{
	
	
	@Override
	public ICommand buildCommand(IFacade facade, String commandType, String jsonParams, int gameID) throws FactoryException {
		switch(commandType) {
		case "login":return new Login(facade, new RequestDecoder().userLogin(jsonParams));
		case "register":return new Register(facade, new RequestDecoder().register(jsonParams));
		}
		return null;
	}

}
