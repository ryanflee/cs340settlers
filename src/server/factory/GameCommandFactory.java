package server.factory;

import server.command.ICommand;
import server.command.game.*;
import server.encoding.RequestDecoder;
import server.encoding.params.AddAIParams;
import server.exceptions.FactoryException;
import server.facade.IFacade;

/**
 * Factory that generates and returns Command objects for all Game action commands
 * @author Jake Mingus
 *
 */
public class GameCommandFactory implements ICommandFactory {

	private RequestDecoder decoder;
	
	public GameCommandFactory() {
		this.decoder = new RequestDecoder();
	}
	
	@Override
	public ICommand buildCommand(IFacade facade, String commandType, String jsonParams, int gameID) throws FactoryException {
		switch (commandType.toLowerCase()) {
		case "model":
			return new ModelCommand(facade, gameID);
		case "listai":
			return new ListAI(facade);
		case "addai":
			AddAIParams addAIParams = decoder.addAI(jsonParams);
			return new AddAI(facade, addAIParams, gameID);
		default:
			throw new FactoryException("Unknown command: " + commandType);
		}
	}

}
