package server.factory;

import server.command.ICommand;
import server.exceptions.FactoryException;
import server.facade.IFacade;

public interface ICommandFactory {

	/**
	 * Builds a Command object of the specified type and returns it.
	 * @param commandType The type of command to build.
	 * @param jsonParams json encoding of command object parameters
	 * @return The Command object corresponding to {@code commandType}.
	 * @throws FactoryException If {@code commandType} is invalid.
	 */
	public ICommand buildCommand(IFacade facade, String commandType, String jsonParams, int gameID) throws FactoryException;
}
