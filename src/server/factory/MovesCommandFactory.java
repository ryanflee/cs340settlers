package server.factory;

import server.command.ICommand;
import server.command.moves.*;
import server.encoding.RequestDecoder;
import server.encoding.params.*;
import server.exceptions.FactoryException;
import server.facade.IFacade;

/**
 * Factory that generates and returns Command objects for all Move action commands
 * @author Jake Mingus
 *
 */
public class MovesCommandFactory implements ICommandFactory {

	private RequestDecoder decoder;
	
	public MovesCommandFactory() {
		this.decoder = new RequestDecoder();
	}
	
	@Override
	public ICommand buildCommand(IFacade facade, String commandType, String jsonParams, int gameID) throws FactoryException {
		switch (commandType) {
		case "buydevcard":
			BuyDevCardParams devCardParams = decoder.buyDevCard(jsonParams);
			return new BuyDevCard(facade, devCardParams, gameID);
		case "year_of_plenty":
			YearOfPlentyParams yopParams = decoder.playYearOfPlenty(jsonParams);
			return new YearOfPlenty(facade, yopParams, gameID);
		case "soldier":
			SoldierParams soldierParams = decoder.playKnight(jsonParams);
			return new Soldier(facade, soldierParams, gameID);
		case "monopoly":
			MonopolyParams monopolyParams = decoder.playMonopoly(jsonParams);
			return new Monopoly(facade, monopolyParams, gameID);
		case "monument":
			MonumentParams monumentParams = decoder.playMonument(jsonParams);
			return new Monument(facade, monumentParams, gameID);
		case "road_building":
			RoadBuildingParams roadBuildingParams = decoder.playRoadBuilding(jsonParams);
			return new RoadBuilding(facade, roadBuildingParams, gameID);
		case "buildroad":
			BuildRoadParams buildRoadParams = decoder.buildRoad(jsonParams);
			return new BuildRoad(facade, buildRoadParams, gameID);
		case "buildsettlement":
			BuildSettlementParams settlementParams = decoder.buildSettlement(jsonParams);
			return new BuildSettlement(facade, settlementParams, gameID);
		case "buildcity":
			BuildCityParams cityParams = decoder.buildCity(jsonParams);
			return new BuildCity(facade, cityParams, gameID);
		case "offertrade":
			OfferTradeParams offerParams = decoder.offerTrade(jsonParams);
			return new OfferTrade(facade, offerParams, gameID);
		case "accepttrade":
			AcceptTradeParams acceptParams = decoder.acceptTrade(jsonParams);
			return new AcceptTrade(facade, acceptParams, gameID);
		case "maritimetrade":
			MaritimeTradeParams maritimeParams = decoder.maritimeTrade(jsonParams);
			return new MaritimeTrade(facade, maritimeParams, gameID);
		case "discardcards":
			DiscardCardsParams discardParams = decoder.discardCards(jsonParams);
			return new DiscardCards(facade, discardParams, gameID);
		case "sendchat":
			SendChatParams chatParams = decoder.sendChat(jsonParams);
			return new SendChat(facade, chatParams, gameID);
		case "finishturn":
			FinishTurnParams finishParams = decoder.finishTurn(jsonParams);
			return new FinishTurn(facade, finishParams, gameID);
		case "rollnumber":
			RollNumberParams rollParams = decoder.rollNumber(jsonParams);
			return new RollNumber(facade, rollParams, gameID);
		case "robplayer":
			RobPlayerParams robParams = decoder.robPlayer(jsonParams);
			return new RobPlayer(facade, robParams, gameID);
		}
		return null;
	}

}
