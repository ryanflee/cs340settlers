package server.factory;

import server.command.ICommand;
import server.command.games.Create;
import server.command.games.Join;
import server.command.games.List;
import server.encoding.RequestDecoder;
import server.encoding.params.JoinParams;
import server.exceptions.FactoryException;
import server.facade.IFacade;

/**
 * Factory that generates and returns Command objects for all Games action commands
 * @author Jake Mingus
 */
public class GamesCommandFactory implements ICommandFactory {

	@Override
	public ICommand buildCommand(IFacade facade, String commandType, String jsonParams, int gameID) throws FactoryException {
		switch(commandType) {
		case "list":return new List(facade);
		case "create":return new Create(facade, new RequestDecoder().createGame(jsonParams));
		case "join":JoinParams param = new RequestDecoder().joinGame(jsonParams);return new Join(facade, param);
		}
		return null;
	}

}
