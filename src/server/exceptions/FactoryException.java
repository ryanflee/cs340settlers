package server.exceptions;

public class FactoryException extends Exception {

	public FactoryException() {
		// TODO Auto-generated constructor stub
	}

	public FactoryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FactoryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public FactoryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FactoryException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
