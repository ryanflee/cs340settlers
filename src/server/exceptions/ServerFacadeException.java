package server.exceptions;

public class ServerFacadeException extends Exception {

	public ServerFacadeException() {
		// TODO Auto-generated constructor stub
	}

	public ServerFacadeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServerFacadeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ServerFacadeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServerFacadeException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
