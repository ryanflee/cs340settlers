package server.encoding.params;

public class CreateParams {
	private String name;
	private boolean randomTiles, randomNumbers, randomPorts;
	
	public CreateParams(String name, boolean rt, boolean rn, boolean rp) {
		this.name = name;
		this.randomTiles = rt;
		this.randomNumbers = rn;
		this.randomPorts = rp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isRandomTiles() {
		return randomTiles;
	}

	public void setRandomTiles(boolean randomTiles) {
		this.randomTiles = randomTiles;
	}

	public boolean isRandomNumbers() {
		return randomNumbers;
	}

	public void setRandomNumbers(boolean randomNumbers) {
		this.randomNumbers = randomNumbers;
	}

	public boolean isRandomPorts() {
		return randomPorts;
	}

	public void setRandomPorts(boolean randomPorts) {
		this.randomPorts = randomPorts;
	}
}
