package server.encoding.params;

import shared.definitions.ResourceType;

public class YearOfPlentyParams {

	int playerIndex;
	ResourceType resource1;
	ResourceType resource2;
	
	public YearOfPlentyParams(){
		this.playerIndex = -1;
		this.resource1 = null;
		this.resource2 = null;
	}
	
	public YearOfPlentyParams(int playerIndex, ResourceType resource1, ResourceType resource2){
		this.playerIndex = playerIndex;
		this.resource1 = resource1;
		this.resource2 = resource2;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public ResourceType getResource1() {
		return resource1;
	}

	public void setResource1(ResourceType resource1) {
		this.resource1 = resource1;
	}

	public ResourceType getResource2() {
		return resource2;
	}

	public void setResource2(ResourceType resource2) {
		this.resource2 = resource2;
	}
}
