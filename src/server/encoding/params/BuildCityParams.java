package server.encoding.params;

import shared.locations.VertexLocation;

public class BuildCityParams {

	private int playerIndex;
	private VertexLocation vertex;
	
	public BuildCityParams() {
		this.playerIndex = -1;
		this.vertex = null;
	}
	
	public BuildCityParams(int playerIndex, VertexLocation vertex) {
		this.playerIndex = playerIndex;
		this.vertex = vertex;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public VertexLocation getVertex() {
		return vertex;
	}

	public void setVertex(VertexLocation vertex) {
		this.vertex = vertex;
	}
}
