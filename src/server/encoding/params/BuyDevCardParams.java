package server.encoding.params;

public class BuyDevCardParams {
	
	int playerIndex;
	
	public BuyDevCardParams(){
		this.playerIndex = -1;
	}
	
	public BuyDevCardParams(int playerIndex){
		this.playerIndex = playerIndex;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
}
