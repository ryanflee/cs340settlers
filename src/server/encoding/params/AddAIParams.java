package server.encoding.params;

/**
 * Parameters for the AddAI command.
 * @author Ryan F. Lee
 *
 */
public class AddAIParams {

	String aiType;
	
	public AddAIParams() {
		this.aiType = null;
	}
	
	public AddAIParams(String aiType) {
		this.aiType = aiType;
	}

	/**
	 * @return the aiType
	 */
	public String getAiType() {
		return aiType;
	}

	/**
	 * @param aiType the aiType to set
	 */
	public void setAiType(String aiType) {
		this.aiType = aiType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aiType == null) ? 0 : aiType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddAIParams other = (AddAIParams) obj;
		if (aiType == null) {
			if (other.aiType != null)
				return false;
		} else if (!aiType.equals(other.aiType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AddAIParams [aiType=" + aiType + "]";
	}

	
}
