package server.encoding.params;

public class DiscardCardsParams {
	
	private int playerIndex;
	private int[] toDiscard;
	
	public DiscardCardsParams() {
		this.playerIndex = -1;
		this.toDiscard = null;
	}
	
	public DiscardCardsParams(int playerIndex, int[] toDiscard) {
		this.playerIndex = playerIndex;
		this.toDiscard = toDiscard;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int[] getToDiscard() {
		return toDiscard;
	}

	public void setToDiscard(int[] toDiscard) {
		this.toDiscard = toDiscard;
	}
	
}
