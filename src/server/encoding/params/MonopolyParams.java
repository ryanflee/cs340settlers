package server.encoding.params;

public class MonopolyParams {

	int playerIndex;
	String resource;
	
	public MonopolyParams(){
		this.playerIndex = -1;
		this.resource = "";
	}
	
	public MonopolyParams(int playerIndex, String resource){
		this.playerIndex = playerIndex;
		this.resource = resource;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}
}
