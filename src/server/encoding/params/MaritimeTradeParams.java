package server.encoding.params;

public class MaritimeTradeParams {

	private int playerIndex;
	private int ratio;
	private String input;
	private String output;
	
	public MaritimeTradeParams() {
		this.playerIndex = -1;
		this.ratio = 0;
		this.input = null;
		this.output = null;
	}
	
	public MaritimeTradeParams(int playerIndex, int ratio, String input, String output) {
		this.playerIndex = playerIndex;
		this.ratio = ratio;
		this.input = input;
		this.output = output;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}
	
}
