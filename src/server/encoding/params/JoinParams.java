package server.encoding.params;

import shared.definitions.CatanColor;

public class JoinParams {
	private int gameId, pid;
	private String name;
	private CatanColor color;
	private boolean hasCookie;
	
	public JoinParams(int gid, String color, String name, int pid) {
		this.gameId = gid;
		this.color = CatanColor.getColor(color.toLowerCase());
		this.pid = pid;
		this.name = name;
		this.hasCookie = hasCookie;
	}

	public boolean hasCookie() {
		return hasCookie;
	}

	public void setHasCookie(boolean hasCookie) {
		this.hasCookie = hasCookie;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public CatanColor getColor() {
		return color;
	}

	public void setColor(CatanColor color) {
		this.color = color;
	}
}
