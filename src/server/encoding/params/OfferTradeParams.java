package server.encoding.params;

public class OfferTradeParams {

	private int playerIndex;
	private int[] resourceList;
	private int receiverIndex;
	
	public OfferTradeParams() {
		this.playerIndex = -1;
		this.resourceList = null;
		this.receiverIndex = -1;
	}
	
	public OfferTradeParams(int playerIndex, int[] resourceList, int receiverIndex) {
		this.playerIndex = playerIndex;
		this.resourceList = resourceList;
		this.receiverIndex = receiverIndex;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int[] getResourceList() {
		return resourceList;
	}

	public void setResourceList(int[] resourceList) {
		this.resourceList = resourceList;
	}

	public int getReceiverIndex() {
		return receiverIndex;
	}

	public void setReceiverIndex(int receiverIndex) {
		this.receiverIndex = receiverIndex;
	}
	
}
