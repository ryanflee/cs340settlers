package server.encoding.params;

public class LoginParams {
	private String username, password;

	public LoginParams() {}

	public LoginParams(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
}
