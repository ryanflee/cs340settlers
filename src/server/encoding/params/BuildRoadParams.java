package server.encoding.params;

import shared.locations.EdgeLocation;

public class BuildRoadParams {

	int playerIndex;
	EdgeLocation roadLocation;
	boolean free;
	
	public BuildRoadParams(){
		this.playerIndex = -1;
		this.roadLocation = null;
		this.free = false;
	}
	
	public BuildRoadParams(int playerIndex, EdgeLocation roadLocation, boolean free){
		this.playerIndex = playerIndex;
		this.roadLocation = roadLocation;
		this.free = free;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public EdgeLocation getRoadLocation() {
		return roadLocation;
	}

	public void setRoadLocation(EdgeLocation roadLocation) {
		this.roadLocation = roadLocation;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}
}
