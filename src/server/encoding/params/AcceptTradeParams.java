package server.encoding.params;

public class AcceptTradeParams {

	private int playerIndex;
	private boolean willAccept;
	
	public AcceptTradeParams() {
		this.playerIndex = -1;
		this.willAccept = false;
	}
	
	public AcceptTradeParams(int playerIndex, boolean willAccept) {
		this.playerIndex = playerIndex;
		this.willAccept = willAccept;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public boolean isWillAccept() {
		return willAccept;
	}

	public void setWillAccept(boolean willAccept) {
		this.willAccept = willAccept;
	}
	
	
}
