package server.encoding.params;

/**
 * Parameters for the RollNumber command. 
 * @author Ryan F. Lee
 *
 */
public class RollNumberParams {

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	int playerIndex;
	int number;
	
	public RollNumberParams() {
		playerIndex = -1;
		number = -1;
	}
	
	public RollNumberParams(int playerIndex, int number) {
		this.playerIndex = playerIndex;
		this.number = number;
	}

}
