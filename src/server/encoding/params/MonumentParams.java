package server.encoding.params;

public class MonumentParams {

	int playerIndex;
	
	public MonumentParams(){
		this.playerIndex = -1;
	}
	
	public MonumentParams(int playerIndex){
		this.playerIndex = playerIndex;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
}
