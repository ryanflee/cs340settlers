package server.encoding.params;

import shared.locations.VertexLocation;

public class BuildSettlementParams {
	
	private int playerIndex;
	private VertexLocation vertex;
	private boolean free;
	
	public BuildSettlementParams() {
		this.playerIndex = -1;
		this.vertex = null;
		this.free = false;
	}
	
	public BuildSettlementParams(int playerIndex, VertexLocation vertex, boolean free) {
		this.playerIndex = playerIndex;
		this.vertex = vertex;
		this.free = free;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public VertexLocation getVertex() {
		return vertex;
	}

	public void setVertex(VertexLocation vertex) {
		this.vertex = vertex;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}
}
