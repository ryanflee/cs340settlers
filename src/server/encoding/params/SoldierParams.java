package server.encoding.params;

import shared.locations.HexLocation;

public class SoldierParams {

	int playerIndex;
	int victimIndex;
	HexLocation location;
	
	public SoldierParams(){
		this.playerIndex = -1;
		this.victimIndex = -1;
		this.location = null;
	}
	
	public SoldierParams(int playerIndex, int victimIndex, HexLocation location){
		this.playerIndex = playerIndex;
		this.victimIndex = victimIndex;
		this.location = location;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getVictimIndex() {
		return victimIndex;
	}

	public void setVictimIndex(int victimIndex) {
		this.victimIndex = victimIndex;
	}

	public HexLocation getLocation() {
		return location;
	}

	public void setLocation(HexLocation location) {
		this.location = location;
	}
}
