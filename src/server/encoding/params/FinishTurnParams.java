package server.encoding.params;

/**
 * Parameters for the FinishTurn command.
 * @author Ryan F. Lee
 *
 */
public class FinishTurnParams {

	int playerIndex;
	
	public FinishTurnParams() {
		this.playerIndex = -1;
	}
	
	public FinishTurnParams(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	/**
	 * @return the playerIndex
	 */
	public int getPlayerIndex() {
		return playerIndex;
	}

	/**
	 * @param playerIndex the playerIndex to set
	 */
	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + playerIndex;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinishTurnParams other = (FinishTurnParams) obj;
		if (playerIndex != other.playerIndex)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FinishTurnParams [playerIndex=" + playerIndex + "]";
	}
	
	

}
