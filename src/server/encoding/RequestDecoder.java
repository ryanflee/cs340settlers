package server.encoding;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import server.encoding.params.*;
import server.exceptions.FactoryException;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;

import com.google.gson.stream.JsonWriter;
import com.google.gson.*;

/**
 * Decodes json request to Java Object
 * @author Jake Mingus
 *
 */
public class RequestDecoder {

	private JsonParser jp;

	public RequestDecoder() {
		jp = new JsonParser();
	}

	//User Operations
	public LoginParams userLogin(String json) {
		JsonElement jelly = jp.parse(json);
		
		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String username = job.get("username").getAsString(),
				   password = job.get("password").getAsString();

			LoginParams result = new LoginParams(username, password);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	public RegisterParams register(String json){
		JsonElement jelly = jp.parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String username = job.get("username").getAsString(),
				   password = job.get("password").getAsString();

			RegisterParams result = new RegisterParams(username, password);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	//Games Operations
	public String getGames() {return null;}

	/**
	 * Decodes a games/Create request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the games.Create command.
	 */
	/*{
  "randomTiles": "boolean",
  "randomNumbers": "boolean",
  "randomPorts": "boolean",
  "name": "string"
}*/
	public CreateParams createGame(String json){
		JsonElement jelly = jp.parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String name = job.get("name").getAsString();
			boolean rTiles = job.get("randomTiles").getAsBoolean(),
					rNumbers = job.get("randomNumbers").getAsBoolean(),
					rPorts = job.get("randomPorts").getAsBoolean();

			CreateParams result = new CreateParams(name,rTiles, rNumbers, rPorts);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a games/Join request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the games.Join command.
	 */
	public JoinParams joinGame(String json){
		JsonElement jelly = jp.parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String color = job.get("color").getAsString(), username = job.get("username").getAsString();
			int gameId = job.get("id").getAsInt(), pid = job.get("pid").getAsInt();

			JoinParams result = new JoinParams(gameId,color,username, pid);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a games/Save request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the games.Save command.
	 */
	public String saveGame() {
		return null;
	}

	/**
	 * Decodes a games/Load request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the games.Load	 command.
	 */
	public String loadGame(){
		return null;
	}


	//In-game Operations
	public String postCommands() {
		return null;
	}

	/**
	 * Decodes an AddAI request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the AddAI command.
	 */
	public AddAIParams addAI(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();
			
			String aiType = job.get("AIType").getAsString();
			
			AddAIParams result = new AddAIParams(aiType);
			return result;
		}
		
		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}


	//Moves
	/**
	 * Decodes a SendChat request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the SendChat command.
	 * @throws FactoryException 
	 */
	public SendChatParams sendChat(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			String content = job.get("content").getAsString();

			SendChatParams result = new SendChatParams(playerIndex, content);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a RollNumber request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the RollNumber command.
	 */
	public RollNumberParams rollNumber(String jsonParams) {

		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			int number = job.get("number").getAsInt();

			RollNumberParams result = new RollNumberParams(playerIndex, number);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a RobPlayer request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the RobPlayer command.
	 */
	public RobPlayerParams robPlayer(String jsonParams) {

		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			int victimIndex = job.get("victimIndex").getAsInt();

			JsonObject jsonHexLoc = job.get("location").getAsJsonObject();
			int x = jsonHexLoc.get("x").getAsInt();
			int y = jsonHexLoc.get("y").getAsInt();
			HexLocation hexLoc = new HexLocation(x, y);

			RobPlayerParams result = new RobPlayerParams(playerIndex, victimIndex, hexLoc);
			return result;
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a FinishTurn request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the FinishTurn command.
	 */
	public FinishTurnParams finishTurn(String jsonParams) {
		
		JsonElement jelly = jp.parse(jsonParams);
		
		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			
			FinishTurnParams result = new FinishTurnParams(playerIndex);
			return result;
		}
		
		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a BuyDevCard request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the BuyDevCard command.
	 */
	public BuyDevCardParams buyDevCard(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			
			BuyDevCardParams result = new BuyDevCardParams(playerIndex);
			return result;
		}
		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Year_Of_Plenty request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Year_Of_Plenty command.
	 */
	public YearOfPlentyParams playYearOfPlenty(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			String r1 = job.get("resource1").getAsString();
			String r2 = job.get("resource2").getAsString();
			ResourceType resource1 = getResourceType(r1);
			ResourceType resource2 = getResourceType(r2);
			
			YearOfPlentyParams result = new YearOfPlentyParams(playerIndex, resource1, resource2);
			return result;
		}
		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Road_Building request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Road_Building command.
	 */
	public RoadBuildingParams playRoadBuilding(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			JsonObject firstRoad = job.get("spot1").getAsJsonObject();
			JsonObject secondRoad = job.get("spot2").getAsJsonObject();
			int x = firstRoad.get("x").getAsInt();
			int y = firstRoad.get("y").getAsInt();
			String dir1 = firstRoad.get("direction").getAsString();
			EdgeDirection eDir1 = getEdgeDirection(dir1);
			EdgeLocation spot1 = new EdgeLocation(new HexLocation(x, y), eDir1);
			x = secondRoad.get("x").getAsInt();
			y = secondRoad.get("y").getAsInt();
			String dir2 = secondRoad.get("direction").getAsString();
			EdgeDirection eDir2 = getEdgeDirection(dir2);
			EdgeLocation spot2 = new EdgeLocation(new HexLocation(x, y), eDir2);
			
			RoadBuildingParams result = new RoadBuildingParams(playerIndex, spot1, spot2);
			return result;
		}
		else{
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Soldier request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Soldier command.
	 */
	public SoldierParams playKnight(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			int victimIndex = job.get("victimIndex").getAsInt();
			JsonObject loc = job.get("location").getAsJsonObject();
			int x = loc.get("x").getAsInt();
			int y = loc.get("y").getAsInt();
			HexLocation location = new HexLocation(x, y);
			
			SoldierParams result = new SoldierParams(playerIndex, victimIndex, location);
			return result;
		}
		else{
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Monopoly request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Monopoly command.
	 */
	public MonopolyParams playMonopoly(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
//TODO Should this be a string or a ResourceType ***********************************
			String resource = job.get("resource").getAsString();
			
			MonopolyParams result = new MonopolyParams(playerIndex, resource);
			return result;
		}
		else{
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Monument request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Monument command.
	 */
	public MonumentParams playMonument(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			
			MonumentParams result = new MonumentParams(playerIndex);
			return result;
		}
		else{
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a BuildRoad request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the BuildRoad command.
	 */
	public BuildRoadParams buildRoad(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);
		
		if(jelly.isJsonObject()){
			JsonObject job = jelly.getAsJsonObject();
			
			int playerIndex = job.get("playerIndex").getAsInt();
			JsonObject loc = job.get("roadLocation").getAsJsonObject();
			int x = loc.get("x").getAsInt();
			int y = loc.get("y").getAsInt();
			String dir = loc.get("direction").getAsString();
			EdgeDirection eDir = getEdgeDirection(dir);
			EdgeLocation roadLocation = new EdgeLocation(new HexLocation(x, y), eDir);
			boolean free = job.get("free").getAsBoolean();
			
			BuildRoadParams result = new BuildRoadParams(playerIndex, roadLocation, free);
			return result;
		}
		else{
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a BuildSettlement request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the BuildSettlement command.
	 */
	public BuildSettlementParams buildSettlement(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			JsonObject vert = job.get("vertexLocation").getAsJsonObject();
			int x = vert.get("x").getAsInt();
			int y = vert.get("y").getAsInt();
			String dir = vert.get("direction").getAsString();
			VertexDirection vDir = getVertexDirection(dir);
			VertexLocation vertex = new VertexLocation(new HexLocation(x,y), vDir);

			boolean free = job.get("free").getAsBoolean();

			return new BuildSettlementParams(playerIndex, vertex, free);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a BuildCity request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the BuildCity command.
	 */
	public BuildCityParams buildCity(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			JsonObject vert = job.get("vertexLocation").getAsJsonObject();
			int x = vert.get("x").getAsInt();
			int y = vert.get("y").getAsInt();
			String dir = vert.get("direction").getAsString();
			VertexDirection vDir = getVertexDirection(dir);

			VertexLocation vertex = new VertexLocation(new HexLocation(x,y), vDir);

			return new BuildCityParams(playerIndex, vertex);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a OfferTrade request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the OfferTrade command.
	 */
	public OfferTradeParams offerTrade(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			int receiver = job.get("receiver").getAsInt();
			JsonObject jsonOffer = job.get("offer").getAsJsonObject();
			int[] offer = new int[5];
			offer[0] = jsonOffer.get("brick").getAsInt();
			offer[1] = jsonOffer.get("ore").getAsInt();
			offer[2] = jsonOffer.get("sheep").getAsInt();
			offer[3] = jsonOffer.get("wheat").getAsInt();
			offer[4] = jsonOffer.get("wood").getAsInt();

			return new OfferTradeParams(playerIndex, offer, receiver);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a AcceptTrade request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the AcceptTrade command.
	 */
	public AcceptTradeParams acceptTrade(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			boolean willAccept = job.get("willAccept").getAsBoolean();

			return new AcceptTradeParams(playerIndex, willAccept);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a Maritime request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the Maritime command.
	 */
	public MaritimeTradeParams maritimeTrade(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			int ratio = job.get("ratio").getAsInt();
			String input = job.get("inputResource").getAsString();
			String output = job.get("outputResource").getAsString();

			return new MaritimeTradeParams(playerIndex, ratio, input, output);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}

	/**
	 * Decodes a DiscardCards request body and returns the result as an IParams object.
	 * @param jsonParams The request body to decode.
	 * @return A parameters object containing parameters for the DiscardCards command.
	 */
	public DiscardCardsParams discardCards(String jsonParams) {
		JsonElement jelly = jp.parse(jsonParams);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int playerIndex = job.get("playerIndex").getAsInt();
			JsonObject jsonDiscard = job.get("discardedCards").getAsJsonObject();
			int[] discard = new int[5];
			discard[0] = jsonDiscard.get("brick").getAsInt();
			discard[1] = jsonDiscard.get("ore").getAsInt();
			discard[2] = jsonDiscard.get("sheep").getAsInt();
			discard[3] = jsonDiscard.get("wheat").getAsInt();
			discard[4] = jsonDiscard.get("wood").getAsInt();

			return new DiscardCardsParams(playerIndex, discard);
		}

		else {
			throw new IllegalArgumentException("Invalid request - body is not a JSON object");
		}
	}


	//Util
	public ChangeLogLevelParams changeLogLevel() {
		return null;
	}

	private ResourceType getResourceType(String type){
		switch(type){
		case "brick":
			return ResourceType.BRICK;
		case "ore":
			return ResourceType.ORE;
		case "sheep":
			return ResourceType.SHEEP;
		case "wheat":
			return ResourceType.WHEAT;
		case "wood":
			return ResourceType.WOOD;
		default:
			return null;
		}
	}
	
	private VertexDirection getVertexDirection(String direction) {
		switch (direction) {
		case "W":
			return VertexDirection.West;
		case "NW":
			return VertexDirection.NorthWest;
		case "SW":
			return VertexDirection.SouthWest;
		case "E":
			return VertexDirection.East;
		case "NE":
			return VertexDirection.NorthEast;
		case "SE":
			return VertexDirection.SouthEast;
		default:
			return null;
		}
	}

	private EdgeDirection getEdgeDirection(String direction){
		switch (direction) {
		case "NW":
			return EdgeDirection.NorthWest;
		case "N":
			return EdgeDirection.North;
		case "NE":
			return EdgeDirection.NorthEast;
		case "SW":
			return EdgeDirection.SouthWest;
		case "S":
			return EdgeDirection.South;
		case "SE":
			return EdgeDirection.SouthEast;
		default:
			return null;
		}
	}

}
