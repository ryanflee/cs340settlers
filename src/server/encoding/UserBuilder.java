package server.encoding;

import server.data.lazy.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jtmingus on 4/6/16.
 */
public class UserBuilder {

    public UserBuilder() {}

    public List<User> buildUsers(List<String> userInfo) {
        if(userInfo == null || userInfo.size() == 0) { return null; }
        List<User> users = new ArrayList<User>();
        for(String s : userInfo) {
            String[] sArr = s.split("\\^_\\^");
            if(sArr.length != 3) { return null; }
            users.add(new User(sArr[1], sArr[2], Integer.parseInt(sArr[0])));
        }
        return users;
    }
}
