package server.encoding;

import client.data.PlayerInfo;
import client.facade.Facade;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import server.data.lazy.Game;
import shared.definitions.*;
import shared.locations.*;
import shared.model.GameManager;
import shared.model.Model;
import shared.model.board.Board;
import shared.model.board.Hex;
import shared.model.developmentcard.DevCard;
import shared.model.exceptions.InvalidLocationException;
import shared.model.logs.ChatEntry;
import shared.model.logs.HistoryEntry;
import shared.model.pieces.City;
import shared.model.pieces.Road;
import shared.model.pieces.Settlement;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

import java.util.*;

/**
 * Created by jtmingus on 4/6/16.
 */
public class GameBuilder {

    private JsonParser jp;
    private Game game;

    public GameBuilder() {
        jp = new JsonParser();
    }

    public List<Game> buildGames(List<String> jsonGames) {
        if(jsonGames == null || jsonGames.size() == 0) {
            return null;
        }
        List<Game> games = new ArrayList<Game>();
        for(String s : jsonGames) {
            Game g = null;
            String[] sArr = s.split("\\^_\\^");
            if(sArr.length == 2) {
                g = buildGameFromJson(sArr[1]);
                g.setName(sArr[0]);
            } else if(sArr.length == 3) {
                g = buildGameFromJson(sArr[2]);
                g.setGameId(Integer.parseInt(sArr[0]));
                g.setName(sArr[1]);
            }
            games.add(g);
        }
        return games;
    }

    public Game buildGameFromJson(String json) {
        JsonElement jelly = jp.parse(json);
        if(jelly.isJsonObject()) {
            JsonObject job = jelly.getAsJsonObject();

            this.game = new Game();

            JsonObject jsonBank = job.get("bank").getAsJsonObject();
            JsonObject jsonDeck = job.get("deck").getAsJsonObject();
            JsonArray jsonPlayers = job.get("players").getAsJsonArray();
            JsonObject jsonTurnTracker = job.get("turnTracker").getAsJsonObject();
            JsonElement jsonTradeOfferElem = job.get("tradeOffer");

            // treat this one separately - it's an optional element
            JsonObject jsonTradeOffer = null;
            if (jsonTradeOfferElem != null && !jsonTradeOfferElem.isJsonNull()) {
                jsonTradeOffer = jsonTradeOfferElem.getAsJsonObject();
            }

            // model version
            int version = job.get("version").getAsInt();


            // winner
            int winner = job.get("winner").getAsInt();

            GameManager newGM = parseGameManager(jsonBank, jsonDeck, jsonPlayers, jsonTurnTracker, jsonTradeOffer);

            // "map" element = Board
            Board newBoard = parseBoard(job.get("map").getAsJsonObject(), newGM.getPlayers());

            // add board to GM
            newGM.setBoard(newBoard);

            // "log" element = game history
            List<HistoryEntry> newHistory = parseHistory(job.get("log").getAsJsonObject(), newGM.getPlayers());

            // "chat" element = chat log
            List<ChatEntry> newChat = parseChat(job.get("chat").getAsJsonObject(), newGM.getPlayers());

            Model m = new Model(newGM, newBoard, newGM.getPlayers(), (List<ChatEntry>)newChat, (List<HistoryEntry>)newHistory);
            m.setVersion(version);
            m.setWinner(winner);
            game.setModel(m);

            return game;
        } else {
            return null;
        }
    }

    private Board parseBoard(JsonObject jsonBoard, List<Player> players) {

        JsonArray jsonHexes = jsonBoard.get("hexes").getAsJsonArray();
        List<Hex> hexes = parseHexes(jsonHexes);

        JsonArray jsonRoads = jsonBoard.get("roads").getAsJsonArray();
        JsonArray jsonCities = jsonBoard.get("cities").getAsJsonArray();
        JsonArray jsonSettlements = jsonBoard.get("settlements").getAsJsonArray();
        JsonArray jsonPorts = jsonBoard.get("ports").getAsJsonArray();
        JsonObject jsonRobberLoc = jsonBoard.get("robber").getAsJsonObject();

        Board result = new Board(hexes);

        addPieces(result, jsonRobberLoc, jsonRoads, jsonCities, jsonSettlements, jsonPorts, players);

        result.setPorts(parsePorts(jsonPorts)); // add map of ports to Board
        return result;
    }

    private Map<EdgeLocation, PortType> parsePorts(JsonArray jsonPorts) {

        Map<EdgeLocation, PortType> result = new HashMap<EdgeLocation, PortType>();
        for (int p = 0; p < jsonPorts.size(); p++) {
            JsonObject thisPort = jsonPorts.get(p).getAsJsonObject();

            int ratio = thisPort.get("ratio").getAsInt();

            String strPortType = null;
            JsonElement jsonPortType = thisPort.get("resource");
            if ((jsonPortType == null || jsonPortType.isJsonNull())) {
                if (ratio == 3) {
                    strPortType = "three";
                }
            }

            else {
                strPortType = jsonPortType.getAsString();
            }

            PortType ptype = stringToPortType(strPortType);

            String direction = thisPort.get("direction").getAsString();
            JsonObject loc = thisPort.get("location").getAsJsonObject();

            int portX = loc.get("x").getAsInt();
            int portY = loc.get("y").getAsInt();

            try {
                EdgeDirection edir = stringToEdgeDirection(direction);
                HexLocation hloc = new HexLocation(portX, portY);
                EdgeLocation eloc = new EdgeLocation(hloc, edir);

                result.put(eloc, ptype);
            }
            catch (InvalidLocationException ile) {
                ile.printStackTrace();
            }
        }

        return result;
    }

    private List<Hex> parseHexes(JsonArray jsonHexes) {

        List<Hex> hexes = new ArrayList<Hex>();

        for (int i = 0; i < jsonHexes.size(); i++) {

            HexType htype = null;
            HexLocation hloc = null;
            int number = 0;

            JsonObject thisHex = jsonHexes.get(i).getAsJsonObject();

            // figure out HexType
            JsonElement jsonResource = thisHex.get("resource");
            htype = jsonToHexType(jsonResource);

            // figure out HexLocation
            JsonElement jsonLocation = thisHex.get("location");

            if (jsonLocation.isJsonNull()) {
                throw new IllegalArgumentException("Hex #" + i + " has no HexLocation.");
            }

            else {
                JsonObject objLocation = jsonLocation.getAsJsonObject();
                int x = objLocation.get("x").getAsInt();
                int y = objLocation.get("y").getAsInt();

                hloc = new HexLocation(x, y);
            }

            // figure out number token
            JsonElement jsonNumber = thisHex.get("number");

            if (jsonNumber == null) {
                if ((htype != HexType.DESERT) && (htype != HexType.WATER)) {
                    throw new IllegalArgumentException("Hex #" + i + " has no number token.");
                }
            }

            else {
                number = jsonNumber.getAsInt();
            }

            // add Hex to the list
            Hex newHex = new Hex(htype, hloc, number, false);
            hexes.add(newHex);
        }

        // add ocean hexes too
        hexes.add(new Hex(HexType.WATER, new HexLocation(0, -3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(0, 3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(1, -3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(1, 2), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(2, -3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(2, 1), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-1, -2), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-1, 3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-2, -1), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-2, 3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(3, 0), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(3, -1), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(3, -2), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(3, -3), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 0), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 1), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 2), 0, false));
        hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 3), 0, false));

        return hexes;
    }

    private HexType jsonToHexType(JsonElement jsonResource) {

        HexType htype = null;

        if (jsonResource == null) {
            htype = HexType.DESERT;
        }

        else {
            String resource = jsonResource.getAsString();

            if (resource.equals("brick")) {
                htype = HexType.BRICK;
            }

            else if (resource.equals("wood")) {
                htype = HexType.WOOD;
            }

            else if (resource.equals("wheat")) {
                htype = HexType.WHEAT;
            }

            else if (resource.equals("sheep")) {
                htype = HexType.SHEEP;
            }

            else if (resource.equals("ore")) {
                htype = HexType.ORE;
            }

            else {
                throw new IllegalArgumentException("Invalid or missing HexType.");
            }
        }

        return htype;
    }

    private PortType stringToPortType(String strPortType) {
        PortType ptype = null;

        if (strPortType == null || strPortType.isEmpty()) {
            // do nothing
        }

        else {
            if (strPortType.equalsIgnoreCase("brick")) {
                ptype = PortType.BRICK;
            }

            else if (strPortType.equalsIgnoreCase("wood")) {
                ptype = PortType.WOOD;
            }

            else if (strPortType.equalsIgnoreCase("wheat")) {
                ptype = PortType.WHEAT;
            }

            else if (strPortType.equalsIgnoreCase("sheep")) {
                ptype = PortType.SHEEP;
            }

            else if (strPortType.equalsIgnoreCase("ore")) {
                ptype = PortType.ORE;
            }

            else if (strPortType.equalsIgnoreCase("three")) {
                ptype = PortType.THREE;
            }
        }

        return ptype;
    }

    private ResourceType jsonToResourceType(JsonElement jsonResource) {

        ResourceType rtype = null;

        if (jsonResource == null) {
            // do nothing
        }

        else {
            String resource = jsonResource.getAsString();

            if (resource.equalsIgnoreCase("brick")) {
                rtype = ResourceType.BRICK;
            }

            else if (resource.equalsIgnoreCase("wood")) {
                rtype = ResourceType.WOOD;
            }

            else if (resource.equalsIgnoreCase("wheat")) {
                rtype = ResourceType.WHEAT;
            }

            else if (resource.equals("sheep")) {
                rtype = ResourceType.SHEEP;
            }

            else if (resource.equals("ore")) {
                rtype = ResourceType.ORE;
            }
        }

        return rtype;
    }

    private Board addPieces(Board board, JsonObject jsonRobberLoc, JsonArray jsonRoads, JsonArray jsonCities, JsonArray jsonSettlements, JsonArray jsonPorts, List<Player> players) {

        List<Hex> hexes = board.getAllHexes();

        // figure out Robber's location
        int x = jsonRobberLoc.get("x").getAsInt();
        int y = jsonRobberLoc.get("y").getAsInt();

        HexLocation robberLoc = new HexLocation(x, y);
        Hex robberHex = board.getHexByLocation(robberLoc);
        board.setRobberHex(robberHex);

        // add Roads
        for (int r = 0; r < jsonRoads.size(); r++) {
            JsonObject thisRoad = jsonRoads.get(r).getAsJsonObject();
            int ownerIndex = thisRoad.get("owner").getAsInt();

            JsonObject thisRoadLoc = thisRoad.get("location").getAsJsonObject();
            int roadX = thisRoadLoc.get("x").getAsInt();
            int roadY = thisRoadLoc.get("y").getAsInt();

            try {
                EdgeDirection edir = stringToEdgeDirection(thisRoadLoc.get("direction").getAsString());

                HexLocation hloc = new HexLocation(roadX, roadY);
                EdgeLocation eloc = new EdgeLocation(hloc, edir);
                Hex h = null;

                try {
                    h = board.getHex(eloc.getNormalizedLocation().getHexLoc());
                    eloc = eloc.getNormalizedLocation();
                }

                catch (InvalidLocationException ile) {
                    h = board.getHex(eloc.getHexLoc());
                }

                Road road = new Road(eloc, ownerIndex);
                if (h != null) {
                    h.addRoad(eloc, road);
                }

            }
            catch (InvalidLocationException ile) {
                ile.printStackTrace();
            }
        }

        // build Port vertex map
        Map<VertexLocation, AbstractMap.SimpleEntry<Integer, ResourceType>> portVertexMap = new HashMap<VertexLocation, AbstractMap.SimpleEntry<Integer, ResourceType>>();

        for (int p = 0; p < jsonPorts.size(); p++) {
            JsonObject thisPort = jsonPorts.get(p).getAsJsonObject();

            JsonElement jsonResource = thisPort.get("resource");
            ResourceType resource = jsonToResourceType(jsonResource);

            int ratio = thisPort.get("ratio").getAsInt();
            String direction = thisPort.get("direction").getAsString();
            JsonObject loc = thisPort.get("location").getAsJsonObject();

            int portX = loc.get("x").getAsInt();
            int portY = loc.get("y").getAsInt();

            try {
                EdgeDirection edir = stringToEdgeDirection(direction);
                HexLocation hloc = new HexLocation(portX, portY);
                EdgeLocation eloc = new EdgeLocation(hloc, edir).getNormalizedLocation();

                VertexLocation vloc1 = edgeLocToNormalizedVertexLoc(eloc, true);
                VertexLocation vloc2 = edgeLocToNormalizedVertexLoc(eloc, false);

                AbstractMap.SimpleEntry<Integer, ResourceType> pEntry = new AbstractMap.SimpleEntry<Integer, ResourceType>(ratio, resource);
                portVertexMap.put(vloc1, pEntry);
                portVertexMap.put(vloc2, pEntry);
            }
            catch (InvalidLocationException ile) {
                ile.printStackTrace();
            }
        }

        // add Cities, check for Port locations
        for (int c = 0; c < jsonCities.size(); c++) {
            JsonObject thisCity = jsonCities.get(c).getAsJsonObject();
            int ownerIndex = thisCity.get("owner").getAsInt();

            JsonObject thisCityLoc = thisCity.get("location").getAsJsonObject();
            int cityX = thisCityLoc.get("x").getAsInt();
            int cityY = thisCityLoc.get("y").getAsInt();

            try {
                VertexDirection vdir = stringToVertexDirection(thisCityLoc.get("direction").getAsString());

                HexLocation hloc = new HexLocation(cityX, cityY);
                VertexLocation vloc = new VertexLocation(hloc, vdir);
                Hex h = null;
                try {
                    h = board.getHex(vloc.getNormalizedLocation().getHexLoc());
                    vloc = vloc.getNormalizedLocation();
                }
                catch (InvalidLocationException ile) {
                    h = board.getHex(vloc.getHexLoc());
                }

                City city = new City(vloc, ownerIndex);
                if (h != null) {
                    h.addBuilding(vloc, city);
                }

                // check for Port, update owner's trade rate
                if (portVertexMap.containsKey(vloc.getNormalizedLocation())) {
                    AbstractMap.SimpleEntry<Integer, ResourceType> port = portVertexMap.get(vloc.getNormalizedLocation());
                    Player owner = players.get(ownerIndex);

                    updatePlayerTradeRate(owner, port);
                }
            }

            catch (InvalidLocationException ile) {
                ile.printStackTrace();
            }
        }

        // add Settlements, check for Port locations
        for (int s = 0; s < jsonSettlements.size(); s++) {
            JsonObject thisSettlement = jsonSettlements.get(s).getAsJsonObject();
            int ownerIndex = thisSettlement.get("owner").getAsInt();

            JsonObject thisSettlementLoc = thisSettlement.get("location").getAsJsonObject();
            int settleX = thisSettlementLoc.get("x").getAsInt();
            int settleY = thisSettlementLoc.get("y").getAsInt();

            try {
                VertexDirection vdir = stringToVertexDirection(thisSettlementLoc.get("direction").getAsString());

                HexLocation hloc = new HexLocation(settleX, settleY);
                VertexLocation vloc = new VertexLocation(hloc, vdir);
                Hex h = null;

                try {
                    h = board.getHex(vloc.getNormalizedLocation().getHexLoc());
                    vloc = vloc.getNormalizedLocation();
                }
                catch (InvalidLocationException ile) {
                    h = board.getHex(vloc.getHexLoc());
                }

                Settlement settlement = new Settlement(vloc, ownerIndex);
                if (h != null) {
                    h.addBuilding(vloc, settlement);
                }


                // check for Port, update owner's trade rate
                VertexLocation normal = vloc.getNormalizedLocation();
                if (portVertexMap.containsKey(vloc.getNormalizedLocation())) {
                    AbstractMap.SimpleEntry<Integer, ResourceType> port = portVertexMap.get(vloc.getNormalizedLocation());
                    Player owner = players.get(ownerIndex);

                    updatePlayerTradeRate(owner, port);
                }
            }
            catch (InvalidLocationException ile) {
                ile.printStackTrace();
            }
        }

        return board;
    }

    private VertexLocation edgeLocToNormalizedVertexLoc(EdgeLocation eloc, boolean firstInClockwiseOrder) {

        EdgeDirection edir = eloc.getDir();

        VertexDirection vdir = null;

        switch (edir) {
            case North:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.NorthEast;
                }
                else {
                    vdir = VertexDirection.NorthWest;
                }
                break;
            case NorthEast:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.NorthEast;
                }
                else {
                    vdir = VertexDirection.East;
                }
                break;
            case SouthEast:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.East;
                }
                else {
                    vdir = VertexDirection.SouthEast;
                }
                break;
            case South:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.SouthEast;
                }
                else {
                    vdir = VertexDirection.SouthWest;
                }
                break;
            case SouthWest:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.SouthWest;
                }
                else {
                    vdir = VertexDirection.West;
                }
                break;
            case NorthWest:
                if (firstInClockwiseOrder) {
                    vdir = VertexDirection.West;
                }
                else {
                    vdir = VertexDirection.NorthWest;
                }
                break;
        }

        VertexLocation normal = new VertexLocation(eloc.getHexLoc(), vdir).getNormalizedLocation();
        return normal;
    }

    private void updatePlayerTradeRate(Player p, AbstractMap.SimpleEntry<Integer, ResourceType> port) {

        int ratio = port.getKey();
        ResourceType resource = port.getValue();

        // 3:1 ports are generic
        if (ratio == 3) {
            for (int i = 0; i < 5; i++) {
                if (p.getTradeValue(i) < 3) {
                    continue;
                }
                else {
                    p.setTradeRate(i, ratio);
                }
            }
        }

        // 2:1 ports are resource-specific
        else if ((ratio == 2) && (resource != null)) {
            int rpos = resource.ordinal();
            p.setTradeRate(rpos, ratio);
        }

        else {
            throw new IllegalArgumentException("Invalid port! Ratio: " + ratio + ", Resource type: "  + resource.toJson());
        }
    }

    private EdgeDirection stringToEdgeDirection(String dir) throws InvalidLocationException {

        if (dir.equalsIgnoreCase("N")) {
            return EdgeDirection.North;
        }

        else if (dir.equalsIgnoreCase("S")) {
            return EdgeDirection.South;
        }

        else if (dir.equalsIgnoreCase("NE")) {
            return EdgeDirection.NorthEast;
        }

        else if (dir.equalsIgnoreCase("NW")) {
            return EdgeDirection.NorthWest;
        }

        else if (dir.equalsIgnoreCase("SE")) {
            return EdgeDirection.SouthEast;
        }

        else if (dir.equalsIgnoreCase("SW")) {
            return EdgeDirection.SouthWest;
        }

        else {
            throw new InvalidLocationException("Invalid EdgeDirection specified: " + dir);
        }
    }

    private VertexDirection stringToVertexDirection(String dir) throws InvalidLocationException {

        if (dir.equalsIgnoreCase("E")) {
            return VertexDirection.East;
        }

        else if (dir.equalsIgnoreCase("W")) {
            return VertexDirection.West;
        }

        else if (dir.equalsIgnoreCase("NE")) {
            return VertexDirection.NorthEast;
        }

        else if (dir.equalsIgnoreCase("NW")) {
            return VertexDirection.NorthWest;
        }

        else if (dir.equalsIgnoreCase("SE")) {
            return VertexDirection.SouthEast;
        }

        else if (dir.equalsIgnoreCase("SW")) {
            return VertexDirection.SouthWest;
        }

        else {
            throw new InvalidLocationException("Invalid VertexDirection specified: " + dir);
        }
    }

    private CatanColor stringToColor(String color) {

        if (color.equalsIgnoreCase("red")) {
            return CatanColor.RED;
        }

        else if (color.equalsIgnoreCase("blue")) {
            return CatanColor.BLUE;
        }

        else if (color.equalsIgnoreCase("green")) {
            return CatanColor.GREEN;
        }

        else if (color.equalsIgnoreCase("brown")) {
            return CatanColor.BROWN;
        }

        else if (color.equalsIgnoreCase("orange")) {
            return CatanColor.ORANGE;
        }

        else if (color.equalsIgnoreCase("puce")) {
            return CatanColor.PUCE;
        }

        else if (color.equalsIgnoreCase("purple")) {
            return CatanColor.PURPLE;
        }

        else if (color.equalsIgnoreCase("white")) {
            return CatanColor.WHITE;
        }

        else if (color.equalsIgnoreCase("yellow")) {
            return CatanColor.YELLOW;
        }

        else {
            throw new IllegalArgumentException("Unknown CatanColor specified.");
        }
    }

    private GameManager parseGameManager(JsonObject jsonBank, JsonObject jsonDeck, JsonArray jsonPlayers, JsonObject jsonTurnTracker, JsonObject jsonTradeOffer) {

        // ResourceCards
        int numWood = jsonBank.get("wood").getAsInt();
        int numBrick = jsonBank.get("brick").getAsInt();
        int numSheep = jsonBank.get("sheep").getAsInt();
        int numWheat = jsonBank.get("wheat").getAsInt();
        int numOre = jsonBank.get("ore").getAsInt();

        List<ResourceCard> woodList = buildResourceList(numWood, ResourceType.WOOD);
        List<ResourceCard> brickList = buildResourceList(numBrick, ResourceType.BRICK);
        List<ResourceCard> sheepList = buildResourceList(numSheep, ResourceType.SHEEP);
        List<ResourceCard> wheatList = buildResourceList(numWheat, ResourceType.WHEAT);
        List<ResourceCard> oreList = buildResourceList(numOre, ResourceType.ORE);

        // DevCards
        List<DevCard> gmDevList = buildDevCardList(jsonDeck, false);

        Map<Integer, Player> players = new HashMap<Integer, Player>();
        List<PlayerInfo> gameInfoPlayers = new ArrayList<PlayerInfo>();

        for (int i = 0; i < jsonPlayers.size(); i++) {
            JsonElement thisPlayer = jsonPlayers.get(i);
            if (thisPlayer.isJsonNull()) {
                continue;
            }

            else {
                JsonObject playerObj = thisPlayer.getAsJsonObject();

                // 'personal' info
                int id = playerObj.get("playerID").getAsInt();
                int index = playerObj.get("playerIndex").getAsInt();
                String pName = playerObj.get("name").getAsString();
                CatanColor pColor = stringToColor(playerObj.get("color").getAsString());

                // 'game' info
                gameInfoPlayers.add(new PlayerInfo(id, index, pName, pColor));

                // player's resource cards
                JsonObject pResources = playerObj.get("resources").getAsJsonObject();
                int pNumWood = pResources.get("wood").getAsInt();
                int pNumBrick = pResources.get("brick").getAsInt();
                int pNumSheep = pResources.get("sheep").getAsInt();
                int pNumWheat = pResources.get("wheat").getAsInt();
                int pNumOre = pResources.get("ore").getAsInt();

                List<ResourceCard> pWoodList = buildResourceList(pNumWood, ResourceType.WOOD);
                List<ResourceCard> pBrickList = buildResourceList(pNumBrick, ResourceType.BRICK);
                List<ResourceCard> pSheepList = buildResourceList(pNumSheep, ResourceType.SHEEP);
                List<ResourceCard> pWheatList = buildResourceList(pNumWheat, ResourceType.WHEAT);
                List<ResourceCard> pOreList = buildResourceList(pNumOre, ResourceType.ORE);

                List<ResourceCard> finalResourceList = new ArrayList<ResourceCard>();
                finalResourceList.addAll(pWoodList);
                finalResourceList.addAll(pBrickList);
                finalResourceList.addAll(pSheepList);
                finalResourceList.addAll(pWheatList);
                finalResourceList.addAll(pOreList);

                // player's development cards
                JsonObject pOldDevCards = playerObj.get("oldDevCards").getAsJsonObject();
                JsonObject pNewDevCards = playerObj.get("newDevCards").getAsJsonObject();
                List<DevCard> pDevList = buildDevCardList(pOldDevCards, false);
                pDevList.addAll(buildDevCardList(pNewDevCards, true));

                // see if player has played a dev card this turn
                boolean playedDevCard = playerObj.get("playedDevCard").getAsBoolean();

                // not sure if we need this
                boolean discarded = playerObj.get("discarded").getAsBoolean();

                // player's Buildings
                int numRoads = playerObj.get("roads").getAsInt();
                int numCities = playerObj.get("cities").getAsInt();
                int numSettlements = playerObj.get("settlements").getAsInt();

                // victory points, number of soldiers and monuments in play
                int victoryPoints = playerObj.get("victoryPoints").getAsInt();
                int pSoldiers = playerObj.get("soldiers").getAsInt();
                int pMonuments = playerObj.get("monuments").getAsInt();

                Player p = new Player(id, pName, pColor, victoryPoints, numRoads, numCities, numSettlements, finalResourceList, pDevList, discarded, playedDevCard, pSoldiers, pMonuments);

                players.put(index, p);
            }
        }

        /*if(Facade.getGameInfo() != null) {
            Facade.getGameInfo().setPlayers(gameInfoPlayers);
        }*/

        Player p1 = players.get(0);
        Player p2 = players.get(1);
        Player p3 = players.get(2);
        Player p4 = players.get(3);

        List<Player> playerList = new ArrayList<Player>();
        playerList.add(p1);
        playerList.add(p2);
        playerList.add(p3);
        playerList.add(p4);

        // "\"turnTracker\": { \"status\": \"FirstRound\", \"currentTurn\": 0, \"longestRoad\": -1, \"largestArmy\": -1 }";
        String status = jsonTurnTracker.get("status").getAsString();
        int currentTurn = jsonTurnTracker.get("currentTurn").getAsInt();
        int longestRoadIndex = jsonTurnTracker.get("longestRoad").getAsInt();
        int largestArmyIndex = jsonTurnTracker.get("largestArmy").getAsInt();


        if (jsonTradeOffer != null) {
            int sender = jsonTradeOffer.get("sender").getAsInt();
            int receiver = jsonTradeOffer.get("receiver").getAsInt();
            JsonObject offer = jsonTradeOffer.get("offer").getAsJsonObject();

            game.setTradeSender(sender);
            game.setTradeReceiver(receiver);


            Map<String, Integer> offerMap = new HashMap<String, Integer>();
            offerMap.put("wood", offer.get("wood").getAsInt());
            offerMap.put("brick", offer.get("brick").getAsInt());
            offerMap.put("sheep", offer.get("sheep").getAsInt());
            offerMap.put("wheat", offer.get("wheat").getAsInt());
            offerMap.put("ore", offer.get("ore").getAsInt());

            game.setTradeOffer(offerMap);
        } else {
            game.setTradeSender(-1);
            game.setTradeReceiver(-1);
            game.setTradeOffer(null);
        }

        GameManager gm = new GameManager();
        gm.setPlayers((ArrayList<Player>)playerList);
        gm.setCurrentPlayer(currentTurn % 4);
        gm.setWood((ArrayList<ResourceCard>)woodList);
        gm.setBrick((ArrayList<ResourceCard>)brickList);
        gm.setSheep((ArrayList<ResourceCard>)sheepList);
        gm.setWheat((ArrayList<ResourceCard>)wheatList);
        gm.setOre((ArrayList<ResourceCard>)oreList);
        gm.setDevelopmentCards((ArrayList<DevCard>)gmDevList);
        gm.setLongestRoad(longestRoadIndex);
        gm.setLargestArmy(largestArmyIndex);
        gm.setStatus(status);

        return gm;
    }

    private List<ResourceCard> buildResourceList(int amount, ResourceType type) {

        List<ResourceCard> result = new ArrayList<ResourceCard>();

        for (int i = 0; i < amount; i++) {
            result.add(new ResourceCard(type));
        }

        return result;
    }

    private List<DevCard> buildDevCardList(JsonObject jsonObj, boolean sameTurn) {

        int yops = jsonObj.get("yearOfPlenty").getAsInt();
        int monopolies = jsonObj.get("monopoly").getAsInt();
        int soldiers = jsonObj.get("soldier").getAsInt();
        int rbs = jsonObj.get("roadBuilding").getAsInt();
        int monuments = jsonObj.get("monument").getAsInt();

        List<DevCard> result = new ArrayList<DevCard>();

        // Year of Plenty
        for (int y = 0; y < yops; y++) {
            DevCard d = new DevCard(0, DevCardType.YEAR_OF_PLENTY);
            d.setSameTurn(sameTurn);
            result.add(d);
        }

        // Monopoly
        for (int m = 0; m < monopolies; m++) {
            DevCard d = new DevCard(1, DevCardType.MONOPOLY);
            d.setSameTurn(sameTurn);
            result.add(d);
        }

        // Soldier
        for (int s = 0; s < soldiers; s++) {
            DevCard d = new DevCard(2, DevCardType.SOLDIER);
            d.setSameTurn(sameTurn);
            result.add(d);
        }

        // Road Building
        for (int r = 0; r < rbs; r++) {
            DevCard d = new DevCard(3, DevCardType.ROAD_BUILD);
            d.setSameTurn(sameTurn);
            result.add(d);
        }

        // Monument - can be played immediately
        for (int o = 0; o < monuments; o++) {
            DevCard d = new DevCard(4, DevCardType.MONUMENT);
            d.setSameTurn(false);
            result.add(d);
        }

        return result;
    }

    private List<ChatEntry> parseChat(JsonObject jsonLog, List<Player> players) {

        List<ChatEntry> result = new ArrayList<ChatEntry>();
        JsonArray jsonLines = jsonLog.get("lines").getAsJsonArray();

        if (jsonLines.isJsonNull()) {
            // no lines, so just return an empty list
            return result;
        }

        else {
            for (int i = 0; i < jsonLines.size(); i++) {

                JsonObject line = jsonLines.get(i).getAsJsonObject();
                String source = (!line.get("source").isJsonNull() && line.get("source") != null) ?
                        line.get("source").getAsString() : null;

                String message = (!line.get("message").isJsonNull() && line.get("message") != null) ?
                        line.get("message").getAsString() : null;

                if (source == null || message == null) {
                    continue;
                }

                ChatEntry entry = null;

                // search through the list of players for one with the right name
                for (Player p : players) {
                    if (p == null) {
                        continue;
                    }
                    if ((p.getName() != null) && p.getName().equals(source)) {
                        entry = new ChatEntry(p, message);
                    }
                }

                if (entry != null) {
                    result.add(entry);
                }

                else {
                    // player is not in this game; throw an exception
                    throw new IllegalArgumentException(source + " is not a valid player name.");
                }
            }
        }

        return result;
    }

    private List<HistoryEntry> parseHistory(JsonObject jsonLog, List<Player> players) {

        List<HistoryEntry> result = new ArrayList<HistoryEntry>();
        JsonArray jsonLines = jsonLog.get("lines").getAsJsonArray();

        if (jsonLines.isJsonNull()) {
            // no lines, so just return an empty list
            return result;
        }

        else {
            for (int i = 0; i < jsonLines.size(); i++) {

                JsonObject line = jsonLines.get(i).getAsJsonObject();
                String source = line.get("source").getAsString();
                String message = line.get("message").getAsString();

                HistoryEntry entry = null;

                // search through the list of players for one with the right name
                for (Player p : players) {
                    if (p == null) {
                        continue;
                    }
                    if ((p.getName() != null) && p.getName().equals(source)) {
                        entry = new HistoryEntry(p, message);
                    }
                }

                if (entry != null) {
                    result.add(entry);
                }

                else {
                    // player is not in this game; throw an exception
                    throw new IllegalArgumentException(source + " is not a valid player name.");
                }
            }
        }

        return result;
    }


}
