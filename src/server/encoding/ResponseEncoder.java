package server.encoding;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import server.data.lazy.Game;
import shared.locations.*;
import shared.model.Model;
import shared.model.board.*;
import shared.model.developmentcard.DevCard;
import shared.model.logs.*;
import shared.definitions.*;
import shared.model.pieces.*;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

import com.google.gson.stream.JsonWriter;

/**
 * Takes Java object and builds json encoding to be sent in server response body
 * @author Jake Mingus
 *
 */
public class ResponseEncoder {

	private JsonWriter writer;
	private Writer out;
	
	private String[] aiTypes = new String[] {"LARGEST_ARMY"};
	
	public ResponseEncoder() {}
	
	public String encodeAIList() {
		out = new StringWriter();
		writer = new JsonWriter(out);
		
		try {
			writer.beginArray();
			for (String ai : aiTypes) {
				writer.value(ai);
			}
			writer.endArray();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return out.toString();
	}
	
	/**
	 * Encodes the list of games as a json string
	 * @param games - List of game objects
	 * @return json encoding of games list
	 */
	public String encodeGames(List<Game> games) {
		out = new StringWriter();
		writer = new JsonWriter(out);
		
		String result = null;
		
		try {
			writer.beginArray();
			for (Game g : games) {
				writer.beginObject();
				writer.name("title").value(g.getTitle());
				writer.name("id").value(g.getGameId());
				
				writer.name("players");
				writer.beginArray();
				for (Player p : g.getModel().getPlayers()) {
					writer.beginObject();
					writer.name("color").value(p.getColor().toJson());
					writer.name("name").value(p.getName());
					writer.name("id").value(p.getId());
					writer.endObject();
				}
				writer.endArray();
				writer.endObject();
			}
			writer.endArray();
			
			result = out.toString();
			writer.close();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * Encodes model for current game as a json string
	 * @param g - Game object for storing current game's info
	 * @return json encoding of game
	 */
	public String encodeGame(Game g) {
		
		//System.out.println("Model Status: " + g.getModel().gameManager.getStatus());
		
		out = new StringWriter();
		writer = new JsonWriter(out);
		
		String result = null;
		Model m = g.getModel();
		
		try {
			// BEGIN JSON model object
			writer.beginObject();
			
			encodeBank(m);
			encodeDeck(m);
			encodeChat(m);
			encodeHistory(m);
			encodeMap(m);
			encodePlayers(m);
			encodeTradeOffer(g.getTradeSender(), g.getTradeReceiver(), g.getTradeOffer());
			encodeTurnTracker(m);
			
			// VERSION and WINNER
			writer.name("version").value(m.getVersion());
			writer.name("winner").value(m.getWinner());
			
			// END model
			writer.endObject();
			
			// return resulting string
			result = out.toString();
			writer.close();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return result;
	}
	
	private void encodeBank(Model m) throws IOException {
		writer.name("bank");
		writer.beginObject();
		int brick = m.gameManager.getCardsByType("brick").size();
		int ore = m.gameManager.getCardsByType("ore").size();
		int sheep = m.gameManager.getCardsByType("sheep").size();
		int wheat = m.gameManager.getCardsByType("wheat").size();
		int wood = m.gameManager.getCardsByType("wood").size();
		writer.name("brick").value(brick);
		writer.name("ore").value(ore);
		writer.name("sheep").value(sheep);
		writer.name("wheat").value(wheat);
		writer.name("wood").value(wood);
		writer.endObject();
	}
	
	private void encodeDeck(Model m) throws IOException {
		writer.name("deck");
		writer.beginObject();
		int monopolies, monuments, roadBuildings, soldiers, yearOfPlenties;
		monopolies = monuments = roadBuildings = soldiers = yearOfPlenties = 0;
		
		for (DevCard d : m.gameManager.getDevelopmentCards()) {
			switch (d.getType()) {
			case MONOPOLY:
				monopolies++;
				break;
			case MONUMENT:
				monuments++;
				break;
			case ROAD_BUILD:
				roadBuildings++;
				break;
			case SOLDIER:
				soldiers++;
				break;
			case YEAR_OF_PLENTY:
				yearOfPlenties++;
				break;
			}
		}
		
		writer.name("monopoly").value(monopolies);
		writer.name("monument").value(monuments);
		writer.name("roadBuilding").value(roadBuildings);
		writer.name("soldier").value(soldiers);
		writer.name("yearOfPlenty").value(yearOfPlenties); 
		writer.endObject();
	}
	
	private void encodeChat(Model m) throws IOException {
		writer.name("chat");
		writer.beginObject();
		writer.name("lines");
		writer.beginArray();
		for (ChatEntry entry : m.getComments()) {
			writer.beginObject();
			writer.name("source").value(entry.getPlayer().getName());
			writer.name("message").value(entry.getValue());
			writer.endObject();
		}
		writer.endArray();
		writer.endObject();
	}
	
	private void encodeHistory(Model m) throws IOException {
		writer.name("log");
		writer.beginObject();
		writer.name("lines");
		writer.beginArray();
		for (HistoryEntry entry : m.getHistory()) {
			writer.beginObject();
			writer.name("source").value(entry.getPlayer().getName());
			writer.name("message").value(entry.getValue());
			writer.endObject();
		}
		writer.endArray();
		writer.endObject();
	}
	
	private void encodeMap(Model m) throws IOException {
		Board b = m.gameManager.getBoard();
		
		writer.name("map");
		writer.beginObject();
		
		writer.name("radius").value(3); // hard-coded because we don't use this
		writer.name("robber");
		encodeLocation(b.getRobberHex().getHexLocation());
		
		encodeHexes(b.getAllHexes());
		encodePorts(b.getPorts());
		encodeRoads(b.getAllRoads());
		
		List<Settlement> allSettlements = new ArrayList<Settlement>();
		List<City> allCities = new ArrayList<City>();
		
		for (Building bldg : b.getAllBuildings()) {
			if (bldg instanceof Settlement) {
				Settlement s = (Settlement)bldg;
				allSettlements.add(s);
			}
			else if (bldg instanceof City) {
				City c = (City)bldg;
				allCities.add(c);
			}
		}
		
		encodeSettlements(allSettlements);
		encodeCities(allCities);
		
		writer.endObject();
	}
	
	private void encodeHexes(List<Hex> hexes) throws IOException {
		writer.name("hexes");
		writer.beginArray();
		for (Hex h : hexes) {
			if(h.getHexType() != HexType.WATER) {
				writer.beginObject();

				writer.name("location");
				encodeLocation(h.getHexLocation());

				if ((h.getHexType() != HexType.DESERT) && (h.getHexType() != HexType.WATER)) {
					writer.name("resource").value(h.getHexType().toString().toLowerCase());
					writer.name("number").value(h.getNumberToken());
				}

				writer.endObject();
			}
		}
		writer.endArray();
	}
	
	private void encodePorts(Map<EdgeLocation, PortType> ports) throws IOException {
		writer.name("ports");
		writer.beginArray();
		for (EdgeLocation eloc : ports.keySet()) {
			writer.beginObject();
			writer.name("location");
			encodeLocation(eloc.getHexLoc());
			writer.name("direction").value(eloc.getJsonDir());
			
			PortType ptype = ports.get(eloc);
			int ratio = 2;
			if (ptype == PortType.THREE) {
				ratio = 3;
			}
			else {
				writer.name("resource").value(ptype.toString().toLowerCase());
			}
			writer.name("ratio").value(ratio);
			
			writer.endObject();
		}
		writer.endArray();
	}
	
	private void encodeRoads(List<Road> allRoads) throws IOException {
		writer.name("roads");
		writer.beginArray();
		for (Road r : allRoads) {
			writer.beginObject();
			writer.name("owner").value(r.getOwner());
			writer.name("location");
			encodeLocation(r.getLocation());
			writer.endObject();
		}
		writer.endArray();
	}
	
	private void encodeSettlements(List<Settlement> allSettlements) throws IOException {
		writer.name("settlements");
		writer.beginArray();
		for (Settlement s : allSettlements) {
			writer.beginObject();
			writer.name("owner").value(s.getOwner());
			writer.name("location");
			encodeLocation(s.getLocation());
			writer.endObject();
		}
		writer.endArray();
	}
	
	private void encodeCities(List<City> allCities) throws IOException {
		writer.name("cities");
		writer.beginArray();
		for (City c : allCities) {
			writer.beginObject();
			writer.name("owner").value(c.getOwner());
			writer.name("location");
			encodeLocation(c.getLocation());
			writer.endObject();
		}
		writer.endArray();
	}
	
	private void encodeLocation(VertexLocation vloc) throws IOException {
		writer.beginObject();
		writer.name("x").value(vloc.getHexLoc().getX());
		writer.name("y").value(vloc.getHexLoc().getY());
		writer.name("direction").value(vloc.getJsonDir());
		writer.endObject();
	}
	
	private void encodeLocation(EdgeLocation eloc) throws IOException {
		writer.beginObject();
		writer.name("x").value(eloc.getHexLoc().getX());
		writer.name("y").value(eloc.getHexLoc().getY());
		writer.name("direction").value(eloc.getJsonDir());
		writer.endObject();
	}
	
	private void encodeLocation(HexLocation hloc) throws IOException {
		writer.beginObject();
		writer.name("x").value(hloc.getX());
		writer.name("y").value(hloc.getY());
		writer.endObject();
	}
	
	private void encodePlayers(Model m) throws IOException {
		writer.name("players");
			
		writer.beginArray();
		for (int i = 0; i < m.getPlayers().size(); i++) {
			Player p = m.getPlayers().get(i);
			if(p != null) {
				writer.beginObject();
				writer.name("cities").value(p.getCities());
				writer.name("color").value(p.getColor().toJson());
				writer.name("discarded").value(p.getDiscarded());
				writer.name("monuments").value(p.getMonuments());
				writer.name("name").value(p.getName());
				writer.name("playedDevCard").value(p.getPlayedDevCard());
				writer.name("playerID").value(p.getId());
				writer.name("playerIndex").value(i);
				writer.name("roads").value(p.getRoads());
				writer.name("settlements").value(p.getSettlements());
				writer.name("soldiers").value(p.getSoldiers());
				writer.name("victoryPoints").value(p.getVictoryPoints());

				encodePlayerDevCards(p.getDevCards());
				encodePlayerResources(p.getHand());

				writer.endObject();
			}
		}
		writer.endArray();
	}
	
	private void encodePlayerDevCards(List<DevCard> devHand) throws IOException {
		// DEV CARDS
		int monopolies, monuments, roadBuildings, soldiers, yearOfPlenties;
		monopolies = monuments = roadBuildings = soldiers = yearOfPlenties = 0;
		
		int newMonopolies, newRoadBuildings, newSoldiers, newYearOfPlenties;
		newMonopolies = newRoadBuildings = newSoldiers = newYearOfPlenties = 0;
		
		for (DevCard d : devHand) {
			switch (d.getType()) {
			case MONOPOLY:
				if (d.canPlayCard()) {
					monopolies++;
				}
				else {
					newMonopolies++;
				}
				break;
			case MONUMENT:
				monuments++; // can play monuments immediately
				break;
			case ROAD_BUILD:
				if (d.canPlayCard()) {
					roadBuildings++;
				}
				else {
					newRoadBuildings++;
				}
				break;
			case SOLDIER:
				if (d.canPlayCard()) {
					soldiers++;
				}
				else {
					newSoldiers++;
				}
				break;
			case YEAR_OF_PLENTY:
				if (d.canPlayCard()) {
					yearOfPlenties++;
				}
				else {
					newYearOfPlenties++;
				}
				break;
			}
		}
			
		// NEW DEV CARDS
		writer.name("newDevCards");
		writer.beginObject();
		writer.name("monopoly").value(newMonopolies);
		writer.name("monument").value(0);
		writer.name("roadBuilding").value(newRoadBuildings);
		writer.name("soldier").value(newSoldiers);
		writer.name("yearOfPlenty").value(newYearOfPlenties);
		writer.endObject();
		
		// OLD DEV CARDS
		writer.name("oldDevCards");
		writer.beginObject();
		writer.name("monopoly").value(monopolies);
		writer.name("monument").value(monuments);
		writer.name("roadBuilding").value(roadBuildings);
		writer.name("soldier").value(soldiers);
		writer.name("yearOfPlenty").value(yearOfPlenties);
		writer.endObject();
	}
	
	private void encodePlayerResources(List<ResourceCard> hand) {
		try {
			writer.name("resources");
			writer.beginObject();
			
			int pbrick; int pore; int psheep; int pwheat; int pwood;
			pbrick = pore = psheep = pwheat = pwood = 0;
			
			for (ResourceCard r : hand) {
				switch (r.getType()) {
				case BRICK:
					pbrick++;
					break;
				case ORE:
					pore++;
					break;
				case SHEEP:
					psheep++;
					break;
				case WHEAT:
					pwheat++;
					break;
				case WOOD:
					pwood++;
					break;
				}
			}
			writer.name("brick").value(pbrick);
			writer.name("ore").value(pore);
			writer.name("sheep").value(psheep);
			writer.name("wheat").value(pwheat);
			writer.name("wood").value(pwood);
			writer.endObject();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	private void encodeTradeOffer(int sender, int receiver, Map<String, Integer> offer) throws IOException {
		if (offer != null) {
			writer.name("tradeOffer");
			writer.beginObject();
			writer.name("sender").value(sender);
			writer.name("receiver").value(receiver);
			
			writer.name("offer");
			writer.beginObject();
			writer.name("brick").value(offer.get("brick"));
			writer.name("ore").value(offer.get("ore"));
			writer.name("sheep").value(offer.get("sheep"));
			writer.name("wheat").value(offer.get("wheat"));
			writer.name("wood").value(offer.get("wood"));
			writer.endObject();
			
			writer.endObject();
		}
	}
	
	private void encodeTurnTracker(Model m) throws IOException {
		writer.name("turnTracker");
		writer.beginObject();
		writer.name("currentTurn").value(m.gameManager.getCurrentPlayer());
		writer.name("status").value(m.gameManager.getStatus());
		writer.name("largestArmy").value(m.gameManager.getLargestArmy());
		writer.name("longestRoad").value(m.gameManager.getLongestRoad());
		writer.endObject();
	}
}
