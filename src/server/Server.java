package server;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.logging.*;
import java.util.regex.Pattern;

import server.exceptions.ServerFacadeException;
import server.facade.*;
import server.persistence.IPersistPlugin;

import com.sun.net.httpserver.*;

public class Server {
	private static int SERVER_PORT_NUMBER;
	private static final int MAX_WAITING_CONNECTIONS = 10;
	private static final int DEFAULT_PORT = 8081;
	private static IFacade facade;
	private static IPersistPlugin plugin;
	private static int commandsBetweenCheckpoints = 10;
	
	private static Logger logger;
	
	static {
		try {
			initLog();
		}
		catch (IOException e) {
			System.out.println("Could not initialize log: " + e.getMessage());
		}
	}
	
	private static void initLog() throws IOException {
		
		Level logLevel = Level.FINE;
		
		logger = Logger.getLogger("indexrecord"); 
		logger.setLevel(logLevel);
		logger.setUseParentHandlers(false);
		
		ConsoleHandler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(logLevel);
		consoleHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(consoleHandler);

		FileHandler fileHandler = new FileHandler("log.txt", false);
		fileHandler.setLevel(logLevel);
		fileHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(fileHandler);
	}

	
	private HttpServer server;
	
	public Server() {
		return;
	}
	
	public void stop() {
		logger.severe("KILLING SERVER");
		server.stop(0);
	}

	public void run() {
		handler = new Handler(facade, plugin, commandsBetweenCheckpoints);
		
		logger.info("Initializing Model");
		
		try {
			//ServerFacade.initialize();
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return;
		}
		
		logger.info("Initializing HTTP Server");
		
		SERVER_PORT_NUMBER = DEFAULT_PORT;
		
		try {
			server = HttpServer.create(new InetSocketAddress(SERVER_PORT_NUMBER),
											MAX_WAITING_CONNECTIONS);
		} 
		catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);			
			return;
		}

		server.setExecutor(null); // use the default executor

		server.createContext("/docs/api/data", new SwaggerHandlers.JSONAppender(""));
		server.createContext("/docs/api/view", new SwaggerHandlers.BasicFile(""));
		server.createContext("/", handler);
		
		logger.info("Starting HTTP Server on " + SERVER_PORT_NUMBER);

		server.start();
	}

	private HttpHandler handler;
	
	public static void main(String[] args) throws IOException {
		//Set commands to default of 10
		//int commandsBetweenCheckpoints = 10;
		File config = new File("plugins.txt");
		Scanner sc = new Scanner(config);
		String dbType = null;
		boolean clear = false;
		boolean isValid = false;

		for(String s : args) {
			System.out.println(s);
		}

		if(args.length >= 2){
			dbType = args[0];
			commandsBetweenCheckpoints = Integer.parseInt(args[1]);

			clear = args.length > 2 && (args[2].equalsIgnoreCase("clear"));
						
			String[] values = null;
			while (sc.hasNextLine())
			{
				String myLine = sc.nextLine();
			    values = myLine.split(",");
			    if (values[0].equalsIgnoreCase(dbType)){
			    	isValid = true;
			    	break;
			    }
			}
			
			if(isValid){
				// load selected plugin
				String mainClass = values[1];
				String path = values[2];
				
				ClassLoader cl = new URLClassLoader(new URL[] {new URL("file://" + path)});

				try {
					Class c = Class.forName(mainClass, true, cl);
					plugin = (IPersistPlugin)c.newInstance();
				}
				catch (ClassNotFoundException e) {
					e.printStackTrace();
					return;
				}
				catch (IllegalAccessException e) {
					e.printStackTrace();
					return;
				}
				catch (InstantiationException e) {
					e.printStackTrace();
					return;
				}
				
				if (clear) {
					plugin.clear();
				}
			}
		}
		else if(args.length == 1){
			if(args[0].equalsIgnoreCase("clear")){
				//clear all databases
			}
			else{
				//invalid args
				logger.info("Invalid arguments");
				return;
			}
		}
		else{
			//Default to use SQL and 10 commands per checkpoint
			
		}
			
		facade = new RealFacade();
		new Server().run();
	}
}
