package server.command;

import server.exceptions.ServerFacadeException;
import server.facade.IFacade;

/**
 * Interface for each of the command objects
 * @author Jake Mingus
 *
 */
public interface ICommand {
	
	/**
	 * Executes action determined in each concretion
	 * @return Json string response
	 * @throws ServerFacadeException 
	 */
	public String execute() throws ServerFacadeException;
	
}
