package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.locations.VertexLocation;

/**
 * Command objects that builds a settlement
 * @author Jake Mingus
 */
public class BuildSettlement implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private VertexLocation vertex;
	private boolean free;
	private int gameID;
	
	public BuildSettlement() {
		this.playerIndex = -1;
		this.vertex = null;
		this.free = false;
	}
	
	public BuildSettlement(IFacade facade, BuildSettlementParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.vertex = params.getVertex();
		this.free = params.isFree();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.buildSettlement(playerIndex, vertex, free, gameID);
	}

}
