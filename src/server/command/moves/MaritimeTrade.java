package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that makes a maritime trade
 * @author wintersoldier
 *
 */
public class MaritimeTrade implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private int ratio;
	private String input;
	private String output;
	private int gameID;
	
	public MaritimeTrade() {
		this.playerIndex = -1;
		this.ratio = 0;
		this.input = null;
		this.output = null;
	}
	
	public MaritimeTrade(IFacade facade, MaritimeTradeParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.ratio = params.getRatio();
		this.input = params.getInput();
		this.output = params.getOutput();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.maritimeTrade(playerIndex, ratio, input, output, gameID);
	}

}
