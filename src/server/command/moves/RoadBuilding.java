package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.locations.EdgeLocation;

/**
 * Command object that lets a player use the build road development card.
 * @author jjwilbur
 *
 */
public class RoadBuilding implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private EdgeLocation spot1;
	private EdgeLocation spot2;
	private int gameID;
	
	public RoadBuilding(){
		this.playerIndex = -1;
		this.spot1 = null;
		this.spot2 = null;
	}
	
	public RoadBuilding(IFacade facade, RoadBuildingParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.spot1 = params.getSpot1();
		this.spot2 = params.getSpot2();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if(facade == null){return null;}
		return facade.roadBuilding(playerIndex, spot1, spot2, gameID);
	}

}
