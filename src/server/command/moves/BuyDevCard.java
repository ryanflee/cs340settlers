package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that allows a player to buy a development card
 * @author jjwilbur
 *
 */
public class BuyDevCard implements ICommand {
	
	private IFacade facade;
	private int playerIndex;
	private int gameID;
	
	public BuyDevCard (){
		this.playerIndex = -1;
	}
	
	public BuyDevCard (IFacade facade, BuyDevCardParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if(facade == null){ return null; }
		return facade.buyDevCard(playerIndex, gameID);
	}

}
