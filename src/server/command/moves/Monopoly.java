package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that lets a player play the monopoly development card.
 * @author jjwilbur
 *
 */
public class Monopoly implements ICommand {

	private IFacade facade;
	private String resource;
	private int playerIndex;
	private int gameID;
	
	public Monopoly (){
		this.resource = "";
		this.playerIndex = -1;
	}
	
	public Monopoly (IFacade facade, MonopolyParams params, int gameID){
		this.playerIndex = params.getPlayerIndex();
		this.resource = params.getResource();
		this.gameID = gameID;
		this.facade = facade;
	}
	
	@Override
	public String execute() {
		if(facade == null){return null;}
		return facade.monopoly(playerIndex, resource, gameID);
	}

}
