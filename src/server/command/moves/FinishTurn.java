package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;


/**
 * Command object that allows a player to finish his turn.
 * @author Ryan F. Lee
 *
 */
public class FinishTurn implements ICommand {

	private IFacade facade;
	private int gameId;
	private int playerIndex;
	
	public FinishTurn(IFacade facade) {
		this.facade = facade;
		this.gameId = -1;
		this.playerIndex = -1;
	}
	
	public FinishTurn(IFacade facade, FinishTurnParams params, int gameId) {
		this.facade = facade;
		this.gameId = gameId;
		this.playerIndex = params.getPlayerIndex();
	}
	
	@Override
	public String execute() {
		return this.facade.finishTurn(this.gameId, this.playerIndex);
	}

}
