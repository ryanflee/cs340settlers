package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.definitions.ResourceType;

/**
 * Command object that lets the user play a year of plenty development card.
 * @author jjwilbur
 *
 */
public class YearOfPlenty implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private ResourceType resource1;
	private ResourceType resource2;
	private int gameID;
	
	public YearOfPlenty(){
		this.playerIndex = -1;
		this.resource1 = null;
		this.resource2 = null;
	}
	
	public YearOfPlenty(IFacade facade, YearOfPlentyParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.resource1 = params.getResource1();
		this.resource2 = params.getResource2();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if(facade == null) {return null;}
		return facade.yearOfPlenty(playerIndex, resource1, resource2, gameID);
	}

}
