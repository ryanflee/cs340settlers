package server.command.moves;

import server.command.ICommand;
import server.data.lazy.Game;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that sends a chat message.
 * @author Ryan F. Lee
 *
 */
public class SendChat implements ICommand {

	private IFacade facade;
	private int gameId;
	private int playerIndex;
	private String content;
	
	public SendChat(IFacade facade) {
		this.facade = facade;
		this.gameId = -1;
		this.playerIndex = -1;
		this.content = null;
	}
	
	public SendChat(IFacade facade, SendChatParams params, int gameId) {
		this.facade = facade;
		this.gameId = gameId;
		this.playerIndex = params.getPlayerIndex();
		this.content = params.getContent();
	}
	
	@Override
	public String execute() {
		return this.facade.sendChat(this.gameId, this.playerIndex, this.content);
	}

}
