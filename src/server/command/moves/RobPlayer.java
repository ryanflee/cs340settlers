package server.command.moves;

import server.command.ICommand;
import server.encoding.params.RobPlayerParams;
import server.facade.IFacade;
import shared.locations.HexLocation;

/**
 * Command object that allows one player to rob another.
 * @author Ryan F. Lee
 *
 */
public class RobPlayer implements ICommand {

	private int playerIndex;
	private int victimIndex;
	private HexLocation location;
	private int gameID;
	private IFacade facade;
	
	public RobPlayer() {
		
	}
	
	public RobPlayer(IFacade facade, RobPlayerParams params, int gameID) {
		this.facade = facade;
		this.gameID = gameID;
		this.playerIndex = params.getPlayerIndex();
		this.victimIndex = params.getVictimIndex();
		this.location = params.getLocation();
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.robPlayer(playerIndex, victimIndex, location, gameID);
	}

}
