package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that Discards a list of cards
 * @author Jake Mingus
 *
 */
public class DiscardCards implements ICommand {


	private IFacade facade;
	private int playerIndex;
	private int[] toDiscard;
	private int gameID;
	
	public DiscardCards() {
		this.playerIndex = -1;
		this.toDiscard = null;
	}
	
	public DiscardCards(IFacade facade, DiscardCardsParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.toDiscard = params.getToDiscard();
		this.gameID = gameID;
	}
	
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.discardCards(playerIndex, toDiscard, gameID);
	}

}
