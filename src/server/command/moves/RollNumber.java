package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that records the result of rolling the dice.
 * @author Ryan F. Lee
 *
 */
public class RollNumber implements ICommand {

	private int playerIndex;
	private int number;
	private IFacade facade;
	private int gameID;
	
	public RollNumber() {
		this.playerIndex = -1;
		this.number = -1;
	}
	
	public RollNumber(IFacade facade, RollNumberParams params, int gameID) {
		this.playerIndex = params.getPlayerIndex();
		this.number = params.getNumber();
		this.facade = facade;
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.rollNumber(playerIndex, number, gameID);
	}

}
