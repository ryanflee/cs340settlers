package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.locations.EdgeLocation;

/**
 * Command object that Builds a road on the model
 * @author jjwilbur
 *
 */
public class BuildRoad implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private EdgeLocation roadLocation;
	private boolean free;
	private int gameID;
	
	public BuildRoad(){
		this.playerIndex = -1;
		this.roadLocation = null;
		this.free = false;
	}
	
	public BuildRoad(IFacade facade, BuildRoadParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.roadLocation = params.getRoadLocation();
		this.free = params.isFree();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
	if (facade == null) { return null; }
		return facade.buildRoad(playerIndex, roadLocation, free, gameID);
	}


}
