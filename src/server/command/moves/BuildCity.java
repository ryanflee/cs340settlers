package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.locations.VertexLocation;

/**
 * Command object that builds a city
 * @author Jake Mingus
 *
 */
public class BuildCity implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private VertexLocation vertex;
	private int gameID;
	
	public BuildCity() {
		this.playerIndex = -1;
		this.vertex = null;
		this.facade = null;
	}
	
	public BuildCity(IFacade facade, BuildCityParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.vertex = params.getVertex();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.buildCity(playerIndex, vertex, gameID);
	}

}
