package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that Offers a trade
 * 
 */
public class OfferTrade implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private int[] resourceList;
	private int receiverIndex;
	private int gameID;
	
	public OfferTrade() {
		this.playerIndex = -1;
		this.resourceList = null;
		this.receiverIndex = -1;
	}
	
	public OfferTrade(IFacade facade, OfferTradeParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.resourceList = params.getResourceList();
		this.receiverIndex = params.getReceiverIndex();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.offerTrade(playerIndex, resourceList, receiverIndex, gameID);
	}

}
