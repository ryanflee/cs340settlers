package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command object that accepts/declines the offered trade
 * @author wintersoldier
 */
public class AcceptTrade implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private boolean willAccept;
	private int gameID;
	
	public AcceptTrade() {
		this.playerIndex = -1;
		this.willAccept = false;
	}
	
	public AcceptTrade(IFacade facade, AcceptTradeParams params, int gameID) {
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.willAccept = params.isWillAccept();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if (facade == null) { return null; }
		return facade.acceptTrade(playerIndex, willAccept, gameID);
	}

}
