package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;

/**
 * Command Object that lets a player play a monument development card.
 * @author jjwilbur
 *
 */
public class Monument implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private int gameID;
	
	public Monument(){
		this.playerIndex = -1;
	}
	
	public Monument (IFacade facade, MonumentParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if(facade == null){return null;}
		return facade.monument(playerIndex, gameID);
	}

}
