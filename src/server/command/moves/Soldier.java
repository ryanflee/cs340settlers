package server.command.moves;

import server.command.ICommand;
import server.encoding.params.*;
import server.facade.IFacade;
import shared.locations.HexLocation;

/**
 * Command object that lets the user play a soldier development card.
 * @author jjwilbur
 *
 */
public class Soldier implements ICommand {

	private IFacade facade;
	private int playerIndex;
	private int victimIndex;
	private HexLocation location;
	private int gameID;
	
	public Soldier(){
		this.playerIndex = -1;
		this.victimIndex = -1;
		this.location = null;
	}
	
	public Soldier(IFacade facade, SoldierParams params, int gameID){
		this.facade = facade;
		this.playerIndex = params.getPlayerIndex();
		this.victimIndex = params.getVictimIndex();
		this.location = params.getLocation();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		if(facade == null){return null;}
		return facade.soldier(playerIndex, victimIndex, location, gameID);
	}

}
