package server.command.games;

import server.command.ICommand;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.IFacade;

/**
 * Used to create a List Command that can be executed
 * @author kevin
 *
 */
public class List implements ICommand {
	private IFacade facade;
	
	public List(IFacade facade) {
		this.facade = facade;
	}

	@Override
	public String execute() {
		try {
			return facade.list();
		} catch (ServerFacadeException e) {
			e.printStackTrace();
			return null;
		}
	}
}
