package server.command.games;

import server.command.ICommand;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.IFacade;

/**
 * Create a Create Command that can be executed
 * @author kevin
 *
 */
public class Create implements ICommand {
	private IFacade facade;
	private CreateParams param;
	
	public Create(){}
	
	public Create(IFacade facade, CreateParams param){
		this.facade = facade;
		this.param = param;
	}

	@Override
	public String execute() throws ServerFacadeException {
		return facade.create(param.getName(), param.isRandomTiles(), param.isRandomNumbers(), param.isRandomPorts());
	}
}
