package server.command.games;

import server.command.ICommand;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.IFacade;

/**
 * Used to create a Join Command that can be executed
 * @author kevin
 *
 */
public class Join implements ICommand {
	private IFacade facade;
	private JoinParams param;
	
	public Join(IFacade facade, JoinParams param) {
		this.facade = facade;
		this.param = param;
	}

	@Override
	public String execute() throws ServerFacadeException {
		int valid = facade.join(param.getGameId(),param.getColor(), param.getName(), param.getPid(), param.hasCookie());

		return valid + "";
	}
}
