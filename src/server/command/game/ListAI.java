package server.command.game;

import java.util.List;

import server.command.ICommand;
import server.facade.IFacade;

/***
 * Command object that lists the available AI types.
 * @author Ryan F. Lee
 *
 */
public class ListAI implements ICommand {

	private IFacade facade;
	
	public ListAI(IFacade facade) {
		this.facade = facade;
	}
	
	@Override
	public String execute() {
		return facade.listAI();
	}

}
