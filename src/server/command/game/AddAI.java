package server.command.game;

import server.command.ICommand;
import server.encoding.params.AddAIParams;
import server.facade.IFacade;

/***
 * Command object that adds an AI to a game.
 * @author Ryan F. Lee
 *
 */
public class AddAI implements ICommand {

	private IFacade facade;
	private String aiType;
	private int gameID;
	
	public AddAI(IFacade facade) {
		this.facade = facade;
		this.aiType = "LARGEST_ARMY";
		this.gameID = -1;
	}
	
	public AddAI(IFacade facade, AddAIParams params, int gameID) {
		this.facade = facade;
		this.aiType = params.getAiType();
		this.gameID = gameID;
	}
	
	@Override
	public String execute() {
		return facade.addAI(aiType, gameID);
	}

}
