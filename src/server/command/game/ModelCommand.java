package server.command.game;

import server.command.ICommand;
import server.facade.IFacade;

/***
 * Command object that gets the current game model.
 * @author Ryan F. Lee
 *
 */
public class ModelCommand implements ICommand {

	private IFacade facade;
	private int gameId;
	
	public ModelCommand(IFacade facade, int gameId) {
		this.facade = facade;
		this.gameId = gameId;
	}
	
	@Override
	public String execute() {
		return facade.getModel(gameId);
	}

}
