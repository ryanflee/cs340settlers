package server.command.user;

import java.io.IOException;
import java.io.StringWriter;

import com.google.gson.stream.JsonWriter;

import client.facade.Facade;
import server.command.ICommand;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.*;

/**
 * Used to create a Login Command object that can be executed
 * @author kevin
 *
 */
public class Login implements ICommand {
	private LoginParams params;
	private IFacade facade;
	
	public Login() {}
	
	public Login(IFacade facade, LoginParams param) {
		this.params = param;
		this.facade = facade;
	}
	
	@Override
	public String execute() throws ServerFacadeException {
		try {
			int valid = facade.login(params.getUsername(), params.getPassword());
			if(valid != -1) {
				StringWriter out = new StringWriter();
				JsonWriter writer = new JsonWriter(out);
				try {
					writer.beginObject();
					writer.name("name").value(params.getUsername());
					writer.name("password").value(params.getPassword());
					writer.name("playerID").value(valid);
					writer.endObject();
					String ret = out.toString();
					writer.close();
					return ret;
				} catch (IOException e) {
					e.printStackTrace();
					throw new ServerFacadeException("Invalid Somethign happened bad");
				}
			}
			else {
				throw new ServerFacadeException("INvalid Username");
			}
		} catch (ServerFacadeException e) {
			e.printStackTrace();
			throw new ServerFacadeException("Invalid Somethign happened bad");
		}
	}

}
