package server.command.user;

import java.io.IOException;
import java.io.StringWriter;

import com.google.gson.stream.JsonWriter;

import server.command.ICommand;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.*;

/**
 * Creates a Register command object that can be executed
 * @author kevin
 *
 */
public class Register implements ICommand {
	private RegisterParams params;
	private IFacade facade;
	
	public Register(IFacade facade, RegisterParams params) {
		this.facade = facade;
		this.params = params;
	}
	@Override
	public String execute() throws ServerFacadeException {
		try {
			int valid = facade.register(params.getName(), params.getPassword());
			if(valid != -1) {
				StringWriter out = new StringWriter();
				JsonWriter writer = new JsonWriter(out);
				try {
					writer.beginObject();
					writer.name("name").value(params.getName());
					writer.name("password").value(params.getPassword());
					writer.name("playerID").value(valid);
					writer.endObject();
					String ret = out.toString();
					writer.close();
					return ret;
				} catch (IOException e) {
					e.printStackTrace();
					throw new ServerFacadeException("Something went wrong");
				}
			}
			else {
				throw new ServerFacadeException("Invalid username");
			}
		} catch (ServerFacadeException e) {
			e.printStackTrace();
			throw new ServerFacadeException("Something went wrong");
		}
	}

}
