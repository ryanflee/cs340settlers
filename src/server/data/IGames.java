package server.data;

import java.util.List;

import server.data.lazy.Game;
import shared.model.Model;

/**
 * Interface for a list of games.
 * @author Ryan F. Lee
 *
 */
public interface IGames {
	
	/**
	 * Method to save a game.
	 * @param gameId The unique ID of the game to be saved.
	 * @param model The current game model.
	 * @return {@code true} if the game is saved successfully, {@code false} otherwise.
	 */
	public boolean saveGame(int gameId, Game game);
	
	/**
	 * Method to load a game.
	 * @param gameId The unique ID of the game to be loaded.
	 * @return The saved model of the game.
	 */
	public Game loadGame(int gameId);
	
	/**
	 * Method to list all games currently stored.
	 * @return A list of games.
	 */
	public List<Game> getAllGames();

	/**
	 * Method to get a specified game from the list.
	 * @param gameId The ID of the game to get.
	 * @return The current model of the specified game.
	 */
	public Game getGameById(int gameID);

	/**
	 * Method to add a new game to the list.
	 * @param game
	 * @return {@code true} if the game was successfully added; {@code false} otherwise.
	 */
	public boolean addNewGame(Game game);
	
	/**
	 * Method to get the ID of the next game to be added.
	 * @return The (integer) ID that will be assigned to the next game added to the list.
	 */
	public int getNextId();
}
