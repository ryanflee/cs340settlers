package server.data.lazy;

import shared.definitions.CatanColor;
import shared.model.Model;
import shared.model.developmentcard.DevCard;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a single game on the server.
 * @author Ryan F. Lee
 *
 */
public class Game {
	private int gameId;
	private String name;
	private Model model;
	private Map<String, Integer> tradeOffer;
	private int tradeSender;
	private int tradeReceiver;
	
	public Game() {
		this.gameId = -1;
		this.model = null;
		this.tradeOffer = null;
		this.tradeSender = -1;
		this.tradeReceiver = -1;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTitle() {
		return getName();
	}
	
	public void setTitle(String name) {
		setName(name);
	}

	public Game(int gameId, Model model) {
		this.gameId = gameId;
		this.model = model;
		this.tradeOffer = null;
		this.tradeSender = -1;
		this.tradeReceiver = -1;
	}


	
	public Map<String, Integer> getTradeOffer() {
		return tradeOffer;
	}

	public void setTradeOffer(Map<String, Integer> offer) {
		this.tradeOffer = offer;
	}

	public int getTradeSender() {
		return tradeSender;
	}

	public void setTradeSender(int tradeSender) {
		this.tradeSender = tradeSender;
	}

	public int getTradeReceiver() {
		return tradeReceiver;
	}

	public void setTradeReceiver(int tradeReceiver) {
		this.tradeReceiver = tradeReceiver;
	}
	
	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the model
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(Model model) {
		this.model = model;
	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + gameId;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (gameId != other.gameId)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Game [gameId=" + gameId + ", model=" + model + "]";
	}

	public boolean getPlayer(int pid) {
		for(Player p: model.getPlayers()) {
			if(p != null)
				if(p.getId() == pid)
					return true;
		}
		return false;
	}

	public void addPlayer(String name2, int pid, CatanColor c) {
		//for(Player p: model.getPlayers())
		for(int i=0;i<4;i++)
			if(model.getPlayers().size()>i) 
				if(model.getPlayers().get(i) == null){
				model.getPlayers().set(i, new Player(pid,name2,c,0,15,4,5,new ArrayList<ResourceCard>(), new ArrayList<DevCard>(), false, false, 0, 0));
				return;
			}
		model.getPlayers().add(new Player(pid,name2,c,0,15,4,5,new ArrayList<ResourceCard>(), new ArrayList<DevCard>(), false, false, 0, 0));
	}

	public void setPlayerColor(int pid, CatanColor color) {
		int index = this.model.getPlayerIndex(pid);
		this.model.getPlayers().get(index).setColor(color);
	}
	
	
}
