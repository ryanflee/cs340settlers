package server.data.lazy;

import java.sql.Connection;

import server.data.*;


public class FakeDB implements IDatabase{
	private static GameList games;
	private static UserList users;
	private static Persist persist;
	
	public FakeDB() {
		games = new GameList();
		users = new UserList();
		persist = new Persist();
	}
	
	@Override
	public void initialize() {
		// TODO: add initialization code here
	}
	
	@Override
	public IUsers getUserDAO() {
		return users;
	}

	@Override
	public IGames getGamesDAO() {
		return games;
	}

	@Override
	public IPersist getPersistDAO() {
		return persist;
	}

	@Override
	public void startTransaction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endTransaction(boolean commit) {
		// TODO Auto-generated method stub
		
	}

}
