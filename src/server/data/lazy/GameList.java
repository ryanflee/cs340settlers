package server.data.lazy;

import java.util.*;

import server.data.IGames;
import shared.definitions.CatanColor;
import shared.model.Model;
import shared.model.developmentcard.DevCard;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

public class GameList implements IGames {

	private List<Game> games;

	public GameList() {
		games = new ArrayList<Game>();
	}
	
	@Override
	public boolean saveGame(int gameId, Game game) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Game loadGame(int gameId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Game> getAllGames() {
		return games;
	}

	@Override
	public boolean addNewGame(Game game) {

		int newIndex = games.size();
		return games.add(game);
	}

	@Override
	public int getNextId() {
		return games.size();
	}

	@Override
	public Game getGameById(int gameId) {
		if (gameId > -1 && gameId < games.size()) {
			return games.get(gameId);
		}

		return null;
	}
}
