package server.data.lazy;

import java.util.*;

import server.data.IUsers;

public class UserList implements IUsers {
	private ArrayList<User> users;	
	private Map<String,Integer> userMap;
	
	public UserList() {
		users = new ArrayList<User>();
		userMap = new HashMap<String, Integer>();
		/*users.add(new User("Sam","sam",0));
		users.add(new User("Brooke","brook",1));
		users.add(new User("Ken","ken",2));
		users.add(new User("Pete","pete",3));
		users.add(new User("aaa","asdff",4));
		users.add(new User("sss","asdff",5));
		users.add(new User("ddd","asdff",6));
		users.add(new User("fff","asdff",7));
		users.add(new User("proxy","proxy",8));
		
		userMap.put("Sam", 0);
		userMap.put("Brooke", 1);
		userMap.put("Ken", 2);
		userMap.put("Pete", 3);
		userMap.put("aaa", 4);
		userMap.put("sss", 5);
		userMap.put("ddd", 6);
		userMap.put("fff", 7);
		userMap.put("proxy", 8);*/
	}
	
	@Override
	public int validate(String name, String password) {
		if(!userMap.containsKey(name))
			return -1;
		if(!password.equals(users.get(userMap.get(name)).getPassword()))
			return -1;
		return users.get(userMap.get(name)).getId();
	}


	@Override
	public int register(String name, String password) {
		if(userMap.containsKey(name))
			return -1;
		users.add(new User(name,password,users.size()));
		userMap.put(name, users.size()-1);
		return users.size()-1;
	}

	@Override
	public void addUser(User u) {
		users.add(u);
		userMap.put(u.getUsername(), users.size() - 1);
	}

}
