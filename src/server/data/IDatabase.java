package server.data;

import java.sql.Connection;

public interface IDatabase {

	public void initialize();
	
	public IUsers getUserDAO();
	
	public IGames getGamesDAO();
	
	public IPersist getPersistDAO();
		
	public void startTransaction();
	
	public void endTransaction(boolean commit);
}
