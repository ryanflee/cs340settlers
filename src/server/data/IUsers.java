package server.data;

import server.data.lazy.User;

public interface IUsers {

	public int validate(String name, String password);
	public int register(String name, String password);
	public void addUser(User u);
}
