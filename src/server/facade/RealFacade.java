package server.facade;

import server.data.IDatabase;
import server.data.IGames;
import server.exceptions.*;
import server.data.lazy.*;
import server.encoding.ResponseEncoder;
import shared.definitions.CatanColor;
import shared.definitions.DevCardType;
import shared.definitions.HexType;
import shared.definitions.PortType;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.board.*;
import shared.model.developmentcard.DevCard;
import shared.model.exceptions.*;
import shared.model.logs.*;
import shared.model.pieces.*;
import shared.model.player.*;
import shared.model.resourcecard.ResourceCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RealFacade implements IFacade {
	
	private ResponseEncoder encoder;
	private IDatabase db;
	
	public RealFacade() {
		this.encoder = new ResponseEncoder();
		this.db = new FakeDB();
	}
	
	public RealFacade(ResponseEncoder encoder, IDatabase db) {
		this.encoder = encoder;
		this.db = db;
	}
	
	@Override
	public String acceptTrade(int playerIndex, boolean willAccept, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		
		List<HistoryEntry> history = m.getHistory();
		
		if(curGame.getTradeOffer() == null || curGame.getTradeReceiver() == -1 || curGame.getTradeSender() == -1) {
			return null;
		}
		
		Player receiver = m.getPlayers().get(playerIndex);
		String update;
		if(willAccept) {
			Player sender = m.getPlayers().get(curGame.getTradeSender());
			Map<String, Integer> offer = curGame.getTradeOffer();
			if(offer.get("brick") != 0) {
				ResourceCard brick = new ResourceCard(ResourceType.BRICK);
				for(int i = 0; i < Math.abs(offer.get("brick")); i++) {
					if(offer.get("brick") < 0) {
						if(receiver.getHand().contains(brick)) {
							receiver.getHand().remove(brick);
						}
						sender.getHand().add(brick);
					} else {
						if(sender.getHand().contains(brick)) {
							sender.getHand().remove(brick);
						}
						receiver.getHand().add(brick);
					}
				}
				
			}
			if(offer.get("ore") != 0) {
				ResourceCard ore = new ResourceCard(ResourceType.ORE);
				for(int i = 0; i < Math.abs(offer.get("ore")); i++) {
					if(offer.get("ore") < 0) {
						if(receiver.getHand().contains(ore)) {
							receiver.getHand().remove(ore);
						}
						sender.getHand().add(ore);
					} else {
						if(sender.getHand().contains(ore)) {
							sender.getHand().remove(ore);
						}
						receiver.getHand().add(ore);
					}
				}
			}
			if(offer.get("sheep") != 0) {
				ResourceCard sheep = new ResourceCard(ResourceType.SHEEP);
				for(int i = 0; i < Math.abs(offer.get("sheep")); i++) {
					if(offer.get("sheep") < 0) {
						if(receiver.getHand().contains(sheep)) {
							receiver.getHand().remove(sheep);
						}
						sender.getHand().add(sheep);
					} else {
						if(sender.getHand().contains(sheep)) {
							sender.getHand().remove(sheep);
						}
						receiver.getHand().add(sheep);
					}
				}
			}
			if(offer.get("wheat") != 0) {
				ResourceCard wheat = new ResourceCard(ResourceType.WHEAT);
				for(int i = 0; i < Math.abs(offer.get("wheat")); i++) {
					if(offer.get("wheat") < 0) {
						if(receiver.getHand().contains(wheat)) {
							receiver.getHand().remove(wheat);
						}
						sender.getHand().add(wheat);
					} else {
						if(sender.getHand().contains(wheat)) {
							sender.getHand().remove(wheat);
						}
						receiver.getHand().add(wheat);
					}
				}
			}
			if(offer.get("wood") != 0) {
				ResourceCard wood = new ResourceCard(ResourceType.WOOD);
				for(int i = 0; i < Math.abs(offer.get("wood")); i++) {
					if(offer.get("wood") < 0) {
						if(receiver.getHand().contains(wood)) {
							receiver.getHand().remove(wood);
						}
						sender.getHand().add(wood);
					} else {
						if(sender.getHand().contains(wood)) {
							sender.getHand().remove(wood);
						}
						receiver.getHand().add(wood);
					}
				}
			}
			
			update = "The trade was accepted.";
		} else {
			update = "The trade was not accepted.";
		}
		HistoryEntry entry = new HistoryEntry(receiver, update);
		history.add(entry);
		
		curGame.setTradeOffer(null);
		curGame.setTradeReceiver(-1);
		curGame.setTradeSender(-1);
		
		m.incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String addAI(String aiType, int gameID) {
		
		if (!aiType.equalsIgnoreCase("largest_army")) {
			return "Bad AI type: " + aiType;
		}
		
		db.startTransaction();
		Game g = db.getGamesDAO().getGameById(gameID);
		db.endTransaction(true);
		
		// don't allow more than four players
		if (g.getModel().getPlayers().size() >= 4) {
			return "Game already full.";
		}
		
		else {
			Player iRobot = new AIPlayer(aiType);
			
			// figure out color - can't have duplicates
			CatanColor aiColor = CatanColor.PUCE;
			boolean[] colorsTaken = new boolean[CatanColor.values().length];
			for (Player p : g.getModel().getPlayers()) {
				colorsTaken[p.getColor().ordinal()] = true;
			}
			
			// just take first unused color
			for (int i = 0; i < colorsTaken.length; i++) {
				if (!colorsTaken[i]) {
					aiColor = CatanColor.values()[i];
				}
			}
			
			// choose next AI player randomly
			int randAID = new Random(System.nanoTime()).nextInt(6);
			randAID = -(randAID+1);
			
			// don't add same player twice
			while (g.getPlayer(randAID)) {
				randAID = -(((randAID - 1) % 6)+1);
			}
			
			// add chosen player to the game
			if (!g.getPlayer(randAID)) {
				switch (randAID) {
				case -1:
					g.addPlayer("R. Daneel", -1, aiColor);
					break;
				case -2:
					g.addPlayer("KardashianFam", -2, aiColor);
					break;
				case -3:
					g.addPlayer("Dumbledore", -3, aiColor);
					break;
				case -4:
					g.addPlayer("Merlin", -4, aiColor);
					break;
				case -5:
					g.addPlayer("Kanye", -5, aiColor);
					break;
				default:
					g.addPlayer("GLaDOS", -6, aiColor);
					break;
				}
			}
			
			db.startTransaction();
			db.getGamesDAO().saveGame(gameID, g);
			db.endTransaction(true);
		}
		
		return "Success";
	}

	@Override
	public String buildCity(int playerIndex, VertexLocation vertex, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		City city = new City(vertex, playerIndex);
		Hex hex = m.getBoard().getHexByLocation(vertex.getHexLoc());

		hex.upgradeToCity(vertex, city);
		
		ResourceCard ore = new ResourceCard(ResourceType.ORE);
		ResourceCard wheat = new ResourceCard(ResourceType.WHEAT);
		for(int i = 0; i < 3; i++) {
			if(p.getHand().contains(ore)) {
				p.getHand().remove(ore);
				m.gameManager.getOre().add(ore);
			}
			if(i != 2 && p.getHand().contains(wheat)) {
				p.getHand().remove(wheat);
				m.gameManager.getWheat().add(wheat);
			}
		}
		p.setCities(p.getCities() - 1);
		p.setSettlements(p.getSettlements() + 1);
		p.setVictoryPoints(p.getVictoryPoints() + 1);
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " built a city."));
		
		m.incrementVersion();
		checkWinner(gameID);
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String buildRoad(int playerIndex, EdgeLocation roadLocation,
			boolean free, int gameID) {		
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		Road road = new Road(roadLocation, playerIndex);
		Hex hex = m.getBoard().getHexByLocation(roadLocation.getHexLoc());

		try{
			hex.addRoad(roadLocation, road);
		} catch (InvalidLocationException e) {
			e.printStackTrace();
			db.endTransaction(false);
			return null;
		}
		
		if(!free){
			ResourceCard brick = new ResourceCard(ResourceType.BRICK);
			ResourceCard wood = new ResourceCard(ResourceType.WOOD);
			if(p.getHand().contains(brick)){
				p.getHand().remove(brick);
				m.gameManager.getBrick().add(brick);
			}
			if(p.getHand().contains(wood)){
				p.getHand().remove(wood);
				m.gameManager.getWood().add(wood);
			}
		}
		p.setRoads(p.getRoads() - 1);
		
		//Longest road calculation
		int minIndex;
		int min;
		if(m.gameManager.getLongestRoad() == -1) {
			minIndex = playerIndex;
			min = p.getRoads();
		} else {
			minIndex = m.gameManager.getLongestRoad();
			min = m.getPlayers().get(minIndex).getRoads();
		}
		int ind = 0;
		for (Player player : m.getPlayers()) {
			if(player.getRoads() < min) {
				min = player.getRoads();
				minIndex = ind;
			}
			ind++;
		}
		if (min <= 10) {
			int oldIndex = m.gameManager.getLongestRoad();
			m.gameManager.setLongestRoad(minIndex);
			if(minIndex != oldIndex) {
				if(oldIndex != -1) {
					m.getPlayers().get(oldIndex)
					.setVictoryPoints(m.getPlayers().get(oldIndex).getVictoryPoints() - 2);
				}
				m.getPlayers().get(minIndex)
						.setVictoryPoints(m.getPlayers().get(minIndex).getVictoryPoints() + 2);
			}
			
		}
		
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " built a road."));
		
		m.incrementVersion();
		checkWinner(gameID);
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String buildSettlement(int playerIndex, VertexLocation vertex,
			boolean free, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		Settlement settlement = new Settlement(vertex, playerIndex);
		Hex hex = m.getBoard().getHexByLocation(vertex.getHexLoc());
		
		try {
			hex.addBuilding(vertex, settlement);
		} catch (InvalidLocationException e) {
			e.printStackTrace();
			db.endTransaction(false);
			return null;
		}
		
		if(!free) {
			ResourceCard sheep = new ResourceCard(ResourceType.SHEEP);
			ResourceCard wheat = new ResourceCard(ResourceType.WHEAT);
			ResourceCard brick = new ResourceCard(ResourceType.BRICK);
			ResourceCard wood = new ResourceCard(ResourceType.WOOD);
			if(p.getHand().contains(sheep)) {
				p.getHand().remove(sheep);
				m.gameManager.getSheep().add(sheep);
			}
			if(p.getHand().contains(wheat)) {
				p.getHand().remove(wheat);
				m.gameManager.getWheat().add(wheat);
			}
			if(p.getHand().contains(brick)) {
				p.getHand().remove(brick);
				m.gameManager.getBrick().add(brick);
			}
			if(p.getHand().contains(wood)) {
				p.getHand().remove(wood);
				m.gameManager.getWood().add(wood);
			}
		}
		p.setSettlements(p.getSettlements() - 1);
		p.setVictoryPoints(p.getVictoryPoints() + 1);
		
		if(m.gameManager.getStatus().equalsIgnoreCase("SecondRound")) {
			String curType = null;
			if(hex != null) { curType = hex.getHexType().toString(); }
			if(curType != null) {
				p.getHand().add(new ResourceCard(getResourceType(curType)));
				m.gameManager.getCardsByType(curType).remove(new ResourceCard(getResourceType(curType)));
			}
			HexLocation above = hex.getHexLocation().getNeighborLoc(EdgeDirection.North);
			HexLocation other = null;
			if(vertex.getDir() == VertexDirection.NorthEast) {
				other = hex.getHexLocation().getNeighborLoc(EdgeDirection.NorthEast);
			} else {
				other = hex.getHexLocation().getNeighborLoc(EdgeDirection.NorthWest);
			}
			hex = m.gameManager.getBoard().getHexByLocation(above);
			if(hex != null) { curType = hex.getHexType().toString(); }
			else { curType = null; }
			if(curType != null) {
				p.getHand().add(new ResourceCard(getResourceType(curType)));
				m.gameManager.getCardsByType(curType).remove(new ResourceCard(getResourceType(curType)));
			}
			hex = m.gameManager.getBoard().getHexByLocation(other);
			if(hex != null) { curType = hex.getHexType().toString(); }
			else { curType = null; }
			if(curType != null) {
				p.getHand().add(new ResourceCard(getResourceType(curType)));
				m.gameManager.getCardsByType(curType).remove(new ResourceCard(getResourceType(curType)));
			}
		}
		
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " built a settlement."));
		
		m.incrementVersion();
		checkWinner(gameID);
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		
		return encoder.encodeGame(curGame);
	}

	@Override
	public String buyDevCard(int playerIndex, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		ArrayList<DevCard> devCards;
		devCards = m.gameManager.getDevelopmentCards();
		if(devCards.size() == 0){
			db.endTransaction(false);
			return null;
		}
		
		ResourceCard sheep = new ResourceCard(ResourceType.SHEEP);
		ResourceCard wheat = new ResourceCard(ResourceType.WHEAT);
		ResourceCard ore = new ResourceCard(ResourceType.ORE);
		List<ResourceCard> pHand = p.getHand();
		
		if(pHand.contains(sheep)){
			pHand.remove(sheep);
			m.gameManager.getSheep().add(sheep);
		}
		if(pHand.contains(wheat)){
			pHand.remove(wheat);
			m.gameManager.getWheat().add(wheat);
		}
		if(pHand.contains(ore)){
			pHand.remove(ore);
			m.gameManager.getOre().add(ore);
		}
		p.setHand(pHand);
		
		ArrayList<DevCard> pDevCards = p.getDevCards();
		DevCard devCard = new DevCard(playerIndex, devCards.get(0).getType());
		devCard.setOwnedBy(p);
		devCard.setSameTurn(true);
		if (devCard.getType() == DevCardType.MONUMENT) {
			devCard.setSameTurn(false);
		}
		
		pDevCards.add(devCard);
		devCards.remove(0);	
	
		m.getHistory().add(new HistoryEntry(p, p.getName() + " just bought a development card."));
		
		p.setDevCards(pDevCards);
		m.gameManager.setDevelopmentCards(devCards);
		
		m.incrementVersion();
		curGame.setModel(m);
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}
	
	@Override
	public String create(String name, boolean randomTiles,
			boolean randomNumbers, boolean randomPorts) throws ServerFacadeException {
		if(name.equals("") || name == null)
			throw new ServerFacadeException();
		Game newGame = new Game();
		int[] hex_list = new int[] {0, 1,0,1,4, 2,4, 2, 3,1,3,3,2,0, 2,0,4,3};
		int[] toks=new int[]{4,11,8,3,9,12,5,10,11,5,6,2,9,4,10,6,3,8};
		ArrayList<Integer> hexes = new ArrayList<>();
		ArrayList<Integer> tokens = new ArrayList<>();
		for(int i=0;i<hex_list.length;i++) {
			hexes.add(hex_list[i]);
			tokens.add(toks[i]);
		}
		int desert = 0;
		long seed = System.nanoTime();
		if(randomTiles) {
			Collections.shuffle(hexes, new Random(seed));
			desert = (int)(Math.random()*19);
		}
		hexes.add(desert, 5);
		if(randomNumbers) {
			Collections.shuffle(tokens, new Random(seed));
		}
		tokens.add(desert,-1);
		Model m = new Model();
		List<Hex> hexList = new ArrayList<Hex>();
		int counter = 0;
		for(int y=-3;y<4;y++) {
			int start = -1*(y+3);
			if(start<-3)start = -3;
			int end = -1*(y-3);
			if(end>3)end = 3;
			for(int x=start;x<=end;x++) {
				//HexType type, HexLocation location, int number, boolean hasRobber
				Hex h;
				if(y == -3 || y == 3 || x == start || x == end) {
					HexType type = HexType.WATER;
					h = new Hex(type, new HexLocation(x,y),0,false);
				}
				else {
					HexType type = HexType.values()[hexes.get(counter)];
					h = new Hex(type, new HexLocation(x,y),tokens.get(counter++),type==HexType.DESERT);
				}
				hexList.add(h);
			}
		}
		Board b = new Board(hexList);
		PortType[] pt = new PortType[]{PortType.THREE,PortType.ORE,PortType.SHEEP,PortType.THREE,
				PortType.WHEAT,PortType.WOOD,PortType.THREE,PortType.THREE,PortType.BRICK};
		ArrayList<PortType> pts = new ArrayList<>();
		for(PortType t : pt)
			pts.add(t);
		if(randomPorts)
			Collections.shuffle(pts, new Random(System.nanoTime()));
		Map<EdgeLocation,PortType> ports = new HashMap<>();
		ports.put(new EdgeLocation(new HexLocation(0,3),  EdgeDirection.North), pts.get(0));
		ports.put(new EdgeLocation(new HexLocation(1,-3),  EdgeDirection.South), pts.get(1));
		ports.put(new EdgeLocation(new HexLocation(3,-1), EdgeDirection.NorthWest), pts.get(2));
		ports.put(new EdgeLocation(new HexLocation(-3,0), EdgeDirection.SouthEast), pts.get(3));
		ports.put(new EdgeLocation(new HexLocation(-1,-2),EdgeDirection.South), pts.get(4));
		ports.put(new EdgeLocation(new HexLocation(-3,2), EdgeDirection.NorthEast), pts.get(5));
		ports.put(new EdgeLocation(new HexLocation(2,1),  EdgeDirection.NorthWest), pts.get(6));
		ports.put(new EdgeLocation(new HexLocation(3,-3), EdgeDirection.SouthWest), pts.get(7));
		ports.put(new EdgeLocation(new HexLocation(-2,3), EdgeDirection.NorthEast), pts.get(8));
		b.setPorts(ports);
		m.setBoard(b);
		m.gameManager.setBoard(b);

		counter = 0;
		ArrayList<DevCard> deck = new ArrayList<>();
		deck.add(new DevCard(counter++, DevCardType.YEAR_OF_PLENTY));deck.add(new DevCard(counter++, DevCardType.YEAR_OF_PLENTY));
		deck.add(new DevCard(counter++, DevCardType.MONOPOLY));deck.add(new DevCard(counter++, DevCardType.MONOPOLY));
		deck.add(new DevCard(counter++, DevCardType.ROAD_BUILD));deck.add(new DevCard(counter++, DevCardType.ROAD_BUILD));
		for(int i=0;i<5;i++)
			deck.add(new DevCard(counter++, DevCardType.MONUMENT));
		for(int i=0;i<14;i++)
			deck.add(new DevCard(counter++, DevCardType.SOLDIER));
		Collections.shuffle(deck, new Random(System.nanoTime()));
		m.gameManager.setDevelopmentCards(deck);		
		
		ArrayList<ResourceCard> wood = new ArrayList<>();for(int i=0;i<19;i++)wood.add(new ResourceCard(ResourceType.WOOD));
		ArrayList<ResourceCard> brick = new ArrayList<>();for(int i=0;i<19;i++)brick.add(new ResourceCard(ResourceType.BRICK));
		ArrayList<ResourceCard> sheep = new ArrayList<>();for(int i=0;i<19;i++)sheep.add(new ResourceCard(ResourceType.SHEEP));
		ArrayList<ResourceCard> wheat = new ArrayList<>();for(int i=0;i<19;i++)wheat.add(new ResourceCard(ResourceType.WHEAT));
		ArrayList<ResourceCard> ore = new ArrayList<>();for(int i=0;i<19;i++)ore.add(new ResourceCard(ResourceType.ORE));
		m.gameManager.setWood(wood);
		m.gameManager.setBrick(brick);
		m.gameManager.setSheep(sheep);
		m.gameManager.setWheat(wheat);
		m.gameManager.setOre(ore);
		
		m.gameManager.setLargestArmy(-1);
		m.gameManager.setLongestRoad(-1);
		m.gameManager.setStatus("FirstRound");
		
		m.setVersion(0);
		
		newGame.setModel(m);
		newGame.setName(name);
		
		db.startTransaction();
		newGame.setGameId(db.getGamesDAO().getNextId());
		int id = newGame.getGameId();
		boolean success = db.getGamesDAO().addNewGame(newGame);
		db.endTransaction(true);
		
		return "{\"title\":\"" + name + "\",\"id\":" + id + ",\"players\":[{},{},{},{}]}";
	}

	@Override
	public String discardCards(int playerIndex, int[] toDiscard, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);
		
		ResourceCard sheep = new ResourceCard(ResourceType.SHEEP);
		ResourceCard wheat = new ResourceCard(ResourceType.WHEAT);
		ResourceCard brick = new ResourceCard(ResourceType.BRICK);
		ResourceCard wood = new ResourceCard(ResourceType.WOOD);
		ResourceCard ore = new ResourceCard(ResourceType.ORE);
		for(int i = 0; i < toDiscard[0]; i++) {
			if(p.getHand().contains(brick)) {
				p.getHand().remove(brick);
				m.gameManager.getBrick().add(brick);
			}
		}
		for(int i = 0; i < toDiscard[1]; i++) {
			if(p.getHand().contains(ore)) {
				p.getHand().remove(ore);
				m.gameManager.getOre().add(ore);
			}
		}
		for(int i = 0; i < toDiscard[2]; i++) {
			if(p.getHand().contains(sheep)) {
				p.getHand().remove(sheep);
				m.gameManager.getSheep().add(sheep);
			}
		}
		for(int i = 0; i < toDiscard[3]; i++) {
			if(p.getHand().contains(wheat)) {
				p.getHand().remove(wheat);
				m.gameManager.getWheat().add(wheat);
			}
		}
		for(int i = 0; i < toDiscard[4]; i++) {
			if(p.getHand().contains(wood)) {
				p.getHand().remove(wood);
				m.gameManager.getWood().add(wood);
			}
		}
		
		p.setDiscarded(true);
		boolean discarding = false;
		for(Player player : m.getPlayers()) {
			if(player.getDiscarded() == false)
				discarding = true;

		}
		if(discarding) {
			m.gameManager.setStatus("Discarding");
		} else {
			m.gameManager.setStatus("Robbing");
		}
		
		m.incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String finishTurn(int gameId, int playerIndex) {
		db.startTransaction();
		Game game = db.getGamesDAO().getGameById(gameId);
		db.endTransaction(true);
		
		Model m = game.getModel();
		Player p = m.getPlayers().get(playerIndex);
		
		// allow all devcards to be played next turn
		for (DevCard d : p.getDevCards()) {
			d.setSameTurn(false);
		}
		
		// reset player state for next turn
		p.setPlayedDevCard(false);
		
		m.getHistory().add(new HistoryEntry(p, p.getName() + "'s turn just ended"));
		
		// figure out which player is next
		int nextPlayer = (playerIndex + 1) % 4;
		if (m.gameManager.getStatus().equalsIgnoreCase("FirstRound")) {
			if (nextPlayer == 0) {
				nextPlayer = 3;
				m.gameManager.setStatus("SecondRound");
			}
		}
		else if (m.gameManager.getStatus().equalsIgnoreCase("SecondRound")) {
			nextPlayer = playerIndex - 1;
			if (nextPlayer == -1) {
				nextPlayer = 0;
				m.gameManager.setStatus("Rolling");
			}
		}
		else {
			m.gameManager.setStatus("Rolling");
		}
		m.gameManager.setCurrentPlayer(nextPlayer);
		Player np = m.getPlayers().get(nextPlayer);
		m.incrementVersion();
		
		while ((p.getId() > -1) && (np.getId() < 0)) {
			takeAITurn(game);
			int npIndex = m.gameManager.getCurrentPlayer();
			np = m.getPlayers().get(npIndex);
		}
		
		db.startTransaction();
		db.getGamesDAO().saveGame(gameId, game);
		db.endTransaction(true);
		
		return encoder.encodeGame(game);
	}
	
	private void takeAITurn(Game game) {
		
		Model m = game.getModel();
		int playerIndex = m.gameManager.getCurrentPlayer();
		int gameId = game.getGameId();
		
		while (m.gameManager.getCurrentPlayer() == playerIndex) {
			Player p = m.getPlayers().get(playerIndex);
			String status = m.gameManager.getStatus();
			
			if (status.equalsIgnoreCase("firstround") || status.equalsIgnoreCase("secondround")) {
				// place a road and a settlement
				boolean builtRoad = false;
				for (Hex h : m.getBoard().getAllHexes()) {
					if (h.getHexType() == null || h.getHexType() == HexType.WATER) {
						continue;
					}
					for (EdgeDirection edir : EdgeDirection.values())
					{
						EdgeLocation eloc = new EdgeLocation(h.getHexLocation(), edir).getNormalizedLocation();
						if (m.gameManager.canBuildRoad(playerIndex, eloc, true, false)) {
							buildRoad(playerIndex, eloc, true, gameId);
							builtRoad = true;
							break;
						}
					}
					if (builtRoad) {
						break;
					}
				}
				
				boolean builtSettlement = false;
				for (Hex h : m.getBoard().getAllHexes()) {
					if (h.getHexType() == null || h.getHexType() == HexType.WATER) {
						continue;
					}
					for (VertexDirection vdir : VertexDirection.values()) {
						VertexLocation vloc = new VertexLocation(h.getHexLocation(), vdir).getNormalizedLocation();
						if (m.gameManager.canBuildSettlement(playerIndex, vloc, true)) {
							buildSettlement(playerIndex, vloc, true, gameId);
							builtSettlement = true;
							break;
						}
					}
					
					if (builtSettlement) {
						break;
					}
				}
				
				m.getHistory().add(new HistoryEntry(p, p.getName() + "'s turn just ended"));
				
				// figure out which player is next
				int nextPlayer = (playerIndex + 1) % 4;
				if (m.gameManager.getStatus().equalsIgnoreCase("FirstRound")) {
					if (nextPlayer == 0) {
						nextPlayer = 3;
						m.gameManager.setStatus("SecondRound");
					}
				}
				else if (m.gameManager.getStatus().equalsIgnoreCase("SecondRound")) {
					nextPlayer = playerIndex - 1;
					if (nextPlayer == -1) {
						nextPlayer = 0;
						m.gameManager.setStatus("Rolling");
					}
				}
				else {
					m.gameManager.setStatus("Rolling");
				}
				m.gameManager.setCurrentPlayer(nextPlayer);
				
				m.incrementVersion();
			}
			
			else if (status.equalsIgnoreCase("rolling")) {
				Random dice = new Random(System.nanoTime());
				int firstDie = dice.nextInt(6)+1;
				int secondDie = dice.nextInt(6)+1;
				
				rollNumber(playerIndex, firstDie+secondDie, gameId);
			}
			
			else if (status.equalsIgnoreCase("discarding")) {
				discardCards(playerIndex, new int[] {0,0,0,0,0}, gameId);
			}
			
			else if (status.equalsIgnoreCase("robbing")) {
				for (Hex h : m.getBoard().getAllHexes()) {
					if (m.getBoard().getRobberHex().equals(h) || h.getHexType().equals(HexType.WATER) || (h.getHexType() == null)) {
						continue;
					}
					robPlayer(playerIndex, -1, h.getHexLocation(), gameId);
				}
			}
			
			else if (status.equalsIgnoreCase("playing")) {
				// just finish turn
//				finishTurn(playerIndex, gameId);
				m.getHistory().add(new HistoryEntry(p, p.getName() + "'s turn just ended"));
				
				// figure out which player is next
				int nextPlayer = (playerIndex + 1) % 4;
				if (m.gameManager.getStatus().equalsIgnoreCase("FirstRound")) {
					if (nextPlayer == 0) {
						nextPlayer = 3;
						m.gameManager.setStatus("SecondRound");
					}
				}
				else if (m.gameManager.getStatus().equalsIgnoreCase("SecondRound")) {
					nextPlayer = playerIndex - 1;
					if (nextPlayer == -1) {
						nextPlayer = 0;
						m.gameManager.setStatus("Rolling");
					}
				}
				else {
					m.gameManager.setStatus("Rolling");
				}
				m.gameManager.setCurrentPlayer(nextPlayer);
				
				m.incrementVersion();
			}
		}
	}

	@Override
	public String getModel(int gameId) {
		db.startTransaction();
		Game game = db.getGamesDAO().getGameById(gameId);
		db.endTransaction(true);
		
		return encoder.encodeGame(game);
	}

	@Override
	public int join(int gameId, CatanColor color, String name, int pid, boolean hasCookie) throws ServerFacadeException {
		db.startTransaction();
		Game game = db.getGamesDAO().getGameById(gameId);
		db.endTransaction(true);
		int count = 0;
		for(Player p : game.getModel().getPlayers()) {
			if (p == null) { break; }
			count++;
		}
		if(count < 4 && !game.getPlayer(pid)) {
			game.addPlayer(name,pid, color);
			return game.getGameId();
		}
		else if(game.getPlayer(pid)) {
			game.setPlayerColor(pid,color);
			return game.getGameId();
		}
		throw new ServerFacadeException();	
	}

	@Override
	public String list() throws ServerFacadeException {
		try {
			db.startTransaction();
			List<Game> games = db.getGamesDAO().getAllGames();
			db.endTransaction(true);
			if(games == null)return "[]";
			if(games.size() == 0)return "[]";
			String result = "[";
			System.out.println(games.size() + ": size");
			for(Game g : games) {
				result += "{" + "\"title\": \"" + g.getName() + "\",\"id\": " + g.getGameId() + ",\"players\": [";
				List<Player> p = g.getModel().getPlayers();
				for(int i=0; i<4; i++) {
					if(i>=p.size())
						result += "{},";
					
					else if(p.get(i) == null) {
						result += "{},";
					}
					else {
						Player x = p.get(i);
						result += "{\"color\": \"" + x.getColor().toJson() + "\", \"name\": \"" + x.getName() + "\", \"id\": " + x.getId() + "},";
					}	
				}
				result = result.substring(0, result.length()-1) + "]},";
			}
			result = result.substring(0, result.length()-1) + "]";
			return result;
		}
		catch(Exception e) {
			throw new ServerFacadeException("Error in the server");
		}
	}

	@Override
	public String listAI() {
		
		return encoder.encodeAIList();
	}

	@Override
	public int login(String username, String password) throws ServerFacadeException {
		try {
			db.startTransaction();
			int valid = db.getUserDAO().validate(username,password);
			db.endTransaction(true);
			return valid;	
		}
		catch(Exception e) {
			throw new ServerFacadeException("Error in the server");
		}
	}

	@Override
	public String maritimeTrade(int playerIndex, int ratio, String input,
			String output, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);
		ResourceType inResource = getResourceType(input);
		ResourceType outResource = getResourceType(output);
		
		if(ratio == -1) { ratio = 4; }
		for(int i = 0; i < ratio; i++) {
			if(p.getHand().contains(new ResourceCard(inResource))) {
				p.getHand().remove(new ResourceCard(inResource));
			}
		}
		p.getHand().add(new ResourceCard(outResource));
		switch(output) {
		case "brick":
			if(m.gameManager.getBrick().size() > 0) { m.gameManager.getBrick().remove(new ResourceCard(outResource)); }
		case "ore":
			if(m.gameManager.getOre().size() > 0) { m.gameManager.getOre().remove(new ResourceCard(outResource)); }
		case "sheep":
			if(m.gameManager.getSheep().size() > 0) { m.gameManager.getSheep().remove(new ResourceCard(outResource)); }
		case "wheat":
			if(m.gameManager.getWheat().size() > 0) { m.gameManager.getWheat().remove(new ResourceCard(outResource)); }
		case "wood":
			if(m.gameManager.getWood().size() > 0) { m.gameManager.getWood().remove(new ResourceCard(outResource)); }
		}
		
		m.incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}
	
	private ResourceType getResourceType(String type) {
		switch(type) {
		case "brick":
			return ResourceType.BRICK;
		case "ore":
			return ResourceType.ORE;
		case "sheep":
			return ResourceType.SHEEP;
		case "wheat":
			return ResourceType.WHEAT;
		case "wood":
			return ResourceType.WOOD;
		}
		return null;
	}
	
	@Override
	public String monopoly(int playerIndex, String resource, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		for(int i = 0; i < p.getDevCards().size(); i++) {
			if(p.getDevCards().get(i).getType() == DevCardType.MONOPOLY) {
				p.getDevCards().remove(i);
				break;
			}
		}
		
		ResourceCard monopolyCard = new ResourceCard(getResourceType(resource));
		
		List<Player> players = m.getPlayers();
		List<ResourceCard> cards;
		int cardCounter = 0;
		for(int i = 0; i < players.size(); i++){
			if(i == playerIndex){
				continue;
			}
			cards = players.get(i).getHand();
			int curCount = 0;
			for(int j = 0; j < cards.size(); j++){
				if (cards.get(j).equals(monopolyCard)){
					curCount++;
					cardCounter++;
				}
			}
			for(int j = 0; j < curCount; j++) {
				if(cards.contains(monopolyCard)) {
					cards.remove(monopolyCard);
				}
			}
			
			players.get(i).setHand(cards);
		}
		
		List<ResourceCard> pHand = p.getHand();
		for(int i = 0; i < cardCounter; i++){
			pHand.add(monopolyCard);
		}
		p.setHand(pHand);
		p.setPlayedDevCard(true);
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " played a monopoly development card"));
		m.incrementVersion();
				
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		
		return encoder.encodeGame(curGame);
	}

	@Override
	public String monument(int playerIndex, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		for(int i = 0; i < p.getDevCards().size(); i++) {
			if(p.getDevCards().get(i).getType() == DevCardType.MONUMENT) {
				p.getDevCards().remove(i);
				break;
			}
		}
		
		p.setVictoryPoints(p.getVictoryPoints() + 1);
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " played a monument development card"));

		m.incrementVersion();
		checkWinner(gameID);
				
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		
		return encoder.encodeGame(curGame);
	}

	@Override
	public String offerTrade(int playerIndex, int[] resourceList,
			int receiverIndex, int gameID) {
		db.startTransaction();
		Game game = db.getGamesDAO().getGameById(gameID);
		
		Map<String, Integer> offer = new HashMap<String, Integer>();
		offer.put("brick", resourceList[0]);
		offer.put("ore", resourceList[1]);
		offer.put("sheep", resourceList[2]);
		offer.put("wheat", resourceList[3]);
		offer.put("wood", resourceList[4]);
		game.setTradeOffer(offer);
		game.setTradeSender(playerIndex);
		game.setTradeReceiver(receiverIndex);
		
		game.getModel().incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, game);
		db.endTransaction(true);
		
		return encoder.encodeGame(game);
	}

	@Override
	public int register(String username, String password) throws ServerFacadeException {
		try {
			db.startTransaction();
			int valid = db.getUserDAO().register(username,password);
			db.endTransaction(true);
			return valid;	
		}
		catch(Exception e) {
			throw new ServerFacadeException("Error in the server");
		}
	}

	@Override
	public String robPlayer(int playerIndex, int victimIndex,
			HexLocation location, int gameID) {
		
		//Do I have to move the robber??
		
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);
		Player victim = null;

		List<HistoryEntry> history = m.getHistory();
		Hex hex = m.gameManager.getBoard().getHexByLocation(location);
		m.gameManager.getBoard().setRobberHex(hex);

		boolean canRob = true;
		if(victimIndex == -1) { canRob = false; }
		else {
			victim = m.getPlayers().get(victimIndex);
			if(victim.getHand().size() == 0) {
				canRob = false;
			}
		}
		if(!canRob) {
			m.incrementVersion();
			history.add(new HistoryEntry(p, p.getName() + " moved the robber but couldn't rob anyone!"));
			m.gameManager.setStatus("Playing");
			db.getGamesDAO().saveGame(gameID, curGame);
			db.endTransaction(true);
			return encoder.encodeGame(curGame);
		}
		
		int randomIndex = new Random().nextInt(victim.getHand().size());
		ResourceCard stolen = victim.getHand().get(randomIndex);
		victim.getHand().remove(randomIndex);
		p.getHand().add(stolen);
		
		history.add(new HistoryEntry(p, p.getName() + " moved the robber and robbed " + victim.getName()));
		m.incrementVersion();
		
		m.gameManager.setStatus("Playing");
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String rollNumber(int playerIndex, int number, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Board board = m.gameManager.getBoard();
		Player p = m.getPlayers().get(playerIndex);
		
		if(number == 7) {
			boolean discarding = false;
			for(Player player : m.getPlayers()) {
				if(player != null && player.getHand().size() > 7) {
					discarding = true;
					player.setDiscarded(false);
				}else if(player.getHand().size() > 7){
					player.setDiscarded(false);
				}else {
					player.setDiscarded(true);
				}
			}
			if(discarding) {
				m.gameManager.setStatus("Discarding");
			} else {
				m.gameManager.setStatus("Robbing");
			}
		} else {
			m.gameManager.setStatus("Playing");
			for(Hex hex : board.getHexesByNumber(number)) {
				ResourceType type = getResourceType(hex.getHexType().toString());
				for(Building b : board.getAssociatedBuildings(hex.getHexLocation())) {
					int index = b.getOwner();
					Player owner = m.getPlayers().get(index);
					if(b instanceof City) {
						owner.getHand().add(new ResourceCard(type));
						owner.getHand().add(new ResourceCard(type));
						if(m.gameManager.getCardsByType(type.toJson()).contains(new ResourceCard(type))){
							m.gameManager.getCardsByType(type.toJson()).remove(new ResourceCard(type));
						}
						if(m.gameManager.getCardsByType(type.toJson()).contains(new ResourceCard(type))){
							m.gameManager.getCardsByType(type.toJson()).remove(new ResourceCard(type));
						}
					}
					if(b instanceof Settlement) {
						owner.getHand().add(new ResourceCard(type));
						if(m.gameManager.getCardsByType(type.toJson()).contains(new ResourceCard(type))){
							m.gameManager.getCardsByType(type.toJson()).remove(new ResourceCard(type));
						}
					}
				}
			}
		}
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " rolled a " + number + "."));
		
		m.incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		
		return encoder.encodeGame(curGame);
	}

	@Override
	public String sendChat(int gameId, int playerIndex, String content) {
		
		db.startTransaction();
		Game game = db.getGamesDAO().getGameById(gameId);
		db.endTransaction(true);
		
		Model m = game.getModel();
		
		Player p = m.getPlayers().get(playerIndex);
		ChatEntry newEntry = new ChatEntry(p, content);
		m.getComments().add(newEntry);
		
		m.incrementVersion();
		game.setModel(m);
		
		db.startTransaction();
		db.getGamesDAO().saveGame(gameId, game);
		db.endTransaction(true);
		
		return encoder.encodeGame(game);
	}

	@Override
	public String soldier(int playerIndex, int victimIndex, HexLocation location, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);
		Player victim = null;
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " played a soldier development card."));
		p.setSoldiers(p.getSoldiers() + 1);
		
		Hex hex = m.gameManager.getBoard().getHexByLocation(location);
		m.gameManager.getBoard().setRobberHex(hex);

		for(int i = 0; i < p.getDevCards().size(); i++) {
			if(p.getDevCards().get(i).getType() == DevCardType.SOLDIER) {
				p.getDevCards().remove(i);
				break;
			}
		}

		boolean canRob = true;
		if(victimIndex == -1) { canRob = false; }
		else {
			victim = m.getPlayers().get(victimIndex);
			if(victim.getHand().size() == 0) {
				canRob = false;
			}
		}
		if(!canRob) {
			m.incrementVersion();
			history.add(new HistoryEntry(p, p.getName() + " moved the robber but couldn't rob anyone!"));
			m.gameManager.setStatus("Playing");
			db.getGamesDAO().saveGame(gameID, curGame);
			db.endTransaction(true);
			return encoder.encodeGame(curGame);
		}

		
		//Largest army calculation
		int maxIndex;
		int max;
		if(m.gameManager.getLargestArmy() == -1) {
			maxIndex = playerIndex;
			max = p.getSoldiers();
		} else {
			maxIndex = m.gameManager.getLargestArmy();
			max = m.getPlayers().get(maxIndex).getSoldiers();
		}
		int ind = 0;
		for (Player player : m.getPlayers()) {
			if(player.getSoldiers() > max) {
				max = player.getSoldiers();
				maxIndex = ind;
			}
			ind++;
		}
		if (max >= 3) {
			int oldIndex = m.gameManager.getLargestArmy();
			m.gameManager.setLargestArmy(maxIndex);
			if (maxIndex != oldIndex) {
				if(oldIndex != -1) {
					m.getPlayers().get(oldIndex)
							.setVictoryPoints(m.getPlayers().get(oldIndex).getVictoryPoints() - 2);
				}
				m.getPlayers().get(maxIndex)
						.setVictoryPoints(m.getPlayers().get(maxIndex).getVictoryPoints() + 2);
			}

		}
		
		int randomIndex = new Random().nextInt(victim.getHand().size());
		ResourceCard stolen = victim.getHand().get(randomIndex);
		victim.getHand().remove(randomIndex);
		p.getHand().add(stolen);
		p.setPlayedDevCard(true);
		
		history.add(new HistoryEntry(p, p.getName() + " moved the robber and robbed " + victim.getName()));
		m.incrementVersion();
		checkWinner(gameID);
		
		m.gameManager.setStatus("Playing");
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}
	
	public int validate(String username, String password) {
		db.startTransaction();
		int valid = (db.getUserDAO().validate(username, password));
		db.endTransaction(true);
		return valid;
	}

	@Override
	public String yearOfPlenty(int playerIndex, ResourceType resource1,
			ResourceType resource2, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);
		
//		ResourceCard card1 = new ResourceCard(resource1);
//		ResourceCard card2 = new ResourceCard(resource2);
		
//		ArrayList<ResourceCard> brick = m.gameManager.getBrick();
//		ArrayList<ResourceCard> ore = m.gameManager.getOre();
//		ArrayList<ResourceCard> sheep = m.gameManager.getSheep();
//		ArrayList<ResourceCard> wheat = m.gameManager.getWheat();
//		ArrayList<ResourceCard> wood = m.gameManager.getWood();

		m.gameManager.getCardsByType(resource1.toJson()).add(new ResourceCard(resource1));
		m.gameManager.getCardsByType(resource2.toJson()).add(new ResourceCard(resource2));

//		List<ResourceCard> pHand = p.getHand();
//		if(card1.getType() == ResourceType.BRICK && brick.size() > 0){brick.remove(card1);}
//		else if(card1.getType() == ResourceType.ORE && ore.size() > 0){ore.remove(card1);}
//		else if(card1.getType() == ResourceType.SHEEP && sheep.size() > 0){sheep.remove(card1);}
//		else if(card1.getType() == ResourceType.WHEAT && wheat.size() > 0){wheat.remove(card1);}
//		else if(card1.getType() == ResourceType.WOOD && wood.size() > 0){wood.remove(card1);}
//
//		if(card2.getType() == ResourceType.BRICK && brick.size() > 0){brick.remove(card2);}
//		else if(card2.getType() == ResourceType.ORE && ore.size() > 0){ore.remove(card2);}
//		else if(card2.getType() == ResourceType.SHEEP && sheep.size() > 0){sheep.remove(card2);}
//		else if(card2.getType() == ResourceType.WHEAT && wheat.size() > 0){wheat.remove(card2);}
//		else if(card2.getType() == ResourceType.WOOD && wood.size() > 0){wood.remove(card2);}
//
//		m.gameManager.setBrick(brick);
//		m.gameManager.setOre(ore);
//		m.gameManager.setSheep(sheep);
//		m.gameManager.setWheat(wheat);
//		m.gameManager.setWood(wood);
		
		p.getHand().add(new ResourceCard(resource1));
		p.getHand().add(new ResourceCard(resource2));

		for(int i = 0; i < p.getDevCards().size(); i++) {
			if(p.getDevCards().get(i).getType() == DevCardType.YEAR_OF_PLENTY) {
				p.getDevCards().remove(i);
				break;
			}
		}
		
		p.setPlayedDevCard(true);
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " played a year of plenty development card."));
		
		m.incrementVersion();
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	@Override
	public String roadBuilding(int playerIndex, EdgeLocation spot1,
			EdgeLocation spot2, int gameID) {
		db.startTransaction();
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		Player p = m.getPlayers().get(playerIndex);

		Road road1 = new Road(spot1, playerIndex);
		Hex hex1 = m.getBoard().getHexByLocation(spot1.getHexLoc());
		Road road2 = new Road(spot2, playerIndex);
		Hex hex2 = m.getBoard().getHexByLocation(spot2.getHexLoc());
		
		try{
			hex1.addRoad(spot1, road1);
			hex2.addRoad(spot2, road2);
		} catch (InvalidLocationException e) {
			e.printStackTrace();
			db.endTransaction(false);
			return null;
		}		
		p.setRoads(p.getRoads() - 2);

		for(int i = 0; i < p.getDevCards().size(); i++) {
			if(p.getDevCards().get(i).getType() == DevCardType.ROAD_BUILD) {
				p.getDevCards().remove(i);
				break;
			}
		}
		
		p.setPlayedDevCard(true);
		
		List<HistoryEntry> history = m.getHistory();
		history.add(new HistoryEntry(p, p.getName() + " played a road building development card and placed two roads."));

		//Longest road calculation
		int minIndex;
		int min;
		if(m.gameManager.getLargestArmy() == -1) {
			minIndex = playerIndex;
			min = p.getRoads();
		} else {
			minIndex = m.gameManager.getLongestRoad();
			min = m.getPlayers().get(minIndex).getRoads();
		}
		int ind = 0;
		for (Player player : m.getPlayers()) {
			if(player.getRoads() < min) {
				min = player.getRoads();
				minIndex = ind;
			}
			ind++;
		}
		if (min <= 10) {
			int oldIndex = m.gameManager.getLongestRoad();
			m.gameManager.setLongestRoad(minIndex);
			if(minIndex != oldIndex) {
				if(oldIndex != -1) {
					m.getPlayers().get(oldIndex)
							.setVictoryPoints(m.getPlayers().get(oldIndex).getVictoryPoints() - 2);
				}
				m.getPlayers().get(minIndex)
						.setVictoryPoints(m.getPlayers().get(minIndex).getVictoryPoints() + 2);
			}
		}
		
		m.incrementVersion();
		checkWinner(gameID);
		
		db.getGamesDAO().saveGame(gameID, curGame);
		db.endTransaction(true);
		return encoder.encodeGame(curGame);
	}

	private void checkWinner(int gameID) {
		Game curGame = db.getGamesDAO().getGameById(gameID);
		Model m = curGame.getModel();
		int index = 0;
		for(Player p : m.getPlayers()) {
			if(p.getVictoryPoints() >= 10) {
				m.setWinner(index);
				m.incrementVersion();
				break;
			}
			index++;
		}
		db.getGamesDAO().saveGame(gameID, curGame);
	}
	
	public IDatabase getDB() {
		return db;
	}
}
