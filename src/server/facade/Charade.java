package server.facade;

import server.data.IDatabase;
import server.data.lazy.FakeDB;
import server.data.lazy.Game;
import server.encoding.ResponseEncoder;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.Model;

public class Charade implements IFacade {

	private ResponseEncoder encoder;
	private IDatabase testDB;
	private Game testGame;
	
	public Charade() {
		this.encoder = new ResponseEncoder();
		this.testDB = new FakeDB();
		testDB.initialize();
		this.testGame = new Game();
	}
	
	@Override
	public String buyDevCard(int playerIndex, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String yearOfPlenty(int playerIndex, ResourceType resource1,
			ResourceType resource2, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String soldier(int playerIndex, int victimIndex, HexLocation location, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String monopoly(int playerIndex, String resource, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String monument(int playerIndex, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String buildRoad(int playerIndex, EdgeLocation roadLocation,
			boolean free, int gameID) {
		return encoder.encodeGame(testGame);
	}
	
	@Override
	public String buildSettlement(int playerIndex, VertexLocation vertex,
			boolean free, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String buildCity(int playerIndex, VertexLocation vertex, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String offerTrade(int playerIndex, int[] resourceList,
			int receiverIndex, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String acceptTrade(int playerIndex, boolean willAccept, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String maritimeTrade(int playerIndex, int ratio, String input,
			String output, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String discardCards(int playerIndex, int[] toDiscard, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public int login(String user, String password) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int register(String user, String password) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String list() {
		return encoder.encodeGames(testDB.getGamesDAO().getAllGames());
	}

	@Override
	public String create(String name, boolean randomTiles,
			boolean randomNumbers, boolean randomPorts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int join(int gameId, CatanColor color, String n, int i, boolean b) {
		return -1;
	}

	@Override
	public String sendChat(int gameId, int playerIndex, String content) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String rollNumber(int playerIndex, int number, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String addAI(String aiType, int gameID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listAI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String finishTurn(int gameId, int playerIndex) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String robPlayer(int playerIndex, int victimIndex,
			HexLocation location, int gameID) {
		return encoder.encodeGame(testGame);
	}

	@Override
	public String getModel(int gameId) {
		return encoder.encodeGame(testGame);
	}
	
	public int validate(String username, String password) {
		return -1;
	}

	@Override
	public String roadBuilding(int playerIndex, EdgeLocation spot1,
			EdgeLocation spot2, int gameID) {
		return encoder.encodeGame(testGame);
	}

	public IDatabase getDB() {
		return null;
	}
}
