package server.facade;

import server.data.IDatabase;
import server.data.lazy.Game;
import server.exceptions.ServerFacadeException;
import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;

public interface IFacade {
	
	public int login(String user, String password) throws ServerFacadeException;
	public int register(String user, String password) throws ServerFacadeException;
	public String list() throws ServerFacadeException;
	public String create(String name, boolean randomTiles, boolean randomNumbers, boolean randomPorts) throws ServerFacadeException;
	public int join(int gameId, CatanColor color, String name, int id, boolean hasCookie) throws ServerFacadeException;
	public String buyDevCard(int playerIndex, int gameID);
	public String yearOfPlenty(int playerIndex, ResourceType resource1, ResourceType resource2, int gameID);
	public String soldier(int playerIndex, int victimIndex, HexLocation location, int gameID);
	public String monopoly(int playerIndex, String resource, int gameID);
	public String monument(int playerIndex, int gameID);
	public String roadBuilding(int playerIndex, EdgeLocation spot1, EdgeLocation spot2, int gameID);
	public String buildRoad(int playerIndex, EdgeLocation roadLocation, boolean free, int gameID);
	public String buildSettlement(int playerIndex, VertexLocation vertex, boolean free, int gameID);
	public String buildCity(int playerIndex, VertexLocation vertex, int gameID);
	public String offerTrade(int playerIndex, int[] resourceList, int receiverIndex, int gameID);
	public String acceptTrade(int playerIndex, boolean willAccept, int gameID);
	public String maritimeTrade(int playerIndex, int ratio, String input, String output, int gameID);
	public String discardCards(int playerIndex, int[] toDiscard, int gameID);
	public String sendChat(int gameId, int playerIndex, String content);
	public String rollNumber(int playerIndex, int number, int gameID);
	public String addAI(String aiType, int gameID);
	public String listAI();
	public String finishTurn(int gameID, int playerIndex);
	public String robPlayer(int playerIndex, int victimIndex, HexLocation location, int gameID);
	public String getModel(int gameID);
	public int validate(String username, String password);

	public IDatabase getDB();
}
