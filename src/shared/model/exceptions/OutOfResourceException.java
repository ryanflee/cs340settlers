package shared.model.exceptions;

public class OutOfResourceException extends Exception {
	private String message;
	
	public OutOfResourceException() {}
	
	public OutOfResourceException(String message) {
		this.message = message;
	}
}
