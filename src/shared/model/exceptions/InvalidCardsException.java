package shared.model.exceptions;

public class InvalidCardsException extends Exception {
	private String message;
	
	public InvalidCardsException(String message) {
		this.message = message;
	}
}
