package shared.model.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import shared.definitions.*;
import shared.locations.*;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.*;

/**
 * Represents a complete Catan board, with Hexes and NumberTokens. 
 * @author Ryan F. Lee
 *
 */
public class Board {

	private List<Hex> allHexes;
	private Map<HexLocation, Hex> locsToHexes;
	private Map<Integer, List<Hex>> tokensToHexes;
	private Map<Hex, Integer> hexesToTokens;
	private Map<EdgeLocation, PortType> ports;
	private Hex robberLocation;

	public Board() {
		this.allHexes = new ArrayList<Hex>();
		this.locsToHexes = new HashMap<HexLocation, Hex>();
		this.tokensToHexes = new HashMap<Integer, List<Hex>>();
		this.hexesToTokens = new HashMap<Hex, Integer>();
		this.robberLocation = new Hex();
	}

	public Board(List<Hex> hexes) {
		this.allHexes = hexes;
		this.ports = new HashMap<EdgeLocation, PortType>();
		this.locsToHexes = new HashMap<HexLocation, Hex>();
		this.tokensToHexes = new HashMap<Integer, List<Hex>>();
		this.hexesToTokens = new HashMap<Hex, Integer>();

		// index all the hexes in the list
		for (Hex h : hexes) {
			HexLocation hloc = h.getHexLocation();
			int number = h.getNumberToken();

			// keys should be unique - one location per Hex
			this.locsToHexes.put(hloc, h);

			// have to handle this one differently to avoid overwriting 
			// other hexes with the same number token (if any)
			if (!tokensToHexes.containsKey(number)) {
				List<Hex> hexList = new ArrayList<Hex>();
				hexList.add(h);
				tokensToHexes.put(number, hexList);
			}
			else {
				List<Hex> hexList = tokensToHexes.get(number);
				hexList.add(h);
				tokensToHexes.put(number, hexList);
			}

			// keys should be unique - each Hex has one number
			this.hexesToTokens.put(h, number);

			// check for the Robber
			if (h.getHasRobber()) {
				this.robberLocation = h;
			}
		}
	}

	public void addPort(EdgeLocation key, PortType value) {
		if(!ports.containsKey(key))
			ports.put(key, value);
	}

	public void setPorts(Map<EdgeLocation, PortType> newPorts) {
		this.ports = newPorts;
	}

	public Map<EdgeLocation, PortType> getPorts() {
		return ports;
	}
	
	/**
	 * Gets all {@code Building} objects associated with the given {@code HexLocation}, 
	 * even if their normalized location technically places them on another hex. This behavior 
	 * differs from {@code Hex.getAllBuildings()}, which only returns buildings whose normalized
	 * locations place them on a particular hex. 
	 * @param hexLoc The HexLocation to retrieve associated buildings for.
	 * @return A list of all {@code Building} object associated with the {@code HexLocation}.
	 */
	public List<Building> getAssociatedBuildings(HexLocation hexLoc) {
		
		List<VertexLocation> vertices = new ArrayList<VertexLocation>();
		
		vertices.add(new VertexLocation(hexLoc, VertexDirection.NorthWest).getNormalizedLocation());
		vertices.add(new VertexLocation(hexLoc, VertexDirection.NorthEast).getNormalizedLocation());
		vertices.add(new VertexLocation(hexLoc, VertexDirection.East).getNormalizedLocation());
		vertices.add(new VertexLocation(hexLoc, VertexDirection.SouthEast).getNormalizedLocation());
		vertices.add(new VertexLocation(hexLoc, VertexDirection.SouthWest).getNormalizedLocation());
		vertices.add(new VertexLocation(hexLoc, VertexDirection.West).getNormalizedLocation());
		
		List<Building> result = new ArrayList<Building>();
		
		for (VertexLocation vloc : vertices) {
			Hex h = getHexByLocation(vloc.getHexLoc());
			if (h != null) {
				Building b = h.getBuilding(vloc.getDir());
				if (b != null) {
					result.add(b);
				}
			}
		}
		
		return result;
	}

	/**
	 * Returns hex given a Hex Location object
	 * @param hl - Hex location object
	 * @return - Hex
	 * @throws InvalidLocationException - Invalid location, hex not found
	 */
	public Hex getHex(HexLocation hl) throws InvalidLocationException {
		for(Hex h : this.allHexes) {
			if(h.getHexLocation().getX() == hl.getX() && h.getHexLocation().getY() == hl.getY())
				return h;
		}
		throw new InvalidLocationException();
	}
	
	public List<Road> getAllRoads() {
		List<Road> result = new ArrayList<Road>();
		for (Hex h : this.allHexes) {
			result.addAll(h.getAllRoads());
		}
		return result;
	}
	
	public List<Building> getAllBuildings() {
		List<Building> result = new ArrayList<Building>();
		for (Hex h: this.allHexes) {
			result.addAll(h.getAllBuildings());
		}
		return result;
	}

	public boolean canBuildSettlement(VertexLocation vl, int player, boolean override) {
		vl = vl.getNormalizedLocation();
		Hex hex = locsToHexes.get(vl.getHexLoc());
		if(hex == null)return false;
		int p = vl.getDir().ordinal();
		if(hex.getBuilding(p) != null)return false;
		if(vl.getDir() == VertexDirection.NorthWest) {
			if(locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[1])) == null)return false;
			Road r = null;
			boolean valid = false;
			if(hex != null) {
				if(hex.getBuilding(2) != null)return false;
				r = hex.getRoad(1);
				if(r != null)
					if(r.getOwner() == player) 
						valid = true;
			}
			Hex neighbor = locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[0]));
			if(neighbor == null)return false;
			if(neighbor != null) {
				if(neighbor.getBuilding(2) != null)return false;
				r = neighbor.getRoad(2);
				if(r != null)
					if(r.getOwner() == player)
						valid = true;
			}
			neighbor = locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[5]));
			if(neighbor != null) {
				if(neighbor.getBuilding(2) != null)return false;
				r = hex.getRoad(0);
				if(r != null)
					if(r.getOwner() == player)
						valid = true;
			}
			return (valid || override);
		}
		else {
			if(locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[1])) == null)return false;
			Road r = null;
			boolean valid = false;
			if(hex != null) {
				if(hex.getBuilding(1) != null)return false;
				r = hex.getRoad(1);
				valid = false;
				if(r != null)
					if(r.getOwner() == player) 
						valid = true;
			}
			Hex neighbor = locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[2]));
			if(neighbor == null)return false;
			if(neighbor != null) {
				if(neighbor.getBuilding(1) != null)return false;
				r = neighbor.getRoad(0);
				if(r != null)
					if(r.getOwner() == player)
						valid = true;
			}
			neighbor = locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[3]));
			if(neighbor != null) {
				if(neighbor.getBuilding(1) != null)return false;
				r = hex.getRoad(2);
				if(r != null)
					if(r.getOwner() == player)
						valid = true;
			}
			return (valid || override);
		}
	}

	public boolean canBuildRoad(EdgeLocation edge, int player, boolean override) {	
		edge = edge.getNormalizedLocation(); // From map, it should already be normalized
		Hex hex = this.locsToHexes.get(edge.getHexLoc());
		int p = edge.getDir().ordinal();
		if(hex == null)return false;
		if(override) {
			VertexLocation v1 = new VertexLocation(hex.getHexLocation(), p);
			VertexLocation v2 = new VertexLocation(hex.getHexLocation(), (p+1==6?0:p+1));
			v1 = v1.getNormalizedLocation();
			v2 = v2.getNormalizedLocation();
			Hex h1 = locsToHexes.get(v1.getHexLoc()), h2 = locsToHexes.get(v2.getHexLoc());
			if(h1 == null || h2 == null)return false;
			if(h1.getBuilding(v1.getDir().ordinal()) != null || h2.getBuilding(v2.getDir().ordinal()) != null)
				return false;
			if(!(canBuildSettlement(v1, player,true) || canBuildSettlement(v2,player,true)))return false;
		}
		Hex h2 = this.locsToHexes.get(hex.getHexLocation().getNeighborLoc(edge.getDir()));
		if(h2 == null)return false;
		if(hex.getHexType() == HexType.WATER && h2.getHexType() == HexType.WATER)
			return false; // if the player is trying to place in the ocean
		if(hex.getRoad(p) != null)return false;// If a road has already been built in location
		if(edge.getDir() == EdgeDirection.North) {
			Hex nwNeighbor = this.locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[p-1]));
			Hex neNeighbor = this.locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[p+1]));
			boolean validLoc = false;
			Road r = hex.getRoad(p-1);
			if(r!=null) // Check the road counterclockwise of location
				if(r.getOwner() == player)
					validLoc = true;
			if(nwNeighbor != null) {
				r = nwNeighbor.getRoad(p+1);
				if(r!=null) // Check the road on the adjacent hex
					if(r.getOwner() == player)
						validLoc = true;
			}
			if(hex.getBuilding(p) != null) {
				if(hex.getBuilding(p).getOwner() == player) return true; // Is the player's building
				else validLoc = false; // Another building is there but not player's
			}
			if(validLoc)return true; // The spot is valid

			r = hex.getRoad(p+1);
			if(r!=null) // Check the road counterclockwise of location
				if(r.getOwner() == player)
					validLoc = true;
			if(neNeighbor != null) {
				r = neNeighbor.getRoad(p-1);
				if(r!=null) // Check the road on the adjacent hex
					if(r.getOwner() == player)
						validLoc = true;
			}
			if(hex.getBuilding(p+1) != null) {
				if(hex.getBuilding(p+1).getOwner() == player) return true; // Is the player's building
				else validLoc = false; // Another building is there but not player's
			}
			return (validLoc || override);
		}
		else {
			int b1,b2,i, x1;
			if(edge.getDir() == EdgeDirection.NorthWest) {
				b1 = 2;
				b2 = 1;
				i = 2;
				x1 = 5;
			}
			else {
				b1 = 1;
				b2 = 2;
				i = 0;
				x1 = 3;
			}
			Hex sNeighbor = this.locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[x1]));
			Hex nNeighbor = this.locsToHexes.get(hex.getHexLocation().getNeighborLoc(EdgeDirection.values()[p]));
			boolean validLoc = false;
			Road r = null;
			if(sNeighbor != null)
				r = sNeighbor.getRoad(i);
			if(r!=null) // Check the road counterclockwise of location
				if(r.getOwner() == player)
					validLoc = true;
			if(sNeighbor != null) {
				r = sNeighbor.getRoad(1);
				if(r!=null) // Check the road on the adjacent hex
					if(r.getOwner() == player)
						validLoc = true;
			}
			if(sNeighbor.getBuilding(b1) != null) {
				if(sNeighbor.getBuilding(b1).getOwner() == player) return true; // Is the player's building
				else validLoc = false; // Another building is there but not player's
			}
			if(validLoc)return true; // The spot is valid

			r = hex.getRoad(1);
			if(r!=null) // Check the road counterclockwise of location
				if(r.getOwner() == player)
					validLoc = true;
			if(nNeighbor != null) {
				r = nNeighbor .getRoad(Math.abs(p-2));
				if(r!=null) // Check the road on the adjacent hex
					if(r.getOwner() == player)
						validLoc = true;
			}
			if(hex.getBuilding(b2) != null) {
				if(hex.getBuilding(b2).getOwner() == player) return true; // Is the player's building
				else validLoc = false; // Another building is there but not player's
			}
			return (validLoc || override);
		}
	}

	/**
	 * Returns a list of all the Hex objects in the Board.
	 * @return A list of all the Hex objects in the Board.
	 */
	public List<Hex> getAllHexes() {
		return allHexes;
	}

	/**
	 * Returns a Hex object based on the given HexLocation.
	 * @param hl The HexLocation of the Hex to return.
	 * @return The Hex object associated with the given HexLocation.
	 */
	public Hex getHexByLocation(HexLocation hl) {
		return locsToHexes.get(hl);
	}

	/**
	 * Returns an array of hex tiles (of length 1 or 2) corresponding to the number token passed in.
	 * @param number A NumberToken object used to determine which hexes should be returned.
	 * @return An list of Hex objects of length 1 or 2.
	 */
	public List<Hex> getHexesByNumber(int number) {
		return tokensToHexes.get(number);
	}

	/**
	 * Returns the NumberToken object associated with a given hex tile.
	 * @param hex A Hex object used to determine which number token should be returned.
	 * @return The NumberToken object associated with the given Hex. 
	 */
	public int getTokenFromHex(Hex hex) {
		return hexesToTokens.get(hex);
	}

	/**
	 * Returns the hex tile where the Robber is currently located.
	 * @return A Hex object representing the Robber's current location.
	 */
	public Hex getRobberHex() {
		return robberLocation;
	}

	public void setRobberHex(Hex robberHex) {
		Hex oldRobberHex = this.robberLocation;

		this.robberLocation = robberHex;
		this.robberLocation.setHasRobber(true);

		if (oldRobberHex != null) {
			oldRobberHex.setHasRobber(false);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((allHexes == null) ? 0 : allHexes.hashCode());
		result = prime * result
				+ ((hexesToTokens == null) ? 0 : hexesToTokens.hashCode());
		result = prime * result
				+ ((locsToHexes == null) ? 0 : locsToHexes.hashCode());
		result = prime * result + ((ports == null) ? 0 : ports.hashCode());
		result = prime * result
				+ ((robberLocation == null) ? 0 : robberLocation.hashCode());
		result = prime * result
				+ ((tokensToHexes == null) ? 0 : tokensToHexes.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (allHexes == null) {
			if (other.allHexes != null)
				return false;
		} else if (!allHexes.equals(other.allHexes))
			return false;
		if (hexesToTokens == null) {
			if (other.hexesToTokens != null)
				return false;
		} else if (!hexesToTokens.equals(other.hexesToTokens))
			return false;
		if (locsToHexes == null) {
			if (other.locsToHexes != null)
				return false;
		} else if (!locsToHexes.equals(other.locsToHexes))
			return false;
		if (ports == null) {
			if (other.ports != null)
				return false;
		} else if (!ports.equals(other.ports))
			return false;
		if (robberLocation == null) {
			if (other.robberLocation != null)
				return false;
		} else if (!robberLocation.equals(other.robberLocation))
			return false;
		if (tokensToHexes == null) {
			if (other.tokensToHexes != null)
				return false;
		} else if (!tokensToHexes.equals(other.tokensToHexes))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Board [allHexes=" + allHexes + ", robberLocation=" + robberLocation + ", ports=" + ports + "]";
	}
}
