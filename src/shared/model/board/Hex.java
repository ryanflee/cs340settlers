package shared.model.board;

import java.util.*;

import shared.definitions.HexType;
import shared.locations.*;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.*;

/**
 * Represents a single hex tile on the Catan board.
 * @author Ryan F. Lee
 *
 */
public class Hex {

	private HexType _type;
	private HexLocation _location;
	private int _number;
	private boolean _hasRobber;
	
	private List<Building> _buildings;
	private List<Road> _roads;
	
	// For both of these Maps, they need to be using the normalized function as the key!
	private Map<VertexLocation, Building> _verticesToBuildings;
	private Map<EdgeLocation, Road> _edgesToRoads;
	
	public Hex() {
		this._type = null;
		this._location = null;
		this._number = 0;
		this._hasRobber = false;
		
		this._buildings = new ArrayList<Building>();
		this._roads = new ArrayList<Road>();
		this._verticesToBuildings = new HashMap<VertexLocation, Building>();
		this._edgesToRoads = new HashMap<EdgeLocation, Road>();
	}
	
	public Hex(HexType type, HexLocation location, int number, boolean hasRobber) {
		this._type = type;
		this._location = location;
		this._number = number;
		this._hasRobber = hasRobber;
		
		this._buildings = new ArrayList<Building>();
		this._roads = new ArrayList<Road>();
		this._verticesToBuildings = new HashMap<VertexLocation, Building>();
		this._edgesToRoads = new HashMap<EdgeLocation, Road>();
	}
	
	public Hex(HexType type, HexLocation location, int number, boolean hasRobber, List<Building> buildings, List<Road> roads) {
		this._type = type;
		this._location = location;
		this._number = number;
		this._hasRobber = hasRobber;
		
		this._buildings = buildings;
		this._roads = roads;
		
		this._verticesToBuildings = new HashMap<VertexLocation, Building>();
		
		for (Building b : this._buildings) {
			this._verticesToBuildings.put(b.getLocation(), b);
		}
		
		this._edgesToRoads = new HashMap<EdgeLocation, Road>();
		
		for (Road r : this._roads) {
			this._edgesToRoads.put(r.getLocation(), r);
		}
	}

	public Building getBuilding(VertexDirection dir) {
		for(Building b : this._buildings){
			if(b.getLocation().getDir() == dir)
				return b;
		}
		return null;
	}
	
	public HexType getHexType() {
		return _type;
	}
	
	public HexLocation getHexLocation() {
		return _location;
	}

	public int getNumberToken() {
		return _number;
	}
	
	public boolean getHasRobber() {
		return _hasRobber;
	}
	
	public void setHasRobber(boolean hasRobber) {
		this._hasRobber = hasRobber;
	}
	
	/**
	 * "Hex-level" can-do method - checks whether a vertex is available for building a city
	 * @param vertex The {@code VertexLocation} in question
	 * @return
	 * True if the vertex can be built on; 
	 * False otherwise
	 */
	public boolean canBuildCity(VertexLocation vertex,int player) {
		vertex = vertex.getNormalizedLocation();
		if(!this._verticesToBuildings.containsKey(vertex))return false;
		if(this._verticesToBuildings.get(vertex).getOwner() != player)return false;
		if(this._verticesToBuildings.get(vertex).getValue() == 1)return true;
		return false;
	}
	
	public Road getRoad(int pos) {
		if(!this._edgesToRoads.containsKey(new EdgeLocation(_location,pos))) return null;
		return this._edgesToRoads.get(new EdgeLocation(_location,pos));
	}
	
	public Building getBuilding(int pos) {
		if(!this._verticesToBuildings.containsKey(new VertexLocation(_location,pos)))return null;
		return this._verticesToBuildings.get(new VertexLocation(_location,pos));
	}
	
	/**
	 * Adds a single settlement to the Hex at the specified vertex location.
	 * @param vertex The vertex to build the building on.
	 * @param building The building to be placed on the Hex at {@code vertex}.
	 * @throws InvalidLocationException if the vertex is occupied, too close to another {@code Building}, or not attached to a road owned by the same player.
	 */
	public void addBuilding(VertexLocation vertex, Building building) throws InvalidLocationException {
		if (!this._verticesToBuildings.containsKey(vertex)) {
			_verticesToBuildings.put(vertex,  building);
			_buildings.add(building);
		}
		
		else {
			throw new InvalidLocationException();
		}
	}
	
	public void upgradeToCity(VertexLocation vl, City city) {
		this._buildings.remove(this._verticesToBuildings.get(vl));
		this._verticesToBuildings.put(vl, city);
		this._buildings.add(city);
		
	}
	
	/**
	 * Gets all the buildings currently on the Hex.
	 * @return A list of {@code Building} objects.
	 */
	public List<Building> getAllBuildings() {
		return _buildings;
	}
	
	/**
	 * Adds a single road to the Hex at the specified edge location. 
	 * @param edge The edge location to build the road on.
	 * @param road The road to be placed on the Hex at {@code edge}.
	 * @throws InvalidLocationException if the edge is occupied or not connected to another road owned by the same player.
	 */
	public void addRoad(EdgeLocation edge, Road road) throws InvalidLocationException {
		if (!this._edgesToRoads.containsKey(edge)){
			_edgesToRoads.put(edge, road);
			_roads.add(road);
		}
		
		else {
			throw new InvalidLocationException();
		}
	}
	
	/**
	 * Gets all the roads currently on the Hex.
	 * @return A list of {@code Road} objects.
	 */
	public List<Road> getAllRoads() {
		return _roads;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((_buildings == null) ? 0 : _buildings.hashCode());
		result = prime * result
				+ ((_edgesToRoads == null) ? 0 : _edgesToRoads.hashCode());
		result = prime * result + (_hasRobber ? 1231 : 1237);
		result = prime * result
				+ ((_location == null) ? 0 : _location.hashCode());
		result = prime * result + _number;
		result = prime * result + ((_roads == null) ? 0 : _roads.hashCode());
		result = prime * result + ((_type == null) ? 0 : _type.hashCode());
		result = prime
				* result
				+ ((_verticesToBuildings == null) ? 0 : _verticesToBuildings
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hex other = (Hex) obj;
		if (_buildings == null) {
			if (other._buildings != null)
				return false;
		} else if (!_buildings.equals(other._buildings))
			return false;
		if (_edgesToRoads == null) {
			if (other._edgesToRoads != null)
				return false;
		} else if (!_edgesToRoads.equals(other._edgesToRoads))
			return false;
		if (_hasRobber != other._hasRobber)
			return false;
		if (_location == null) {
			if (other._location != null)
				return false;
		} else if (!_location.equals(other._location))
			return false;
		if (_number != other._number)
			return false;
		if (_roads == null) {
			if (other._roads != null)
				return false;
		} else if (!_roads.equals(other._roads))
			return false;
		if (_type != other._type)
			return false;
		if (_verticesToBuildings == null) {
			if (other._verticesToBuildings != null)
				return false;
		} else if (!_verticesToBuildings.equals(other._verticesToBuildings))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Hex [_type=" + _type + ", _location=" + _location
				+ ", _number=" + _number + ", _hasRobber=" + _hasRobber
				+ ", _buildings=" + _buildings + ", _roads=" + _roads + "]";
	}
}
