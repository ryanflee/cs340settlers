package shared.model.resourcecard;

import shared.definitions.*;
/**
 * A class that represents the resource cards that 
 * @author Jordan Wilbur
 *
 */
public class ResourceCard {

	private ResourceType type;
	
	public ResourceCard (ResourceType type) {
		this.type = type;
	}
	
	/**
	 * A function that allows you to find out the type of resource it is.
	 * @return Returns the resource type that this card contains.
	 */
	
	public ResourceType getType() {
		return this.type;
	}

	public void setType(ResourceType type){
		this.type = type;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceCard other = (ResourceCard) obj;
		if (type != other.type)
			return false;
		return true;
	}
}
