package shared.model.pieces;

import shared.locations.VertexLocation;

public class City extends Building{
	private int id;
	
	public City() {super();}
	
	public City(VertexLocation vloc, int owner) {
		super(vloc, owner);
		
		this.value = 2;
	}
	
	public int getValue() { return 2; }

	public int getId() { return id;}

	@Override
	public VertexLocation getLocation() {
		return this.vertexLocation;
	}
	
	@Override
	public int getOwner() {
		return this.ownerIndex;
	}
}
