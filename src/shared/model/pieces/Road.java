package shared.model.pieces;

import shared.locations.EdgeLocation;
import shared.model.board.*;
import shared.model.player.Player;

public class Road {
	private EdgeLocation edgeLocation;
	private int id;
	private int ownerIndex;
	
	public Road() {
		this.edgeLocation = null;
		this.ownerIndex = -1;
	}
	
	public Road(EdgeLocation edgeLoc, int owner) {
		this.edgeLocation = edgeLoc;
		this.ownerIndex = owner;
	}
	
	public EdgeLocation getLocation() {return edgeLocation;}
	
	public int getOwner() {return ownerIndex;}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((edgeLocation == null) ? 0 : edgeLocation.hashCode());
		result = prime * result + id;
		result = prime * result + ownerIndex;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Road other = (Road) obj;
		if (edgeLocation == null) {
			if (other.edgeLocation != null)
				return false;
		} else if (!edgeLocation.equals(other.edgeLocation))
			return false;
		if (id != other.id)
			return false;
		if (ownerIndex != other.ownerIndex)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Road [edgeLocation=" + edgeLocation
				+ ", ownerIndex=" + ownerIndex + "]";
	}
}
