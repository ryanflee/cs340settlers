package shared.model.pieces;

import shared.locations.VertexLocation;

public class Settlement extends Building{
	private int id;
	
	public Settlement() {super();}
	
	public Settlement(VertexLocation vloc, int owner) {
		super(vloc, owner);
		
		this.value = 1;
	}
	
	public Settlement(int owner) {
		this(null,owner);
	}

	public int getValue() {return 1;}

	public int getId() { return id;}

	@Override
	public VertexLocation getLocation() {
		return this.vertexLocation;
	}
	
	@Override
	public int getOwner() {
		return this.ownerIndex;
	}
}
