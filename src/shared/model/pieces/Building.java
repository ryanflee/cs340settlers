package shared.model.pieces;

import shared.locations.VertexLocation;
import shared.model.board.Hex;
import shared.model.player.Player;

public abstract class Building {
	protected VertexLocation vertexLocation;
	protected int ownerIndex;
	protected int value;
	
	public Building() {
		this.vertexLocation = null;
		this.ownerIndex = -1;
		this.value = 0;
	}
	
	public Building(VertexLocation vloc, int owner) {
		this.vertexLocation = vloc;
		this.ownerIndex = owner;
	}
	
	public boolean onMap() { return false;}
	
	public abstract VertexLocation getLocation();
	
	public abstract int getOwner();
	
	public abstract int getValue();
	
	abstract int getId();

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ownerIndex;
		result = prime * result + value;
		result = prime * result
				+ ((vertexLocation == null) ? 0 : vertexLocation.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Building other = (Building) obj;
		if (ownerIndex != other.ownerIndex)
			return false;
		if (value != other.value)
			return false;
		if (vertexLocation == null) {
			if (other.vertexLocation != null)
				return false;
		} else if (!vertexLocation.equals(other.vertexLocation))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Building [vertexLocation=" + vertexLocation + ", ownerIndex="
				+ ownerIndex + ", value=" + value + "]";
	}
}
