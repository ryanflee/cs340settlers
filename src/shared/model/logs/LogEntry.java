package shared.model.logs;

import shared.model.player.Player;

public interface LogEntry {
	
	Player getPlayer();
	String getValue();
}
