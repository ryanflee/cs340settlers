package shared.model;

import java.util.ArrayList;
import java.util.List;

import client.data.PlayerInfo;

import shared.definitions.CatanColor;
import shared.definitions.HexType;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.model.board.Board;
import shared.model.board.Hex;
import shared.model.developmentcard.DevCard;
import shared.model.developmentcard.LargestArmy;
import shared.model.developmentcard.LongestRoad;
import shared.model.exceptions.DevCardAlreadyPlayedException;
import shared.model.exceptions.DiceAlreadyRolledException;
import shared.model.exceptions.InvalidCardsException;
import shared.model.exceptions.InvalidLocationException;
import shared.model.exceptions.NotYourTurnException;
import shared.model.exceptions.OutOfResourceException;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

public class GameManager {
	private ArrayList<Player> players;
	private int currentPlayer;
	private ArrayList<ResourceCard> wood;
	private ArrayList<ResourceCard> brick;
	private ArrayList<ResourceCard> sheep;
	private ArrayList<ResourceCard> wheat;
	private ArrayList<ResourceCard> ore;
	private ArrayList<DevCard> developmentCards;
	private int longestRoad;
	private int largestArmy;
	private String status;
	private Board board;
	private PlayerInfo sender;
	private PlayerInfo receiver;
	private int[] tradeOffer;

	public GameManager() {
		longestRoad = -1;
		largestArmy = -1;
	}
	
	private Player getPlayer(int id) {
		return players.get(id);
	}
	
	private boolean playerHas(List<ResourceCard> hand, int[] hasCards) {
		for(ResourceCard c : hand) {
			int index;
			switch(c.getType()){
			case WOOD:index = 0;break;
			case BRICK:index = 1;break;
			case SHEEP:index = 2;break;
			case WHEAT:index = 3;break;
			case ORE:index = 4;break;
			
			default:index = -1;
			}
			hasCards[index]--;
		}
		
		for(int i : hasCards) {
			if(i>0)return false;
		}
		return true;
	}
	
	public boolean canSendChat(int player){ 
		if(currentPlayer != player) return true;
		if(!status.equalsIgnoreCase("playing"))return false;  
		return true;
	}
	
	/**
	 * Can a player accept an offer trade
	 * @param player - player to trade with (tradee)
	 * @param tradeOffer - Int array of length 5. + ints mean if tradee has that card, - ints cards to give to tradee
	 * @return
	 *  True if player has correct cards in hand
	 *  False if not
	 */
	public boolean canAcceptTrade(int player, int[] tradeOffer){ 
		if(!status.equalsIgnoreCase("playing"))return false;
		Player p = getPlayer(player);

		// since we're on the accepting end, need to flip signs
		// (trade in opposite direction as sender)
		int[] offerToAccept = new int[tradeOffer.length];
		for (int i = 0; i < tradeOffer.length; i++) {
			offerToAccept[i] = -tradeOffer[i];
		}
		
		return playerHas(p.getHand(),offerToAccept);
	}
	
	public boolean canDiscardCards(int player) {
		if(!status.toLowerCase().equals("discarding"))return false;
		Player p = getPlayer(player);
		if(p.getHandSize() > 7)return true;
		return false;
	}
	
	public boolean canRollNumber(int player) {
		if(player != currentPlayer)return false;
		if(!status.toLowerCase().equals("rolling")) return false;
		return true;
	}
	
	public boolean canBuildRoad(int player, boolean buildRoadCard) {
		if((status.toLowerCase().equals("firstround") || status.toLowerCase().equals("secondround")) && player == currentPlayer)return true;
		if(!status.toLowerCase().equals("playing")) return false;
		if(player != currentPlayer) return false;
		Player p = getPlayer(player);
		if(p.getRoads() == 0)return false;
		return playerHas(p.getHand(),new int[]{1,1,0,0,0}) || buildRoadCard;
	}
	
	public boolean canBuildSettlement(int player) {
		if((status.toLowerCase().equals("firstround") || status.toLowerCase().equals("secondround")) && player == currentPlayer)return true;
		if(!status.toLowerCase().equals("playing")) return false;
		if(player != currentPlayer) return false;
		Player p = getPlayer(player);
		if(p.getSettlements() == 0)return false;
		return playerHas(p.getHand(),new int[]{1,1,1,1,0});
	}
	
	public boolean canBuildCity(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(player != currentPlayer) return false;
		Player p = getPlayer(player);
		if(p.getCities() == 0)return false;
		if(p.getSettlements() == 5)return false;
		return playerHas(p.getHand(),new int[]{0,0,0,2,3});
	}
	
	public boolean canOfferTrade(int player) {
		if(!status.equalsIgnoreCase("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(p.getHandSize() == 0)return false;
		return true;
	}
	
	public boolean canBuildRoad(int player, EdgeLocation el, boolean override, boolean buildRoadCard) {
		if(!canBuildRoad(player,buildRoadCard)) return false;
		if(board.canBuildRoad(el,player, override))
			return true;
		return false;
	}
	
	/**
	 * To see if a settlement can be placed at vertex location
	 * @param player - Player int
	 * @param vl - Location
	 * @param override - If the game is in the first or second round
	 * @return
	 */
	public boolean canBuildSettlement(int player, VertexLocation vl, boolean override) {
		if(!canBuildSettlement(player))return false;
		return board.canBuildSettlement(vl, player,override);
	}
	
	public boolean canBuildCity(int player, VertexLocation vl) {
		if(!canBuildCity(player)) return false;
		try {
			Hex h = board.getHex(vl.getHexLoc());
			if(h.canBuildCity(vl,player))return true;
		} catch(InvalidLocationException error) {
			return false;
		}
		return false;
	}
	
	public boolean canMaritimeTrade(int player) {
		if(currentPlayer != player) return false;
		if(!status.equalsIgnoreCase("playing"))return false;
		Player p = getPlayer(player);
		if(!(playerHas(p.getHand(),new int[]{p.getTradeValue(0),0,0,0,0}) || 
				playerHas(p.getHand(),new int[]{0,p.getTradeValue(1),0,0,0}) || 
				playerHas(p.getHand(),new int[]{0,0,p.getTradeValue(2),0,0}) || 
				playerHas(p.getHand(),new int[]{0,0,0,p.getTradeValue(3),0}) || 
				playerHas(p.getHand(),new int[]{0,0,0,0,p.getTradeValue(4)})))return false;
		return true;
	}
	
	public int maritimeTradeValue(int player, ResourceType type) {
		Player p = getPlayer(player);
		int[] x = {0,0,0,0,0};
		x[type.ordinal()] = p.getTradeValue(type.ordinal());
		if(playerHas(p.getHand(),x)) {
			return p.getTradeValue(type.ordinal());
		}
		return -1;
	}
	
	public int resourceLeft(ResourceType type) {
		switch(type){
		case WOOD:return wood.size();
		case BRICK:return brick.size();
		case SHEEP:return sheep.size();
		case WHEAT:return wheat.size();
		case ORE:return ore.size();
		default:return -1;
		}
	}
	
	public boolean canFinishTurn(int player) {
		if(currentPlayer != player)return false;
		
		if(!status.equalsIgnoreCase("playing") && 
			!status.equalsIgnoreCase("firstround") &&
			!status.equalsIgnoreCase("secondround"))
			{ return false; }
		
		return true;
	}
	
	public boolean canBuyDevCard(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(player != currentPlayer) return false;
		if(developmentCards.size() == 0)return false;
		Player p = getPlayer(player);
		return playerHas(p.getHand(),new int[]{0,0,1,1,1});
	}
	
	public boolean canUseYearOfPlenty(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(p.getPlayedDevCard()) return false;
		if(!p.hasDevCard("yearofplenty")) return false;
		return true;
	}
	
	public boolean canUseRoadBuilder(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(p.getPlayedDevCard()) return false;
		if(!p.hasDevCard("roadbuild")) return false;
		return true;
	}
	
	public boolean canUseSoldier(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(p.getPlayedDevCard()) return false;
		if(!p.hasDevCard("soldier")) return false;
		return true;
	}
	
	public boolean canUseMonopoly(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(p.getPlayedDevCard()) return false;
		if(!p.hasDevCard("monopoly")) return false;
		return true;
	}
	
	public boolean canUseMonument(int player) {
		if(!status.toLowerCase().equals("playing")) return false;
		if(currentPlayer != player) return false;
		Player p = getPlayer(player);
		if(!p.hasDevCard("monument")) return false;
		return true;
	}
	
	public boolean canPlaceRobber(int player) {
		if(currentPlayer != player)return false;
		if(!(status.toLowerCase().equals("robbing") || status.equalsIgnoreCase("playing"))) return false;
		return true;
	}
	
	public boolean canPlaceRobber(int player, HexLocation hloc) {
		if(!canPlaceRobber(player)) return false;
		Hex h = board.getHexByLocation(hloc);
		if ((h == null) || (h.getHexType() == HexType.WATER)) return false;
		else if (h.getHasRobber()) return false;
		return true;
	}
	
	//Getters and Setters
	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int i) {
		this.currentPlayer = i;
	}

	public ArrayList<ResourceCard> getWood() {
		return wood;
	}

	public void setWood(ArrayList<ResourceCard> wood) {
		this.wood = wood;
	}

	public ArrayList<ResourceCard> getBrick() {
		return brick;
	}

	public void setBrick(ArrayList<ResourceCard> brick) {
		this.brick = brick;
	}

	public ArrayList<ResourceCard> getSheep() {
		return sheep;
	}

	public void setSheep(ArrayList<ResourceCard> sheep) {
		this.sheep = sheep;
	}

	public ArrayList<ResourceCard> getWheat() {
		return wheat;
	}

	public void setWheat(ArrayList<ResourceCard> wheat) {
		this.wheat = wheat;
	}

	public ArrayList<ResourceCard> getOre() {
		return ore;
	}

	public void setOre(ArrayList<ResourceCard> ore) {
		this.ore = ore;
	}

	public ArrayList<DevCard> getDevelopmentCards() {
		return developmentCards;
	}

	public void setDevelopmentCards(ArrayList<DevCard> developmentCards) {
		this.developmentCards = developmentCards;
	}

//	public LongestRoad getLongestRoad() {
//		return longestRoad;
//	}
//	public void setLongestRoad(LongestRoad longestRoad) {
//		this.longestRoad = longestRoad;
//	}
//	public LargestArmy getLargestArmy() {
//		return largestArmy;
//	}
//	public void setLargestArmy(LargestArmy largestArmy) {
//		this.largestArmy = largestArmy;
//	}
	
	public int getLongestRoad() {
		return longestRoad;
	}
	public void setLongestRoad(int longestRoad) {
		this.longestRoad = longestRoad;
	}
	public int getLargestArmy() {
		return largestArmy;
	}
	public void setLargestArmy(int largestArmy) {
		this.largestArmy = largestArmy;
	}

	public String getStatus() {
		return status;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int[] getTradeOffer(){
		return this.tradeOffer;
	}
	
	public void setTradeOffer(int[] tradeOffer){
		this.tradeOffer = tradeOffer;
	}
	
	public PlayerInfo getTradeSender(){
		return this.sender;
	}
	
	public void setTradeSender(PlayerInfo sender){
		this.sender = sender;
	}
	
	public PlayerInfo getTradeReceiver(){
		return this.receiver;
	}
	
	public void setTradeReciever(PlayerInfo receiver){
		this.receiver = receiver;
	}
	
	public List<ResourceCard> getCardsByType(String type) {
		switch(type) {
		case "brick":
			return brick;
		case "ore":
			return ore;
		case "sheep":
			return sheep;
		case "wheat":
			return wheat;
		case "wood":
			return wood;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		result = prime * result + ((brick == null) ? 0 : brick.hashCode());
		result = prime * result + currentPlayer;
		result = prime
				* result
				+ ((developmentCards == null) ? 0 : developmentCards.hashCode());
		result = prime * result + largestArmy;
		result = prime * result + longestRoad;
		result = prime * result + ((ore == null) ? 0 : ore.hashCode());
		result = prime * result + ((players == null) ? 0 : players.hashCode());
		result = prime * result + ((sheep == null) ? 0 : sheep.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((wheat == null) ? 0 : wheat.hashCode());
		result = prime * result + ((wood == null) ? 0 : wood.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameManager other = (GameManager) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		if (brick == null) {
			if (other.brick != null)
				return false;
		} else if (!brick.equals(other.brick))
			return false;
		if (currentPlayer != other.currentPlayer)
			return false;
		if (developmentCards == null) {
			if (other.developmentCards != null)
				return false;
		} else if (!developmentCards.equals(other.developmentCards))
			return false;
		if (largestArmy != other.getLargestArmy()) {
			return false;
		}
		if (longestRoad != other.getLongestRoad()) {
			return false;
		}
		if (ore == null) {
			if (other.ore != null)
				return false;
		} else if (!ore.equals(other.ore))
			return false;
		if (players == null) {
			if (other.players != null)
				return false;
		} else if (!players.equals(other.players))
			return false;
		if (sheep == null) {
			if (other.sheep != null)
				return false;
		} else if (!sheep.equals(other.sheep))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (wheat == null) {
			if (other.wheat != null)
				return false;
		} else if (!wheat.equals(other.wheat))
			return false;
		if (wood == null) {
			if (other.wood != null)
				return false;
		} else if (!wood.equals(other.wood))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GameManager [players=" + players + ", currentPlayer="
				+ currentPlayer + ", wood=" + wood + ", brick=" + brick
				+ ", sheep=" + sheep + ", wheat=" + wheat + ", ore=" + ore
				+ ", developmentCards=" + developmentCards + ", longestRoad="
				+ longestRoad + ", largestArmy=" + largestArmy + ", status="
				+ status + ", board=" + board + "]";
	}	
	
}
