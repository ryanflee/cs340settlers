package shared.model;

import java.util.ArrayList;
import java.util.List;

import shared.model.board.Board;
import shared.model.logs.*;
import shared.model.player.Player;

public class Model {
	public GameManager gameManager;
	private Board board;
	private List<Player> players;
	private List<ChatEntry> comments;
	private List<HistoryEntry> history;
	private boolean gameStarted;
	private int winner;
	private int version;
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Model() {
		this.gameManager = new GameManager();
		this.players = new ArrayList<Player>();
		this.comments = new ArrayList<ChatEntry>();
		this.history = new ArrayList<HistoryEntry>();
		this.gameStarted = false;
		this.winner = -1;
	}
	
	public Model(GameManager gameManager, Board board, List<Player> players, List<ChatEntry> comments, List<HistoryEntry> history) {
		this.version = 0;
		this.gameManager = gameManager;
		gameManager.setBoard(board);
		this.board = board;
		this.players = players;
		this.comments = comments;
		this.history = history;
		this.gameStarted = false;
		this.winner = -1;
	}
	
	public void incrementVersion() {
		this.version++;
	}
	
	public int getWinner() {
		return this.winner;
	}
	
	public void setWinner(int winner) {
		this.winner = winner;
	}
	
	public boolean getGameStarted() {
		return gameStarted;
	}
	
	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public List<ChatEntry> getComments() {
		return comments;
	}

	public void setComments(List<ChatEntry> comments) {
		this.comments = comments;
	}

	public List<HistoryEntry> getHistory() {
		return history;
	}

	public void setHistory(List<HistoryEntry> history) {
		this.history = history;
	}

	/**
	 * Given a player's id, will return that player's index in the game
	 * @param pid - Player ID
	 * @return - Player Index
	 */
	public int getPlayerIndex(int pid) {
		for(int i=0;i<players.size();i++)
			if(pid == players.get(i).getId())
				return i;
		return -1;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board == null) ? 0 : board.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((gameManager == null) ? 0 : gameManager.hashCode());
		result = prime * result + ((history == null) ? 0 : history.hashCode());
		result = prime * result + ((players == null) ? 0 : players.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (board == null) {
			if (other.board != null)
				return false;
		} else if (!board.equals(other.board))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (gameManager == null) {
			if (other.gameManager != null)
				return false;
		} else if (!gameManager.equals(other.gameManager))
			return false;
		if (history == null) {
			if (other.history != null)
				return false;
		} else if (!history.equals(other.history))
			return false;
		if (players == null) {
			if (other.players != null)
				return false;
		} else if (!players.equals(other.players))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.board + "\nPlayers[" + this.players.size() + "]:";
	}
}
