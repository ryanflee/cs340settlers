package shared.model.player;

import java.util.*;

import shared.definitions.CatanColor;
import shared.definitions.DevCardType;
import shared.model.developmentcard.DevCard;
import shared.model.pieces.*;
import shared.model.resourcecard.ResourceCard;
import shared.model.*;

public class Player {
	private int id, victoryPoints,
				roads, cities, settlements,
				soldiers, monuments;
	private List<ResourceCard> hand;
	private List<DevCard> devHand;
	private CatanColor color;
	private String name;
	private boolean discarded;
	private boolean playedDevCard;
	private int[] tradeRate = {4,4,4,4,4};
	
	private GameManager GM;
	
	public Player() {}
	
	/**
	 * 
	 * @param id The player's unique ID
	 * @param name The player's username
	 * @param color The player's color for this game
	 * @param victoryPoints The player's victoryPoints
	 * @param roads int of player's remaining roads
	 * @param cities int of player's remaining cities
	 * @param settlements int of player's remaining settlements
	 * @param hand List of ResourceCards the player owns
	 * @param devHand List of DevCards the player owns
	 * @param turnTracker turnTracker last time a dev card was played
	 */
	public Player(int id, String name, CatanColor color, 
				int victoryPoints, int roads, int cities, int settlements, 
				List<ResourceCard> hand, List<DevCard> devHand, boolean discarded, boolean devPlayed,
				int soldiers, int monuments) {
		
		this.id = id;
		this.name = name;
		this.color = color;
		this.roads = roads;
		this.cities = cities;
		this.settlements = settlements;
		this.victoryPoints = victoryPoints;
		this.hand = hand;
		this.devHand = devHand;
		this.discarded = discarded;
		this.playedDevCard = devPlayed;
		this.soldiers = soldiers;
		this.monuments = monuments;
	}
	
	public Player(GameManager GM) {
		this.GM = GM;
	}
	
	public String getName() {
		return this.name;
	}
	
	public CatanColor getColor() {
		return this.color;
	}
	
	public int getId() { return id;}
	
	/**
	 * getVictoryPoints - Number of victory points as seen by other plays (includes longest road/largest army)
	 * @return Number of points visible
	 */
	public int getVictoryPoints() {return this.victoryPoints;}
	
	public void setVictoryPoints(int victoryPoints) {this.victoryPoints = victoryPoints;}
	/**
	 * getTotalVictoryPoints - Total number of points + dev card points (longest road/largest army included)
	 * @return Number of total points
	 */
	public int getTotalVictoryPoints() { return 0;}
	
	/**
	 * getSoldiers - gets the number of soldiers played
	 * @return - Number of soldiers
	 */
	public int getSoldiers() {return this.soldiers;}
	
	public void setSoldiers(int soldiers) {this.soldiers = soldiers;} 
	/**
	 * getHandSize - gets the number of cards in player's hand
	 * @return - Number of cards
	 */
	public int getHandSize() {return hand.size();}
	
	/**
	 * getDevCards - gets the development cards that the player has
	 * @return - list of dev cards
	 */
	public ArrayList<DevCard> getDevCards() {return (ArrayList<DevCard>) this.devHand;}
	
	public void setDevCards(ArrayList<DevCard> devHand) { this.devHand = devHand; }
	
	/**
	 * gets the number of cities player has that are not on board
	 * @return number of cities
	 */
	public int getCities() {return this.cities;}
	
	public void setCities(int cities) { this.cities = cities; }
	
	/**
	 * gets the number of settlements player has that are not on board
	 * @return number of settlements
	 */
	public int getSettlements() {return this.settlements;}
	
	public void setSettlements(int settlements) { this.settlements = settlements; }
	/**
	 * gets the number of roads player has that are not on board
	 * @return number of roads
	 */
	public int getRoads() {return this.roads;}
	
	public void setRoads(int roads){ this.roads = roads; }
	
	public boolean getPlayedDevCard() {
		return playedDevCard;
	}
	
	public void setPlayedDevCard(boolean playedDevCard) {
		this.playedDevCard = playedDevCard;
	}
	
	/**
	 * Returns trade rate with a selected resource
	 * @param resource - [0,1,2,3,4][WOOD,BRICK,SHEEP,WHEAT,ORE]
	 * @return - Integer of trade rate with bank
	 */
	public int getTradeValue(int resource){
		return tradeRate[resource];
	}

	/**
	 * Set the trade rate that a player can do with the bank
	 * @param position - Resource [WOOD,BRICK,SHEEP,WHEAT,ORE]
	 * @param value - an integer between 2-4
	 */
	public void setTradeRate(int position, int value) {
		if(value > 4 || value < 2)return;
		tradeRate[position] = value;
	}
	
	/**
	 * hasDevCard - Given a dev card type, returns if a player has certain type in dev hand
	 * @param type - STRING no spaces ["soldier","yearofplenty","monopoly","roadbuild","monument"]
	 * @return - if Dev Card type is in player's hand
	 */
	public boolean hasDevCard(String type) {
		DevCardType t;
		switch(type.toLowerCase()) {
		case "soldier":t=DevCardType.SOLDIER;break;
		case "yearofplenty":t=DevCardType.YEAR_OF_PLENTY;break;
		case "monopoly":t=DevCardType.MONOPOLY;break;
		case "roadbuild":t=DevCardType.ROAD_BUILD;break;
		case "monument":t=DevCardType.MONUMENT;break;
		default: t=null;
		}
		for(DevCard dc : devHand) {
			if(dc.getType() == t)
				if(dc.canPlayCard())
					return true;
		}
		return false;
	}
	
	public List<ResourceCard> getHand() {
		return this.hand;
	}
	
	public void setHand(List<ResourceCard> hand){
		this.hand = hand;
	}
	
	public boolean getDiscarded() {
		return this.discarded;
	}
	
	public void setDiscarded(boolean discarded) {
		this.discarded = discarded;
	}
	
	public int getMonuments() {
		return this.monuments;
	}
	
	public void setMonuments(int monuments) {
		this.monuments = monuments;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((GM == null) ? 0 : GM.hashCode());
		result = prime * result + cities;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((devHand == null) ? 0 : devHand.hashCode());
		result = prime * result + ((hand == null) ? 0 : hand.hashCode());
		result = prime * result + id;
		result = prime * result + monuments;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (playedDevCard ? 1231 : 1237);
		result = prime * result + roads;
		result = prime * result + settlements;
		result = prime * result + soldiers;
		result = prime * result + victoryPoints;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (GM == null) {
			if (other.GM != null)
				return false;
		} else if (!GM.equals(other.GM))
			return false;
		if (cities != other.cities)
			return false;
		if (color != other.color)
			return false;
		if (devHand == null) {
			if (other.devHand != null)
				return false;
		} else if (!devHand.equals(other.devHand))
			return false;
		if (hand == null) {
			if (other.hand != null)
				return false;
		} else if (!hand.equals(other.hand))
			return false;
		if (id != other.id)
			return false;
		if (monuments != other.monuments)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (playedDevCard != other.playedDevCard)
			return false;
		if (roads != other.roads)
			return false;
		if (settlements != other.settlements)
			return false;
		if (soldiers != other.soldiers)
			return false;
		if (victoryPoints != other.victoryPoints)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		int[] h = new int[]{0,0,0,0,0};
		for(ResourceCard c : hand)
			h[c.getType().ordinal()]++;
		String hn = "[";
		for(int i:h)hn += i+ ", ";
				
		return "Player [id=" + id + ", victoryPoints=" + victoryPoints
				+ ", roads=" + roads + ", cities=" + cities + ", settlements="
				+ settlements + ", soldiers=" + soldiers + ", monuments="
				+ monuments + ", hand=" + hn + "], devHand=" + devHand
				+ ", color=" + color + ", name=" + name + ", playedDevCard="
				+ playedDevCard + ", tradeRate=" + Arrays.toString(tradeRate)
				+ ", GM=" + GM + "]";
	}

	public void setColor(CatanColor color2) {
		this.color = color2;
		
	}

}
