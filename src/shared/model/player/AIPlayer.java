package shared.model.player;

import java.util.List;

import client.facade.Facade;
import shared.definitions.CatanColor;
import shared.model.GameManager;
import shared.model.developmentcard.DevCard;
import shared.model.resourcecard.ResourceCard;

public class AIPlayer extends Player {

	String type;
	
	public AIPlayer(String type) {
		super();
		this.type = type;
	}

	public AIPlayer(int id, String name, CatanColor color, int victoryPoints,
			int roads, int cities, int settlements, List<ResourceCard> hand,
			List<DevCard> devHand, boolean discarded, boolean devPlayed,
			int soldiers, int monuments, String type) {
		
		super(id, name, color, victoryPoints, roads, cities, settlements, hand,
				devHand, discarded, devPlayed, soldiers, monuments);
		
		this.type = type;
	}

	public AIPlayer(GameManager GM, String type) {
		super(GM);
		
		this.type = type;
	}

	public void takeTurn() {
		
		if (type.equalsIgnoreCase("LARGEST_ARMY")) {
			while (!Facade.canFinishTurn()) {
				
			}
			Facade.finishTurn();
		}
	}
}
