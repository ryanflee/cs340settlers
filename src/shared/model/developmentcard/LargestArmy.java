package shared.model.developmentcard;

import shared.model.player.Player;

/**
 * A special card that is held by the player with the most knight cards used. Only can obtain once three knights have been played.
 * @author Jordan Wilbur
 *
 */
public class LargestArmy {
	private Player ownedBy;
	private int ownerIndex;
	private int armySize;
	private int value;
	
	public LargestArmy() {
		armySize = 3;
		value = 2;
		ownerIndex = -1;
	}

	/**
	 * A method that allows you to make sure you are continually adding knights to make the army bigger.
	 */
	public void addKnight() {
		armySize++;
	}
	
	/**
	 * A method that returns the amount of victory points this card represents.
	 * @return value returns how many victory points the card is worth.
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * A method that returns the size of the largest army.
	 * @return armySize returns how big the largest army is.
	 */
	public int getSize() {
		return armySize;
	}
	/**
	 * A method that allows you to set how big the largest army's size is.
	 * @param armySize The value that the size of the army will be set to.
	 */
	public void setSize(int armySize) {
		this.armySize = armySize;
	}
	
	public int getOwnerIndex() {
		return ownerIndex;
	}
	
	public void setOwnerIndex(int index) {
		this.ownerIndex = index;
	}
	
	public Player getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(Player ownedBy) {
		this.ownedBy = ownedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + armySize;
		result = prime * result + ((ownedBy == null) ? 0 : ownedBy.hashCode());
		result = prime * result + value;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LargestArmy other = (LargestArmy) obj;
		if (armySize != other.armySize)
			return false;
		if (ownedBy == null) {
			if (other.ownedBy != null)
				return false;
		} else if (!ownedBy.equals(other.ownedBy))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
