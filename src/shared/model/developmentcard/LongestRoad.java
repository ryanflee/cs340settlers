package shared.model.developmentcard;

import shared.model.player.Player;

/**
 * A special card type that is held by the player with the longest road connection.  Must have at least 5 roads connected.
 * @author Jordan Wilbur
 *
 */
public class LongestRoad {
	private Player ownedBy;
	private int ownerIndex;
	private int roadLength;
	private int value;
	
	public LongestRoad() {
		roadLength = 5;
		value = 2;
	}

	/**
	 * A method that when used increases the length of the longest road.
	 */
	public void addRoad() {
		roadLength++;
	}
	
	/**
	 * A method that returns the amount of victory points this card represents
	 * @return value returns how many victory points the card is worth.
	 */
	public int getValue() {
		return value;
	}
	
	
	public int getOwnerIndex() {
		return ownerIndex;
	}
	
	public void setOwnerIndex(int index) {
		this.ownerIndex = index;
	}
	
	/**
	 * A method that returns the length that is represented as the longest road.
	 * @return roadLength returns how long the longest road is. 
	 */
	public int getLength() {
		return roadLength;
	}
	
	public Player getOwnedBy() {
		return this.ownedBy;
	}

	public void setOwnedBy(Player ownedBy) {
		this.ownedBy = ownedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ownedBy == null) ? 0 : ownedBy.hashCode());
		result = prime * result + roadLength;
		result = prime * result + value;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LongestRoad other = (LongestRoad) obj;
		if (ownedBy == null) {
			if (other.ownedBy != null)
				return false;
		} else if (!ownedBy.equals(other.ownedBy))
			return false;
		if (roadLength != other.roadLength)
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
