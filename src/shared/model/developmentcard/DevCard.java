package shared.model.developmentcard;

import shared.definitions.DevCardType;
import shared.model.player.Player;
/**
 * A class that contains all the different development card types in the game.
 * @author Jordan Wilbur
 *
 */
public class DevCard {
	private int ID;
	private int turnBought;
	private Player ownedBy;
	private DevCardType type;
	private boolean boughtThisTurn;
	
	public DevCard(int ID, DevCardType type) {
		this.ID = ID;
		this.type = type;
	}
	
	/**
	 * A method that allows you to use the development card. Depending on the type it is will depend on what action will happen when played.
	 */
	public void playCard() {
		DevCardType cardType = this.getType();
		//Depending on the type of card it will do a different action.
		//Knight card, Monopoly, Add Road, Year Of Plenty, or Monument
	}
	
	public Player getOwnedBy() {
		return ownedBy;
	}
	
	public void setOwnedBy(Player ownedBy) {
		this.ownedBy = ownedBy;
	}
	
	public DevCardType getType() {
		return type;
	}
	
	public void setType(DevCardType type) {
		this.type = type;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public void setSameTurn(boolean x) {
		this.boughtThisTurn = x;
	}
	
	public boolean canPlayCard() {
		return !this.boughtThisTurn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((ownedBy == null) ? 0 : ownedBy.hashCode());
		result = prime * result + turnBought;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DevCard other = (DevCard) obj;
		if (ID != other.ID)
			return false;
		if (ownedBy == null) {
			if (other.ownedBy != null)
				return false;
		} else if (!ownedBy.equals(other.ownedBy))
			return false;
		if (turnBought != other.turnBought)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DevCard [ID=" + ID + ", turnBought=" + turnBought
				+ ", ownedBy=" + ownedBy + ", type=" + type + "]";
	}
}
