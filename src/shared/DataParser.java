package shared;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import client.data.*;
import client.facade.Facade;

import com.google.gson.*;

import shared.definitions.*;
import shared.locations.*;
import shared.model.*;
import shared.model.board.*;
import shared.model.developmentcard.*;
import shared.model.exceptions.*;
import shared.model.logs.*;
import shared.model.pieces.*;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

/**
 * Parses data returned from the Proxy and produces an updated Model.
 */
public class DataParser extends Observable {
	
	Model model;
	int modelVersion;
	boolean gameStarted;
	
	public DataParser() {
		super();
		this.model = new Model();
		this.modelVersion = 0;
		this.gameStarted = false;
	}
	
	public Model getModel() {
		return this.model;
	}
	
	public void setModel(Model m) {
		this.model = m;
	}
	
	public int getModelVersion() {
		return this.modelVersion;
	}
	
	public void setModelVersion(int version) {
		this.modelVersion = version;
	}
	
	public boolean gameStarted() {
		return this.gameStarted;
	}
	
	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}
	
	public int parseVersionNumber(String json) {
		JsonParser jp = new JsonParser();
		JsonObject job = jp.parse(json).getAsJsonObject();
		
		int version = job.get("version").getAsInt();
		
		return version;
	}
	
	/**
	 * Method to parse a JSON string and build a new Model from it.
	 * @param json The JSON string representing the Model object.
	 * @throws IllegalArgumentException if {@code json} does not represent a valid Model object.
	 */
	public int buildModelFromJson(String json, boolean override) throws IllegalArgumentException {

//		String testJson = "{ \"deck\": { \"yearOfPlenty\": 2, \"monopoly\": 2, \"soldier\": 14, \"roadBuilding\": 2, \"monument\": 5 }," +
//							"\"map\": { \"hexes\": [ { \"resource\": \"brick\", \"location\": { \"x\": 0, \"y\": -2 }, \"number\": 11 }, { \"resource\": \"wood\", \"location\": { \"x\": 1, \"y\": -2 }, \"number\": 4 }, { \"resource\": \"wheat\", \"location\": { \"x\": 2, \"y\": -2 }, \"number\": 11 }, { \"resource\": \"brick\", \"location\": { \"x\": -1, \"y\": -1 }, \"number\": 8 }, { \"location\": { \"x\": 0, \"y\": -1 } }, { \"resource\": \"sheep\", \"location\": { \"x\": 1, \"y\": -1 }, \"number\": 9 }, { \"resource\": \"ore\", \"location\": { \"x\": 2, \"y\": -1 }, \"number\": 12 }, { \"resource\": \"wood\", \"location\": { \"x\": -2, \"y\": 0 }, \"number\": 5 }, { \"resource\": \"ore\", \"location\": { \"x\": -1, \"y\": 0 }, \"number\": 10 }, { \"resource\": \"wood\", \"location\": { \"x\": 0, \"y\": 0 }, \"number\": 3 }, { \"resource\": \"sheep\", \"location\": { \"x\": 1, \"y\": 0 }, \"number\": 5 }, { \"resource\": \"wheat\", \"location\": { \"x\": 2, \"y\": 0 }, \"number\": 6 }, { \"resource\": \"brick\", \"location\": { \"x\": -2, \"y\": 1 }, \"number\": 2 }, { \"resource\": \"wood\", \"location\": { \"x\": -1, \"y\": 1 }, \"number\": 9 }, { \"resource\": \"ore\", \"location\": { \"x\": 0, \"y\": 1 }, \"number\": 4 }, { \"resource\": \"wheat\", \"location\": { \"x\": 1, \"y\": 1 }, \"number\": 10 }, { \"resource\": \"sheep\", \"location\": { \"x\": -2, \"y\": 2 }, \"number\": 6 }, { \"resource\": \"wheat\", \"location\": { \"x\": -1, \"y\": 2 }, \"number\": 3 }, { \"resource\": \"sheep\", \"location\": { \"x\": 0, \"y\": 2 }, \"number\": 8 } ], \"roads\": [], \"cities\": [], \"settlements\": [], \"radius\": 3, \"ports\": [ { \"ratio\": 3, \"direction\": \"SE\", \"location\": { \"x\": -3, \"y\": 0 } }, { \"ratio\": 3, \"direction\": \"N\", \"location\": { \"x\": 0, \"y\": 3 } }, { \"ratio\": 3, \"direction\": \"NW\", \"location\": { \"x\": 2, \"y\": 1 } }, { \"ratio\": 2, \"resource\": \"sheep\", \"direction\": \"NW\", \"location\": { \"x\": 3, \"y\": -1 } }, { \"ratio\": 2, \"resource\": \"brick\", \"direction\": \"NE\", \"location\": { \"x\": -2, \"y\": 3 } }, { \"ratio\": 2, \"resource\": \"wood\", \"direction\": \"NE\", \"location\": { \"x\": -3, \"y\": 2 } }, { \"ratio\": 2, \"resource\": \"ore\", \"direction\": \"S\", \"location\": { \"x\": 1, \"y\": -3 } }, { \"ratio\": 3, \"direction\": \"SW\", \"location\": { \"x\": 3, \"y\": -3 } }, { \"ratio\": 2, \"resource\": \"wheat\", \"direction\": \"S\", \"location\": { \"x\": -1, \"y\": -2 } } ], \"robber\": { \"x\": 0, \"y\": -1 } }, "+
//							"\"players\": [ { \"resources\": { \"brick\": 0, \"wood\": 0, \"sheep\": 0, \"wheat\": 0, \"ore\": 0 }, \"oldDevCards\": { \"yearOfPlenty\": 0, \"monopoly\": 0, \"soldier\": 0, \"roadBuilding\": 0, \"monument\": 0 }, \"newDevCards\": { \"yearOfPlenty\": 0, \"monopoly\": 0, \"soldier\": 0, \"roadBuilding\": 0, \"monument\": 0 }, \"roads\": 15, \"cities\": 4, \"settlements\": 5, \"soldiers\": 0, \"victoryPoints\": 0, \"monuments\": 0, \"playedDevCard\": false, \"discarded\": false, \"playerID\": 12, \"playerIndex\": 0, \"name\": \"rfl\", \"color\": \"red\" }, null, null, null ], "+
//							"\"log\": { \"lines\": [] }, "+
//							"\"chat\": { \"lines\": [] }, "+
//							"\"bank\": { \"brick\": 24, \"wood\": 24, \"sheep\": 24, \"wheat\": 24, \"ore\": 24 }, " +
//							"\"turnTracker\": { \"status\": \"FirstRound\", \"currentTurn\": 0, \"longestRoad\": -1, \"largestArmy\": -1 }, \"winner\": -1, \"version\": 0}";
		JsonParser jp = new JsonParser();
		JsonObject job = jp.parse(json).getAsJsonObject();
		
		// "bank", "deck", "players" and "turnTracker" elements = GameManager
		JsonObject jsonBank = job.get("bank").getAsJsonObject();
		JsonObject jsonDeck = job.get("deck").getAsJsonObject();
		JsonArray jsonPlayers = job.get("players").getAsJsonArray();
		JsonObject jsonTurnTracker = job.get("turnTracker").getAsJsonObject();
		JsonElement jsonTradeOfferElem = job.get("tradeOffer");
		
		// treat this one separately - it's an optional element
		JsonObject jsonTradeOffer = null;
		if (jsonTradeOfferElem != null && !jsonTradeOfferElem.isJsonNull()) {
			jsonTradeOffer = jsonTradeOfferElem.getAsJsonObject();
		}
		
		// model version
		int version = job.get("version").getAsInt();
		if (version == this.modelVersion && this.gameStarted && !override) {
			return this.modelVersion;
		}
		
		this.modelVersion = version;
		// if game hasn't started, check number of players
		// set flag to true if there are 4+ players
		if (!this.gameStarted) {
			int numPlayers = getNumPlayers(jsonPlayers);
			if (numPlayers >= 4) {
				this.gameStarted = true;
			}
		}
		
		// winner
		int winner = job.get("winner").getAsInt();		
				
		GameManager newGM = parseGameManager(jsonBank, jsonDeck, jsonPlayers, jsonTurnTracker, jsonTradeOffer);
		
		// "map" element = Board
		Board newBoard = parseBoard(job.get("map").getAsJsonObject(), newGM.getPlayers());
		
		// add board to GM
		newGM.setBoard(newBoard);
		
		// "log" element = game history
		List<HistoryEntry> newHistory = parseHistory(job.get("log").getAsJsonObject(), newGM.getPlayers());
		
		// "chat" element = chat log
		List<ChatEntry> newChat = parseChat(job.get("chat").getAsJsonObject(), newGM.getPlayers());
		
		this.model = new Model(newGM, newBoard, newGM.getPlayers(), (List<ChatEntry>)newChat, (List<HistoryEntry>)newHistory);
		this.model.setGameStarted(this.gameStarted);
		this.model.setWinner(winner);
		this.setChanged();
		this.notifyObservers(this.model);
		
		return version;
	}
	
	// gets the number of players from the JSON array
	private int getNumPlayers(JsonArray jsonPlayers) {
		
		int result = 0;
		
		for (int p = 0; p < jsonPlayers.size(); p++) {
			if ((jsonPlayers.get(p) == null) || jsonPlayers.get(p).isJsonNull()) {
				continue;
			}
			
			else {
				result++;
			}
		}
		
		return result;
	}
	
	/**
	 * Parses a JsonObject representing the Board
	 * @param jsonBoard The JsonObject to be parsed into a Board
	 * @return A new Board instantiated from the JSON.
	 */
	private Board parseBoard(JsonObject jsonBoard, List<Player> players) {
		
		JsonArray jsonHexes = jsonBoard.get("hexes").getAsJsonArray();
		List<Hex> hexes = parseHexes(jsonHexes);
		
		JsonArray jsonRoads = jsonBoard.get("roads").getAsJsonArray();
		JsonArray jsonCities = jsonBoard.get("cities").getAsJsonArray();
		JsonArray jsonSettlements = jsonBoard.get("settlements").getAsJsonArray();
		JsonArray jsonPorts = jsonBoard.get("ports").getAsJsonArray();
		JsonObject jsonRobberLoc = jsonBoard.get("robber").getAsJsonObject();
		
		Board result = new Board(hexes);
		
		addPieces(result, jsonRobberLoc, jsonRoads, jsonCities, jsonSettlements, jsonPorts, players);
		
		result.setPorts(parsePorts(jsonPorts)); // add map of ports to Board
		return result;
	}

	private Map<EdgeLocation, PortType> parsePorts(JsonArray jsonPorts) {
		
		Map<EdgeLocation, PortType> result = new HashMap<EdgeLocation, PortType>();
		for (int p = 0; p < jsonPorts.size(); p++) {
			JsonObject thisPort = jsonPorts.get(p).getAsJsonObject();

			int ratio = thisPort.get("ratio").getAsInt();
			
			String strPortType = null;
			JsonElement jsonPortType = thisPort.get("resource");
			if ((jsonPortType == null || jsonPortType.isJsonNull())) {
				if (ratio == 3) {
					strPortType = "three";  
				}
			}
			
			else {
				strPortType = jsonPortType.getAsString();
			}
			
			PortType ptype = stringToPortType(strPortType);

			String direction = thisPort.get("direction").getAsString();
			JsonObject loc = thisPort.get("location").getAsJsonObject();
			
			int portX = loc.get("x").getAsInt();
			int portY = loc.get("y").getAsInt();
			
			try {
				EdgeDirection edir = stringToEdgeDirection(direction);
				HexLocation hloc = new HexLocation(portX, portY);
				EdgeLocation eloc = new EdgeLocation(hloc, edir);
				
				result.put(eloc, ptype);
			}
			catch (InvalidLocationException ile) {
				ile.printStackTrace();
			}
		}
		
		return result;
	}
	
	/**
	 * Parses a JsonArray of Hex objects
	 * @param jsonHexes The JsonArray of Hexes.
	 * @return A List of Hex objects instantiated from the JSON.
	 */
	private List<Hex> parseHexes(JsonArray jsonHexes) {
	
		/*
		 * \"map\": { \"hexes\": 
		 *
		 *[ 
		 *{ \"resource\": \"brick\", \"location\": { \"x\": 0, \"y\": -2 }, \"number\": 11 },	HEX 1 
		 *{ \"resource\": \"wood\", \"location\": { \"x\": 1, \"y\": -2 }, \"number\": 4 }, 	HEX 2
		 *{ \"resource\": \"wheat\", \"location\": { \"x\": 2, \"y\": -2 }, \"number\": 11 }, 	HEX 3
		 *{ \"resource\": \"brick\", \"location\": { \"x\": -1, \"y\": -1 }, \"number\": 8 }, 	HEX 4
		 *{ \"location\": { \"x\": 0, \"y\": -1 } }, 											HEX 5
		 *{ \"resource\": \"sheep\", \"location\": { \"x\": 1, \"y\": -1 }, \"number\": 9 }, 	HEX 6
		 *{ \"resource\": \"ore\", \"location\": { \"x\": 2, \"y\": -1 }, \"number\": 12 }, 	HEX 7
		 *{ \"resource\": \"wood\", \"location\": { \"x\": -2, \"y\": 0 }, \"number\": 5 }, 	HEX 8
		 *{ \"resource\": \"ore\", \"location\": { \"x\": -1, \"y\": 0 }, \"number\": 10 }, 	HEX 9
		 *{ \"resource\": \"wood\", \"location\": { \"x\": 0, \"y\": 0 }, \"number\": 3 }, 		HEX 10
		 *{ \"resource\": \"sheep\", \"location\": { \"x\": 1, \"y\": 0 }, \"number\": 5 }, 	HEX 11
		 *{ \"resource\": \"wheat\", \"location\": { \"x\": 2, \"y\": 0 }, \"number\": 6 }, 	HEX 12
		 *{ \"resource\": \"brick\", \"location\": { \"x\": -2, \"y\": 1 }, \"number\": 2 }, 	HEX 13
		 *{ \"resource\": \"wood\", \"location\": { \"x\": -1, \"y\": 1 }, \"number\": 9 }, 	HEX 14
		 *{ \"resource\": \"ore\", \"location\": { \"x\": 0, \"y\": 1 }, \"number\": 4 }, 		HEX 15
		 *{ \"resource\": \"wheat\", \"location\": { \"x\": 1, \"y\": 1 }, \"number\": 10 }, 	HEX 16
		 *{ \"resource\": \"sheep\", \"location\": { \"x\": -2, \"y\": 2 }, \"number\": 6 }, 	HEX 17
		 *{ \"resource\": \"wheat\", \"location\": { \"x\": -1, \"y\": 2 }, \"number\": 3 }, 	HEX 18
		 *{ \"resource\": \"sheep\", \"location\": { \"x\": 0, \"y\": 2 }, \"number\": 8 } ], 	HEX 19
		*/
		
		List<Hex> hexes = new ArrayList<Hex>();
		
		for (int i = 0; i < jsonHexes.size(); i++) {
			
			HexType htype = null;
			HexLocation hloc = null;
			int number = 0;
			
			JsonObject thisHex = jsonHexes.get(i).getAsJsonObject();
			
			// figure out HexType
			JsonElement jsonResource = thisHex.get("resource");
			htype = jsonToHexType(jsonResource);
			
			// figure out HexLocation
			JsonElement jsonLocation = thisHex.get("location");
			
			if (jsonLocation.isJsonNull()) {
				throw new IllegalArgumentException("Hex #" + i + " has no HexLocation.");
			}
			
			else {
				JsonObject objLocation = jsonLocation.getAsJsonObject();
				int x = objLocation.get("x").getAsInt();
				int y = objLocation.get("y").getAsInt();
				
				hloc = new HexLocation(x, y);
			}
			
			// figure out number token
			JsonElement jsonNumber = thisHex.get("number");
			
			if (jsonNumber == null) {
				if ((htype != HexType.DESERT) && (htype != HexType.WATER)) {
					throw new IllegalArgumentException("Hex #" + i + " has no number token.");
				}
			}
			
			else {
				number = jsonNumber.getAsInt();
			}
			
			// add Hex to the list
			Hex newHex = new Hex(htype, hloc, number, false);
			hexes.add(newHex);
		}
		
		// add ocean hexes too
		hexes.add(new Hex(HexType.WATER, new HexLocation(0, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(0, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(1, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(1, 2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(2, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(2, 1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-1, -2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-1, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-2, -1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-2, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, 0), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 0), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 3), 0, false));
		
		return hexes;
	}
	
	private HexType jsonToHexType(JsonElement jsonResource) {
		
		HexType htype = null;
		
		if (jsonResource == null) {
			htype = HexType.DESERT;
		}
		
		else {
			String resource = jsonResource.getAsString();
			
			if (resource.equals("brick")) {
				htype = HexType.BRICK;
			}
			
			else if (resource.equals("wood")) {
				htype = HexType.WOOD;
			}
			
			else if (resource.equals("wheat")) {
				htype = HexType.WHEAT;
			}
			
			else if (resource.equals("sheep")) {
				htype = HexType.SHEEP;
			}
			
			else if (resource.equals("ore")) {
				htype = HexType.ORE;
			}
			
			else {
				throw new IllegalArgumentException("Invalid or missing HexType.");
			}
		}
		
		return htype;
	}
	
	private PortType stringToPortType(String strPortType) {
		PortType ptype = null;
		
		if (strPortType == null || strPortType.isEmpty()) {
			// do nothing
		}
		
		else {
			if (strPortType.equalsIgnoreCase("brick")) {
				ptype = PortType.BRICK;
			}
			
			else if (strPortType.equalsIgnoreCase("wood")) {
				ptype = PortType.WOOD;
			}
			
			else if (strPortType.equalsIgnoreCase("wheat")) {
				ptype = PortType.WHEAT;
			}
			
			else if (strPortType.equalsIgnoreCase("sheep")) {
				ptype = PortType.SHEEP;
			}
			
			else if (strPortType.equalsIgnoreCase("ore")) {
				ptype = PortType.ORE;
			}
			
			else if (strPortType.equalsIgnoreCase("three")) {
				ptype = PortType.THREE;
			}
		}
		
		return ptype;
	}
	
	private ResourceType jsonToResourceType(JsonElement jsonResource) {
		
		ResourceType rtype = null;
		
		if (jsonResource == null) {
			// do nothing
		}
		
		else {
			String resource = jsonResource.getAsString();
			
			if (resource.equalsIgnoreCase("brick")) {
				rtype = ResourceType.BRICK;
			}
			
			else if (resource.equalsIgnoreCase("wood")) {
				rtype = ResourceType.WOOD;
			}
			
			else if (resource.equalsIgnoreCase("wheat")) {
				rtype = ResourceType.WHEAT;
			}
			
			else if (resource.equals("sheep")) {
				rtype = ResourceType.SHEEP;
			}
			
			else if (resource.equals("ore")) {
				rtype = ResourceType.ORE;
			}
		}
		
		return rtype;
	}
	
	/**
	 * Adds pieces to their Hexes
	 * @param hexes The list of Hexes in the board.
	 * @param jsonRobberLoc A JsonObject representing the location of the Robber.
	 * @param jsonRoads A JsonArray representing all the Roads on the board.
	 * @param jsonCities A JsonArray representing all the Cities on the board.
	 * @param jsonSettlements A JsonArray representing all the Settlements on the board.
	 * @return {@code hexes}, updated so that each Hex has a reference to all the Roads and Buildings on it, 
	 * and with the Hex containing the Robber marked. 
	 */
	private Board addPieces(Board board, JsonObject jsonRobberLoc, JsonArray jsonRoads, JsonArray jsonCities, JsonArray jsonSettlements, JsonArray jsonPorts, List<Player> players) {
		/*
		*\"roads": [
      	{
        	"owner": 0,
        	"location": {
          	"direction": "S",
          	"x": 0,
          	"y": 0
        	}
      	}
    	], 
		 *\"cities\": [], 
		 *\"settlements\": [], 
		 *
		 *\"radius\": 3, 
		 *
		 *\"ports\": [ 
		 *{ \"ratio\": 3, \"direction\": \"SE\", \"location\": { \"x\": -3, \"y\": 0 } }, 
		 *{ \"ratio\": 3, \"direction\": \"N\", \"location\": { \"x\": 0, \"y\": 3 } }, 
		 *{ \"ratio\": 3, \"direction\": \"NW\", \"location\": { \"x\": 2, \"y\": 1 } }, 
		 *{ \"ratio\": 2, \"resource\": \"sheep\", \"direction\": \"NW\", \"location\": { \"x\": 3, \"y\": -1 } }, 
		 *{ \"ratio\": 2, \"resource\": \"brick\", \"direction\": \"NE\", \"location\": { \"x\": -2, \"y\": 3 } }, 
		 *{ \"ratio\": 2, \"resource\": \"wood\", \"direction\": \"NE\", \"location\": { \"x\": -3, \"y\": 2 } }, 
		 *{ \"ratio\": 2, \"resource\": \"ore\", \"direction\": \"S\", \"location\": { \"x\": 1, \"y\": -3 } }, 
		 *{ \"ratio\": 3, \"direction\": \"SW\", \"location\": { \"x\": 3, \"y\": -3 } }, 
		 *{ \"ratio\": 2, \"resource\": \"wheat\", \"direction\": \"S\", \"location\": { \"x\": -1, \"y\": -2 } } ],
		 *
		 * \"robber\": { \"x\": 0, \"y\": -1 } }
		*/
		
		List<Hex> hexes = board.getAllHexes();
		
		// figure out Robber's location
		int x = jsonRobberLoc.get("x").getAsInt();
		int y = jsonRobberLoc.get("y").getAsInt();
		
		HexLocation robberLoc = new HexLocation(x, y);
		Hex robberHex = board.getHexByLocation(robberLoc);
		board.setRobberHex(robberHex);
		
		//for (Hex h : hexes) {
		//	if (h.getHexLocation().equals(robberLoc)) {
		//		h.setHasRobber(true);
		//	}
		//}
		
		// add Roads
		for (int r = 0; r < jsonRoads.size(); r++) {
			JsonObject thisRoad = jsonRoads.get(r).getAsJsonObject();
			int ownerIndex = thisRoad.get("owner").getAsInt();
			
			JsonObject thisRoadLoc = thisRoad.get("location").getAsJsonObject();
			int roadX = thisRoadLoc.get("x").getAsInt();
			int roadY = thisRoadLoc.get("y").getAsInt();
			
			try {
				EdgeDirection edir = stringToEdgeDirection(thisRoadLoc.get("direction").getAsString());
			
				HexLocation hloc = new HexLocation(roadX, roadY);
				EdgeLocation eloc = new EdgeLocation(hloc, edir);
				Hex h = null;
				
				try {
					h = board.getHex(eloc.getNormalizedLocation().getHexLoc());
					eloc = eloc.getNormalizedLocation();
				}
				
				catch (InvalidLocationException ile) {
					h = board.getHex(eloc.getHexLoc());
				}
				
				Road road = new Road(eloc, ownerIndex);
				if (h != null) {
					h.addRoad(eloc, road);
				}
				
				/*
				for (Hex h : hexes) {
					if (h.getHexLocation().equals(eloc.getNormalizedLocation().getHexLoc())) { // normalized Hex loc might be different, so DON'T REUSE hloc
						h.addRoad(eloc.getNormalizedLocation(), road);
						roadAdded = true;
						break;
					}
				}
				
				if (!roadAdded) {
					// road is on an edge hex, normalized location is on the ocean
					for (Hex h : hexes) {
						if (h.getHexLocation().equals(eloc.getHexLoc())) {
							h.addRoad(eloc,  road);
							break;
						}
					}
				}
				*/
			}
			catch (InvalidLocationException ile) {
				ile.printStackTrace();
			}
		}
		
		// build Port vertex map
		Map<VertexLocation, SimpleEntry<Integer, ResourceType>> portVertexMap = new HashMap<VertexLocation, SimpleEntry<Integer, ResourceType>>();
		
		for (int p = 0; p < jsonPorts.size(); p++) {
			JsonObject thisPort = jsonPorts.get(p).getAsJsonObject();
			
			JsonElement jsonResource = thisPort.get("resource");
			ResourceType resource = jsonToResourceType(jsonResource);
			
			int ratio = thisPort.get("ratio").getAsInt();
			String direction = thisPort.get("direction").getAsString();
			JsonObject loc = thisPort.get("location").getAsJsonObject();
			
			int portX = loc.get("x").getAsInt();
			int portY = loc.get("y").getAsInt();
			
			try {
				EdgeDirection edir = stringToEdgeDirection(direction);
				HexLocation hloc = new HexLocation(portX, portY);
				EdgeLocation eloc = new EdgeLocation(hloc, edir).getNormalizedLocation();
				
				VertexLocation vloc1 = edgeLocToNormalizedVertexLoc(eloc, true);
				VertexLocation vloc2 = edgeLocToNormalizedVertexLoc(eloc, false);
				
				SimpleEntry<Integer, ResourceType> pEntry = new SimpleEntry<Integer, ResourceType>(ratio, resource);
				portVertexMap.put(vloc1, pEntry);
				portVertexMap.put(vloc2, pEntry);
			}
			catch (InvalidLocationException ile) {
				ile.printStackTrace();
			}
		}
		
		// add Cities, check for Port locations
		for (int c = 0; c < jsonCities.size(); c++) {
			JsonObject thisCity = jsonCities.get(c).getAsJsonObject();
			int ownerIndex = thisCity.get("owner").getAsInt();
			
			JsonObject thisCityLoc = thisCity.get("location").getAsJsonObject();
			int cityX = thisCityLoc.get("x").getAsInt();
			int cityY = thisCityLoc.get("y").getAsInt();
			
			try {
				VertexDirection vdir = stringToVertexDirection(thisCityLoc.get("direction").getAsString());
				
				HexLocation hloc = new HexLocation(cityX, cityY);
				VertexLocation vloc = new VertexLocation(hloc, vdir);
				Hex h = null;
				try {
					h = board.getHex(vloc.getNormalizedLocation().getHexLoc());
					vloc = vloc.getNormalizedLocation();
				}
				catch (InvalidLocationException ile) {
					h = board.getHex(vloc.getHexLoc());
				}
				
				City city = new City(vloc, ownerIndex);
				if (h != null) {
					h.addBuilding(vloc, city);
				}
				
				/*
				for (Hex h : hexes) {
					if (h.getHexLocation().equals(vloc.getNormalizedLocation().getHexLoc())) { // normalized Hex loc might be different, so DON'T REUSE hloc
						h.addBuilding(vloc.getNormalizedLocation(), city);
						cityAdded = true;
						break;
					}
				}
				
				if (!cityAdded) {
					// city is on an edge hex, normalized location is in the ocean
					for (Hex h : hexes) {
						if (h.getHexLocation().equals(vloc.getHexLoc())) {
							h.addBuilding(vloc, city);
							break;
						}
					}
				}
				*/
				
				// check for Port, update owner's trade rate
				if (portVertexMap.containsKey(vloc.getNormalizedLocation())) {
					SimpleEntry<Integer, ResourceType> port = portVertexMap.get(vloc.getNormalizedLocation());
					Player owner = players.get(ownerIndex);
					
					updatePlayerTradeRate(owner, port);
				}
			}
			
			catch (InvalidLocationException ile) {
				ile.printStackTrace();
			}
		}
		
		// add Settlements, check for Port locations
		for (int s = 0; s < jsonSettlements.size(); s++) {
			JsonObject thisSettlement = jsonSettlements.get(s).getAsJsonObject();
			int ownerIndex = thisSettlement.get("owner").getAsInt();
			
			JsonObject thisSettlementLoc = thisSettlement.get("location").getAsJsonObject();
			int settleX = thisSettlementLoc.get("x").getAsInt();
			int settleY = thisSettlementLoc.get("y").getAsInt();
			
			try {
				VertexDirection vdir = stringToVertexDirection(thisSettlementLoc.get("direction").getAsString());
				
				HexLocation hloc = new HexLocation(settleX, settleY);
				VertexLocation vloc = new VertexLocation(hloc, vdir);
				Hex h = null;
				
				try {
					h = board.getHex(vloc.getNormalizedLocation().getHexLoc());
					vloc = vloc.getNormalizedLocation();
				}
				catch (InvalidLocationException ile) {
					h = board.getHex(vloc.getHexLoc());
				}
				
				Settlement settlement = new Settlement(vloc, ownerIndex);
				if (h != null) {
					h.addBuilding(vloc, settlement);
				}
				
				/*
				for (Hex h : hexes) {
					if (h.getHexLocation().equals(vloc.getNormalizedLocation().getHexLoc())) { // normalized Hex loc might be different, so DON'T REUSE hloc
						h.addBuilding(vloc.getNormalizedLocation(), settlement);
						settlementAdded = true;
						break;
					}
				}
				
				if (!settlementAdded) {
					// settlement is on an edge hex, normalized location is in the ocean
					for (Hex h : hexes) {
						if (h.getHexLocation().equals(vloc.getHexLoc())) {
							h.addBuilding(vloc,  settlement);
							break;
						}
					}
				}
				*/
				
				// check for Port, update owner's trade rate
				VertexLocation normal = vloc.getNormalizedLocation();
				if (portVertexMap.containsKey(vloc.getNormalizedLocation())) {
					SimpleEntry<Integer, ResourceType> port = portVertexMap.get(vloc.getNormalizedLocation());
					Player owner = players.get(ownerIndex);
					
					updatePlayerTradeRate(owner, port);
				}
			}
			catch (InvalidLocationException ile) {
				ile.printStackTrace();
			}
		}
		
		return board;
	}
	
	/**
	 * Helper method to convert an EdgeLocation to two VertexLocations representing the endpoints of the edge.
	 * @param eloc EdgeLocation to convert
	 * @param firstInClockwiseOrder Boolean flag indicating whether to return the endpoint that comes first in a clockwise ordering, or the one that comes second.
	 * The order starts counting from 12:00 (directly North).
	 * @return A VertexLocation representing one of the endpoints of the given edge.
	 */
	private VertexLocation edgeLocToNormalizedVertexLoc(EdgeLocation eloc, boolean firstInClockwiseOrder) {
		
		EdgeDirection edir = eloc.getDir();
		
		VertexDirection vdir = null;
		
		switch (edir) {
		case North:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.NorthEast;
			}
			else {
				vdir = VertexDirection.NorthWest;
			}
			break;
		case NorthEast:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.NorthEast;
			}
			else {
				vdir = VertexDirection.East;
			}
			break;
		case SouthEast:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.East;
			}
			else {
				vdir = VertexDirection.SouthEast;
			}
			break;
		case South:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.SouthEast;
			}
			else {
				vdir = VertexDirection.SouthWest;
			}
			break;
		case SouthWest:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.SouthWest;
			}
			else {
				vdir = VertexDirection.West;
			}
			break;
		case NorthWest:
			if (firstInClockwiseOrder) {
				vdir = VertexDirection.West;
			}
			else {
				vdir = VertexDirection.NorthWest;
			}
			break;
		}
		
		VertexLocation normal = new VertexLocation(eloc.getHexLoc(), vdir).getNormalizedLocation();
		return normal;
	}
	
	private void updatePlayerTradeRate(Player p, SimpleEntry<Integer, ResourceType> port) {
		
		int ratio = port.getKey();
		ResourceType resource = port.getValue();
		
		// 3:1 ports are generic
		if (ratio == 3) {
			for (int i = 0; i < 5; i++) {
				if (p.getTradeValue(i) < 3) {
					continue;
				}
				else {
					p.setTradeRate(i, ratio);
				}
			}
		}
			
		// 2:1 ports are resource-specific
		else if ((ratio == 2) && (resource != null)) {
			int rpos = resource.ordinal();
			p.setTradeRate(rpos, ratio);
		}
		
		else {
			throw new IllegalArgumentException("Invalid port! Ratio: " + ratio + ", Resource type: "  + resource.toJson());
		}
	}
	
	/**
	 * Helper method to convert a string to an EdgeDirection
	 * @param dir The string representing an EdgeDirection.
	 * @return The EdgeDirection corresponding to {@code dir}.
	 * @throws InvalidLocationException If {@code dir} cannot be mapped to an EdgeDirection.
	 */
	private EdgeDirection stringToEdgeDirection(String dir) throws InvalidLocationException {
		
		if (dir.equalsIgnoreCase("N")) {
			return EdgeDirection.North;
		}
		
		else if (dir.equalsIgnoreCase("S")) {
			return EdgeDirection.South;
		}
		
		else if (dir.equalsIgnoreCase("NE")) {
			return EdgeDirection.NorthEast;
		}
		
		else if (dir.equalsIgnoreCase("NW")) {
			return EdgeDirection.NorthWest;
		}
		
		else if (dir.equalsIgnoreCase("SE")) {
			return EdgeDirection.SouthEast;
		}
		
		else if (dir.equalsIgnoreCase("SW")) {
			return EdgeDirection.SouthWest;
		}
		
		else {
			throw new InvalidLocationException("Invalid EdgeDirection specified: " + dir);
		}
	}
	
	/**
	 * Helper method to convert a string to a VertexDirection
	 * @param dir The string representing a VertexDirection.
	 * @return The VertexDirection corresponding to {@code dir}.
	 * @throws InvalidLocationException If {@code dir} cannot be mapped to a VertexDirection.
	 */
	private VertexDirection stringToVertexDirection(String dir) throws InvalidLocationException {
		
		if (dir.equalsIgnoreCase("E")) {
			return VertexDirection.East;
		}
		
		else if (dir.equalsIgnoreCase("W")) {
			return VertexDirection.West;
		}
		
		else if (dir.equalsIgnoreCase("NE")) {
			return VertexDirection.NorthEast;
		}
		
		else if (dir.equalsIgnoreCase("NW")) {
			return VertexDirection.NorthWest;
		}
		
		else if (dir.equalsIgnoreCase("SE")) {
			return VertexDirection.SouthEast;
		}
		
		else if (dir.equalsIgnoreCase("SW")) {
			return VertexDirection.SouthWest;
		}
		
		else {
			throw new InvalidLocationException("Invalid VertexDirection specified: " + dir);
		}
	}
	
	/**
	 * Helper method to convert a string to a CatanColor
	 * @param color The string representing a CatanColor.
	 * @return The CatanColor corresponding to {@code color}.
	 */
	private CatanColor stringToColor(String color) {
		
		if (color.equalsIgnoreCase("red")) {
			return CatanColor.RED;
		}
		
		else if (color.equalsIgnoreCase("blue")) {
			return CatanColor.BLUE;
		}
		
		else if (color.equalsIgnoreCase("green")) {
			return CatanColor.GREEN;
		}
		
		else if (color.equalsIgnoreCase("brown")) {
			return CatanColor.BROWN;
		}
		
		else if (color.equalsIgnoreCase("orange")) {
			return CatanColor.ORANGE;
		}
		
		else if (color.equalsIgnoreCase("puce")) {
			return CatanColor.PUCE;
		}
		
		else if (color.equalsIgnoreCase("purple")) {
			return CatanColor.PURPLE;
		}
		
		else if (color.equalsIgnoreCase("white")) {
			return CatanColor.WHITE;
		}
		
		else if (color.equalsIgnoreCase("yellow")) {
			return CatanColor.YELLOW;
		}
		
		else {
			throw new IllegalArgumentException("Unknown CatanColor specified.");
		}
	}

	/**
	 * Builds the GameManager from JSON components
	 * @param jsonBank A JsonObject representing ResourceCards that don't belong to any Player.
	 * @param jsonDeck A JsonObject representing DevCards that don't belong to any Player.
	 * @param jsonPlayers A JsonArray representing the Players in the current game.
	 * @param jsonTurnTracker A JsonObject representing the turn tracker for the current game.
	 * @return A new GameManager object, instantiated from the JSON.
	 */
	private GameManager parseGameManager(JsonObject jsonBank, JsonObject jsonDeck, JsonArray jsonPlayers, JsonObject jsonTurnTracker, JsonObject jsonTradeOffer) {
		
		// ResourceCards
		int numWood = jsonBank.get("wood").getAsInt();
		int numBrick = jsonBank.get("brick").getAsInt();
		int numSheep = jsonBank.get("sheep").getAsInt();
		int numWheat = jsonBank.get("wheat").getAsInt();
		int numOre = jsonBank.get("ore").getAsInt();
		
		List<ResourceCard> woodList = buildResourceList(numWood, ResourceType.WOOD);
		List<ResourceCard> brickList = buildResourceList(numBrick, ResourceType.BRICK);
		List<ResourceCard> sheepList = buildResourceList(numSheep, ResourceType.SHEEP);
		List<ResourceCard> wheatList = buildResourceList(numWheat, ResourceType.WHEAT);
		List<ResourceCard> oreList = buildResourceList(numOre, ResourceType.ORE);
		
		// DevCards
		List<DevCard> gmDevList = buildDevCardList(jsonDeck, false);
		
		// Players
		/* "\"players\": 
		 * [ { \"resources\": { \"brick\": 0, \"wood\": 0, \"sheep\": 0, \"wheat\": 0, \"ore\": 0 }, 
		 * \"oldDevCards\": { \"yearOfPlenty\": 0, \"monopoly\": 0, \"soldier\": 0, \"roadBuilding\": 0, \"monument\": 0 }, 
		 * \"newDevCards\": { \"yearOfPlenty\": 0, \"monopoly\": 0, \"soldier\": 0, \"roadBuilding\": 0, \"monument\": 0 }, 
		 * \"roads\": 15, 
		 * \"cities\": 4, 
		 * \"settlements\": 5, 
		 * \"soldiers\": 0, 
		 * \"victoryPoints\": 0, 
		 * \"monuments\": 0, 
		 * \"playedDevCard\": false, 
		 * \"discarded\": false, 
		 * \"playerID\": 12, 
		 * \"playerIndex\": 0, 
		 * \"name\": \"rfl\", 
		 * \"color\": \"red\" }, null, null, null ], "
		*/
		
		Map<Integer, Player> players = new HashMap<Integer, Player>();
		List<PlayerInfo> gameInfoPlayers = new ArrayList<PlayerInfo>();
		
		for (int i = 0; i < jsonPlayers.size(); i++) {
			JsonElement thisPlayer = jsonPlayers.get(i);
			if (thisPlayer.isJsonNull()) {
				continue;
			}
			
			else {
				JsonObject playerObj = thisPlayer.getAsJsonObject();
				
				// 'personal' info
				int id = playerObj.get("playerID").getAsInt();
				int index = playerObj.get("playerIndex").getAsInt();
				String pName = playerObj.get("name").getAsString();
				CatanColor pColor = stringToColor(playerObj.get("color").getAsString());
				
				// 'game' info
				gameInfoPlayers.add(new PlayerInfo(id, index, pName, pColor));
				
				// player's resource cards
				JsonObject pResources = playerObj.get("resources").getAsJsonObject();
				int pNumWood = pResources.get("wood").getAsInt();
				int pNumBrick = pResources.get("brick").getAsInt();
				int pNumSheep = pResources.get("sheep").getAsInt();
				int pNumWheat = pResources.get("wheat").getAsInt();
				int pNumOre = pResources.get("ore").getAsInt();
				
				List<ResourceCard> pWoodList = buildResourceList(pNumWood, ResourceType.WOOD);
				List<ResourceCard> pBrickList = buildResourceList(pNumBrick, ResourceType.BRICK);
				List<ResourceCard> pSheepList = buildResourceList(pNumSheep, ResourceType.SHEEP);
				List<ResourceCard> pWheatList = buildResourceList(pNumWheat, ResourceType.WHEAT);
				List<ResourceCard> pOreList = buildResourceList(pNumOre, ResourceType.ORE);
				
				List<ResourceCard> finalResourceList = new ArrayList<ResourceCard>();
				finalResourceList.addAll(pWoodList);
				finalResourceList.addAll(pBrickList);
				finalResourceList.addAll(pSheepList);
				finalResourceList.addAll(pWheatList);
				finalResourceList.addAll(pOreList);
				
				// player's development cards
				JsonObject pOldDevCards = playerObj.get("oldDevCards").getAsJsonObject();
				JsonObject pNewDevCards = playerObj.get("newDevCards").getAsJsonObject();
				List<DevCard> pDevList = buildDevCardList(pOldDevCards, false);
				pDevList.addAll(buildDevCardList(pNewDevCards, true));
				
				// see if player has played a dev card this turn
				boolean playedDevCard = playerObj.get("playedDevCard").getAsBoolean();
				
				// not sure if we need this
				boolean discarded = playerObj.get("discarded").getAsBoolean();
				
				// player's Buildings
				int numRoads = playerObj.get("roads").getAsInt();
				int numCities = playerObj.get("cities").getAsInt();
				int numSettlements = playerObj.get("settlements").getAsInt();
				
				// victory points, number of soldiers and monuments in play
				int victoryPoints = playerObj.get("victoryPoints").getAsInt();
				int pSoldiers = playerObj.get("soldiers").getAsInt();
				int pMonuments = playerObj.get("monuments").getAsInt();
				
				Player p = new Player(id, pName, pColor, victoryPoints, numRoads, numCities, numSettlements, finalResourceList, pDevList, discarded, playedDevCard, pSoldiers, pMonuments);
				
				players.put(index, p);
			}
		}
		
		if(Facade.getGameInfo() != null) {
			Facade.getGameInfo().setPlayers(gameInfoPlayers);
		}
		
		Player p1 = players.get(0);
		Player p2 = players.get(1);
		Player p3 = players.get(2);
		Player p4 = players.get(3);
		
		List<Player> playerList = new ArrayList<Player>();
		playerList.add(p1);
		playerList.add(p2);
		playerList.add(p3);
		playerList.add(p4);
		
		// "\"turnTracker\": { \"status\": \"FirstRound\", \"currentTurn\": 0, \"longestRoad\": -1, \"largestArmy\": -1 }";
		String status = jsonTurnTracker.get("status").getAsString();
		int currentTurn = jsonTurnTracker.get("currentTurn").getAsInt();
		int longestRoadIndex = jsonTurnTracker.get("longestRoad").getAsInt();
		int largestArmyIndex = jsonTurnTracker.get("largestArmy").getAsInt();
		
//		LongestRoad lr = new LongestRoad();
//		if (longestRoadIndex > -1 && longestRoadIndex < 4) {
//			Player longestRoadOwner = playerList.get(longestRoadIndex);
//			lr.setOwnedBy(longestRoadOwner);
//		}
//		LargestArmy la = new LargestArmy();
//		if (largestArmyIndex > -1 && largestArmyIndex < 4) {
//			Player largestArmyOwner = playerList.get(largestArmyIndex);
//			int armySize = largestArmyOwner.getSoldiers();
//			
//			la.setOwnedBy(largestArmyOwner);
//			la.setSize(armySize);
//		}
		
		// tradeOffer
		PlayerInfo senderPI = null;
		PlayerInfo receiverPI = null;
		int[] offerArray = null;
		
		if (jsonTradeOffer != null) {
			int sender = jsonTradeOffer.get("sender").getAsInt();
			int receiver = jsonTradeOffer.get("receiver").getAsInt();
			JsonObject offer = jsonTradeOffer.get("offer").getAsJsonObject();
			
			senderPI = new PlayerInfo();
			senderPI.setPlayerIndex(sender);
			senderPI.setName(playerList.get(sender).getName());
			senderPI.setId(playerList.get(sender).getId());
			senderPI.setColor(playerList.get(sender).getColor());
			
			receiverPI = new PlayerInfo();
			receiverPI.setPlayerIndex(receiver);
			receiverPI.setName(playerList.get(receiver).getName());
			receiverPI.setId(playerList.get(receiver).getId());
			receiverPI.setColor(playerList.get(receiver).getColor());
			
			offerArray = new int[5];
			offerArray[0] = offer.get("wood").getAsInt();
			offerArray[1] = offer.get("brick").getAsInt();
			offerArray[2] = offer.get("sheep").getAsInt();
			offerArray[3] = offer.get("wheat").getAsInt();
			offerArray[4] = offer.get("ore").getAsInt();
		}
		
		GameManager gm = new GameManager();
		gm.setPlayers((ArrayList<Player>)playerList);
		gm.setCurrentPlayer(currentTurn % 4);
		gm.setWood((ArrayList<ResourceCard>)woodList);
		gm.setBrick((ArrayList<ResourceCard>)brickList);
		gm.setSheep((ArrayList<ResourceCard>)sheepList);
		gm.setWheat((ArrayList<ResourceCard>)wheatList);
		gm.setOre((ArrayList<ResourceCard>)oreList);
		gm.setDevelopmentCards((ArrayList<DevCard>)gmDevList);
		gm.setLongestRoad(longestRoadIndex);
		gm.setLargestArmy(largestArmyIndex);
		gm.setStatus(status);
		gm.setTradeSender(senderPI);
		gm.setTradeReciever(receiverPI);
		gm.setTradeOffer(offerArray);
		
		return gm;
	}

	
	/**
	 * Helper method to create a List of ResourceCards
	 * @param amount The amount of this resource to put in the List.
	 * @param type The type of resource.
	 * @return A List of {@code amount} ResourceCards, all with the same ResourceType {@code type}.
	 */
	private List<ResourceCard> buildResourceList(int amount, ResourceType type) {
		
		List<ResourceCard> result = new ArrayList<ResourceCard>();
		
		for (int i = 0; i < amount; i++) {
			result.add(new ResourceCard(type));
		}
		
		return result;
	}
	
	/**
	 * Helper method to create a List of DevCards
	 * @param jsonObj A JSON object representing a list of DevCards
	 * @param sameTurn A Boolean flag indicating whether these cards were bought this turn or on a previous turn
	 * @return A List of DevCards, with the specified number of each type.
	 */
	private List<DevCard> buildDevCardList(JsonObject jsonObj, boolean sameTurn) {
		
		int yops = jsonObj.get("yearOfPlenty").getAsInt();
		int monopolies = jsonObj.get("monopoly").getAsInt();
		int soldiers = jsonObj.get("soldier").getAsInt();
		int rbs = jsonObj.get("roadBuilding").getAsInt();
		int monuments = jsonObj.get("monument").getAsInt();
		
		List<DevCard> result = new ArrayList<DevCard>();
		
		// Year of Plenty
		for (int y = 0; y < yops; y++) {
			DevCard d = new DevCard(0, DevCardType.YEAR_OF_PLENTY);
			d.setSameTurn(sameTurn);
			result.add(d);
		}
		
		// Monopoly
		for (int m = 0; m < monopolies; m++) {
			DevCard d = new DevCard(1, DevCardType.MONOPOLY);
			d.setSameTurn(sameTurn);
			result.add(d);
		}
		
		// Soldier
		for (int s = 0; s < soldiers; s++) {
			DevCard d = new DevCard(2, DevCardType.SOLDIER);
			d.setSameTurn(sameTurn);
			result.add(d);
		}
		
		// Road Building
		for (int r = 0; r < rbs; r++) {
			DevCard d = new DevCard(3, DevCardType.ROAD_BUILD);
			d.setSameTurn(sameTurn);
			result.add(d);
		}
		
		// Monument - can be played immediately
		for (int o = 0; o < monuments; o++) {
			DevCard d = new DevCard(4, DevCardType.MONUMENT);
			d.setSameTurn(false);
			result.add(d);
		}
		
		return result;
	}
	
	/**
	 * Parses a JsonObject representing either the game history or the chat log.
	 * @param jsonLog The JsonObject to be parsed into a List of LogEntry objects.
	 * @param players The List of Players in the game.
	 * //@param isHistory A flag indicating whether this is the game history or the chat log.
	 * @return A new List of LogEntry objects instantiated from the JSON.
	 */
	private List<ChatEntry> parseChat(JsonObject jsonLog, List<Player> players) {
		
		// "chat": {
		//  "lines": [
		//    {
		//      "source": "rfl",
		//      "message": "anybody there"
		//    }
		// ]
		// }
		
		List<ChatEntry> result = new ArrayList<ChatEntry>();
		JsonArray jsonLines = jsonLog.get("lines").getAsJsonArray();
		
		if (jsonLines.isJsonNull()) {
			// no lines, so just return an empty list
			return result;
		}
		
		else {
			for (int i = 0; i < jsonLines.size(); i++) {
				
				JsonObject line = jsonLines.get(i).getAsJsonObject();
				String source = (!line.get("source").isJsonNull() && line.get("source") != null) ? 
						line.get("source").getAsString() : null;
						
				String message = (!line.get("message").isJsonNull() && line.get("message") != null) ? 
						line.get("message").getAsString() : null;
				
				if (source == null || message == null) {
					continue;
				}
						
				ChatEntry entry = null;
				
				// search through the list of players for one with the right name
				for (Player p : players) {
					if (p == null) {
						continue;
					}
					if ((p.getName() != null) && p.getName().equals(source)) {
						entry = new ChatEntry(p, message);
					}
				}
				
				if (entry != null) {
					result.add(entry);
				}
				
				else {
					// player is not in this game; throw an exception
					throw new IllegalArgumentException(source + " is not a valid player name.");
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Parses a JsonObject representing either the game history or the chat log.
	 * @param jsonLog The JsonObject to be parsed into a List of LogEntry objects.
	 * @param players The List of Players in the game.
	 * //@param isHistory A flag indicating whether this is the game history or the chat log.
	 * @return A new List of LogEntry objects instantiated from the JSON.
	 */
	private List<HistoryEntry> parseHistory(JsonObject jsonLog, List<Player> players) {
		
		// "chat": {
		//  "lines": [
		//    {
		//      "source": "rfl",
		//      "message": "anybody there"
		//    }
		// ]
		// }
		
		List<HistoryEntry> result = new ArrayList<HistoryEntry>();
		JsonArray jsonLines = jsonLog.get("lines").getAsJsonArray();
		
		if (jsonLines.isJsonNull()) {
			// no lines, so just return an empty list
			return result;
		}
		
		else {
			for (int i = 0; i < jsonLines.size(); i++) {
				
				JsonObject line = jsonLines.get(i).getAsJsonObject();
				String source = line.get("source").getAsString();
				String message = line.get("message").getAsString();
				
				HistoryEntry entry = null;
				
				// search through the list of players for one with the right name
				for (Player p : players) {
					if (p == null) {
						continue;
					}
					if ((p.getName() != null) && p.getName().equals(source)) {
							entry = new HistoryEntry(p, message);
					}
				}
				
				if (entry != null) {
					result.add(entry);
				}
				
				else {
					// player is not in this game; throw an exception
					throw new IllegalArgumentException(source + " is not a valid player name.");
				}
			}
		}
		
		return result;
	}
	
	public void parseGameList(String json) {
		
		JsonParser jp = new JsonParser();
		JsonArray gameArray = jp.parse(json).getAsJsonArray();
		
		List<GameInfo> games = new ArrayList<GameInfo>();
		
		for (int i = 0; i < gameArray.size(); i++) {
			
			JsonElement thisGame = gameArray.get(i);
			if ((thisGame == null) || thisGame.isJsonNull()) {
				continue;
			}
			
			else {
				JsonObject gameObj = thisGame.getAsJsonObject();
				String title = gameObj.get("title").getAsString();
				int gameId = gameObj.get("id").getAsInt();
				
				GameInfo gInfo = new GameInfo();
				gInfo.setTitle(title);
				gInfo.setId(gameId);
				
				JsonArray playerArray = gameObj.get("players").getAsJsonArray();
				for (int p = 0; p < playerArray.size(); p++) {
					
					JsonElement thisPlayer = playerArray.get(p);
					if ((thisPlayer == null) || thisPlayer.isJsonNull()) {
						continue;
					}
					
					else {
						JsonObject playerObj = thisPlayer.getAsJsonObject();
						if (!playerObj.has("name")) {
							break;
						}
						String name = playerObj.get("name").getAsString();
						CatanColor color = stringToColor(playerObj.get("color").getAsString());
						int playerId = playerObj.get("id").getAsInt();
						
						PlayerInfo pInfo = new PlayerInfo();
						pInfo.setName(name);
						pInfo.setColor(color);
						pInfo.setId(playerId);
						pInfo.setPlayerIndex(p);
						
						gInfo.addPlayer(pInfo);
					}
				}
				
				games.add(gInfo);
			}
		}
		
		this.setChanged();
		this.notifyObservers(games);
	}
	
	/**
	 * Method to parse a JSON string into a new game, and return the gameID
	 * @param json JSON string representing the new game.
	 * @return The game ID.
	 */
	public int parseNewGame(String json) {
		
		JsonParser jp = new JsonParser();
		JsonObject job = jp.parse(json).getAsJsonObject();
		
		int gameID = job.get("id").getAsInt();
		
		return gameID;
	}

	/**
	 * Parses user cookie json to return to proxy
	 * @param json string
	 * @return pid int
	 */
	public void parseUserCookie(String json) {
		if (json == null) { return; }
		JsonParser jp = new JsonParser();
		JsonObject job = jp.parse(json).getAsJsonObject();
		
		PlayerInfo pInfo = new PlayerInfo();
		int pid = job.get("playerID").getAsInt();
		String name = job.get("name").getAsString();
		System.out.println("Parsing: " + pid + " " + name);
		pInfo.setId(pid);
		pInfo.setName(name);
		pInfo.setPlayerIndex(-1);
		pInfo.setColor(null);
		
		this.setChanged();
		this.notifyObservers(pInfo);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataParser other = (DataParser) obj;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		return true;
	}
}
