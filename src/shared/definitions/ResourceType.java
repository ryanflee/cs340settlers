package shared.definitions;

public enum ResourceType
{
	WOOD, BRICK, SHEEP, WHEAT, ORE;
	
	public String toJson() {
		if(this.name().equals("WOOD")) {return "wood";}
		if(this.name().equals("BRICK")) {return "brick";}
		if(this.name().equals("SHEEP")) {return "sheep";}
		if(this.name().equals("WHEAT")) {return "wheat";}
		if(this.name().equals("ORE")) {return "ore";}
		return null;
	}
	
}

