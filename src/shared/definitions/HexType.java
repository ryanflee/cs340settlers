package shared.definitions;

public enum HexType
{
	
	WOOD, BRICK, SHEEP, WHEAT, ORE, DESERT, WATER;
	
	@Override
	public String toString() {
		switch(this) {
		case WOOD:
			return "wood";
		case BRICK:
			return "brick";
		case SHEEP:
			return "sheep";
		case WHEAT:
			return "wheat";
		case ORE:
			return "ore";
		}
		return null;
		
	}
}



