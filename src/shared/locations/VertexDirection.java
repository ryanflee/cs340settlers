package shared.locations;

public enum VertexDirection
{
	West, NorthWest, NorthEast, East, SouthEast, SouthWest;
	
	private VertexDirection opposite;
	
	static
	{
		West.opposite = East;
		NorthWest.opposite = SouthEast;
		NorthEast.opposite = SouthWest;
		East.opposite = West;
		SouthEast.opposite = NorthWest;
		SouthWest.opposite = NorthEast;
	}
	
	public VertexDirection getOppositeDirection()
	{
		return opposite;
	}
	
	public String toJson() {
		if(this.name().equals("West")) {return "W";}
		if(this.name().equals("NorthWest")) {return "NW";}
		if(this.name().equals("NorthEast")) {return "NE";}
		if(this.name().equals("East")) {return "E";}
		if(this.name().equals("SouthEast")) {return "SE";}
		if(this.name().equals("SouthWest")) {return "SW";}
		return null;		
	}
}

