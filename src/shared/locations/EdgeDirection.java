package shared.locations;

public enum EdgeDirection
{
	
	NorthWest, North, NorthEast, SouthEast, South, SouthWest;
	
	private EdgeDirection opposite;
	
	static
	{
		NorthWest.opposite = SouthEast;
		North.opposite = South;
		NorthEast.opposite = SouthWest;
		SouthEast.opposite = NorthWest;
		South.opposite = North;
		SouthWest.opposite = NorthEast;
	}
	
	public EdgeDirection getOppositeDirection()
	{
		return opposite;
	}
	
	public String toJson() {
		if(this.name().equals("NorthWest")) {return "NW";}
		if(this.name().equals("North")) {return "N";}
		if(this.name().equals("NorthEast")) {return "NE";}
		if(this.name().equals("SouthWest")) {return "SW";}
		if(this.name().equals("South")) {return "S";}
		if(this.name().equals("SouthEast")) {return "SE";}
		return null;

	}
}

