package plugins;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.persistence.IGiveUp;

public class TxtGiveUp implements IGiveUp {
	
	private TxtPluginUtils utils;
	
	public TxtGiveUp(TxtPluginUtils utils) {
		this.utils = utils;
	}
	
	@Override
	public boolean create(String type, String params, int gameId) {
		
		File cmdFile = utils.getCmdFile(gameId);
		
		try {
			if (!cmdFile.exists()) {
				cmdFile.createNewFile();
			}
			
			return utils.writeCommand(cmdFile, type, params);
		}
		catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}		
	}

	@Override
	public List<String> getCommands(int gameId) {
		
		List<String> commands = new ArrayList<String>();
		
		File cmdFile = utils.getCmdFile(gameId);
		
		if (cmdFile.exists()) {
			try (Scanner s = new Scanner(cmdFile)) {
				while (s.hasNextLine()) {
					commands.add(s.nextLine());
				}
			}
			catch (FileNotFoundException ex) {
				ex.printStackTrace();
			}
		}
		
		return commands;
	}

	@Override
	public boolean deleteAll(int gameId) {
		
		File cmdFile = utils.getCmdFile(gameId);
		try {
			if (cmdFile.exists()) {
				cmdFile.delete();
			}
			return cmdFile.createNewFile();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return false;
	}

	@Override
	public int enumerateCommands(int gameId) {
		
		File cmdFile = utils.getCmdFile(gameId);
		try (Scanner s = new Scanner(cmdFile)) {
			int result = 0;
			while (s.hasNextLine()) {
				s.nextLine();
				result++;
			}
			return result;
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		return 0;
	}
}
