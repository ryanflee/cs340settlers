package plugins;

import java.io.File;
import java.io.IOException;

import server.persistence.IGameDAO;
import org.apache.commons.io.*;
import server.persistence.IGiveUp;
import server.persistence.IPersistPlugin;
import server.persistence.IUserDAO;

/**
 * Class that implements the Plugin factory.  This uses a relational database as its structure for saving Data.
 * @author jjwilbur
 *
 */

public class SQLPlugin implements IPersistPlugin {
	private SQLDatabase db;
	
	public SQLPlugin() {
		db = new SQLDatabase();
	}
	/**
	 * The clear function will clear all data from the database.
	 */
	@Override
	public void clear() {
		db.clearDB();
	}

	/**
	 * This function starts a transaction with the database to know it is going to be accessing its information.
	 * @throws Exception 
	 */
	@Override
	public void startTransaction() throws Exception {
		db.startTransaction();
	}

	/**
	 * This function ends the transaction.  
	 * @param commit tells whether or not the transaction should commit or not.  If some error happened you will rollback the 
	 * commit as if no transaction ever took place.
	 */
	@Override
	public void endTransaction(boolean commit) {
		db.endTransaction(commit);
	}

	/**
	 * Function that gets a game data access object for an SQL style database.
	 */
	@Override
	public IGameDAO getGameDAO() {
		return db.getGameDAO();
	}

	/**
	 * Function that gets a user data access object for an SQL style database.
	 */
	@Override
	public IUserDAO getUserDAO() {
		return db.getUserDAO();
	}

	/**
	 * Function that gets the command object data access objects for an SQL style database.
	 */
	@Override
	public IGiveUp getCommandDAO() {
		return db.getGiveUp();
	}

	/**
	 * Function that saves the specified command to the database.
	 * @throws Exception 
	 */
	@Override
	public void saveCommand(String commandType, String commandParams, int gameId) throws Exception {
		db.getGiveUp().create(commandType, commandParams, gameId);
	}
}
