package plugins;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TxtPluginUtils {

	private File gamesPath;
	
	public TxtPluginUtils() {
		gamesPath = new File("./plugins/txt").getAbsoluteFile();
	}
	
	public TxtPluginUtils(String path) {
		gamesPath = new File(path).getAbsoluteFile();
	}
	
	public int getNextGameId() {
		boolean foundGames = false;
		int highestId = 0;
		for (File f : listFiles()) {
			if (f.isDirectory()) {
				String[] parts = f.getName().split("_");
				if (parts[0].equals("game")) {
					foundGames = true;
					int id = Integer.parseInt(parts[1]);
					if (id > highestId) {
						highestId = id;
					}
				}
			}
		}
		
		if (foundGames) {
			return highestId+1;
		}
		return 0;
	}
	
	public int getNextUserId() {
		File userFile = getUserFile();
		
		if (userFile.exists()) {
			try (Scanner s = new Scanner(userFile)) {
				int highestId = 0;
				while (s.hasNextLine()) {
					String line = s.nextLine();
					int id = Integer.parseInt(line.split("\\^_\\^")[0]);
					if (id > highestId) {
						highestId = id;
					}
				}
				
				return highestId;
			}
			catch (IOException ex) {
				ex.printStackTrace();
				return 0;
			}
		}
		else {
			return 0;
		}
	}
	
	public int writeUser(File userFile, int id, String username, String password) throws IOException {
		PrintWriter writer = new PrintWriter(new FileOutputStream(userFile, true), true);
		writer.println(id + "^_^" + username + "^_^" + password);
		writer.close();
		return id;
	}
	
	public File getUserFile() {
		return new File(gamesPath.getAbsolutePath() + File.separatorChar + "users.json");
	}
	
	public File getGameDir(int id) {
		return new File(gamesPath.getAbsolutePath() + File.separatorChar + "game_" + id);
	}
	
	public boolean deleteGameDir(File gameDir) {
		
		for (File f : gameDir.listFiles()) {
			f.delete();
		}
		
		return gameDir.delete();
	}
	
	public File getGameFile(int id) {
		return new File(getGameDir(id).getAbsolutePath() + File.separatorChar + "game.json");
	}
	
	public File getCmdFile(int id) {
		return new File(getGameDir(id).getAbsolutePath() + File.separatorChar + "cmds.json");
	}
	
	public boolean writeGame(File gameFile, int id, String name, String game) throws IOException {
		PrintWriter gameWriter = new PrintWriter(gameFile); 
		gameWriter.println(id + "^_^" + name + "^_^" + game);
		gameWriter.close();
		return true;
	}
	
	public boolean writeCommand(File cmdFile, String type, String params) throws IOException {
		PrintWriter writer = new PrintWriter(new FileOutputStream(cmdFile, true), true);
		writer.println(type + "^_^" + params);
		writer.close();
		return true;
	}
	
	public File[] listFiles() {
		return gamesPath.listFiles();
	}
}
