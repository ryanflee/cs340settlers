package plugins;

import java.sql.*;
import java.util.*;
import server.data.lazy.User;
import server.persistence.IUserDAO;

public class SQLUserDAO implements IUserDAO {
	
	private SQLDatabase db;
	
	public SQLUserDAO(SQLDatabase db) {
		this.db = db;
	}
	
	@Override
	public int register(String username, String password) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		try {
			String query = "INSERT INTO users (username,password,pid) VALUES (?,?,?)";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.setInt(3, this.getUsers().size());
			if(stmt.executeUpdate() == 1) {
				return this.getUsers().size()-1;
			}
			else {
				throw new Exception("Could not insert user");
			}
		}
		catch (SQLException e) {
			throw new Exception("Could not insert user into database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public List<String> getUsers() throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		try {
			String query = "SELECT pid, username, password FROM users";
			stmt = db.getConnection().prepareStatement(query);
			keyRS = stmt.executeQuery();
			while(keyRS.next()) {
				int id = keyRS.getInt(1);
				String username = keyRS.getString(2);
				String password = keyRS.getString(3);
				rval.add(id+"^_^" + username + "^_^" + password);
			}
			return rval;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public boolean delete(int id) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		try {
			String query = "DELETE FROM users WHERE gid = ?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, id);
			keyRS = stmt.executeQuery();
			return true;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}
}
