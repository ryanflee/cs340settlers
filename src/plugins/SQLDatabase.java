package plugins;

import java.io.File;
import java.sql.*;
import java.util.logging.*;

public class SQLDatabase {
	private static final String DATABASE_DIRECTORY = "database";
	private static final String DATABASE_FILE = "test.sqlite";
	private static final String DATABASE_URL = "jdbc:sqlite:" + DATABASE_DIRECTORY + File.separator + "catandb.sqlite";
	
	private static Logger logger;
	
	static {
		logger = Logger.getLogger("indexerdb");
	}
	
	public static void initialize() throws Exception {
		try {
			final String driver = "org.sqlite.JDBC";
			Class.forName(driver);
		}
		catch(ClassNotFoundException e) {
			
			Exception serverEx = new Exception("Could not load database driver", e);
			
			logger.throwing("server.database.Database", "initialize", serverEx);

			throw serverEx; 
		}
		System.out.println(DATABASE_URL);
	}
	
	private SQLGameDAO gameDAO;
	private SQLGiveUp giveUp;
	private SQLUserDAO userDAO;
	private Connection connection;
	
	/**
	 * Creates a new instance of a Database and initializes DAO's
	 */
	public SQLDatabase(){
		try {
			SQLDatabase.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		gameDAO = new SQLGameDAO(this);
		giveUp = new SQLGiveUp(this);
		userDAO = new SQLUserDAO(this);
		connection = null;
		if(!new File("database" + File.separator).exists()) {
			new File("database" + File.separator).mkdir();
			clearDB();
		}
	}
	
	public void clearDB() {
		try {
			
			this.startTransaction();
			Statement stmt = connection.createStatement();
			String query = "DROP TABLE IF EXISTS users;DROP TABLE IF EXISTS games;DROP TABLE IF EXISTS commands;CREATE TABLE games(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,gid INTEGER NOT NULL UNIQUE,name VARCHAR(255) NOT NULL,game VARCHAR(5000) NOT NULL);CREATE TABLE users(id integer not null primary key autoincrement,pid INTEGER NOT NULL UNIQUE,username varchar(255) not null,password varchar(255) not null);CREATE TABLE commands(id integer not null primary key autoincrement,gid integer not null,sequenceNum integer not null,type varchar(255) not null,params varchar(255) not null);";
			stmt.executeUpdate(query);
			this.endTransaction(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SQLUserDAO getUserDAO() { 
		return userDAO;
	}
		
	public SQLGiveUp getGiveUp() {
		return this.giveUp;
	}
	
	public SQLGameDAO getGameDAO() {
		return this.gameDAO;
	}
	
	/**
	 * @return Connection object
	 */
	public Connection getConnection() { return connection; }
	
	
	/**
	 * Opens connection with the database
	 * @throws DatabaseException
	 */
	public void startTransaction() throws Exception {
		try {
			assert(connection == null);
			connection = DriverManager.getConnection(DATABASE_URL);
			connection.setAutoCommit(false);
		}
		catch (Exception e) {
			throw new Exception("Couldn't connect to database. Make sure " + 
					DATABASE_FILE + " is available in ./" + DATABASE_DIRECTORY,e);
		}
	}
	
	/**
	 * Commits changes if commit is True else rollback changes, and then closes connection
	 * @param commit True to commit changes
	 */
	public void endTransaction(boolean commit) {
		if(connection != null) {
			try {
				if(commit)
					connection.commit();
				else
					connection.rollback();
			}
			catch(Exception e) {
				System.out.println("Could not end transaction");
				e.printStackTrace();
			}
			finally {
				safeClose(connection);
				connection = null;
			}
		}
	}
	
	/**
	 * Checks if the Connection is not null, if not, it will close it
	 * @param conn Connection to close
	 */
	public static void safeClose(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			}
			catch (SQLException e) {
				
			}
		}
	}
	
	/**
	 * Checks if the Statement is not null, if not, it will close it
	 * @param stmt Statement to close
	 */
	public static void safeClose(Statement stmt) {
		if(stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException e) {
				
			}
		}
	}
	
	/**
	 * Checks if the PreparedStatement is not null, if not, it will close it
	 * @param stmt PreparedStatement to close
	 */
	public static void safeClose(PreparedStatement stmt) {
		if(stmt != null) {
			try {
				stmt.close();
			}
			catch (SQLException e) {
				
			}
		}
	}
	
	/**
	 * Checks if the ResultSet is not null, if not, it will close it
	 * @param rs ResultSet to close
	 */
	public static void safeClose(ResultSet rs) {
		if(rs != null) {
			try {
				rs.close();
			}
			catch (SQLException e) {
				
			}
		}
	}
}