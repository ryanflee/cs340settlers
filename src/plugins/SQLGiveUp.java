package plugins;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.command.ICommand;
import server.persistence.IGiveUp;

public class SQLGiveUp implements IGiveUp {
	private SQLDatabase db;
	
	public SQLGiveUp(SQLDatabase db) {
		this.db = db;
	}
	
	@Override
	public boolean create(String type, String params, int gameId) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		try {
			String query = "INSERT INTO commands (gid,sequenceNum,type,params) VALUES (?,?,?,?)";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, gameId);
			stmt.setInt(2, this.enumerateCommands(gameId)+1);
			stmt.setString(3, type);
			stmt.setString(4, params);
			if(stmt.executeUpdate() != 1) {
				throw new Exception("Error in inserting command");
			}
			return true;
		}
		catch (SQLException e) {
			throw new Exception("Could not insert user into database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public List<String> getCommands(int gameId) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		for(int i=0;i<this.enumerateCommands(gameId);i++)
			rval.add("");
		try {
			String query = "SELECT type,params,sequenceNum FROM commands WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, gameId);
			keyRS = stmt.executeQuery();
			while(keyRS.next()) {
				int num = keyRS.getInt(3);
				String type = keyRS.getString(1);
				String params = keyRS.getString(2);
				rval.set(num-1,type+"^_^"+params);
			}
			return rval;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public boolean deleteAll(int gameId) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		try {
			String query = "DELETE FROM commands WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, gameId);
			stmt.execute();

			return true;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public int enumerateCommands(int gameId) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		int rval = 0;
		try {
			String query = "SELECT * FROM commands WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, gameId);
			keyRS = stmt.executeQuery();
			while(keyRS.next()) {
				int num = keyRS.getInt(3);
				String type = keyRS.getString(1);
				String params = keyRS.getString(2);
				rval++;
			}
			return rval;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}


}