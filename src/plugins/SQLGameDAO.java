package plugins;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.data.lazy.Game;
import server.persistence.IGameDAO;

public class SQLGameDAO implements IGameDAO {
	private SQLDatabase db;
	
	public SQLGameDAO(SQLDatabase db) {
		this.db = db;
	}
	@Override
	public int create(String name, String game) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		try {
			String query = "INSERT INTO games (name, game, gid) VALUES (?,?,?)";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setString(1, name);
			stmt.setString(2, game);
			stmt.setInt(3, this.getGames().size());
			if(stmt.executeUpdate() == 1) {
				Statement keyStmt = db.getConnection().createStatement();
				keyRS = keyStmt.executeQuery("select last_insert_rowid()");
				keyRS.next();
				return this.getGames().size()-1;
			}
			else {
				throw new Exception("Could not insert user");
			}
		}
		catch (SQLException e) {
			throw new Exception("Could not insert user into database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public String getGameById(int id) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		try {
			String query = "SELECT name, game FROM games WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, id);
			keyRS = stmt.executeQuery();
			if(keyRS.next()) {
				String name = keyRS.getString(1);
				String game = keyRS.getString(2);
				return name+"^_^"+game;
			}
			else {
				throw new Exception("Could not insert user");
			}
		}
		catch (SQLException e) {
			throw new Exception("Could not insert user into database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public List<String> getGames() throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		try {
			String query = "SELECT gid,name,game FROM games";
			stmt = db.getConnection().prepareStatement(query);
			keyRS = stmt.executeQuery();
			while(keyRS.next()) {
				int id = keyRS.getInt(1);
				String username = keyRS.getString(2);
				String password = keyRS.getString(3);
				rval.add(id+"^_^" + username + "^_^" + password);
			}
			return rval;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public boolean update(int id, String updatedGame) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		try {
			String query = "UPDATE games SET game=? WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setString(1, updatedGame);
			stmt.setInt(2, id);
			stmt.execute();

			return true;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}

	@Override
	public boolean delete(int id) throws Exception {
		PreparedStatement stmt = null;
		ResultSet keyRS = null;
		List<String> rval = new ArrayList<String>();
		try {
			String query = "DELETE FROM games WHERE gid=?";
			stmt = db.getConnection().prepareStatement(query);
			stmt.setInt(1, id);
			stmt.execute();
			return true;
		}
		catch (SQLException e) {
			throw new Exception("Could not access the database",e);
		}
		finally {
			SQLDatabase.safeClose(stmt);
			SQLDatabase.safeClose(keyRS);
		}
	}
}
