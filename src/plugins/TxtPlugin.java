package plugins;

import server.persistence.IGameDAO;
import server.persistence.IGiveUp;
import server.persistence.IPersistPlugin;
import server.persistence.IUserDAO;

/**
 * Class that implements the Persistence Plugin interface.  This uses a text based database.
 * @author jjwilbur
 *
 */
public class TxtPlugin implements IPersistPlugin {
	
	private TxtGameDAO games;
	private TxtUserDAO users;
	private TxtGiveUp cmds;
	private TxtPluginUtils utils = new TxtPluginUtils("./plugins/txt");
	
	@Override
	public void clear() {
		if (games == null) {
			games = new TxtGameDAO(utils);
		}
		games.deleteAll();
		
		if (users == null) {
			users = new TxtUserDAO(utils);
		}
		users.deleteAll();
		
	}

	@Override
	public void startTransaction() {
		// does nothing
	}

	@Override
	public void endTransaction(boolean commit) {
		// does nothing
	}

	@Override
	public IGameDAO getGameDAO() {
		if (games == null) {
			games = new TxtGameDAO(utils); 
		}
		
		return games;
	}

	@Override
	public IUserDAO getUserDAO() {
		if (users == null) {
			users = new TxtUserDAO(utils);
		}
		
		return users;
	}

	@Override
	public IGiveUp getCommandDAO() {
		if (cmds == null) {
			cmds = new TxtGiveUp(utils);
		}
		
		return cmds;
	}

	@Override
	public void saveCommand(String commandType, String commandParams, int gameId) {
		if (cmds == null) {
			cmds = new TxtGiveUp(utils);
		}
		cmds.create(commandType, commandParams, gameId);
	}
}
