package plugins;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.persistence.IUserDAO;

public class TxtUserDAO implements IUserDAO {

	private TxtPluginUtils utils;
	private int nextUserId;
	
	public TxtUserDAO(TxtPluginUtils utils) {
		this.utils = utils;
		this.nextUserId = utils.getNextUserId();
	}
	
	@Override
	public int register(String username, String password) {
		
		File userFile = utils.getUserFile();
		
		try {
			if (!userFile.exists()) {
				userFile.createNewFile();
			}
			int id = nextUserId;
			nextUserId++;
			return utils.writeUser(userFile, id, username, password);
		}
		catch (IOException ex) {
			ex.printStackTrace();
			return -1;
		}
	}

	@Override
	public List<String> getUsers() {
		
		List<String> users = new ArrayList<String>();
		
		File userFile = utils.getUserFile();
		
		if (userFile.exists()) {
			try (Scanner s = new Scanner(userFile)) {
				while (s.hasNextLine()) {
					users.add(s.nextLine()); 
				}
			}
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return users;
	}

	@Override
	public boolean delete(int id) {
		
		File userFile = utils.getUserFile();
		File tmpFile = new File(userFile.getAbsolutePath() + ".tmp");
		
		if (userFile.renameTo(tmpFile)) {
			try (Scanner s = new Scanner(tmpFile)) {
				try (PrintWriter writer = new PrintWriter(userFile)) {
					while (s.hasNextLine()) {
						String line = s.nextLine();
						int lineId = Integer.parseInt(line.split("\\^_\\^")[0]);
						if (lineId != id) {
							writer.println(line);
						}
					}
				}
			}
			catch (IOException ex) {
				ex.printStackTrace();
				return false;
			}
		}
		
		// if we get here and can delete tmpFile, then
		// everything is good (copied all users except the
		// one to be deleted)
		return tmpFile.delete();
	}
	
	public boolean deleteAll() {
		
		File userFile = utils.getUserFile();
		
		try {
			if (userFile.exists()) {
				userFile.delete();
			}
			return userFile.createNewFile();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return false;
	}
}
