package plugins;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.persistence.IGameDAO;

public class TxtGameDAO implements IGameDAO {

	TxtPluginUtils utils;
	int nextGameId;
	
	public TxtGameDAO(TxtPluginUtils utils) {
		this.utils = utils;
		nextGameId = utils.getNextGameId();
	}
	
	@Override
	public int create(String name, String game) {
		
		File gameDir = utils.getGameDir(nextGameId);
		gameDir.mkdir();
		int id = nextGameId;
		nextGameId++;
		
		File gameFile = utils.getGameFile(id);
		File cmdFile = utils.getCmdFile(id);
		
		try {
			// create game file and cmd file
			cmdFile.createNewFile();
			if (utils.writeGame(gameFile, id, name, game)) {
				return id;
			}
			else {
				return -1;
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
			return -1;
		}
	}
	
	@Override
	public String getGameById(int id) {
		
		String result = null;
		
		File gameFile = utils.getGameFile(id);
		
		try (Scanner s = new Scanner(gameFile)) {
			if (s.hasNextLine()) {
				result = s.nextLine();
			}
		}
		catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		
		return result;
	}

	@Override
	public List<String> getGames() {
		List<String> games = new ArrayList<String>();
		
		for (File f : utils.listFiles()) {
			
			if (f.isDirectory()) {
				String name = f.getName();
				String[] parts = name.split("_");
				
				if ((parts.length == 2) && (parts[0].equals("game"))) {
					int id = Integer.parseInt(parts[1]);
					games.add(getGameById(id));
				}
			}
		}
		
		return games;
	}

	@Override
	public boolean update(int id, String updatedGame) {
		
		File gameFile = utils.getGameFile(id);
		String oldGame = getGameById(id);
		String title = oldGame.split("\\^_\\^")[1];
		try {
			System.out.println("Deleting game file: " + gameFile.delete());
			return utils.writeGame(gameFile, id, title, updatedGame);
		}
		catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(int id) {
		File gameDir = utils.getGameDir(id);
		try {
			if (gameDir.exists() && gameDir.isDirectory()) {
				return utils.deleteGameDir(gameDir);
			}
			
			else {
				// assume we should just silently
				// do nothing if given a bad ID
				return true;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	
	public boolean deleteAll() {
		
		for (File dir : utils.listFiles()) {
			if (dir.isDirectory()) {
				if (!utils.deleteGameDir(dir)) {
					return false;
				}
			}
		}
		
		return true;
	}
}
