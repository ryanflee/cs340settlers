package client.poller;

import static org.junit.Assert.*;
import shared.DataParser;
import shared.definitions.*;
import shared.model.Model;

import java.util.*;
import org.junit.*;

import client.facade.Facade;
import client.proxy.Mocksy;

public class PollerTest {

	@Test 
	public void testPollerStart() {
		DataParser dp = new DataParser();
		Facade.init(dp);
		Poller.init(new Mocksy(dp), 3);
		Poller.start();
		try{
			Thread.sleep(7);
		}
		catch(Exception e){}
		Model m = Facade.getModel();
		try{
			Thread.sleep(7);
		}
		catch(Exception e){}
		Model m1 = Facade.getModel();
		assertTrue(m1.equals(m));
	}
}
