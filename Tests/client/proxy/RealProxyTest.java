package client.proxy;

import client.facade.Facade;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.locations.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.*;

public class RealProxyTest {

	Proxy p;
	
//	@Before
//	public void init() {
//		Proxy p = new RealProxy("localhost", "8081");
//		p.register("proxy", "proxy");
//		
//	}
	
	@After
	public void cleanup() {
		p.logout();
	}
	
	@Test
	public void testUserLogin() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertTrue("Valid - Correct login ", p.userLogin("proxy", "proxy"));
		assertFalse("Invalid - Wrong username", p.userLogin("fail", "test"));
		assertFalse("Invalid - Null username", p.userLogin(null, "test"));
		
	}

	@Test
	public void testRegister() {
		p = new RealProxy("localhost", "8081");
		
		//prevent duplicates
		int test = new Random().nextInt();
		assertTrue("Valid - Register with new name", p.register(Integer.toString(test), "success"));
		assertFalse("Invalid - Name already in use", p.register("proxy", "proxy"));
		assertFalse("Invalid - Null username", p.register(null, "test"));
		
	}

	@Test
	public void testGetGames() {
		p = new RealProxy("localhost", "8081");
		assertTrue("Get list of games", p.getGames());
	}

	@Test
	public void testCreateGame() {
		p = new RealProxy("localhost", "8081");
		
		assertTrue("Valid - game all true booleans",p.createGame(true, true, true, "testGame"));
		assertTrue("Valid - game all false booleans", p.createGame(false, false, false, "testGame2"));
		assertFalse("Invalid - null game name", p.createGame(false, false, false, null));
	}

	@Test
	public void testJoinGame() {
		p = new RealProxy("localhost", "8081");
		
		assertFalse("Invalid - Not yet logged in", p.joinGame(0, CatanColor.ORANGE));
		p.userLogin("proxy", "proxy");
		p.createGame(true, true, true, "joinTest");
		assertTrue("Valid - game joined", p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE));
		assertFalse("Invalid - bad game id", p.joinGame(Integer.MIN_VALUE, CatanColor.BLUE));
		
	}

	@Test
	public void testGetModel() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - Not yet logged in", p.getModel());
		p.userLogin("proxy", "proxy");
		p.createGame(false, false, false, "getModelTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		assertTrue("Valid - No version included", p.getModel());
		
	}

	@Test
	public void testGetAI() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - no preconditions", p.getAI());
		p.userLogin("proxy", "proxy");
		p.createGame(false, false, false, "aiGetTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		assertTrue("Valid - test 2", p.getAI());
		assertTrue("Valid - test 3", p.getAI());
		
	}

	@Test
	public void testAddAI() {
		p = new RealProxy("localhost", "8081");

		Facade.init(p);
		//NEED TO CREATE A GAME, AND GET GAME ID RETURNED FROM DATA PARSER
		assertFalse("Invalid - not logged in",p.addAI("LARGEST_ARMY"));
		p.userLogin("proxy", "proxy");
		p.createGame(false, false, false, "aiTestGame");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		assertTrue("Valid - add ai (assuming the empty game is still empty)", p.addAI("LARGEST_ARMY"));
		assertTrue("Valid - add ai", p.addAI("LARGEST_ARMY"));
		assertTrue("Valid - add ai", p.addAI("LARGEST_ARMY"));
		assertFalse("Invalid - add invalid ai", p.addAI("INVALID"));
		
	}

	@Test
	public void testSendChat() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.sendChat(0, "test"));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.sendChat(1, "test"));
		p.createGame(false, false, false, "aiTestGame");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		assertTrue("Valid - game joined", p.sendChat(0,  "test"));
		assertFalse("Invalid - bad id", p.sendChat(7, "test"));
		assertFalse("Invalid - null message", p.sendChat(0, null));
		
	}

	@Test
	public void testRollNumber() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.rollNumber(0, 8));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.rollNumber(0,8));
		p.createGame(false, false, false, "rollNumberTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - rolled 7", p.rollNumber(0, 7));
		assertTrue("Valid - rolled 2", p.rollNumber(0, 2));
		assertFalse("Invalid - bad number rolled", p.rollNumber(0, 1));
		
	}

	@Test
	public void testRobPlayer() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.robPlayer(0, 1, new HexLocation(0,0)));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.robPlayer(0, 1, new HexLocation(0,0)));
		p.createGame(false, false, false, "robPlayerTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - game joined", p.robPlayer(0, -1, new HexLocation(-1,-1)));
		assertTrue("Valid - game joined", p.robPlayer(0, -1, new HexLocation(0,1)));
		assertFalse("Invalid - invalid player id", p.robPlayer(0, 6, new HexLocation(0,0)));
		
	}

	@Test
	public void testFinishTurn() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.finishTurn(0));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.finishTurn(0));
		p.createGame(false, false, false, "finishTurnTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 turn ends", p.finishTurn(0));
		assertTrue("Valid - player 1 turn ends", p.finishTurn(1));
		assertFalse("Invalid - invalid player id", p.finishTurn(9));
		
	}

	@Test
	public void testBuyDevCard() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.buyDevCard(0));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.buyDevCard(0));
		p.createGame(false, false, false, "buyDevCardTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 buy dev card", p.buyDevCard(0));
		assertTrue("Valid - player 1 buy dev card", p.buyDevCard(1));
		assertFalse("Invalid - invalid player id", p.buyDevCard(9));
		
	}

	@Test
	public void testPlayYearOfPlenty() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.playYearOfPlenty(0, ResourceType.BRICK, ResourceType.WOOD));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.playYearOfPlenty(0, ResourceType.BRICK, ResourceType.WOOD));
		p.createGame(false, false, false, "YoPTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 Year of Plenty", p.playYearOfPlenty(0, ResourceType.BRICK, ResourceType.WOOD));
		assertTrue("Valid - player 1 Year of Plenty", p.playYearOfPlenty(1, ResourceType.WHEAT, ResourceType.ORE));
		assertTrue("Valid - player 2 Year of Plenty", p.playYearOfPlenty(2, ResourceType.SHEEP, ResourceType.ORE));
		assertFalse("Invalid - invalid player id", p.playYearOfPlenty(6, ResourceType.BRICK, ResourceType.WOOD));
		
	}

	@Test
	public void testPlayRoadBuilding() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.playRoadBuilding(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthWest), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.North)));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.playRoadBuilding(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthWest), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.North)));
		
		p.createGame(false, false, false, "roadBuildingTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		
		assertTrue("Valid - player 0 Road Building", p.playRoadBuilding(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthWest), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.North)));
		assertTrue("Valid - player 1 Road Building", p.playRoadBuilding(1, new EdgeLocation(new HexLocation(1,1), EdgeDirection.South), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.SouthEast)));
		assertTrue("Valid - player 2 Road Building", p.playRoadBuilding(2, new EdgeLocation(new HexLocation(-1,-1), EdgeDirection.SouthWest), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthEast)));
		assertFalse("Invalid - invalid player id", p.playRoadBuilding(6, new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthWest), 
				new EdgeLocation(new HexLocation(0,0), EdgeDirection.North)));
		
	}

	@Test
	public void testPlayKnight() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.playKnight(0, 1, new HexLocation(0,0)));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.playKnight(0, 1, new HexLocation(0,0)));
		p.createGame(false, false, false, "knightTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 knight", p.playKnight(0, -1, new HexLocation(0,-1)));
		assertTrue("Valid - player 0 knight", p.playKnight(0, -1, new HexLocation(1,-1)));
		assertFalse("Invalid - invalid player id", p.playKnight(6, 1, new HexLocation(1,0)));
		
	}

	@Test
	public void testPlayMonopoly() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.playMonopoly(0, ResourceType.BRICK));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.playMonopoly(0, ResourceType.BRICK));
		p.createGame(false, false, false, "monopolyTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - brick", p.playMonopoly(0, ResourceType.BRICK));
		assertTrue("Valid - ore", p.playMonopoly(0, ResourceType.ORE));
		assertTrue("Valid - sheep", p.playMonopoly(0, ResourceType.SHEEP));
		assertTrue("Valid - wheat", p.playMonopoly(0, ResourceType.WHEAT));
		assertTrue("Valid - wood", p.playMonopoly(0, ResourceType.WOOD));
		assertFalse("Invalid - invalid player id", p.playMonopoly(4, ResourceType.BRICK));
		
	}

	@Test
	public void testPlayMonument() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.playMonument(0));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.playMonument(0));
		p.createGame(false, false, false, "monumentTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 knight", p.playMonument(0));
		assertTrue("Valid - player 1 knight", p.playMonument(1));
		assertFalse("Invalid - invalid player id", p.playMonument(5));
		
	}

	@Test
	public void testBuildRoad() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.buildRoad(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.North), true));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.buildRoad(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.North), true));
		p.createGame(false, false, false, "buildRoadTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 build road", p.buildRoad(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.North), true));
		assertTrue("Valid - player 1 build road", p.buildRoad(0, new EdgeLocation(new HexLocation(0,1), EdgeDirection.South), false));
		assertFalse("Invalid - invalid player id", p.buildRoad(4, new EdgeLocation(new HexLocation(0,0), EdgeDirection.North), true));
		
	}

	@Test
	public void testBuildSettlement() {

		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.buildSettlement(0, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East) , true));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.buildSettlement(0, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East) , true));
		p.createGame(false, false, false, "buildSettlementTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - player 0 build settlement", p.buildSettlement(0, 
				new VertexLocation(new HexLocation(1,0), VertexDirection.SouthWest) , true));
		assertTrue("Valid - player 1 build settlement", p.buildSettlement(0, 
				new VertexLocation(new HexLocation(1,0), VertexDirection.West) , true));
		assertTrue("Valid - player 1 build settlement", p.buildSettlement(0, 
				new VertexLocation(new HexLocation(0,1), VertexDirection.SouthWest) , false));
		assertFalse("Invalid - invalid player id", p.buildSettlement(8, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East) , false));
		
	}

	@Test
	public void testBuildCity() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.buildCity(0, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East)));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.buildCity(0, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East)));
		p.createGame(false, false, false, "buildCityTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.buildSettlement(0,new VertexLocation(new HexLocation(0,0), VertexDirection.East) , true);
		p.buildSettlement(0,new VertexLocation(new HexLocation(0,1), VertexDirection.East) , true);
		assertTrue("Valid - player 0 build settlement", p.buildCity(0, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East)));
		assertTrue("Valid - player 1 build settlement", p.buildCity(0, 
				new VertexLocation(new HexLocation(0,1), VertexDirection.East)));
		assertFalse("Invalid - invalid player id", p.buildCity(8, 
				new VertexLocation(new HexLocation(0,0), VertexDirection.East)));
		
	}

	@Test
	public void testOfferTrade() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		Map<ResourceType, Integer> map = new HashMap<ResourceType, Integer>();
		map.put(ResourceType.BRICK, 0);
		map.put(ResourceType.ORE, 0);
		map.put(ResourceType.SHEEP, 0);
		map.put(ResourceType.WHEAT, 0);
		map.put(ResourceType.WOOD, 0);
		
		assertFalse("Invalid - not logged in", p.offerTrade(0, 5, map));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.offerTrade(0, 5, map));
		p.createGame(false, false, false, "offerTradeTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - valid offer",p.offerTrade(0, 1, map));
		assertFalse("Invalid - bad player id", p.offerTrade(6, 5, map));
		
	}

	@Test
	public void testAcceptTrade() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		Map<ResourceType, Integer> map = new HashMap<ResourceType, Integer>();
		map.put(ResourceType.BRICK, 0);
		map.put(ResourceType.ORE, 0);
		map.put(ResourceType.SHEEP, 0);
		map.put(ResourceType.WHEAT, 0);
		map.put(ResourceType.WOOD, 0);
		
		assertFalse("Invalid - not logged in", p.acceptTrade(0, true));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.acceptTrade(0, true));
		p.createGame(false, false, false, "offerTradeTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.offerTrade(1, 0, map);
		assertTrue("Valid - accept trade 0", p.acceptTrade(0, true));
		p.offerTrade(1,0, map);
		assertTrue("Valid - decline trade", p.acceptTrade(0, false));
		assertFalse("Invalid - bad player id", p.acceptTrade(5, true));
		
	}

	@Test
	public void testMaritimeTrade() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		assertFalse("Invalid - not logged in", p.maritimeTrade(0, 2, ResourceType.ORE, ResourceType.WHEAT));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.maritimeTrade(0, 2, ResourceType.ORE, ResourceType.WHEAT));
		p.createGame(false, false, false, "offerTradeTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		assertTrue("Valid - maritime trade", p.maritimeTrade(0, 3, ResourceType.BRICK, ResourceType.SHEEP));
		assertTrue("Valid - maritime trade", p.maritimeTrade(0, 2, ResourceType.ORE, ResourceType.WHEAT));
		assertFalse("Invalid - bad player id", p.maritimeTrade(4, 2, ResourceType.ORE, ResourceType.WHEAT));
		
	}

	@Test
	public void testDiscardCards() {
		p = new RealProxy("localhost", "8081");
		Facade.init(p);
		Map<ResourceType, Integer> map = new HashMap<ResourceType, Integer>();
		map.put(ResourceType.BRICK, 0);
		map.put(ResourceType.ORE, 1);
		map.put(ResourceType.SHEEP, 0);
		map.put(ResourceType.WHEAT, 1);
		map.put(ResourceType.WOOD, 0);
		
		assertFalse("Invalid - not logged in", p.discardCards(4, map));
		p.userLogin("proxy", "proxy");
		assertFalse("Invalid - no game joined", p.discardCards(4, map));
		p.createGame(false, false, false, "offerTradeTest");
		p.joinGame(((RealProxy) p).getGameID(), CatanColor.ORANGE);
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");
		p.addAI("LARGEST_ARMY");

		assertTrue("Valid - will discard Cards", p.discardCards(0, map));
		assertFalse("Invalid - bad id number", p.discardCards(4, map));
		
	}

}
