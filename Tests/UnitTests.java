import org.junit.*;
import client.proxy.RealProxyTest;
import shared.DataParserTest;
import shared.model.GameManagerTest;

public class UnitTests {



    public static void main(String[] args) {
        System.out.println("ant file");

        String[] test = new String[] {
                "shared.model.GameManagerTest",
                "shared.DataParserTest",
                "client.poller.PollerTest",
                "shared.model.BoardTest",
                "shared.model.HexTest",
                "server.MovesTest"
        };

        org.junit.runner.JUnitCore.main(test);
    }

}