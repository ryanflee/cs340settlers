package server;

import static org.junit.Assert.*;
import server.command.ICommand;
import server.exceptions.FactoryException;
import server.exceptions.ServerFacadeException;
import server.facade.*;
import server.factory.UserCommandFactory;
import org.junit.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserTest {
	private RealFacade f;

	@Before
	public void initialize() {
		f = new RealFacade();
	}

	@Test
	public void Login() throws FactoryException {
		String param = "{\"username\":\"Sam\",\"password\":\"sam\"}";
		ICommand cmd = new UserCommandFactory().buildCommand(f, "login", param, -1);
		try {
			String response = (cmd.execute());
			assertTrue(0==getID(response));
		} catch (ServerFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Invalid password for a valid username
		param = "{\"username\":\"Sam\",\"password\":\"SAM\"}";
		cmd = new UserCommandFactory().buildCommand(f, "login", param, -1);
		try {
			String response = (cmd.execute());
			assertTrue(false);
		} catch (ServerFacadeException e) {
			// Should get here because this player is not a valid username and password
			assertTrue(true);
		}

		// Invalid username for a valid password
		param = "{\"username\":\"aaf\",\"password\":\"asdff\"}";
		cmd = new UserCommandFactory().buildCommand(f, "login", param, -1);
		try {
			String response = (cmd.execute());
			assertTrue(false);
		} catch (ServerFacadeException e) {
			// Should get here because this player is not a valid username and password
			assertTrue(true);
		}
	}
	
	@Test
	public void registerTest() throws FactoryException {
		String param = "{\"username\":\"kevin\",\"password\":\"kevin\"}";
		ICommand cmd = new UserCommandFactory().buildCommand(f, "register", param, -1);
		try {
			String response = (cmd.execute());
			assertTrue(getID(response) != -1);
		} catch (ServerFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Invalid - username already register
		param = "{\"username\":\"aaa\",\"password\":\"kevin\"}";
		cmd = new UserCommandFactory().buildCommand(f, "register", param, -1);
		try {
			String response = (cmd.execute());
			assertTrue(false);
		} catch (ServerFacadeException e) {
			// Should get here because that username is already in use
			assertTrue(true);
		}
	}

	private String getUsername(String json) {
		JsonElement jelly = new JsonParser().parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String username = job.get("username").getAsString();
			return username;
		}
		return null;
	}

	private String getPassword(String json) {
		JsonElement jelly = new JsonParser().parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			String username = job.get("password").getAsString();
			return username;
		}
		return null;
	}

	private int getID(String json) {
		JsonElement jelly = new JsonParser().parse(json);

		if (jelly.isJsonObject()) {
			JsonObject job = jelly.getAsJsonObject();

			int username = job.get("playerID").getAsInt();
			return username;
		}
		return -1;
	}

}
