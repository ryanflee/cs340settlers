package server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import server.command.ICommand;
import server.command.game.*;
import server.encoding.params.AddAIParams;
import server.exceptions.ServerFacadeException;
import server.facade.IFacade;
import server.facade.RealFacade;
import shared.DataParser;
import shared.definitions.CatanColor;
import shared.model.Model;

public class GameTest {

	/*IFacade facade;
	int gameId1;
	int gameId2;
	int gameId3;
	int gameId4;
	DataParser dp;
	
	@Before
	public void setUp() throws Exception {
		
		dp = new DataParser();
		client.facade.Facade.init(dp);
		
		facade = new RealFacade();
		gameId1 = dp.parseNewGame(facade.create("gameTestGame", false, false, false));
		
		facade.join(gameId1, CatanColor.RED, "aaa", 4, false);
		facade.join(gameId1, CatanColor.YELLOW, "sss", 5, false);
		facade.join(gameId1, CatanColor.BLUE, "ddd", 6, false);
		facade.join(gameId1, CatanColor.PURPLE, "fff", 7, false);
		
		gameId2 = dp.parseNewGame(facade.create("gameTestGame2", false, false, false));
		facade.join(gameId2, CatanColor.RED, "aaa", 4, false);
		facade.join(gameId2, CatanColor.YELLOW, "sss", 5, false);
		facade.join(gameId2, CatanColor.BLUE, "ddd", 6, false);
		facade.join(gameId2, CatanColor.PURPLE, "fff", 7, false);
		
		gameId3 = dp.parseNewGame(facade.create("gameTestGame3", false, false, false));
		gameId4 = dp.parseNewGame(facade.create("gameTestGame4", false, false, false));
	}
	
	private Model executeCommand(ICommand cmd) {
		try {
			String jsonModel = cmd.execute();
			dp.buildModelFromJson(jsonModel, true);
		}
		catch(ServerFacadeException ex) {
			fail(ex.getMessage());
		}
		
		return dp.getModel();
	}

	@After
	public void tearDown() throws Exception {
		facade = null;
		gameId1 = -1;
		dp = null;
	}
	
	@Test
	public void testAddAI() {
		
		try { // invalid gameId
			new AddAI(facade, new AddAIParams("LARGEST_ARMY"), -8);
		}
		catch (NullPointerException ex) {
			assertTrue(true);
		}
		
		// invalid - game already has 4 players
		String response = new AddAI(facade, new AddAIParams("LARGEST_ARMY"), gameId2).execute();
		assertEquals("Game already full.", response);
		
		// valid - add AI to non-full game
		response = new AddAI(facade, new AddAIParams("LARGEST_ARMY"), gameId3).execute();
		assertEquals("Success", response);
		Model m = executeCommand(new ModelCommand(facade, gameId3));
		int aid = m.getPlayers().get(0).getId();
		assertTrue(aid < 0);
	}

	@Test
	public void testListAI() {
		String response = new ListAI(facade).execute();
		assertEquals("[\"LARGEST_ARMY\"]", response);
	}
	
	@Test
	public void testModelCommand() {
		
		try { // invalid game ID
			new ModelCommand(facade, -3).execute();
		}
		catch (NullPointerException ex) {
			assertTrue(true);
		}
		
		String json = new ModelCommand(facade, gameId1).execute();
		String json2 = new ModelCommand(facade, gameId2).execute();
		assertEquals(json, json2);
	}*/
}
