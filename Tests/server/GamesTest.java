package server;

import static org.junit.Assert.*;
import server.command.ICommand;
import server.encoding.params.CreateParams;
import server.exceptions.FactoryException;
import server.exceptions.ServerFacadeException;
import server.facade.*;
import server.factory.GamesCommandFactory;
import server.factory.UserCommandFactory;
import org.junit.*;

public class GamesTest {
	private RealFacade f;
	
	@Before
	public void initialize() throws FactoryException {
		f = new RealFacade();
		String param = "{\"username\":\"Sam\",\"password\":\"sam\"}";
		ICommand cmd = new UserCommandFactory().buildCommand(f, "login", param, -1);
		try {
			String response = (cmd.execute());
		} catch (ServerFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void listTest() throws FactoryException, ServerFacadeException {
		ICommand cmd = new GamesCommandFactory().buildCommand(f, "list", "", -1);
		String response = cmd.execute();
		String expected = "[{\"title\": \"Invalid game\",\"id\": 0,\"players\": [{\"color\": \"blue\", \"name\": \"Kevin\", \"id\": -1},{\"color\": \"orange\", \"name\": \"Scott\", \"id\": -1},{\"color\": \"puce\", \"name\": \"Clark\", \"id\": -1},{\"color\": \"puce\", \"name\": \"Full\", \"id\": -1}]}]";
		assertTrue(response.equals(expected));
	}
	
	@Test
	public void createTest() throws Exception {
		ICommand cmd = new GamesCommandFactory().buildCommand(f, "create", "{\"name\":\"something\",\"randomNumbers\":false,\"randomTiles\":false,\"randomPorts\":false}", -1);
		cmd.execute();
		String expected = "[{\"title\": \"Invalid game\",\"id\": 0,\"players\": [{\"color\": \"blue\", \"name\": \"Kevin\", \"id\": -1},{\"color\": \"orange\", \"name\": \"Scott\", \"id\": -1},{\"color\": \"puce\", \"name\": \"Clark\", \"id\": -1},{\"color\": \"puce\", \"name\": \"Full\", \"id\": -1}]},{\"title\": \"something\",\"id\": 1,\"players\": [{},{},{},{}]}]";
		assertTrue(expected.equals(new GamesCommandFactory().buildCommand(f, "list", "", -1).execute()));
		
		// Invalid game, string is null
		cmd = new GamesCommandFactory().buildCommand(f, "create", "{\"name\":\"\",\"randomNumbers\":false,\"randomTiles\":false,\"randomPorts\":false}", -1);
		try {
			cmd.execute();
			assertTrue(false);
		}catch (Exception e) {
			// Should only get here if an exception is thrown (which should be)
			assertTrue(true);
		}
	}
	
	@Test
	public void joinTest() throws Exception {
		ICommand cmd = new GamesCommandFactory().buildCommand(f, "create", "{\"name\":\"something\",\"randomNumbers\":false,\"randomTiles\":false,\"randomPorts\":false}", -1);
		cmd.execute();
		cmd = new GamesCommandFactory().buildCommand(f, "join", "{\"id\":\"1\",\"color\":\"blue\",\"username\":\"Sam\",\"pid\":\"0\"}", -1);
		assertTrue(cmd.execute().equals("1"));
		
		// Invalid - game already full
		cmd = new GamesCommandFactory().buildCommand(f, "join", "{\"id\":\"0\",\"color\":\"blue\",\"username\":\"Sam\",\"pid\":\"0\"}", -1);
		try {
			cmd.execute();
		}catch(Exception e) {
			// Should get here because the game is already full
			assertTrue(true);
		}
	}
}
