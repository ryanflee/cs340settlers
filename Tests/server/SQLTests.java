package server;

import static org.junit.Assert.*;
import server.command.ICommand;
import server.encoding.params.CreateParams;
import server.exceptions.FactoryException;
import server.exceptions.ServerFacadeException;
import server.facade.*;
import server.factory.GamesCommandFactory;
import server.factory.UserCommandFactory;
import org.junit.*;

import plugins.*;

	

public class SQLTests {
	private SQLDatabase db;
	

	@Before
	public void setUp() throws Exception {
		db = new SQLDatabase();
	}
	
	@Test
	public void registerTest() {		
		try {
			db.startTransaction();
			db.getConnection().prepareStatement("DELETE FROM users").execute();
			db.endTransaction(true);	
		}
		catch(Exception e) {
			db.endTransaction(false);
			assertTrue(false);
		}
		try {
			db.startTransaction();
			db.getUserDAO().register("Kevin", "King");
			db.endTransaction(true);
			assertTrue(true);
		} catch (Exception e) {
			db.endTransaction(false);
			e.printStackTrace();
			assertTrue(false);
		}
		try {
			db.startTransaction();
			db.getUserDAO().register("Kevin", "King");
			db.endTransaction(true);
			assertTrue(false);
		} catch (Exception e) {
			db.endTransaction(false);
			assertTrue(true);
		}
	}
	
	@Test
	public void getUsersTest() {
		try {
			db.startTransaction();
			assertTrue(db.getUserDAO().getUsers().size()==1);
			db.endTransaction(true);
			assertTrue(true);
		} catch (Exception e) {
			db.endTransaction(false);
			e.printStackTrace();
			assertTrue(false);
		}
		try {
			db.startTransaction();
			db.getUserDAO().register("asdf", "asdf");
			db.endTransaction(true);
		} catch (Exception e) {
			db.endTransaction(false);
			e.printStackTrace();
		}
		try {
			db.startTransaction();
			assertTrue(db.getUserDAO().getUsers().size()==2);
			db.endTransaction(true);
			assertTrue(true);
		} catch (Exception e) {
			db.endTransaction(false);
			assertTrue(false);
		}
	}
	
	@Test
	public void giveUpTests() {
		try{
			db.startTransaction();
			db.getConnection().prepareStatement("DELETE FROM commands").execute();
			assertTrue(db.getGiveUp().enumerateCommands(0) == 0);
			db.endTransaction(true);
		}catch(Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
		try {
			db.startTransaction();
			db.getGiveUp().create("/moves/stuff", "{parameters}", 0);
			db.endTransaction(true);
		}catch(Exception e){assertTrue(false);}
		try{
			db.startTransaction();
			assertTrue(db.getGiveUp().enumerateCommands(0) == 1);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();assertTrue(false);}
		try {
			db.startTransaction();
			db.getGiveUp().deleteAll(0);
			db.endTransaction(true);
		}catch(Exception e){assertTrue(false);}
		try{
			db.startTransaction();
			assertTrue(db.getGiveUp().enumerateCommands(0) == 0);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();assertTrue(false);}
		try {
			db.startTransaction();
			db.getGiveUp().create("/moves/stuff", "{parameters}", 0);
			db.endTransaction(true);
		}catch(Exception e){assertTrue(false);}
		try {
			db.startTransaction();
			db.getGiveUp().create("/moves/other", "{more parameters}", 0);
			db.endTransaction(true);
		}catch(Exception e){assertTrue(false);}
		try {
			db.startTransaction();
			assertTrue(db.getGiveUp().getCommands(0).size() == 2);
			db.endTransaction(true);
		}catch(Exception e){assertTrue(false);}
	}
	
	@Test
	public void gamesTest() {
		try{
			db.startTransaction();
			db.getConnection().prepareStatement("DELETE FROM games").execute();
			assertTrue(db.getGameDAO().getGames().size() == 0);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();}
		int gameid = -1;
		try{
			db.startTransaction();
			gameid = db.getGameDAO().create("game's name", "{This is the game's json to store all of it's data}");
			assertTrue(db.getGameDAO().getGames().size() == 1);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();}
		try{
			db.startTransaction();
			db.getGameDAO().create("la;ksdf name", "{This is the game's json to store all of it's data}");
			assertTrue(db.getGameDAO().getGames().size() == 2);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();}
		try{
			db.startTransaction();
			db.getGameDAO().update(gameid, "something new");
			assertTrue(db.getGameDAO().getGames().size() == 2);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();}
		try{
			db.startTransaction();
			db.getGameDAO().delete(gameid);
			assertTrue(db.getGameDAO().getGames().size() == 1);
			db.endTransaction(true);
		}catch(Exception e) {e.printStackTrace();}
	}
	
	
}
