package server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import server.command.ICommand;
import server.command.moves.*;
import server.encoding.params.*;
import server.exceptions.ServerFacadeException;
import server.facade.IFacade;
import server.facade.RealFacade;
import shared.DataParser;
import shared.definitions.CatanColor;
import shared.definitions.HexType;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.Model;
import shared.model.board.Hex;
import shared.model.logs.ChatEntry;
import shared.model.logs.HistoryEntry;
import shared.model.player.Player;

public class MovesTest {

	IFacade facade; // server facade
	int gameId;
	DataParser dp;
	
	@Before
	public void setUp() throws Exception {
		
		dp = new DataParser();
		client.facade.Facade.init(dp);
		
		gameId = 1;
		
		facade = new RealFacade();
		facade.create("k", false, false, false);
		facade.create("testGame", false, false, false);
		facade.register("sam", "asdff");
		facade.register("pete", "asdff");
		facade.register("ken", "asdff");
		facade.register("brook", "asdff");
		facade.register("aaa", "asdff");
		facade.register("sss", "asdff");
		facade.register("ddd", "asdff");
		facade.register("fff", "asdff");
		facade.join(gameId, CatanColor.RED, "aaa", 4, false);
		facade.join(gameId, CatanColor.YELLOW, "sss", 5, false);
		facade.join(gameId, CatanColor.BLUE, "ddd", 6, false);
		facade.join(gameId, CatanColor.PURPLE, "fff", 7, false);
	}

	@After
	public void tearDown() throws Exception {
		facade = null;
		gameId = -1;
		dp = null;
	}
	
	private Model executeCommand(ICommand cmd) {
		try {
			String jsonModel = cmd.execute();
			dp.buildModelFromJson(jsonModel, true);
		}
		catch(ServerFacadeException ex) {
			fail(ex.getMessage());
		}
		
		return dp.getModel();
	}
	
	@Test
	public void testAcceptTrade() {
		AcceptTrade at;
		AcceptTradeParams params = new AcceptTradeParams(1, true);
		at = new AcceptTrade(facade, params, gameId);
		assertNull("No trade offer yet", at.execute());
		
		OfferTradeParams otparams = new OfferTradeParams(0, new int[]{1,-1,0,0,0}, 1);
		OfferTrade ot = new OfferTrade(facade, otparams, gameId);
		ot.execute();
		Model m = executeCommand(at);
		assertTrue("Offer Accepted", m != null);
		
		otparams = new OfferTradeParams(0, new int[]{1,-1,0,0,0}, 1);
		ot = new OfferTrade(facade, otparams, gameId);
		ot.execute();
		params = new AcceptTradeParams(1, false);
		at = new AcceptTrade(facade, params, gameId);
		assertTrue("Offer Declined", m != null);

	}
	
	@Test
	public void testBuildCity() {
		BuildCity bt;
		VertexLocation validVertex = new VertexLocation(new HexLocation(0,0), VertexDirection.NorthEast);
		VertexLocation invalidVertex = new VertexLocation(new HexLocation(4,5), VertexDirection.NorthEast);
		BuildCityParams bpParams = new BuildCityParams(0, validVertex);
		bt = new BuildCity(facade, bpParams, gameId);
		Model m = executeCommand(bt);
		assertTrue("Valid city built", m.getPlayers().get(0).getCities() == 3);
		
		bpParams = new BuildCityParams(0, invalidVertex);
		bt = new BuildCity(facade, bpParams, gameId);
		try {
			bt.execute();
		}
		catch(Exception e) {
			assertTrue("Invalid Location throws exception", true);
		}
		
	}
	
	@Test
	public void testBuildRoad() {
		BuildRoad br;
		EdgeLocation roadLoc = new EdgeLocation(new HexLocation(0,0), EdgeDirection.North);
		BuildRoadParams brParams = new BuildRoadParams(0, roadLoc, true);
		br = new BuildRoad(facade, brParams, gameId);
		assertTrue("Free, valid road", br.execute() != null);
		
		roadLoc = new EdgeLocation(new HexLocation(0,1), EdgeDirection.North);
		brParams = new BuildRoadParams(0, roadLoc, false);
		br = new BuildRoad(facade, brParams, gameId);
		assertTrue("Free, valid road", br.execute() != null);
		
		roadLoc = new EdgeLocation(new HexLocation(4,-4), EdgeDirection.North);
		brParams = new BuildRoadParams(0, roadLoc, true);
		br = new BuildRoad(facade, brParams, gameId);
		try {
			br.execute();
			assert(false);
		} catch (Exception e) {
			assertTrue("Invalid edge location", true);
		}
		
	}
	
	@Test
	public void testBuildSettlement() {
		BuildSettlement bs;
		VertexLocation validVertex = new VertexLocation(new HexLocation(0,0), VertexDirection.NorthEast);
		BuildSettlementParams bsParams = new BuildSettlementParams(0, validVertex, false);
		bs = new BuildSettlement(facade, bsParams, gameId);
		Model m = executeCommand(bs);
		assertTrue(m.getPlayers().get(0).getSettlements() == 4);
		
		VertexLocation invalidVertex = new VertexLocation(new HexLocation(4,5), VertexDirection.NorthEast);
		bsParams = new BuildSettlementParams(0, invalidVertex, true);
		bs = new BuildSettlement(facade, bsParams, gameId);
		try {
			bs.execute();
		}
		catch(Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testBuyDevCard() {
		BuyDevCard bdc;
		BuyDevCardParams bdcParams = new BuyDevCardParams(0);
		bdc = new BuyDevCard(facade, bdcParams, gameId);
		Model m = executeCommand(bdc);
		assertTrue("Bought a Dev Card", m.getPlayers().get(0).getDevCards().size() == 1);
		
		//Trying to pass in an invalid playerID.
		bdcParams = new BuyDevCardParams(5);
		bdc = new BuyDevCard(facade, bdcParams, gameId);
		try{
			bdc.execute();
			assert(false);
		} catch (Exception e){
			assertTrue("Invalid player index", true);
		}
	}
	
	@Test
	public void testDiscardCards() {
		DiscardCards dc;
		DiscardCardsParams dcParams = new DiscardCardsParams(0, new int[]{3,2,1,0,2});
		dc = new DiscardCards(facade, dcParams, gameId);
		assertTrue("Valid Discard", dc.execute() != null);
		
		dcParams = new DiscardCardsParams(5, new int[]{1,1,1,1,1});
		dc = new DiscardCards(facade, dcParams, gameId);
		try{
			dc.execute();
			assert(false);
		} catch (Exception e) {
			assertTrue("Invalid player index", true);
		}
	}
	
	@Test
	public void testFinishTurn() {
		FinishTurn ft;
		try { // invalid player index
			ft = new FinishTurn(facade, new FinishTurnParams(), gameId);
			ft.execute();
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			assertTrue(true);
		}
		
		ft = new FinishTurn(facade, new FinishTurnParams(0), gameId);
		Model m = executeCommand(ft);
		
		// valid - finished first turn in setup
		assertEquals("firstround", m.gameManager.getStatus().toLowerCase());
		assertEquals(1, m.gameManager.getCurrentPlayer());
		
		ft = new FinishTurn(facade, new FinishTurnParams(1), gameId);
		ft.execute();
		ft = new FinishTurn(facade, new FinishTurnParams(2), gameId);
		ft.execute();
		ft = new FinishTurn(facade, new FinishTurnParams(3), gameId);
		m = executeCommand(ft);
		
		// valid - transition to second round in setup
		assertEquals("secondround", m.gameManager.getStatus().toLowerCase());
		assertEquals(3, m.gameManager.getCurrentPlayer());
		
		ft = new FinishTurn(facade, new FinishTurnParams(3), gameId);
		ft.execute();
		ft = new FinishTurn(facade, new FinishTurnParams(2), gameId);
		ft.execute();
		ft = new FinishTurn(facade, new FinishTurnParams(1), gameId);
		ft.execute();
		ft = new FinishTurn(facade, new FinishTurnParams(0), gameId);
		m = executeCommand(ft);
		
		// valid - transition from setup to regular gameplay
		assertEquals("rolling", m.gameManager.getStatus().toLowerCase());
		assertEquals(0, m.gameManager.getCurrentPlayer());
	}
	
	@Test
	public void testMaritimeTrade() {
		MaritimeTrade mt;
		MaritimeTradeParams mtParams = new MaritimeTradeParams(0, 3, "wheat", "ore");
		mt = new MaritimeTrade(facade, mtParams, gameId);
		assertTrue("Maritime trade returns valid model json", mt.execute() != null);
		
		mtParams = new MaritimeTradeParams(5, 3, "wheat", "ore");
		mt = new MaritimeTrade(facade, mtParams, gameId);
		try {
			mt.execute();
			assert(false);
		} catch (Exception e) {
			assertTrue("Invalid user id", true);
		}
		
	}
	
	@Test
	public void testMonopoly() {
		Monopoly mon;
		MonopolyParams mParams = new MonopolyParams(0, "ore");
		mon = new Monopoly(facade, mParams, gameId);
		assertTrue("valid monopoly card", mon.execute() != null);
		
		mParams = new MonopolyParams(4, "wheat");
		mon = new Monopoly(facade, mParams, gameId);
		try {
			mon.execute();
			assert(false);
		} catch(Exception e) {
			assertTrue("invalid player index", true);
		}
	}
	
	@Test
	public void testMonument() {
		Monument mon;
		MonumentParams mParams = new MonumentParams(0);
		mon = new Monument(facade, mParams, gameId);
		Model m = executeCommand(mon);
		assertTrue("Points updated", m.getPlayers().get(0).getVictoryPoints() == 1);
		
		mParams = new MonumentParams(4);
		mon = new Monument(facade, mParams, gameId);
		try {
			mon.execute();
			assert(false);
		} catch(Exception e) {
			assertTrue("Invalid player index", true);
		}
		
	}
	
	@Test
	public void testOfferTrade() {
		OfferTradeParams otparams = new OfferTradeParams(0, new int[]{1,-1,0,0,0}, 1);
		OfferTrade ot = new OfferTrade(facade, otparams, gameId);
		assertTrue("Valid tradeOffer", ot.execute() != null);
		
		otparams = new OfferTradeParams(2, new int[]{1,5,0,-1,0}, 3);
		ot = new OfferTrade(facade, otparams, gameId);
		assertTrue("Another valid offer", ot.execute() != null);
		
		otparams = new OfferTradeParams(5, new int[]{1,-1,0,0,0}, 3);
		ot = new OfferTrade(facade, otparams, gameId);

		// not checking this in the server, so removing this test -RFL
//		try {
//			ot.execute();
//			assert(false);
//		} catch(Exception e) {
//			assertTrue("Invalid player index", true);
//		}
	}
	
	@Test
	public void testRoadBuilding() {
		RoadBuilding rb;
		EdgeLocation spot1 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.North);
		EdgeLocation spot2 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.NorthEast);
		RoadBuildingParams rbParams = new RoadBuildingParams(0, spot1, spot2);

		rb = new RoadBuilding(facade, rbParams, gameId);
		assertTrue("Valid build road card", rb.execute() != null);
		
		spot1 = new EdgeLocation(new HexLocation(0,1), EdgeDirection.South);
		spot2 = new EdgeLocation(new HexLocation(0,1), EdgeDirection.SouthWest);
		rbParams = new RoadBuildingParams(0, spot1, spot2);
		rb = new RoadBuilding(facade, rbParams, gameId);
		assertTrue("Valid build road card", rb.execute() != null);

		spot1 = new EdgeLocation(new HexLocation(5, 5), EdgeDirection.South);
		spot2 = new EdgeLocation(new HexLocation(5, 5), EdgeDirection.SouthWest);
		rbParams = new RoadBuildingParams(0, spot1, spot2);
		rb = new RoadBuilding(facade, rbParams, gameId);
		try {
			rb.execute();
			assert(false);
		} catch(Exception e){
			assertTrue("Invalid location for roads", true);
		}
	}
	
	@Test
	public void testRobPlayer() {
		RobPlayer rp;
		
		try { // invalid player index
			rp = new RobPlayer(facade, new RobPlayerParams(), gameId);
			rp.execute();
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			assertTrue(true);
		}
		
		try { // invalid victim index
			rp = new RobPlayer(facade, new RobPlayerParams(0, 5, new HexLocation(0,0)), gameId);
			rp.execute();
		}
		catch (IndexOutOfBoundsException ex) {
			assertTrue(true);
		}
		
		try { // invalid robber location
			rp = new RobPlayer(facade, new RobPlayerParams(0, -1, null), gameId);
			rp.execute();
		}
		catch (NullPointerException ex) {
			assertTrue(true);
		}
		
		rp = new RobPlayer(facade, new RobPlayerParams(0, 1, new HexLocation(0,0)), gameId);
		Model m = executeCommand(rp);
		
		HistoryEntry expectedHistory = new HistoryEntry(m.getPlayers().get(0), m.getPlayers().get(0).getName() + " moved the robber but couldn't rob anyone!");
		Hex expectedRobberHex = new Hex(HexType.WHEAT, new HexLocation(0,0), 11, true);
		
		// valid - victim doesn't have any cards
		assertEquals("playing", m.gameManager.getStatus().toLowerCase());
		assertEquals(expectedHistory, m.getHistory().get(0));
		assertEquals(expectedRobberHex, m.getBoard().getRobberHex());
	}
	
	@Test
	public void testRollNumber() {
		RollNumber rn;
		
		try { // invalid player index
			rn = new RollNumber(facade, new RollNumberParams(), gameId);
			rn.execute();
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			assertTrue(true);
		}
		
		try { // invalid number
			rn = new RollNumber(facade, new RollNumberParams(0, -1), gameId);
			rn.execute();
		}
		catch (NullPointerException ex) {
			assertTrue(true);
		}
		
		rn = new RollNumber(facade, new RollNumberParams(0, 8), gameId);
		Model m = executeCommand(rn);
		Player p = m.getPlayers().get(0);
		
		// valid - normal roll
		HistoryEntry expected = new HistoryEntry(p, p.getName() + " rolled a 8.");
		assertEquals(expected, m.getHistory().get(0));
		
		rn = new RollNumber(facade, new RollNumberParams(0, 7), gameId);
		m = executeCommand(rn);
		
		// valid - rolling a 7
		expected = new HistoryEntry(p, p.getName() + " rolled a 7.");
		assertEquals(expected, m.getHistory().get(1));
		assertEquals("robbing", m.gameManager.getStatus().toLowerCase());
	}
	
	@Test
	public void testSendChat() {
		SendChat sc;
		
		try { // invalid player index
			sc = new SendChat(facade, new SendChatParams(), gameId);
			sc.execute();
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			assertTrue(true);
		}
		
		try { // invalid message content
			sc = new SendChat(facade, new SendChatParams(0, null), gameId);
			sc.execute();
		}
		catch (NullPointerException ex) {
			assertTrue(true);
		}
		
		sc = new SendChat(facade, new SendChatParams(0, "test message"), gameId);
		Model m = executeCommand(sc);
		
		// valid - can send chat on your turn
		ChatEntry expected = new ChatEntry(m.getPlayers().get(0), "test message");
		assertEquals(expected, m.getComments().get(0));
		
		sc = new SendChat(facade, new SendChatParams(1, "test message 2"), gameId);
		m = executeCommand(sc);
		
		// valid - can send chat when it's not your turn
		expected = new ChatEntry(m.getPlayers().get(1), "test message 2");
		assertEquals(expected, m.getComments().get(1));
	}
	
	@Test
	public void testSoldier() {
		//fail("Not yet implemented");
	}
	
	@Test
	public void testYearOfPlenty() {
		YearOfPlenty yop;
		YearOfPlentyParams yopParams = new YearOfPlentyParams(0, ResourceType.BRICK, ResourceType.WOOD);
		yop = new YearOfPlenty(facade, yopParams, gameId);
		assertTrue("Valid Year of Plenty card", yop.execute() != null);

		yopParams = new YearOfPlentyParams(6, ResourceType.WHEAT, ResourceType.WHEAT);
		yop = new YearOfPlenty(facade, yopParams, gameId);
		try{
			yop.execute();
			assert(false);
		} catch(Exception e){
			assertTrue("Invalid player index", true);
		}
	}
}
