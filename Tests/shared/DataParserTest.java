package shared;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import shared.definitions.*;
import shared.locations.*;
import shared.model.*;
import shared.model.board.*;
import shared.model.logs.*;
import shared.model.pieces.*;
import shared.model.developmentcard.*;
import shared.model.resourcecard.*;
import shared.model.exceptions.*;
import shared.model.player.Player;
import client.facade.Facade;

public class DataParserTest {

	@Test
	public void testParseNewGame() {
		DataParser dp = new DataParser();
		
		String json = "{  \"title\": \"test\",  \"id\": 3,  \"players\": [    {},    {},    {},    {}  ]}";
		
		int gameID = dp.parseNewGame(json);
		assertEquals(gameID, 3);
	}
	
	@Test
	public void testBuildModelFromJsonEqual() {
		DataParser dp = new DataParser();
		Facade.init(dp);
		
		String json = "{  \"deck\": {    \"yearOfPlenty\": 2,    \"monopoly\": 2,    \"soldier\": 14,    \"roadBuilding\": 2,    \"monument\": 5  },  \"map\": {    \"hexes\": [      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 0,          \"y\": -2        },        \"number\": 9      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 1,          \"y\": -2        },        \"number\": 11      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": -2        },        \"number\": 8      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": -1,          \"y\": -1        },        \"number\": 9      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": -1        },        \"number\": 3      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": -1        },        \"number\": 5      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": 2,          \"y\": -1        },        \"number\": 11      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": -1,          \"y\": 0        },        \"number\": 2      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": 0        },        \"number\": 5      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 1,          \"y\": 0        },        \"number\": 12      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -2,          \"y\": 1        },        \"number\": 10      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -1,          \"y\": 1        },        \"number\": 3      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": 0,          \"y\": 1        },        \"number\": 4      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": 1        },        \"number\": 4      },      {        \"location\": {          \"x\": -2,          \"y\": 2        }      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -1,          \"y\": 2        },        \"number\": 10      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 0,          \"y\": 2        },        \"number\": 8      }    ],    \"roads\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SE\",          \"x\": 0,          \"y\": 1        }      }    ],    \"cities\": [],    \"settlements\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SW\",          \"x\": 0,          \"y\": 0        }      }    ],    \"radius\": 3,    \"ports\": [      {        \"ratio\": 3,        \"direction\": \"SE\",        \"location\": {          \"x\": -3,          \"y\": 0        }      },      {        \"ratio\": 2,        \"resource\": \"wood\",        \"direction\": \"S\",        \"location\": {          \"x\": 1,          \"y\": -3        }      },      {        \"ratio\": 2,        \"resource\": \"ore\",        \"direction\": \"SW\",        \"location\": {          \"x\": 3,          \"y\": -3        }      },      {        \"ratio\": 3,        \"direction\": \"S\",        \"location\": {          \"x\": -1,          \"y\": -2        }      },      {        \"ratio\": 2,        \"resource\": \"wheat\",        \"direction\": \"NE\",        \"location\": {          \"x\": -2,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NW\",        \"location\": {          \"x\": 3,          \"y\": -1        }      },      {        \"ratio\": 2,        \"resource\": \"sheep\",        \"direction\": \"NW\",        \"location\": {          \"x\": 2,          \"y\": 1        }      },      {        \"ratio\": 2,        \"resource\": \"brick\",        \"direction\": \"N\",        \"location\": {          \"x\": 0,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NE\",        \"location\": {          \"x\": -3,          \"y\": 2        }      }    ],    \"robber\": {      \"x\": -2,      \"y\": 2    }  },  \"players\": [    {      \"resources\": {        \"brick\": 0,        \"wood\": 0,        \"sheep\": 0,        \"wheat\": 0,        \"ore\": 0      },      \"oldDevCards\": {        \"yearOfPlenty\": 0,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"newDevCards\": {        \"yearOfPlenty\": 0,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"roads\": 14,      \"cities\": 4,      \"settlements\": 4,      \"soldiers\": 0,      \"victoryPoints\": 1,      \"monuments\": 0,      \"playedDevCard\": false,      \"discarded\": false,      \"playerID\": 12,      \"playerIndex\": 0,      \"name\": \"rfl\",      \"color\": \"red\"    },    null,    null,    null  ],  \"log\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"rfl built a settlement\"      },      {        \"source\": \"rfl\",        \"message\": \"rfl built a road\"      }    ]  },  \"chat\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"hello out there\"      }    ]  },  \"bank\": {    \"brick\": 24,    \"wood\": 24,    \"sheep\": 24,    \"wheat\": 24,    \"ore\": 24  },  \"turnTracker\": {    \"status\": \"FirstRound\",    \"currentTurn\": 0,    \"longestRoad\": -1,    \"largestArmy\": -1  },  \"winner\": -1,  \"version\": 3}";
		
		int version = dp.buildModelFromJson(json, true);
		
		assertTrue(version == 3);
		
		Model dpModel = dp.getModel();
		Board dpBoard = dpModel.getBoard();
		List<Player> dpPlayers = dpModel.getPlayers();
		List<ChatEntry> dpComments = dpModel.getComments();
		List<HistoryEntry> dpHistory = dpModel.getHistory();
		GameManager dpGM = dpModel.gameManager;
		
		Model testModel = buildTestModel();
		Board testBoard = testModel.getBoard();
		List<Player> testPlayers = testModel.getPlayers();
		List<ChatEntry> testComments = testModel.getComments();
		List<HistoryEntry> testHistory = testModel.getHistory();
		GameManager testGM = testModel.gameManager;
			
		assertEquals(dpBoard.getRobberHex(), testBoard.getRobberHex());
		
		assertEquals(dpBoard, testBoard);
		assertEquals(dpPlayers, testPlayers);
		
		assertEquals(dpComments.size(), testComments.size());
		for (int c = 0; c < dpComments.size(); c++) {
			if (!dpComments.get(c).equals(testComments.get(c))) {
				fail("Expected: " + dpComments.get(c).toString() + "but was: " + testComments.get(c).toString());
			}
		}
		
		assertEquals(dpHistory, testHistory);
		assertEquals(dpGM, testGM);
		
		assertEquals(dpModel, testModel);
	}
	
	@Test
	public void testBuildModelFromJsonNotEqual() {
		DataParser dp = new DataParser();
		Facade.init(dp);
		
		String json = "{  \"deck\": {    \"yearOfPlenty\": 1,    \"monopoly\": 2,    \"soldier\": 14,    \"roadBuilding\": 2,    \"monument\": 5  },  \"map\": {    \"hexes\": [      {        \"resource\": \"wood\",        \"location\": {          \"x\": 0,          \"y\": -2        },        \"number\": 9      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 1,          \"y\": -2        },        \"number\": 11      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": -2        },        \"number\": 8      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": -1,          \"y\": -1        },        \"number\": 9      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": -1        },        \"number\": 3      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": -1        },        \"number\": 5      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 2,          \"y\": -1        },        \"number\": 11      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": -1,          \"y\": 0        },        \"number\": 2      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": 0        },        \"number\": 5      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 1,          \"y\": 0        },        \"number\": 12      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -2,          \"y\": 1        },        \"number\": 10      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -1,          \"y\": 1        },        \"number\": 3      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": 0,          \"y\": 1        },        \"number\": 4      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": 1        },        \"number\": 4      },      {        \"location\": {          \"x\": -2,          \"y\": 2        }      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -1,          \"y\": 2        },        \"number\": 10      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 0,          \"y\": 2        },        \"number\": 8      }    ],    \"roads\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SE\",          \"x\": 0,          \"y\": 0        }      }    ],    \"cities\": [],    \"settlements\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SW\",          \"x\": 0,          \"y\": -1        }      }    ],    \"radius\": 3,    \"ports\": [      {        \"ratio\": 3,        \"direction\": \"SE\",        \"location\": {          \"x\": -3,          \"y\": 0        }      },      {        \"ratio\": 2,        \"resource\": \"wood\",        \"direction\": \"S\",        \"location\": {          \"x\": 1,          \"y\": -3        }      },      {        \"ratio\": 2,        \"resource\": \"ore\",        \"direction\": \"SW\",        \"location\": {          \"x\": 3,          \"y\": -3        }      },      {        \"ratio\": 3,        \"direction\": \"S\",        \"location\": {          \"x\": -1,          \"y\": -2        }      },      {        \"ratio\": 2,        \"resource\": \"wheat\",        \"direction\": \"NE\",        \"location\": {          \"x\": -2,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NW\",        \"location\": {          \"x\": 3,          \"y\": -1        }      },      {        \"ratio\": 2,        \"resource\": \"sheep\",        \"direction\": \"NW\",        \"location\": {          \"x\": 2,          \"y\": 1        }      },      {        \"ratio\": 2,        \"resource\": \"brick\",        \"direction\": \"N\",        \"location\": {          \"x\": 0,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NE\",        \"location\": {          \"x\": -3,          \"y\": 2        }      }    ],    \"robber\": {      \"x\": 0,      \"y\": 0    }  },  \"players\": [    {      \"resources\": {        \"brick\": 0,        \"wood\": 0,        \"sheep\": 0,        \"wheat\": 0,        \"ore\": 0      },      \"oldDevCards\": {        \"yearOfPlenty\": 1,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"newDevCards\": {        \"yearOfPlenty\": 0,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"roads\": 14,      \"cities\": 4,      \"settlements\": 4,      \"soldiers\": 0,      \"victoryPoints\": 1,      \"monuments\": 0,      \"playedDevCard\": false,      \"discarded\": false,      \"playerID\": 12,      \"playerIndex\": 0,      \"name\": \"rfl\",      \"color\": \"red\"    },    null,    null,    null  ],  \"log\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"rfl built a city\"      },      {        \"source\": \"rfl\",        \"message\": \"rfl built a road\"      }    ]  },  \"chat\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"hello in there\"      }    ]  },  \"bank\": {    \"brick\": 24,    \"wood\": 24,    \"sheep\": 24,    \"wheat\": 24,    \"ore\": 24  },  \"turnTracker\": {    \"status\": \"FirstRound\",    \"currentTurn\": 1,    \"longestRoad\": -1,    \"largestArmy\": -2  },  \"winner\": -1,  \"version\": 2}";
		
		int version = dp.buildModelFromJson(json, false);
		
		assertFalse(version == 3);
		
		Model dpModel = dp.getModel();
		Board dpBoard = dpModel.getBoard();
		List<Player> dpPlayers = dpModel.getPlayers();
		List<ChatEntry> dpComments = dpModel.getComments();
		List<HistoryEntry> dpHistory = dpModel.getHistory();
		GameManager dpGM = dpModel.gameManager;
		
		Model testModel = buildTestModel();
		Board testBoard = testModel.getBoard();
		List<Player> testPlayers = testModel.getPlayers();
		List<ChatEntry> testComments = testModel.getComments();
		List<HistoryEntry> testHistory = testModel.getHistory();
		GameManager testGM = testModel.gameManager;
			
		assertNotEquals(dpBoard.getRobberHex(), testBoard.getRobberHex());
		
		assertNotEquals(dpBoard, testBoard);
		assertNotEquals(dpPlayers, testPlayers);
		assertNotEquals(dpComments, testComments);
		assertNotEquals(dpHistory, testHistory);
		assertNotEquals(dpGM, testGM);
		
		assertNotEquals(dpModel, testModel); 
	}
	
//	@Test
//	public void testFacadeIntegration() {
//		
//		DataParser dp = new DataParser();
//		Facade.init(dp);
//		
//		String json = "{  \"deck\": {    \"yearOfPlenty\": 2,    \"monopoly\": 2,    \"soldier\": 14,    \"roadBuilding\": 2,    \"monument\": 5  },  \"map\": {    \"hexes\": [      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 0,          \"y\": -2        },        \"number\": 9      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 1,          \"y\": -2        },        \"number\": 11      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": -2        },        \"number\": 8      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": -1,          \"y\": -1        },        \"number\": 9      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": -1        },        \"number\": 3      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": -1        },        \"number\": 5      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": 2,          \"y\": -1        },        \"number\": 11      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": -1,          \"y\": 0        },        \"number\": 2      },      {        \"resource\": \"ore\",        \"location\": {          \"x\": 0,          \"y\": 0        },        \"number\": 5      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": 1,          \"y\": 0        },        \"number\": 12      },      {        \"resource\": \"wheat\",        \"location\": {          \"x\": 2,          \"y\": 0        },        \"number\": 6      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -2,          \"y\": 1        },        \"number\": 10      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": -1,          \"y\": 1        },        \"number\": 3      },      {        \"resource\": \"wood\",        \"location\": {          \"x\": 0,          \"y\": 1        },        \"number\": 4      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 1,          \"y\": 1        },        \"number\": 4      },      {        \"location\": {          \"x\": -2,          \"y\": 2        }      },      {        \"resource\": \"sheep\",        \"location\": {          \"x\": -1,          \"y\": 2        },        \"number\": 10      },      {        \"resource\": \"brick\",        \"location\": {          \"x\": 0,          \"y\": 2        },        \"number\": 8      }    ],    \"roads\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SE\",          \"x\": 0,          \"y\": 1        }      }    ],    \"cities\": [],    \"settlements\": [      {        \"owner\": 0,        \"location\": {          \"direction\": \"SW\",          \"x\": 0,          \"y\": 0        }      }    ],    \"radius\": 3,    \"ports\": [      {        \"ratio\": 3,        \"direction\": \"SE\",        \"location\": {          \"x\": -3,          \"y\": 0        }      },      {        \"ratio\": 2,        \"resource\": \"wood\",        \"direction\": \"S\",        \"location\": {          \"x\": 1,          \"y\": -3        }      },      {        \"ratio\": 2,        \"resource\": \"ore\",        \"direction\": \"SW\",        \"location\": {          \"x\": 3,          \"y\": -3        }      },      {        \"ratio\": 3,        \"direction\": \"S\",        \"location\": {          \"x\": -1,          \"y\": -2        }      },      {        \"ratio\": 2,        \"resource\": \"wheat\",        \"direction\": \"NE\",        \"location\": {          \"x\": -2,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NW\",        \"location\": {          \"x\": 3,          \"y\": -1        }      },      {        \"ratio\": 2,        \"resource\": \"sheep\",        \"direction\": \"NW\",        \"location\": {          \"x\": 2,          \"y\": 1        }      },      {        \"ratio\": 2,        \"resource\": \"brick\",        \"direction\": \"N\",        \"location\": {          \"x\": 0,          \"y\": 3        }      },      {        \"ratio\": 3,        \"direction\": \"NE\",        \"location\": {          \"x\": -3,          \"y\": 2        }      }    ],    \"robber\": {      \"x\": -2,      \"y\": 2    }  },  \"players\": [    {      \"resources\": {        \"brick\": 0,        \"wood\": 0,        \"sheep\": 0,        \"wheat\": 0,        \"ore\": 0      },      \"oldDevCards\": {        \"yearOfPlenty\": 0,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"newDevCards\": {        \"yearOfPlenty\": 0,        \"monopoly\": 0,        \"soldier\": 0,        \"roadBuilding\": 0,        \"monument\": 0      },      \"roads\": 14,      \"cities\": 4,      \"settlements\": 4,      \"soldiers\": 0,      \"victoryPoints\": 1,      \"monuments\": 0,      \"playedDevCard\": false,      \"discarded\": false,      \"playerID\": 12,      \"playerIndex\": 0,      \"name\": \"rfl\",      \"color\": \"red\"    },    null,    null,    null  ],  \"log\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"rfl built a settlement\"      },      {        \"source\": \"rfl\",        \"message\": \"rfl built a road\"      }    ]  },  \"chat\": {    \"lines\": [      {        \"source\": \"rfl\",        \"message\": \"hello out there\"      }    ]  },  \"bank\": {    \"brick\": 24,    \"wood\": 24,    \"sheep\": 24,    \"wheat\": 24,    \"ore\": 24  },  \"turnTracker\": {    \"status\": \"FirstRound\",    \"currentTurn\": 0,    \"longestRoad\": -1,    \"largestArmy\": -1  },  \"winner\": -1,  \"version\": 3}";
//		
//		dp.buildModelFromJson(json, true);
//		
//		Model dpModel = dp.getModel();
//		Model fModel = Facade.getModel();
//		
//		assertEquals(dpModel, fModel);
//	}
	
	private Model buildTestModel() {
		// Hexes
		Hex hex0 = new Hex(HexType.SHEEP, new HexLocation(0, -2), 9, false);
		Hex hex1 = new Hex(HexType.WHEAT, new HexLocation(1, -2), 11, false);
		Hex hex2 = new Hex(HexType.WHEAT, new HexLocation(2, -2), 8, false);
		Hex hex3 = new Hex(HexType.ORE, new HexLocation(-1, -1), 9, false);
		Hex hex4 = new Hex(HexType.ORE, new HexLocation(0, -1), 3, false);
		Hex hex5 = new Hex(HexType.BRICK, new HexLocation(1, -1), 5, false);
		Hex hex6 = new Hex(HexType.WOOD, new HexLocation(2, -1), 11, false);
		Hex hex7 = new Hex(HexType.WOOD, new HexLocation(-2, 0), 6, false);
		Hex hex8 = new Hex(HexType.WHEAT, new HexLocation(-1, 0), 2, false);
		Hex hex9 = new Hex(HexType.ORE, new HexLocation(0, 0), 5, false);
		Hex hex10 = new Hex(HexType.SHEEP, new HexLocation(1, 0), 12, false);
		Hex hex11 = new Hex(HexType.WHEAT, new HexLocation(2, 0), 6, false);
		Hex hex12 = new Hex(HexType.SHEEP, new HexLocation(-2, 1), 10, false);
		Hex hex13 = new Hex(HexType.WOOD, new HexLocation(-1, 1), 3, false);
		Hex hex14 = new Hex(HexType.WOOD, new HexLocation(0, 1), 4, false);
		Hex hex15 = new Hex(HexType.BRICK, new HexLocation(1, 1), 4, false);
		Hex hex16 = new Hex(HexType.DESERT, new HexLocation(-2, 2), 0, true);
		Hex hex17 = new Hex(HexType.SHEEP, new HexLocation(-1, 2), 10, false);
		Hex hex18 = new Hex(HexType.BRICK, new HexLocation(0, 2), 8, false);
		
		// Pieces
		Road r0 = new Road(new EdgeLocation(new HexLocation(0, 1), EdgeDirection.SouthEast).getNormalizedLocation(), 0);
		Settlement s0 = new Settlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.SouthWest).getNormalizedLocation(), 0);
		try {
			hex15.addRoad(r0.getLocation(), r0);
			hex14.addBuilding(s0.getLocation(), s0);
		}
		catch (InvalidLocationException ile) {
			fail("Pieces have invalid locations");
		}
		
		// add all hexes to list
		List<Hex> hexes = new ArrayList<Hex>();
		hexes.add(hex0);
		hexes.add(hex1);
		hexes.add(hex2);
		hexes.add(hex3);
		hexes.add(hex4);
		hexes.add(hex5);
		hexes.add(hex6);
		hexes.add(hex7);
		hexes.add(hex8);
		hexes.add(hex9);
		hexes.add(hex10);
		hexes.add(hex11);
		hexes.add(hex12);
		hexes.add(hex13);
		hexes.add(hex14);
		hexes.add(hex15);
		hexes.add(hex16);
		hexes.add(hex17);
		hexes.add(hex18);
		
		// add ocean hexes too
		hexes.add(new Hex(HexType.WATER, new HexLocation(0, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(0, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(1, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(1, 2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(2, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(2, 1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-1, -2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-1, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-2, -1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-2, 3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, 0), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(3, -3), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 0), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 1), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 2), 0, false));
		hexes.add(new Hex(HexType.WATER, new HexLocation(-3, 3), 0, false));
		
		// build Board
		Board b = new Board(hexes);
		
		// Ports
		Map<EdgeLocation, PortType> portMap = new HashMap<EdgeLocation, PortType>();
		portMap.put(new EdgeLocation(new HexLocation(-3, 0), EdgeDirection.SouthEast), PortType.THREE);
		portMap.put(new EdgeLocation(new HexLocation(1, -3), EdgeDirection.South), PortType.WOOD);
		portMap.put(new EdgeLocation(new HexLocation(3, -3), EdgeDirection.SouthWest), PortType.ORE);
		portMap.put(new EdgeLocation(new HexLocation(-1, -2), EdgeDirection.South), PortType.THREE);
		portMap.put(new EdgeLocation(new HexLocation(-2, 3), EdgeDirection.NorthEast), PortType.WHEAT);
		portMap.put(new EdgeLocation(new HexLocation(3, -1), EdgeDirection.NorthWest), PortType.THREE);
		portMap.put(new EdgeLocation(new HexLocation(2, 1), EdgeDirection.NorthWest), PortType.SHEEP);
		portMap.put(new EdgeLocation(new HexLocation(0, 3), EdgeDirection.North), PortType.BRICK);
		portMap.put(new EdgeLocation(new HexLocation(-3, 2), EdgeDirection.NorthEast), PortType.THREE);
		b.setPorts(portMap);
		
		// Players
		List<Player> players = new ArrayList<Player>();
		Player p0 = new Player(12, "rfl", CatanColor.RED, 1, 14, 4, 4, new ArrayList<ResourceCard>(), new ArrayList<DevCard>(), false, false, 0, 0);
		players.add(p0);
		players.add(null);
		players.add(null);
		players.add(null);
		
		// Logs
		List<HistoryEntry> history = new ArrayList<HistoryEntry>();
		HistoryEntry he0 = new HistoryEntry(p0, "rfl built a settlement");
		HistoryEntry he1 = new HistoryEntry(p0, "rfl built a road");
		history.add(he0);
		history.add(he1);
		
		List<ChatEntry> comments = new ArrayList<ChatEntry>();
		ChatEntry ce0 = new ChatEntry(p0, "hello out there");
		comments.add(ce0);
		
		// GameManager
		
		// DevCards
		DevCard yop0 = new DevCard(0, DevCardType.YEAR_OF_PLENTY);
		DevCard yop1 = new DevCard(0, DevCardType.YEAR_OF_PLENTY);
		DevCard mono0 = new DevCard(1, DevCardType.MONOPOLY);
		DevCard mono1 = new DevCard(1, DevCardType.MONOPOLY);
		DevCard sol0 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol1 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol2 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol3 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol4 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol5 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol6 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol7 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol8 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol9 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol10 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol11 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol12 = new DevCard(2, DevCardType.SOLDIER);
		DevCard sol13 = new DevCard(2, DevCardType.SOLDIER);
		DevCard road0 = new DevCard(3, DevCardType.ROAD_BUILD);
		DevCard road1 = new DevCard(3, DevCardType.ROAD_BUILD);
		DevCard monu0 = new DevCard(4, DevCardType.MONUMENT);
		DevCard monu1 = new DevCard(4, DevCardType.MONUMENT);
		DevCard monu2 = new DevCard(4, DevCardType.MONUMENT);
		DevCard monu3 = new DevCard(4, DevCardType.MONUMENT);
		DevCard monu4 = new DevCard(4, DevCardType.MONUMENT);
		
		// add all dev cards to list
		List<DevCard> deck = new ArrayList<DevCard>();
		deck.add(yop0);
		deck.add(yop1);
		deck.add(mono0);
		deck.add(mono1);
		deck.add(sol0);
		deck.add(sol1);
		deck.add(sol2);
		deck.add(sol3);
		deck.add(sol4);
		deck.add(sol5);
		deck.add(sol6);
		deck.add(sol7);
		deck.add(sol8);
		deck.add(sol9);
		deck.add(sol10);
		deck.add(sol11);
		deck.add(sol12);
		deck.add(sol13);
		deck.add(road0);
		deck.add(road1);
		deck.add(monu0);
		deck.add(monu1);
		deck.add(monu2);
		deck.add(monu3);
		deck.add(monu4);
		
		List<ResourceCard> brickList = new ArrayList<ResourceCard>();
		List<ResourceCard> woodList = new ArrayList<ResourceCard>();
		List<ResourceCard> sheepList = new ArrayList<ResourceCard>();
		List<ResourceCard> wheatList = new ArrayList<ResourceCard>();
		List<ResourceCard> oreList = new ArrayList<ResourceCard>();
		
		for (int i = 0; i < 24; i++) {
			brickList.add(new ResourceCard(ResourceType.BRICK));
			woodList.add(new ResourceCard(ResourceType.WOOD));
			sheepList.add(new ResourceCard(ResourceType.SHEEP));
			wheatList.add(new ResourceCard(ResourceType.WHEAT));
			oreList.add(new ResourceCard(ResourceType.ORE));
		}
		
		LargestArmy la = new LargestArmy();
		LongestRoad lr = new LongestRoad();
		
		// add everything to GM
		GameManager gm = new GameManager();
		gm.setBoard(b);
		
		gm.setBrick((ArrayList<ResourceCard>)brickList);
		gm.setWood((ArrayList<ResourceCard>)woodList);
		gm.setSheep((ArrayList<ResourceCard>)sheepList);
		gm.setWheat((ArrayList<ResourceCard>)wheatList);
		gm.setOre((ArrayList<ResourceCard>)oreList);
		
		gm.setDevelopmentCards((ArrayList<DevCard>)deck);
		gm.setLargestArmy(-1);
		gm.setLongestRoad(-1);
		
		gm.setPlayers((ArrayList<Player>)players);
		gm.setCurrentPlayer(0);
		
		gm.setStatus("FirstRound");
		
		return new Model(gm, b, players, comments, history);
	}
}
