package shared.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import shared.definitions.HexType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.model.board.Board;
import shared.model.board.Hex;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.Road;
import shared.model.pieces.Settlement;

public class BoardTest {

	Board testBoard;
	List<Hex> hexes;
	
	@Before
	public void setUp() throws Exception {
		// Hexes
		Hex hex0 = new Hex(HexType.SHEEP, new HexLocation(0, -2), 9, false);
		Hex hex1 = new Hex(HexType.WHEAT, new HexLocation(1, -2), 11, false);
		Hex hex2 = new Hex(HexType.WHEAT, new HexLocation(2, -2), 8, false);
		Hex hex3 = new Hex(HexType.ORE, new HexLocation(-1, -1), 9, false);
		Hex hex4 = new Hex(HexType.ORE, new HexLocation(0, -1), 3, false);
		Hex hex5 = new Hex(HexType.BRICK, new HexLocation(1, -1), 5, false);
		Hex hex6 = new Hex(HexType.WOOD, new HexLocation(2, -1), 11, false);
		Hex hex7 = new Hex(HexType.WOOD, new HexLocation(-2, 0), 6, false);
		Hex hex8 = new Hex(HexType.WHEAT, new HexLocation(-1, 0), 2, false);
		Hex hex9 = new Hex(HexType.ORE, new HexLocation(0, 0), 5, false);
		Hex hex10 = new Hex(HexType.SHEEP, new HexLocation(1, 0), 12, false);
		Hex hex11 = new Hex(HexType.WHEAT, new HexLocation(2, 0), 6, false);
		Hex hex12 = new Hex(HexType.SHEEP, new HexLocation(-2, 1), 10, false);
		Hex hex13 = new Hex(HexType.WOOD, new HexLocation(-1, 1), 3, false);
		Hex hex14 = new Hex(HexType.WOOD, new HexLocation(0, 1), 4, false);
		Hex hex15 = new Hex(HexType.BRICK, new HexLocation(1, 1), 4, false);
		Hex hex16 = new Hex(HexType.DESERT, new HexLocation(-2, 2), 0, true);
		Hex hex17 = new Hex(HexType.SHEEP, new HexLocation(-1, 2), 10, false);
		Hex hex18 = new Hex(HexType.BRICK, new HexLocation(0, 2), 8, false);
		
		// Pieces
		Road r0 = new Road(new EdgeLocation(new HexLocation(0, 1), EdgeDirection.SouthEast).getNormalizedLocation(), 0);
		Settlement s0 = new Settlement(new VertexLocation(new HexLocation(0, 0), VertexDirection.SouthWest).getNormalizedLocation(), 0);
		try {
			EdgeLocation edge1 = new EdgeLocation(hex14.getHexLocation(), EdgeDirection.North).getNormalizedLocation();
			EdgeLocation edge2 = new EdgeLocation(hex14.getHexLocation(), EdgeDirection.NorthEast).getNormalizedLocation();
			
			hex15.addRoad(r0.getLocation(), r0);
			hex14.addBuilding(s0.getLocation(), s0);
			hex14.addRoad(edge1, new Road(edge1, 0));
			hex14.addRoad(edge2,  new Road(edge2, 0));
		}
		catch (InvalidLocationException ile) {
			fail("Pieces have invalid locations");
		}
		
		// add all hexes to list
		hexes = new ArrayList<Hex>();
		hexes.add(hex0);
		hexes.add(hex1);
		hexes.add(hex2);
		hexes.add(hex3);
		hexes.add(hex4);
		hexes.add(hex5);
		hexes.add(hex6);
		hexes.add(hex7);
		hexes.add(hex8);
		hexes.add(hex9);
		hexes.add(hex10);
		hexes.add(hex11);
		hexes.add(hex12);
		hexes.add(hex13);
		hexes.add(hex14);
		hexes.add(hex15);
		hexes.add(hex16);
		hexes.add(hex17);
		hexes.add(hex18);
		
		// build Board
		testBoard = new Board(hexes);
	}
	
	@Test
	public void testBoardListOfHex() {
		
		List<Hex> otherHexes = new ArrayList<Hex>(hexes);
		otherHexes.remove(18);
		otherHexes.add(new Hex(HexType.DESERT, new HexLocation(0, 2), 0, false));
		
		assertEquals(testBoard, new Board(hexes));
		assertNotEquals(testBoard, new Board(otherHexes));
	}

	@Test
	public void testCanBuildSettlement() {
		// valid - no other Buildings nearby and connected to a road
		VertexLocation sLoc = new VertexLocation(new HexLocation(0, 1), VertexDirection.East).getNormalizedLocation();
		assertTrue(testBoard.canBuildSettlement(sLoc, 0,false));
		
		// invalid = no roads nearby
		VertexLocation badLoc = new VertexLocation(new HexLocation (-2, 0), VertexDirection.SouthWest).getNormalizedLocation();
		assertFalse(testBoard.canBuildSettlement(badLoc, 0,false));
		
		// invalid - too close to another Building
		VertexLocation badLoc2 = new VertexLocation(new HexLocation(0, 1), VertexDirection.NorthEast).getNormalizedLocation();
		assertFalse(testBoard.canBuildSettlement(badLoc2, 0,false));
	}

	@Test
	public void testGetAllHexes() {
		assertEquals(testBoard.getAllHexes(), hexes);
	}

	@Test
	public void testGetHexByLocation() {
		// try getting a Hex by location
		Hex hexByLoc = testBoard.getHexByLocation(new HexLocation(0,-2));
		assertEquals(hexByLoc, new Hex(HexType.SHEEP, new HexLocation(0,-2), 9, false));
		
		// invalid location
		assertNull(testBoard.getHexByLocation(new HexLocation(28, -52)));
	}

	@Test
	public void testGetHexesByNumber() {
		// try getting Hexes by number
		List<Hex> hexesByNumber = testBoard.getHexesByNumber(8);
		List<Hex> contrived = new ArrayList<Hex>();
		contrived.add(new Hex(HexType.WHEAT, new HexLocation(2, -2), 8, false));
		contrived.add(new Hex(HexType.BRICK, new HexLocation(0, 2), 8, false));
		assertEquals(hexesByNumber, contrived);
				
		// getting '0' should return only the desert Hex
		List<Hex> desert = testBoard.getHexesByNumber(0);
		assertEquals(desert.size(), 1);
		assertEquals(desert.get(0), new Hex(HexType.DESERT, new HexLocation(-2, 2), 0, true));
		
		// invalid number
		assertNull(testBoard.getHexesByNumber(42));
	}

	@Test
	public void testGetTokenFromHex() {
		// easy peasy
		Hex h = testBoard.getHexByLocation(new HexLocation(0, -2));
		assertEquals(testBoard.getTokenFromHex(h), 9);
	}

	@Test
	public void testGetRobberHex() {
		// also easy
		assertTrue(testBoard.getRobberHex().getHasRobber());
		assertEquals(testBoard.getRobberHex(), new Hex(HexType.DESERT, new HexLocation(-2, 2), 0, true));
	}

}
