package shared.model;

import static org.junit.Assert.*;
import shared.definitions.*;
import java.util.*;
import org.junit.*;

import shared.locations.*;
import shared.model.GameManager;
import shared.model.board.*;
import shared.model.developmentcard.DevCard;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.*;
import shared.model.player.Player;
import shared.model.resourcecard.ResourceCard;

public class GameManagerTest {

	@Test
	public void testCanSendChat() {
		GameManager gm = new GameManager();
		gm.setStatus("Playing");
		gm.setCurrentPlayer(0);
		assertTrue(gm.canSendChat(0));

		// Valid - Not player's turn
		gm.setStatus("Rolling");
		assertTrue(gm.canSendChat(1));

		// Invalid - Player's turn and not in playing
		assertFalse(gm.canSendChat(0));
	}

	@Test
	public void testCanAcceptTrade() {
		//Valid
		GameManager gm = new GameManager();
		ArrayList<ResourceCard> hand = createHand(new int[]{1,1,1,1,1});
		Player p = new Player(0,"Kevin",CatanColor.BLUE, 9,1,1,1,hand,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setStatus("playing");
		gm.setPlayers(players);
		assertTrue(gm.canAcceptTrade(0, new int[]{1,-1,1,-1,0}));

		// Invalid - wrong turn phase
		gm.setStatus("Rolling");
		assertFalse(gm.canAcceptTrade(0, new int[]{1,-1,1,-1,0}));
	}

	@Test
	public void testCanDiscardCards() {
		// Valid - player must discard half
		GameManager gm = new GameManager();
		ArrayList<ResourceCard> hand = new ArrayList<ResourceCard>();
		hand.add(new ResourceCard(ResourceType.WOOD));
		hand.add(new ResourceCard(ResourceType.BRICK));
		hand.add(new ResourceCard(ResourceType.WOOD));
		hand.add(new ResourceCard(ResourceType.BRICK));
		hand.add(new ResourceCard(ResourceType.WOOD));
		hand.add(new ResourceCard(ResourceType.BRICK));
		hand.add(new ResourceCard(ResourceType.WOOD));
		hand.add(new ResourceCard(ResourceType.BRICK));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,hand,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setStatus("Discarding");
		gm.setPlayers(players);
		assertTrue(gm.canDiscardCards(0));

		// Invalid - not in the right phase [not in "DISCARDING"]
		gm.setStatus("Rolling");
		assertFalse(gm.canDiscardCards(0));

		// Invalid - player has less than or equal to 7 cards in hand
		ArrayList<ResourceCard> hand2 = new ArrayList<ResourceCard>();
		hand2.add(new ResourceCard(ResourceType.WOOD));
		hand2.add(new ResourceCard(ResourceType.BRICK));
		hand2.add(new ResourceCard(ResourceType.BRICK));
		hand2.add(new ResourceCard(ResourceType.WOOD));
		hand2.add(new ResourceCard(ResourceType.BRICK));
		hand2.add(new ResourceCard(ResourceType.WOOD));
		hand2.add(new ResourceCard(ResourceType.BRICK));
		Player p2 = new Player(1,"John",CatanColor.BLUE,4,4,4,2,hand2,null,false,false,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canDiscardCards(1));
	}

	@Test
	public void testCanRollNumber() {
		// Valid - player can roll dice
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Rolling");
		assertTrue(gm.canRollNumber(0));

		// Invalid - Not in the rolling phase of turn
		gm.setStatus("Playing");
		assertFalse(gm.canRollNumber(0));

		// Invalid - Not player's turn
		gm.setCurrentPlayer(1);
		assertFalse(gm.canRollNumber(0));
	}

	@Test
	public void testCanBuildRoad() {
		// Valid
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<ResourceCard> hand = createHand(new int[]{1,1,0,0,0});
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,hand,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canBuildRoad(0,false));

		// Invalid - Not right game phase, not "playing"
		gm.setStatus("Rolling");
		assertFalse(gm.canBuildRoad(0,false));

		// Invalid - Not your turn
		gm.setStatus("Playing");
		ArrayList<ResourceCard> hand2 = createHand(new int[]{3,0,0,0,0});
		Player p2 = new Player(1,"John",CatanColor.RED,4,4,4,2,hand2,null,false,false,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canBuildRoad(1,false));

		// Invalid - Incorrect resource cards
		ArrayList<ResourceCard> hand3 = createHand(new int[]{3,0,0,0,0});
		Player p3 = new Player(2,"Mike",CatanColor.ORANGE,4,4,4,2,hand3,null,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		gm.setCurrentPlayer(2);
		assertFalse(gm.canBuildRoad(2,false));

		// Invalid - No more roads left to play
		ArrayList<ResourceCard> hand4 = createHand(new int[]{3,1,0,0,0});
		Player p4 = new Player(3,"Mike",CatanColor.ORANGE,4,0,4,2,hand4,null,false,false,0,0);
		players.add(p4);
		gm.setPlayers(players);
		gm.setCurrentPlayer(3);
		assertFalse(gm.canBuildRoad(3,false));
	}

	@Test
	public void testCanBuildSettlement() {
		// Valid
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<ResourceCard> hand = createHand(new int[]{1,1,1,1,0});
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,hand,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canBuildSettlement(0));

		// Invalid - Not right game phase, not "playing"
		gm.setStatus("Rolling");
		assertFalse(gm.canBuildSettlement(0));

		// Invalid - Not your turn
		gm.setStatus("Playing");
		ArrayList<ResourceCard> hand2 = createHand(new int[]{1,1,1,1,0});
		Player p2 = new Player(1,"John",CatanColor.RED,4,4,4,2,hand2,null,false,false,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canBuildSettlement(1));

		// Invalid - Incorrect resource cards
		ArrayList<ResourceCard> hand3 = createHand(new int[]{3,0,0,0,0});
		Player p3 = new Player(2,"Mike",CatanColor.ORANGE,4,4,4,2,hand3,null,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		gm.setCurrentPlayer(2);
		assertFalse(gm.canBuildSettlement(2));

		// Invalid - No more settlements left to play
		ArrayList<ResourceCard> hand4 = createHand(new int[]{1,1,1,1,0});
		Player p4 = new Player(3,"Mike",CatanColor.ORANGE,4,4,0,0,hand4,null,false,false,0,0);
		players.add(p4);
		gm.setPlayers(players);
		gm.setCurrentPlayer(3);
		assertFalse(gm.canBuildSettlement(3));
	}

	@Test
	public void testCanBuildCity() {
		// Valid
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<ResourceCard> hand = createHand(new int[]{0,0,0,2,3});
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,hand,null,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canBuildCity(0));

		// Invalid - Not right game phase, not "playing"
		gm.setStatus("Rolling");
		assertFalse(gm.canBuildCity(0));

		// Invalid - Not your turn
		gm.setStatus("Playing");
		ArrayList<ResourceCard> hand2 = createHand(new int[]{0,0,0,2,3});
		Player p2 = new Player(1,"John",CatanColor.RED,4,4,4,2,hand2,null,false,false,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canBuildCity(1));

		// Invalid - Incorrect resource cards
		ArrayList<ResourceCard> hand3 = createHand(new int[]{3,0,0,0,0});
		Player p3 = new Player(2,"Mike",CatanColor.ORANGE,4,4,4,2,hand3,null,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		gm.setCurrentPlayer(2);
		assertFalse(gm.canBuildCity(2));

		// Invalid - No more cities left to play
		ArrayList<ResourceCard> hand4 = createHand(new int[]{0,0,0,2,3});
		Player p4 = new Player(3,"Mike",CatanColor.ORANGE,10,4,0,1,hand4,null,false,false,0,0);
		players.add(p4);
		gm.setPlayers(players);
		gm.setCurrentPlayer(3);
		assertFalse(gm.canBuildCity(3));

		// Invalid - No settlements have been place on board
		ArrayList<ResourceCard> hand5 = createHand(new int[]{0,0,0,2,3});
		Player p5 = new Player(3,"Mike",CatanColor.ORANGE,10,4,0,5,hand5,null,false,false,0,0);
		players.remove(0);
		players.add(p5);
		gm.setPlayers(players);
		gm.setCurrentPlayer(4);
		assertFalse(gm.canBuildCity(3));
	}

	@Test
	public void testCanOfferTrade() {
		GameManager gm = new GameManager();
		Player p = new Player(0, null, null, 0, 0, 0, 0, createHand(new int[]{1,1,1,1,1}), null,false,false, 0, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		gm.setCurrentPlayer(0);
		assertTrue(gm.canOfferTrade(0));

		// Invalid - Not in playing phase
		gm.setStatus("rolling");
		assertFalse(gm.canOfferTrade(0));

		// Invalid - not players turn
		gm.setStatus("playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canOfferTrade(3));

		// Invalid - Player has no cards
		Player p2 = new Player(1, null, null, 0, 0, 0, 0, createHand(new int[]{0,0,0,0,0}), null,false,false, 0, 0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canOfferTrade(1));
	}

	@Test
	public void testCanFinishTurn() {
		GameManager g = new GameManager();
		g.setStatus("playing");
		g.setCurrentPlayer(0);
		assertTrue(g.canFinishTurn(0));

		// Invalid - Wrong turn phase
		g.setStatus("Rolling");
		assertFalse(g.canFinishTurn(0));

		// Invalid - not current player
		g.setStatus("playing");
		g.setCurrentPlayer(1);
		assertFalse(g.canFinishTurn(0));
	}

	@Test
	public void testCanBuyDevCard(){
		GameManager g = new GameManager();
		g.setStatus("playing");
		g.setCurrentPlayer(0);
		Player p = new Player(0, null, null, 0, 0, 0, 0, createHand(new int[]{0,0,1,1,1}), null,false,false, 0, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		g.setPlayers(players);
		ArrayList<DevCard> dc = new ArrayList<DevCard>();
		dc.add(new DevCard(0, null));
		g.setDevelopmentCards(dc);
		assertTrue(g.canBuyDevCard(0));

		// Invalid - Not player's turn
		Player p1 = new Player(1, null, null, 0, 0, 0, 0, createHand(new int[]{0,0,1,1,1}), null,false,false, 0, 0);
		players.add(p1);
		g.setPlayers(players);
		assertFalse(g.canBuyDevCard(1));

		// Invalid - Wrong turn phase
		g.setStatus("Discarding");
		assertFalse(g.canBuyDevCard(0));

		// Invalid - Wrong cards
		Player p2 = new Player(2, null, null, 0, 0, 0, 0, createHand(new int[]{0,0,0,1,1}), null,false,false, 0, 0);
		players.add(p2);
		g.setCurrentPlayer(2);
		g.setPlayers(players);
		assertFalse(g.canBuyDevCard(2));

		// Invalid - No more Dev cards in the game
		ArrayList<DevCard> dc2 = new ArrayList<DevCard>();
		g.setDevelopmentCards(dc2);
		g.setCurrentPlayer(0);
		assertFalse(g.canBuyDevCard(0));
	}

	@Test
	public void testCanUseYearOfPlenty() {
		// Valid - player can play dev card
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<DevCard> devHand = new ArrayList<DevCard>();
		devHand.add(new DevCard(0,DevCardType.YEAR_OF_PLENTY));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canUseYearOfPlenty(0));

		// Invalid - Not correct turn phase ["Playing"]
		gm.setStatus("Discarding");
		assertFalse(gm.canUseYearOfPlenty(0));

		// Invalid - Not the current player's turn
		gm.setStatus("Playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canUseYearOfPlenty(0));

		// Invalid - Player has already played a dev card this turn
		Player p2 = new Player(1,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,true,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canUseYearOfPlenty(1));

		// Invalid - Player does not have correct dev card
		ArrayList<DevCard> dhand = new ArrayList<DevCard>();
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		dhand.add(new DevCard(0,DevCardType.ROAD_BUILD));
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		Player p3 = new Player(2,"Kevin",CatanColor.BLUE,4,4,4,2,null,dhand,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		assertFalse(gm.canUseYearOfPlenty(2));
	}

	@Test
	public void testCanUseRoadBuilder() {
		// Valid - player can play dev card
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<DevCard> devHand = new ArrayList<DevCard>();
		devHand.add(new DevCard(0,DevCardType.ROAD_BUILD));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canUseRoadBuilder(0));

		// Invalid - Not correct turn phase ["Playing"]
		gm.setStatus("Discarding");
		assertFalse(gm.canUseRoadBuilder(0));

		// Invalid - Not the current player's turn
		gm.setStatus("Playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canUseRoadBuilder(0));

		// Invalid - Player has already played a dev card this turn
		Player p2 = new Player(1,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,true,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canUseRoadBuilder(1));

		// Invalid - Player does not have correct dev card
		ArrayList<DevCard> dhand = new ArrayList<DevCard>();
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		dhand.add(new DevCard(0,DevCardType.YEAR_OF_PLENTY));
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		Player p3 = new Player(2,"Kevin",CatanColor.BLUE,4,4,4,2,null,dhand,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		assertFalse(gm.canUseRoadBuilder(2));
	}

	@Test
	public void testCanUseSoldier() {
		// Valid - player can play dev card
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<DevCard> devHand = new ArrayList<DevCard>();
		devHand.add(new DevCard(0,DevCardType.SOLDIER));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canUseSoldier(0));

		// Invalid - Not correct turn phase ["Playing"]
		gm.setStatus("Discarding");
		assertFalse(gm.canUseSoldier(0));

		// Invalid - Not the current player's turn
		gm.setStatus("Playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canUseSoldier(0));

		// Invalid - Player has already played a dev card this turn
		Player p2 = new Player(1,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,true,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canUseSoldier(1));

		// Invalid - Player does not have correct dev card
		ArrayList<DevCard> dhand = new ArrayList<DevCard>();
		dhand.add(new DevCard(0,DevCardType.MONOPOLY));
		dhand.add(new DevCard(0,DevCardType.YEAR_OF_PLENTY));
		dhand.add(new DevCard(0,DevCardType.ROAD_BUILD));
		Player p3 = new Player(2,"Kevin",CatanColor.BLUE,4,4,4,2,null,dhand,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		assertFalse(gm.canUseSoldier(2));	
	}

	@Test
	public void testCanUseMonopoly() {
		// Valid - player can play dev card
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<DevCard> devHand = new ArrayList<DevCard>();
		devHand.add(new DevCard(0,DevCardType.MONOPOLY));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canUseMonopoly(0));

		// Invalid - Not correct turn phase ["Playing"]
		gm.setStatus("Discarding");
		assertFalse(gm.canUseMonopoly(0));

		// Invalid - Not the current player's turn
		gm.setStatus("Playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canUseMonopoly(0));

		// Invalid - Player has already played a dev card this turn
		Player p2 = new Player(1,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,true,0,0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canUseMonopoly(1));

		// Invalid - Player does not have correct dev card
		ArrayList<DevCard> dhand = new ArrayList<DevCard>();
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		dhand.add(new DevCard(0,DevCardType.YEAR_OF_PLENTY));
		dhand.add(new DevCard(0,DevCardType.ROAD_BUILD));
		Player p3 = new Player(2,"Kevin",CatanColor.BLUE,4,4,4,2,null,dhand,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		assertFalse(gm.canUseMonopoly(2));
	}

	@Test
	public void testCanUseMonument() {
		// Valid - player can play dev card
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<DevCard> devHand = new ArrayList<DevCard>();
		devHand.add(new DevCard(0,DevCardType.MONUMENT));
		Player p = new Player(0,"Kevin",CatanColor.BLUE,4,4,4,2,null,devHand,false,false,0,0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p);
		gm.setPlayers(players);
		gm.setStatus("Playing");
		assertTrue(gm.canUseMonument(0));

		// Invalid - Not correct turn phase ["Playing"]
		gm.setStatus("Discarding");
		assertFalse(gm.canUseMonument(0));

		// Invalid - Not the current player's turn
		gm.setStatus("Playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canUseMonument(0));

		// Invalid - Player does not have correct dev card
		ArrayList<DevCard> dhand = new ArrayList<DevCard>();
		dhand.add(new DevCard(0,DevCardType.SOLDIER));
		dhand.add(new DevCard(0,DevCardType.YEAR_OF_PLENTY));
		dhand.add(new DevCard(0,DevCardType.ROAD_BUILD));
		Player p3 = new Player(2,"Kevin",CatanColor.BLUE,4,4,4,2,null,dhand,false,false,0,0);
		players.add(p3);
		gm.setPlayers(players);
		assertFalse(gm.canUseMonument(2));
	}

	@Test
	public void testCanPlaceRobber() {
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		gm.setStatus("Robbing");
		assertTrue(gm.canPlaceRobber(0));

		// Invalid - Not player's turn
		gm.setCurrentPlayer(1);
		assertFalse(gm.canPlaceRobber(0));

		// Invalid - Wrong phase of turn
		gm.setCurrentPlayer(0);
		gm.setStatus("Discarding");
		assertFalse(gm.canPlaceRobber(0));
	}

	@Test
	public void testCanMaritimeTrade() {
		GameManager gm = new GameManager();
		gm.setCurrentPlayer(0);
		ArrayList<Player> players = new ArrayList<Player>();
		Player p = new Player(0, null, null, 0, 0, 0, 0, createHand(new int[]{4,3,2,1,0}), null, false, false, 0, 0);
		gm.setStatus("playing");
		players.add(p);
		gm.setPlayers(players);
		assertTrue(gm.canMaritimeTrade(0));

		// Invalid - Wrong turn status
		gm.setStatus("rolling");
		assertFalse(gm.canMaritimeTrade(0));

		// Invalid - Not player's turn
		gm.setStatus("playing");
		gm.setCurrentPlayer(1);
		assertFalse(gm.canMaritimeTrade(0));

		// Invalid - only has 3 of resource
		Player p2 = new Player(1, null, null, 0, 0, 0, 0, createHand(new int[]{3,3,2,1,0}), null, false, false, 0, 0);
		players.add(p2);
		gm.setPlayers(players);
		assertFalse(gm.canMaritimeTrade(1));

		// Valid - player now has a 3 -1 trading
		p2.setTradeRate(1, 3);
		assertTrue(gm.canMaritimeTrade(1));

		// Invalid - only has 2 of resources
		Player p3 = new Player(2, null, null, 0, 0, 0, 0, createHand(new int[]{2,2,2,1,0}), null, false, false, 0, 0);
		players.add(p3);
		gm.setPlayers(players);
		gm.setCurrentPlayer(2);
		assertFalse(gm.canMaritimeTrade(2));

		// Valid - player has 2-1 trading
		p3.setTradeRate(2, 2);
		assertTrue(gm.canMaritimeTrade(2));
	}

	@Test
	public void testCanBuildRoadLocation() throws InvalidLocationException{
		// Valid - Player has a building on the vertex of where the road is being built
		GameManager gm = new GameManager();
		Hex h1 = new Hex(HexType.DESERT,new HexLocation(0,0),1,false);
		h1.addBuilding(new VertexLocation(h1.getHexLocation(),1), new Settlement(0));
		Hex h2 = new Hex(HexType.WOOD ,new HexLocation(0,-1),2,false);
		Hex h3 = new Hex(HexType.BRICK,new HexLocation(-1,0),3,false);
		Hex h4 = new Hex(HexType.SHEEP,new HexLocation(-1,-1),4,false);
		Player p1 = new Player(0, null, null, 0, 4, 0, 0, createHand(new int[]{2,2,2,1,0}), null, false, false, 0, 0);
		Player p2 = new Player(1, null, null, 0, 4, 0, 0, createHand(new int[]{2,2,2,1,0}), null, false, false, 0, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);players.add(p2);
		gm.setPlayers(players);
		List<Hex> h = new ArrayList<Hex>();
		h.add(h1);h.add(h2);h.add(h3);h.add(h4);
		Board b = new Board(h);
		gm.setBoard(b);
		gm.setStatus("Playing");
		gm.setCurrentPlayer(0);
		assertTrue(gm.canBuildRoad(0, new EdgeLocation(new HexLocation(0,0), EdgeDirection.North),false,false));

		// Invalid - Can't place a road next to nothing
		assertFalse(gm.canBuildRoad(0, new EdgeLocation(new HexLocation(-1,0),EdgeDirection.North),false,false));

		// Invalid - Settlement blocking road
		h1.addRoad(new EdgeLocation(new HexLocation(-1,0),EdgeDirection.SouthEast), new Road(null,1));
		assertFalse(gm.canBuildRoad(1,new EdgeLocation(new HexLocation(-1,0),EdgeDirection.North),false,false));

		// Invalid - Can't build road off of another player's road
		gm.setCurrentPlayer(0);
		assertFalse(gm.canBuildRoad(0,new EdgeLocation(new HexLocation(-1,0),EdgeDirection.North),false,false));

		// Invalid - Road already built at location
		h1.addRoad(new EdgeLocation(new HexLocation(0,0),EdgeDirection.North), new Road(null,1));
		assertFalse(gm.canBuildRoad(0,new EdgeLocation(new HexLocation(0,0),EdgeDirection.North),false,false));
	}

	@Test
	public void testCanBuildSettlementLocation() throws InvalidLocationException {
		// Valid - 
		GameManager gm = new GameManager();
		Hex h1 = new Hex(HexType.DESERT,new HexLocation(0,0),1,false);
		h1.addRoad(new EdgeLocation(h1.getHexLocation(),0), new Road(null,0));
		h1.addRoad(new EdgeLocation(h1.getHexLocation(),1), new Road(null,0));
		Hex h2 = new Hex(HexType.WOOD ,new HexLocation(0,-1),2,false);
		h2.addRoad(new EdgeLocation(h2.getHexLocation(),3), new Road(null,0));
		h2.addRoad(new EdgeLocation(h2.getHexLocation(),5), new Road(null,0));
		Hex h3 = new Hex(HexType.BRICK,new HexLocation(-1,0),3,false);
		Hex h4 = new Hex(HexType.SHEEP,new HexLocation(-1,-1),4,false);
		Player p1 = new Player(0, null, null, 0, 4, 4, 4, createHand(new int[]{2,2,2,1,0}), null, false, false, 0, 0);
		Player p2 = new Player(1, null, null, 0, 4, 4, 4, createHand(new int[]{2,2,2,1,0}), null, false, false, 0, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);players.add(p2);
		gm.setPlayers(players);
		List<Hex> h = new ArrayList<Hex>();
		h.add(h1);h.add(h2);h.add(h3);h.add(h4);
		Board b = new Board(h);
		gm.setBoard(b);
		gm.setStatus("Playing");
		gm.setCurrentPlayer(0);
		assertTrue(gm.canBuildSettlement(0, new VertexLocation(h1.getHexLocation(),1), false));
		
		// Invalid - Settlement too close an other hex
		h2.addBuilding(new VertexLocation(h2.getHexLocation(),3), new Settlement(0));
		assertFalse(gm.canBuildSettlement(0, new VertexLocation(h1.getHexLocation(),2), false));

		// Invalid - None of the roads at vertex are player's
		gm.setCurrentPlayer(1);
		assertFalse(gm.canBuildSettlement(1, new VertexLocation(h1.getHexLocation(),1), false));
	}
	
	@Test
	public void testCanBuildCityLocation() throws InvalidLocationException {
		GameManager gm = new GameManager();
		Hex h1 = new Hex(HexType.DESERT,new HexLocation(0,0),1,false);
		h1.addBuilding(new VertexLocation(h1.getHexLocation(),1), new Settlement(0));
		Hex h2 = new Hex(HexType.WOOD ,new HexLocation(0,-1),2,false);
		Hex h3 = new Hex(HexType.BRICK,new HexLocation(-1,0),3,false);
		Hex h4 = new Hex(HexType.SHEEP,new HexLocation(-1,-1),4,false);
		Player p1 = new Player(0, null, null, 0, 4, 4, 4, createHand(new int[]{2,2,2,5,5}), null, false, false, 0, 0);
		Player p2 = new Player(1, null, null, 0, 4, 4, 4, createHand(new int[]{2,2,2,5,5}), null, false, false, 0, 0);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(p1);players.add(p2);
		gm.setPlayers(players);
		List<Hex> h = new ArrayList<Hex>();
		h.add(h1);h.add(h2);h.add(h3);h.add(h4);
		Board b = new Board(h);
		gm.setBoard(b);
		gm.setStatus("Playing");
		gm.setCurrentPlayer(0);
		assertTrue(gm.canBuildCity(0, new VertexLocation(h1.getHexLocation(),1)));
		
		// Invalid not player's building
		gm.setCurrentPlayer(1);
		assertFalse(gm.canBuildCity(1, new VertexLocation(h1.getHexLocation(),0)));
		
		// Invalid - city already built
		h3.addBuilding(new VertexLocation(h3.getHexLocation(),2), new City(null,1));
		assertFalse(gm.canBuildCity(1, new VertexLocation(h3.getHexLocation(),2)));
		
		// Invalid - No building on vertex
		assertFalse(gm.canBuildCity(1, new VertexLocation(h3.getHexLocation(),4)));
	}

	/**
	 * [WOOD,BRICK,SHEEP,WHEAT,ORE]
	 */
	private ArrayList<ResourceCard> createHand(int[] list) {
		ArrayList<ResourceCard> hand = new ArrayList<ResourceCard>();
		for(int i=0;i<list[0];i++)
			hand.add(new ResourceCard(ResourceType.WOOD));
		for(int i=0;i<list[1];i++)
			hand.add(new ResourceCard(ResourceType.BRICK));
		for(int i=0;i<list[2];i++)
			hand.add(new ResourceCard(ResourceType.SHEEP));
		for(int i=0;i<list[3];i++)
			hand.add(new ResourceCard(ResourceType.WHEAT));
		for(int i=0;i<list[4];i++)
			hand.add(new ResourceCard(ResourceType.ORE));
		return hand;
	}
}
