package shared.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import shared.definitions.*;
import shared.locations.*;
import shared.model.board.Hex;
import shared.model.exceptions.InvalidLocationException;
import shared.model.pieces.*;

public class HexTest {

	private Hex testHex1;
	private Hex testHex2;
	private Hex testHex3;
	
	private HexLocation h1Loc;
	private HexLocation h2Loc;
	private HexLocation h3Loc;
	
	@Before
	public void setUp() throws Exception {
		h1Loc = new HexLocation(1, 1);
		h2Loc = new HexLocation(0, 2);
		h3Loc = new HexLocation(5, 6);
		
		testHex1 = new Hex(HexType.BRICK, h1Loc, 5, false);
		testHex2 = new Hex(HexType.WHEAT, h2Loc, 6, true);
		testHex3 = new Hex(HexType.WATER, h3Loc, 28, false);
		
		VertexLocation buildingLoc = new VertexLocation(h2Loc, VertexDirection.NorthEast);
		Settlement s = new Settlement(buildingLoc, 0);
		EdgeLocation roadLoc = new EdgeLocation(h2Loc, EdgeDirection.North);
		Road r = new Road(roadLoc, 0);
		
		testHex2.addBuilding(buildingLoc, s);
		testHex2.addRoad(roadLoc, r);
	}

	@Test
	public void testHexHexTypeHexLocationIntBoolean() {
		Hex h = new Hex(HexType.BRICK, new HexLocation(1, 1), 5, false);
		assertEquals(h, testHex1);
		assertNotEquals(h, testHex2);
		assertNotEquals(h, testHex3);
	}

	@Test
	public void testHexHexTypeHexLocationIntBooleanListOfBuildingListOfRoad() {
		List<Building> bldgs = new ArrayList<Building>();
		bldgs.add(new Settlement(new VertexLocation(h2Loc, VertexDirection.NorthEast), 0));
		List<Road> roads = new ArrayList<Road>();
		roads.add(new Road(new EdgeLocation(h2Loc, EdgeDirection.North), 0));
		
		Hex h = new Hex(HexType.WHEAT, new HexLocation(0,2), 6, true, bldgs, roads);
		assertNotEquals(h, testHex1);
		assertEquals(h, testHex2);
		assertNotEquals(h, testHex3);
	}

	@Test
	public void testGetBuilding() {
		assertEquals(testHex2.getBuilding(VertexDirection.NorthEast), new Settlement(new VertexLocation(h2Loc, VertexDirection.NorthEast), 0));
	}

	@Test
	public void testCanBuildCity() {
		assertFalse(testHex1.canBuildCity(new VertexLocation(h1Loc, VertexDirection.SouthEast),0));
		assertTrue(testHex2.canBuildCity(new VertexLocation(h2Loc, VertexDirection.NorthEast),0));
	}

/*	@Test
	public void testCanBuildRoad() {
		assertTrue(testHex1.canBuildRoad(new EdgeLocation(h1Loc, EdgeDirection.South)));
		assertFalse(testHex2.canBuildRoad(new EdgeLocation(h2Loc, EdgeDirection.North)));
	}*/

	@Test
	public void testAddBuilding() {
		VertexLocation bLoc = new VertexLocation(h1Loc, VertexDirection.NorthEast);
		
		// build new settlement
		try { testHex1.addBuilding(bLoc, new Settlement(bLoc, 0)); }
		catch (InvalidLocationException ile) { fail("InvalidLocationException thrown"); }
		
		// test that the building was added
		List<Building> bldgs = testHex1.getAllBuildings();
		assertEquals(bldgs.size(), 1);
		assertEquals(bldgs.get(0), new Settlement(bLoc, 0));
		
		// test that you can now build a city on top of the new settlement
		assertTrue(testHex1.canBuildCity(new VertexLocation(h1Loc, VertexDirection.NorthEast),0));
		
		// test that exception is thrown if you try to build twice in one spot
		boolean thrown = false;
		try { testHex1.addBuilding(bLoc, new Settlement(bLoc, 0)); }
		catch (InvalidLocationException ile) { thrown = true; }
		assertTrue(thrown);
	}

	@Test
	public void testAddRoad() {
		EdgeLocation rLoc = new EdgeLocation(h1Loc, EdgeDirection.South);
		
		// build new road
		try { testHex1.addRoad(rLoc, new Road(rLoc, 0)); }
		catch (InvalidLocationException ile) { fail("InvalidLocationException thrown"); }
		
		// test that the road was added
		List<Road> roads = testHex1.getAllRoads();
		assertEquals(roads.size(), 1);
		assertEquals(roads.get(0), new Road(rLoc, 0));
		
		// test that exception is thrown if you try to build twice in one spot
		boolean thrown = false;
		try { testHex1.addRoad(rLoc, new Road(rLoc, 0)); }
		catch (InvalidLocationException ile) { thrown = true; }
		assertTrue(thrown);
	}

}
